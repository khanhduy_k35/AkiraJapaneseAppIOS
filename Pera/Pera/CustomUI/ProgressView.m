//
//  ProgressView.m
//  testAudioRecode
//
//  Created by LeDuy on 12/3/15.
//  Copyright © 2015 LeDuy. All rights reserved.
//

#import "ProgressView.h"

@implementation ProgressView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:212.0f/255 green:212.0f/255 blue:212.0f/255 alpha:212.0f/255];
        self.layer.cornerRadius = self.frame.size.height/2;
        _viewComplete = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width,  self.frame.size.height)];
        if (_currently == 0) {
            _viewComplete.backgroundColor = [UIColor colorWithRed:2.0f/255 green:212.0f/255 blue:2.0f/255 alpha:212.0f/255];
            [self addSubview:_viewComplete];
            [self setPercentComplete:0];
        }
    }
    return self;
}

-(id) initWithFrame:(CGRect)frame withMax:(float)maxValue{
    _maxValue = maxValue;
    self =   [self initWithFrame:frame];
    return self;
}

- (void) setPercentComplete : (float) currentValue{
    _viewComplete.frame = CGRectMake(0, 0, currentValue / _maxValue * self.frame.size.width, self.frame.size.height);
    if (currentValue == 0) {
        
    }else if(currentValue < _maxValue){
        [self roundCornersOnView:_viewComplete onTopLeft:YES topRight:NO bottomLeft:YES bottomRight:NO radius:self.frame.size.width/2];
    }else if(currentValue == _maxValue){
         [self roundCornersOnView:_viewComplete onTopLeft:YES topRight:YES bottomLeft:YES bottomRight:YES radius:self.frame.size.width/2];
    }
}

-(void) roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius {
    
    if (tl || tr || bl || br) {
        UIRectCorner corner = 0; //holds the corner
        //Determine which corner(s) should be changed
        if (tl) {
            corner = corner | UIRectCornerTopLeft;
        }
        if (tr) {
            corner = corner | UIRectCornerTopRight;
        }
        if (bl) {
            corner = corner | UIRectCornerBottomLeft;
        }
        if (br) {
            corner = corner | UIRectCornerBottomRight;
        }
        
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = roundedView.bounds;
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        [self setNeedsDisplay];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
