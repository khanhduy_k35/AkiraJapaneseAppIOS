#import "Tools.h"
#import "QuartzCore/CALayer.h"
#import "QuartzCore/CATransaction.h"
#import "QuartzCore/CAAnimation.h"
#import "QuartzCore/CAMediaTimingFunction.h"


#define SOUND_KEY   @"sound"
#define APPRUN_KEY  @"apprun"

@implementation Tools



+ (NSString *)applicationDocumentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}


+(void) setDisAd:(BOOL)on{
    [[NSUserDefaults standardUserDefaults] setBool:on forKey:@"DisAd"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(BOOL)isDisad{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"DisAd"] == nil)
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"DisAd"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"DisAd"];
}

+ (void)setAppRun:(int)num
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	[prefs setInteger:num forKey:APPRUN_KEY];
    [prefs synchronize];
}

@end