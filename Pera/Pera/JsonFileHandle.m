//
//  JsonFileHandle.m
//  Pera
//
//  Created by Le Trong Thao on 12/1/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import "JsonFileHandle.h"
#import "GlobalVariable.h"

@implementation JsonFileHandle

+(NSString*)getLocalTitleWithType:(NSString *)type andLanguageKey:(NSString *)languageKey
{
    NSString *title;
    
    NSArray *paths  = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    
    
    NSString *filePath = [documentPath stringByAppendingPathComponent:@"locales.json"];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        NSString *bundleFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"locales.json"];
        //[[NSFileManager defaultManager] copyItemAtPath:bundleFilePath toPath:filePath error:nil];
        BOOL success =  [[NSFileManager defaultManager] copyItemAtPath:bundleFilePath toPath:filePath error:nil];
        if (success) {
            NSURL *url = [NSURL fileURLWithPath:filePath];
            [self addSkipBackupAttributeToItemAtURL:url];
        }
    }
    
    
    NSString *contentString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSData *contentData = [contentString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *contentDic = [NSJSONSerialization JSONObjectWithData:contentData options:0 error:nil];
    
    NSDictionary *titleDic = [contentDic objectForKey:type];
    title  = [titleDic objectForKey:languageKey];
    
    return title;
}


+(NSMutableArray*)getTopicTitlesWithLanguageKey:(NSString *)languageKey
{
    NSMutableArray *titles = [NSMutableArray new];
    
    NSArray *paths  = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *filePath = [documentPath stringByAppendingPathComponent:@"common.json"];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        NSString *bundleFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"common.json"];
     //   [[NSFileManager defaultManager] copyItemAtPath:bundleFilePath toPath:filePath error:nil];
        
        BOOL success =  [[NSFileManager defaultManager] copyItemAtPath:bundleFilePath toPath:filePath error:nil];
        if (success) {
            NSURL *url = [NSURL fileURLWithPath:filePath];
            [self addSkipBackupAttributeToItemAtURL:url];
        }
    }
    
    NSString *contentString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSData *contentData = [contentString dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *contentArray = [NSJSONSerialization JSONObjectWithData:contentData options:1 error:nil];
    
    if([languageKey isEqualToString:@"en"])
        languageKey = @"eng";
    
    for(NSMutableDictionary *dic in contentArray)
    {

        [titles addObject:[dic valueForKey:languageKey]];
    }
    
    return titles;
}

+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

+(NSMutableArray*)getAllLessonOfTopic:(NSString*)topicID andCourse:(NSString*)cource
{
    NSMutableArray *lessonsAr = [NSMutableArray new];
    
    NSArray *paths  = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *filePath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"data/%@/%@/data/phrase%@.json",topicID,cource,topicID]];
    NSString *contentString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSData *contentData;
   
    contentData  = [contentString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSArray *contentArray = [NSJSONSerialization JSONObjectWithData:contentData options:1 error:nil];
    
    for(NSMutableDictionary *dic in contentArray)
    {
        
        Lesson *ls = [Lesson new];
        ls.lessonID = [NSString stringWithFormat:@"%@",[dic objectForKey:@"id"]];
//        ls.data = [dic objectForKey:cource];
//        ls.dataPronounce = [dic objectForKey:[NSString stringWithFormat:@"pronounce_%@",cource]];
//        ls.dataDisplay = [dic objectForKey:g_language];
        
        if([cource isEqualToString:@"jp"])
        {
            ls.hiragana = [dic objectForKey:@"hiragana"];
        }
        
//        ls.english = [dic objectForKey:@"english"];
//        ls.pronoun_en = [dic objectForKey:@"pronoun_en"];
//        ls.vietnamese = [dic objectForKey:@"vietnamese"];
//        ls.japanese = [dic objectForKey:@"japanese"];
//        ls.hiragana = [dic objectForKey:@"hiragana"];
//        ls.pronoun_jp = [dic objectForKey:@"pronoun_jp"];
        
    }

    
    return lessonsAr;
}
@end
