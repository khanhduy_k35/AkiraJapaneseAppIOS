//
//  Email.m
//  Pera
//
//  Created by Le Trong Thao on 12/6/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import "Email.h"

@implementation Email


+(void)mailTo:(NSString *)destinationAccount Title:(NSString *)title andContent:(NSString *)content
{
    
    NSString *email = [NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@",destinationAccount,title,content];
    NSString *url = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
    [[UIApplication sharedApplication]  openURL: [NSURL URLWithString: url]];
}


+(void)shareFacebookWithContent:(NSString *)content andViewController:(UIViewController *)vc
{
    SLComposeViewController *fbController=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result){
            
            [fbController dismissViewControllerAnimated:YES completion:nil];
            
            switch(result){
                case SLComposeViewControllerResultCancelled:
                default:
                {
                    NSLog(@"Cancelled.....");
                    // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs://"]];
                    
                }
                    break;
                case SLComposeViewControllerResultDone:
                {
                    NSLog(@"Posted....");
                }
                    break;
            }};
        
        
        [fbController setInitialText:content];
        
        
        [fbController setCompletionHandler:completionHandler];
        [vc presentViewController:fbController animated:YES completion:nil];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sign in!"
                                                       message:@"Please first Sign In!"
                                                      delegate:nil
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles:nil];
        [alert show];
    }
}
@end
