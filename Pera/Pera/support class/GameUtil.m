//
//  GameUtil.m
//  TestGame
//
//  Created by DuyLe on 7/16/15.
//  Copyright (c) 2015 DuyLe. All rights reserved.
//

#import "GameUtil.h"

@implementation GameUtil
+(CGRect)transformFitToScree:(CGRect)original withScreenWidth:(int)screenWidth withScreenHeight:(int)screenHeight{
    float scaleX = 320.0f/ screenWidth;
    float scaleY = 568.0f /screenHeight;
    CGRect newRect= CGRectMake(original.origin.x / scaleX, original.origin.y / scaleY, original.size.width/scaleX, original.size.height/scaleY);
    return newRect;
}

+(CGRect)transformfitToHintView:(CGRect)original withHintViewWidth:(float)viewWidth andViewHeight:(float)viewHeight
{
    float scaleX = 300.0f/ viewWidth;
    float scaleY = 370.0f /viewHeight;
    CGRect newRect= CGRectMake(original.origin.x / scaleX, original.origin.y / scaleY, original.size.width/scaleX, original.size.height/scaleY);
    return newRect;
}

+(CGRect)transformfitToGetHintBt:(CGRect)original withHintViewWidth:(float)viewWidth andViewHeight:(float)viewHeight
{
    float scaleX = 170.0f/ viewWidth;
    float scaleY = 60.0f /viewHeight;
    CGRect newRect= CGRectMake(original.origin.x / scaleX, original.origin.y / scaleY, original.size.width/scaleX, original.size.height/scaleY);
    return newRect;
}

+(CGRect)transformFitToScreen:(float)screenWidth height:(float)screenHeight WithOriginFrame:(CGRect)frame originScreenWidth:(float)width andOriginScreenHeight:(float)height
{
    float scaleX = width/ screenWidth;
    float scaleY = height /screenHeight;
    CGRect newRect= CGRectMake(frame.origin.x / scaleX, frame.origin.y / scaleY, frame.size.width/scaleX, frame.size.height/scaleY);
    return newRect;
}



@end
