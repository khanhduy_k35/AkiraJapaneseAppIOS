//
//  StarBar.h
//  Pera
//
//  Created by Le Trong Thao on 12/4/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StarBar : UIView

-(id)initWithFrame:(CGRect)frame andNumberOfStars:(int)starNumber;

@end
