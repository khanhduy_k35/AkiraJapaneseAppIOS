//
//  CustomAnimation.m
//  Pera
//
//  Created by Le Trong Thao on 12/5/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import "CustomAnimation.h"

@implementation CustomAnimation

+(void)upperAnimationDuration:(NSTimeInterval)time WithView:(UIView*)view andCompletion:(void (^)(BOOL finished))completion
{
    CGRect frame  = view.frame;
    
    view.frame = CGRectMake(frame.origin.x+frame.size.width/2, frame.origin.y+frame.size.height/2, 0, 0);
    
    [UIView animateWithDuration:time
                     animations:^
     {
         view.frame = frame;
     }
                     completion:completion];
}


+(void)upperAnimationDuration1:(NSTimeInterval)time WithViewsArray:(NSArray*)array andCompletion:(void (^)(BOOL finished))completion
{
    
    for(UIView *view1 in array)
    {
        
        view1.subviews[1].hidden = true;
        for(UIView *view in view1.subviews)
        {
                        
            CGRect frame  = view.frame;
            
            view.frame = CGRectMake(frame.origin.x+frame.size.width/2, frame.origin.y+frame.size.height/2, 0, 0);
            
            [UIView animateWithDuration:time
                             animations:^
             {
                 view.frame = frame;
             }
                             completion:^(BOOL finish){
                                 
                                 view1.subviews[1].hidden = false;
                                 
                                 completion(finish);
                                 
                             }];
            break;

        }
        
    }
}

+(void)upperAnimationDuration:(NSTimeInterval)time WithViewsArray:(NSArray*)array andCompletion:(void (^)(BOOL finished))completion
{
    for(UIView *view in array)
    {
        CGRect frame  = view.frame;
        
        for(UIView *view1 in view.subviews)
        {
            view1.hidden = true;
        }
        
        view.frame = CGRectMake(frame.origin.x+frame.size.width/2, frame.origin.y+frame.size.height/2, 0, 0);
        
        [UIView animateWithDuration:time
                         animations:^
         {
             view.frame = frame;
         }
                         completion:^(BOOL finish){
                             
                             for(UIView *view1 in view.subviews)
                             {
                                 view1.hidden = false;
                             }
                         }];
        
    }

}
@end
