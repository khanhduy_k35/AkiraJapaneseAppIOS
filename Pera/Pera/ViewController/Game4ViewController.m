//
//  Game4ViewController.m
//  Pera
//
//  Created by Le Trong Thao on 12/4/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import "Game4ViewController.h"
#import "ProgressView.h"
#import "GameUtil.h"
#import "GlobalVariable.h"
#import "LessonHandle.h"
#import "DataHandle.h"
#import "CustomAnimation.h"
#import "CustomButton.h"
#import "Toast.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

#import "UIViewController+AMSlideMenu.h"


#import "CustomImageView.h"
#import "HttpThread.h"

@interface Game4ViewController ()<UIAlertViewDelegate,AVAudioPlayerDelegate>

@end

@implementation Game4ViewController
{
    NSMutableArray *questionArray;
    
    ProgressView *progressView;
    UIView *gameView;
    
    
    CustomButton *choosedRightBt;
    CustomButton *choosedLeftBt;
    
    AVAudioPlayer *resultPlayer;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self disableSlidePanGestureForLeftMenu];
    
    UIView *downloadView = [self.navigationController.view viewWithTag:10];
    if(downloadView)
    {
        [downloadView setAlpha:0];
    }
    
     ((AppDelegate *)[[UIApplication sharedApplication] delegate]).isGame4Success = NO;
    
    [self initView];

}


-(void)viewDidAppear:(BOOL)animated
{
    currentViewController = self;
}

-(void)viewWillDisappear:(BOOL)animated
{
    
}

#pragma mark initView

-(void)initView
{
    questionArray = [LessonHandle getRandomlyLessonSets:5 fromArray:[DataHandle getAllLessonOfTopic:[self.topicID intValue] Level:g_course Sub:self.sub]];
    
    UIButton *backButton = [UIButton new];
    float heightOfBt = 36;
    float widthOfBt = 36;
    backButton.frame = [GameUtil transformFitToScree:CGRectMake(8, 30 , widthOfBt, heightOfBt) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    [backButton setBackgroundImage:[UIImage imageNamed:@"abc_ic_ab_back_mtrl_am_alpha_blue.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backViewController) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:backButton];
    
    
    float xProgressView = backButton.frame.origin.x + backButton.frame.size.width + 10;
    float widthProgressView = [UIScreen mainScreen].bounds.size.width - xProgressView*2;
    float heightProgressView = 6;
    
    progressView = [[ProgressView alloc] initWithFrame:CGRectMake(xProgressView, backButton.frame.origin.y+backButton.frame.size.height/2-heightProgressView/2, widthProgressView, heightProgressView) withMax:questionArray.count];
    [self.view addSubview:progressView];
    
    
    UILabel *topLabel = [UILabel new];
    topLabel.frame = CGRectMake(10, progressView.frame.origin.y+progressView.frame.size.height+15, [UIScreen mainScreen].bounds.size.width-20,40);
    topLabel.font = [UIFont systemFontOfSize:15];
    topLabel.numberOfLines = 0;
    topLabel.textAlignment = NSTextAlignmentCenter;
    topLabel.textColor = [UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1];
    topLabel.backgroundColor = [UIColor clearColor];
    topLabel.text = @"Nối từ với nghĩa từ tiếng Việt\nVui lòng chọn một từ ở cột bên trái trước";
    [self.view addSubview:topLabel];
    
    
    [self initGameView];
    
}


-(void)initGameView
{
    
    gameView = [UIView new];
    float yGameView = 140;
    gameView.frame = [GameUtil transformFitToScree:CGRectMake(0, yGameView, 320, 568 - yGameView) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    gameView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:gameView];
    
    
    NSMutableArray *rightArray = [LessonHandle rearrangeElementRandomly:[NSMutableArray arrayWithArray:questionArray]];
    
    
    float edgePadding = 15;
    float xdistance = 15;
    float ydistance = 10;
    float width = ([UIScreen mainScreen].bounds.size.width - (edgePadding*2) - xdistance)/2;
    float height = 45;
    float ybegin = (gameView.frame.size.height - ydistance*4 - height*5)/2 - 20;
    
    for(int i = 1; i <= 5; i++)
    {
        CustomButton *leftButton = [CustomButton new];
        leftButton.lesson = questionArray[i-1];
        leftButton.frame = CGRectMake(edgePadding, ybegin, width, height);
        [leftButton setTitle:leftButton.lesson.hiragana forState:UIControlStateNormal];
        [leftButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        leftButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [leftButton setBackgroundColor:[UIColor colorWithRed:82.0/255.0 green:128.0/255.0 blue:195.0/255.0 alpha:1]];
        [leftButton addTarget:self action:@selector(leftButton:) forControlEvents:UIControlEventTouchUpInside];
        [gameView addSubview:leftButton];
        
        
        CustomButton *rightButton = [CustomButton new];
        rightButton.lesson = rightArray[i-1];
        rightButton.frame = CGRectMake(leftButton.frame.origin.x + width + xdistance, leftButton.frame.origin.y, width, height);
        [rightButton setTitle:rightButton.lesson.vietnamese forState:UIControlStateNormal];
        [rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        rightButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [rightButton setBackgroundColor:[UIColor colorWithRed:214.0/255.0 green:13.0/255.0 blue:0.0/255.0 alpha:1]];
        [rightButton addTarget:self action:@selector(rightButton:) forControlEvents:UIControlEventTouchUpInside];
        [gameView addSubview:rightButton];
        
        ybegin = ybegin + height + ydistance;
        
    }
    
}


-(void)initEndingViewWithStars:(int)starNumber
{
    
    UIView *endingView = [UIView new];
    endingView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    endingView.userInteractionEnabled = true;
    endingView.backgroundColor = [UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1];
    [endingView setTag:1];
    [self.view addSubview:endingView];
    
    
    UILabel *infoLabel = [UILabel new];
    infoLabel.frame = CGRectMake(0, 100*[UIScreen mainScreen].bounds.size.height/568.0, [UIScreen mainScreen].bounds.size.width, 40);
    infoLabel.font = [UIFont boldSystemFontOfSize:25];
    infoLabel.textAlignment = NSTextAlignmentCenter;
    infoLabel.textColor = [UIColor whiteColor];
    infoLabel.backgroundColor = [UIColor clearColor];
    if(starNumber == 3)
    {
        infoLabel.text = @"Tuyệt vời";
    }
    else if (starNumber == 2)
    {
        infoLabel.text = @"Tốt";
    }
    else if (starNumber == 1)
    {
        infoLabel.text = @"Cố lên nào";
    }
    else if (starNumber == 0)
    {
        infoLabel.text = @"Cố lên nào";
    }
    
    [endingView addSubview:infoLabel];
    
    
    float sizeOfImg = 40*[UIScreen mainScreen].bounds.size.width/320.0;
    float xStart = ([UIScreen mainScreen].bounds.size.width - (sizeOfImg*3 + 10*2))/2;
    UIImageView *imgViewTemp;
    
    for(int i = 1; i<= 3; i++)
    {
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(xStart, infoLabel.frame.origin.y+infoLabel.frame.size.height+20, sizeOfImg, sizeOfImg)];
        imgView.image = [UIImage imageNamed:@"favorite.png"];
        
        if(i==1&& starNumber==1)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        else if((i==1 ||i ==2) && starNumber == 2)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        else if((i==1 ||i ==2 || i==3) && starNumber == 3)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        
        xStart += sizeOfImg + 10;
        [endingView addSubview:imgView];
        imgViewTemp = imgView;
    }
    
    
    float sizeOfBt = 50*[UIScreen mainScreen].bounds.size.width/320.0;
    
    UIButton *replayButton = [UIButton new];
    float xReplayBt = ([UIScreen mainScreen].bounds.size.width - sizeOfBt*2 - 40)/2;
    replayButton.frame = CGRectMake(xReplayBt, imgViewTemp.frame.origin.y+imgViewTemp.frame.size.height + 30, sizeOfBt, sizeOfBt);
    [replayButton setBackgroundImage:[UIImage imageNamed:@"icon_redo.png"] forState:UIControlStateNormal];
    [replayButton addTarget:self action:@selector(replay) forControlEvents:UIControlEventTouchUpInside];
    [endingView addSubview:replayButton];
    
    
    
    UIButton *nextButton = [UIButton new];
    nextButton.frame = CGRectMake(replayButton.frame.origin.x+sizeOfBt+40, imgViewTemp.frame.origin.y+imgViewTemp.frame.size.height + 30, sizeOfBt, sizeOfBt);
    [nextButton setBackgroundImage:[UIImage imageNamed:@"next_icon.png"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(backViewControllerWhenFinish) forControlEvents:UIControlEventTouchUpInside];
    [endingView addSubview:nextButton];
    
    if(starNumber > [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"%@/topic%d/sub%d/game%d",g_course,[self.topicID intValue],self.sub,[self.gameID intValue]]])
    {
        [[NSUserDefaults standardUserDefaults] setInteger:starNumber forKey:[NSString stringWithFormat:@"%@/topic%d/sub%d/game%d",g_course,[self.topicID intValue],self.sub,[self.gameID intValue]]];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [HttpThread upDateToServerGame:[self.gameID intValue] Topic:[self.topicID intValue] Course:g_course Sub:self.sub Star:starNumber];
        });
    }
    if(starNumber >= 1)
    {
        if([[[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"%@/topic%d/sub%d/game%d_islock",g_course,[self.topicID intValue],self.sub,[self.gameID intValue]+1]] isEqualToString:@"lock"])
        {
            [[NSUserDefaults standardUserDefaults] setValue:@"unlock" forKey:[NSString stringWithFormat:@"%@/topic%d/sub%d/game%d_islock",g_course,[self.topicID intValue],self.sub,[self.gameID intValue]+1]];
        }
    }
    
}


#pragma mark init button

-(void)backViewController
{
    NSString *mes = @"Bạn có chắc chắn muốn thoát game này ?";
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:mes
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    
    [alert show];

}


-(IBAction)leftButton:(CustomButton*)sender
{
    if(choosedLeftBt)
    {
        [choosedLeftBt setBackgroundColor:[UIColor colorWithRed:82.0/255.0 green:128.0/255.0 blue:195.0/255.0 alpha:1]];
    }
    
    choosedLeftBt = sender;
    [choosedLeftBt setBackgroundColor:[UIColor colorWithRed:62.0/255.0 green:103.0/255.0 blue:163.0/255.0 alpha:1]];
    
}


-(IBAction)rightButton:(CustomButton*)sender
{
    
    if(!choosedLeftBt)
    {
        [Toast showToastOnView:self.view withMessage:@"Vui lòng chọn một từ ở cột bên trái trước !"];
    }
    else
    {
        
        if(choosedRightBt)
        {
            [choosedRightBt setBackgroundColor:[UIColor colorWithRed:214.0/255.0 green:13.0/255.0 blue:0.0/255.0 alpha:1]];
        }
        choosedRightBt = sender;
        [choosedRightBt setBackgroundColor:[UIColor colorWithRed:164.0/255.0 green:7.0/255.0 blue:0.0/255.0 alpha:1]];
        
        
        if(choosedRightBt.lesson.lessonID == choosedLeftBt.lesson.lessonID)
        {
            [self handleWhenTrueAnswer];
        }
        else
        {
            [self handleWhenFalseAnswer];
        }
        
    }
}


-(void)backViewControllerWhenFinish
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) replay
{
    for(UIView *view in self.view.subviews)
    {
        [view removeFromSuperview];
    }
    
    [self initView];
    
}

#pragma mark private method


-(void)updateRateDay
{
    NSDate *previousDay = [[NSUserDefaults standardUserDefaults] objectForKey:@"rateDay"];
    if(!previousDay)
    {
        showRateView = true;
        
    }
    else
    {
        NSTimeInterval time = [[NSDate date] timeIntervalSinceDate:previousDay];
        if(time > 24*3600)
        {
            showRateView = true;
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"rateDay"];
    
}



-(Lesson*)randomLesson:(NSMutableArray*)numberArray andSet:(QuestionSet*)set
{
    Lesson *ls;
    
    int random = arc4random_uniform(numberArray.count);
    NSString *str = numberArray[random];
    [numberArray removeObject:str];
    
    if([str isEqualToString:@"1"])
    {
        ls = set.answer;
    }
    else
    {
        ls = set.falseAnswerArray[[str intValue] - 2];
    }
    
    return ls;
}


-(void)handleWhenFalseAnswer
{
    NSTimer *t;
    t = [NSTimer scheduledTimerWithTimeInterval:1.0
                                         target:self
                                       selector:@selector(timerWhenFalse:)
                                       userInfo:nil
                                        repeats:NO];
}

-(void)handleWhenTrueAnswer
{
    NSTimer *t;
    t = [NSTimer scheduledTimerWithTimeInterval:1.0
                                         target:self
                                       selector:@selector(timerWhenTrue:)
                                       userInfo:nil
                                        repeats:NO];
}

-(int)getStars
{
    
    return 3;

}

#pragma mark- timer

//-(void)updateLearnTime
//{
//    [[NSUserDefaults standardUserDefaults] setDouble:([[NSUserDefaults standardUserDefaults] doubleForKey:[NSString stringWithFormat:@"timeLearned_%@",g_course]]+1) forKey:[NSString stringWithFormat:@"timeLearned_%@",g_course]];
//    
//}


-(void)timerWhenFalse:(NSTimer*)timer
{
    NSString *audioFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"fail.mp3"];
    NSURL *url = [[NSURL alloc] initFileURLWithPath:audioFilePath];
    resultPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    resultPlayer.numberOfLoops = 0;
    resultPlayer.delegate = self;
    [resultPlayer play];
    
    [choosedRightBt setBackgroundColor:[UIColor colorWithRed:214.0/255.0 green:13.0/255.0 blue:0.0/255.0 alpha:1]];
    choosedRightBt = NULL;
    
    
}

-(void)timerWhenTrue:(NSTimer*)timer
{
    NSString *audioFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"true.mp3"];
    NSURL *url = [[NSURL alloc] initFileURLWithPath:audioFilePath];
    resultPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    resultPlayer.numberOfLoops = 0;
    resultPlayer.delegate = self;
    [resultPlayer play];
    
    [CustomButton animateWithDuration:0.2
                           animations:^{
        
                               [choosedLeftBt setFrame:CGRectMake(choosedLeftBt.frame.origin.x+choosedLeftBt.frame.size.width/2, choosedLeftBt.frame.origin.y+choosedLeftBt.frame.size.height/2, 0, 0)];
                               
                               [choosedRightBt setFrame:CGRectMake(choosedRightBt.frame.origin.x+choosedRightBt.frame.size.width/2, choosedRightBt.frame.origin.y+choosedRightBt.frame.size.height/2, 0, 0)];
                               
                               
    }completion:^(BOOL finish)
    {
        
        [choosedLeftBt removeFromSuperview];
        [choosedRightBt removeFromSuperview];
        
        choosedLeftBt = NULL;
        choosedRightBt = NULL;
        
        
        if(gameView.subviews.count==0)
        {
            [self initEndingViewWithStars:3];
        }
    }];
    
}


#pragma mark- alert view delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark- avaudio delegate

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [player stop];
    
    if(player == resultPlayer)
    {
        
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
