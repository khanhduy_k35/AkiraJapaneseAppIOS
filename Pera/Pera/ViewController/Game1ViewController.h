//
//  Game1ViewController.h
//  Pera
//
//  Created by Le Trong Thao on 12/4/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@interface Game1ViewController : UIViewController

@property NSString *topicID;
@property NSString *gameID;
@property int sub;

@end
