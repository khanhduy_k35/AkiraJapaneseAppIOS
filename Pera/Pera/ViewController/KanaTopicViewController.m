//
//  KanaTopicViewController.m
//  Pera
//
//  Created by Le Trong Thao on 3/2/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import "KanaTopicViewController.h"
#import "UIViewController+AMSlideMenu.h"
#import "KanaGameListViewController.h"
#import "ProgressView.h"
#import "GameUtil.h"
#import "GlobalVariable.h"
#import "DataHandle.h"
#import "AppDelegate.h"
#import "Reachability.h"


@interface KanaTopicViewController ()<UITableViewDataSource, UITableViewDelegate>

@end

@implementation KanaTopicViewController
{
    
    UITableView *topicTable;
    
    UIView *topView;
    UIView *topLabelView;
    
    NSArray *topicTitleArray;
    
    float heightOfCell;
    
    NSArray *topicTitleAr;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    topicTitleAr = @[@"Học hàng a,ka", @"Học hàng sa,ta", @"Học hàng na,ha", @"Học hàng ma,ya", @"Học hàng ra,wa", @"Âm đục 1", @"Âm đục 2", @"Âm ngắt", @"Âm dài", @"Âm ghép 1", @"Âm ghép 2", @"Âm ghép 3", @"Âm ghép dài"];
    
    
    topView = [UIView new];
    [topView setBackgroundColor:[UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1]];
    [self.view addSubview:topView];
    
    topLabelView = [UIView new];
    [topView addSubview:topLabelView];
    
    topicTable = [UITableView new];
    [topicTable setTag:25];
    [self.view addSubview:topicTable];
    topicTable.delegate = self;
    topicTable.dataSource = self;
    topicTable.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];
    topicTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [topicTable setShowsVerticalScrollIndicator:NO];
    
    heightOfCell = 100*[UIScreen mainScreen].bounds.size.height/568.0;
    
    
    self.view.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];
    
    [self adjustView];
    
}

- (void)buysuccess:(NSNotification *)note {
    [self reloadTable];
}

-(void)viewWillAppear:(BOOL)animated
{
    //    if([g_language isEqualToString:@"vi"])
    //    {
    //        [[self mainSlideMenu] disableSlidePanGestureForRightMenu];
    //    }
    
    
    [self disableSlidePanGestureForLeftMenu];
    
    currentViewController =  self;
    
    
    [topicTable reloadData];
}

-(void)viewDidAppear:(BOOL)animated
{
    //[self adjustView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)adjustView
{
    topView.frame = [GameUtil transformFitToScree:CGRectMake(0, 0, 320, 60) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    
    topLabelView.frame = [GameUtil transformFitToScreen:topView.frame.size.width height:topView.frame.size.height WithOriginFrame:CGRectMake(40, 15, 240, 44) originScreenWidth:320 andOriginScreenHeight:60];
    
    
    topicTable.frame = CGRectMake(0, topView.frame.origin.y+topView.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-topView.frame.size.height);
    
    
    UILabel *topLabel = [[UILabel alloc] initWithFrame:topLabelView.frame];
    topLabel.font = [UIFont boldSystemFontOfSize:17];
    topLabel.backgroundColor = [UIColor clearColor];
    topLabel.textAlignment = NSTextAlignmentCenter;
    topLabel.textColor = [UIColor whiteColor];
    if (self.type == 1)
    {
        topLabel.text = @"Bảng Hiragana";
    }
    else if(self.type == 2)
    {
        topLabel.text = @"Bảng Katakana";
    }
    [topView addSubview:topLabel];
    
    
    UIButton *backButton = [UIButton new];
    //float heightOfBt = 28;
    //float widthOfBt = 28;
    backButton.frame = [GameUtil transformFitToScreen:topView.frame.size.width height:topView.frame.size.height WithOriginFrame:CGRectMake(10, 25, 40, 40) originScreenWidth:320 andOriginScreenHeight:60];
    backButton.frame = CGRectMake(backButton.frame.origin.x, topLabelView.frame.origin.y+topLabelView.frame.size.height/2- backButton.frame.size.height/2, backButton.frame.size.width, backButton.frame.size.height);
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backViewController) forControlEvents:UIControlEventTouchUpInside];
    
    [topView addSubview:backButton];
}


#pragma mark- button



-(void)backViewController
{
    [self.navigationController popViewControllerAnimated:true];
}


#pragma mark table method

-(void)reloadTable
{
    //    if([g_language isEqualToString:@"en"])
    //    {
    //        topicTitleArray = [JsonFileHandle getTopicTitlesWithLanguageKey:@"eng"];
    //    }
    //    else
    //    {
    //        topicTitleArray = [JsonFileHandle getTopicTitlesWithLanguageKey:g_language];
    //    }
    [topicTable reloadData];
}


#pragma mark- tableView data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return heightOfCell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 13;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    cell.backgroundColor =[UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIView *view = [UIView new];
    view.frame = CGRectMake(5, 5, [UIScreen mainScreen].bounds.size.width - 10, heightOfCell-10);
    view.userInteractionEnabled = true;
    view.backgroundColor = [UIColor whiteColor];
    [cell addSubview:view];
    
    float sizeOfImg = view.frame.size.height*3/4;
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, (view.frame.size.height - sizeOfImg)/2, sizeOfImg, sizeOfImg)];
    //imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d.jpg",indexPath.row+1]];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentPath = [paths firstObject];
        
        NSString *typeStr = @"";
        if(self.type == 1)
        {
            typeStr = @"hiragana";
        }
        else
        {
            typeStr = @"katakana";
        }
        
        UIImage *image = [UIImage imageWithContentsOfFile:[documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/courses/kana/category/%@/%d.jpg",typeStr,indexPath.row+1]]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.5 animations:^{
                [imgView setImage:image];
            }];
            
        });
        
    });
    [view addSubview:imgView];
    
    
    UILabel *topicLabel = [UILabel new];
    topicLabel.frame = CGRectMake(imgView.frame.origin.x + imgView.frame.size.width +25, imgView.frame.origin.y, view.frame.size.width -(imgView.frame.origin.x + imgView.frame.size.width + 25 +60) , imgView.frame.size.height/2);
    topicLabel.font = [UIFont boldSystemFontOfSize:15];
    topicLabel.numberOfLines  = 0;
    topicLabel.textAlignment = NSTextAlignmentCenter;
    topicLabel.backgroundColor = [UIColor clearColor];
    topicLabel.textColor = [UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1];
    topicLabel.text = topicTitleAr[indexPath.row];
    [view addSubview:topicLabel];
    
    
    int stars = [DataHandle getStarsOfTopic:indexPath.row+1 Level:g_course Type:self.type];
    
    
    ProgressView *progressBarView = [[ProgressView alloc] initWithFrame:CGRectMake(topicLabel.frame.origin.x, topicLabel.frame.origin.y+topicLabel.frame.size.height+5, topicLabel.frame.size.width, 5) withMax:100];
    [progressBarView setPercentComplete:stars*100/[DataHandle getSumStarOfTopic:indexPath.row+1 Type:self.type]];
    [view addSubview:progressBarView];

    
    return cell;
}

#pragma mark- tableView delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        
    [self performSegueWithIdentifier:@"kanaGameList" sender:indexPath];
}


-(BOOL)currentNetworkStatus
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}


#pragma mark segue method

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    NSIndexPath *indexPath = (NSIndexPath*)sender;
    
    KanaGameListViewController *dvc = (KanaGameListViewController*)segue.destinationViewController;
    dvc.topicId = indexPath.row + 1;
    dvc.type = self.type;
    
    
}



@end
