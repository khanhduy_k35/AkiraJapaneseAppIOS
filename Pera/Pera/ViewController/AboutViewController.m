//
//  AboutViewController.m
//  Pera
//
//  Created by Le Trong Thao on 12/7/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import "AboutViewController.h"
#import "GameUtil.h"
#import "UIViewController+AMSlideMenu.h"
#import "JsonFileHandle.h"
#import "GlobalVariable.h"
#import "Email.h"
#import "AMSlideMenuMainViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController
{
    UIView *topView;
        
    UIButton *backButton;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self disableSlidePanGestureForLeftMenu];
    
    AMSlideMenuMainViewController *mvc = [self mainSlideMenu];
    mvc.rightPanDisabled  = true;
    
    float scrWidth = [UIScreen mainScreen].bounds.size.width;
    float scrHeight = [UIScreen mainScreen].bounds.size.height;
    
    
    topView = [UIView new];
    
    
    backButton = [UIButton new];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backViewController:) forControlEvents:UIControlEventTouchUpInside];
    
    topView.frame = [GameUtil transformFitToScree:CGRectMake(0, 0, 320, 60) withScreenWidth:scrWidth withScreenHeight:scrHeight];
    
    
    backButton.frame = [GameUtil transformFitToScreen:topView.frame.size.width height:topView.frame.size.height WithOriginFrame:CGRectMake(10, 15, 40, 40) originScreenWidth:320 andOriginScreenHeight:60];
    
    [topView setBackgroundColor:[UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1]];
    
    [self.view addSubview:topView];
    
    [topView addSubview:backButton];
    
    
    UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_akira.png"]];
    float logoHeight = 140;
    float logoWidth = 270;
    logo.frame = [GameUtil transformFitToScree:CGRectMake((320-logoWidth)/2, 120, logoWidth, logoHeight) withScreenWidth:scrWidth withScreenHeight:scrHeight];
    [self.view addSubview:logo];
    
    UILabel *headerLabel = [UILabel new];
    headerLabel.frame = [GameUtil transformFitToScree:CGRectMake(0, 265, 320, 40) withScreenWidth:scrWidth withScreenHeight:scrHeight];
    headerLabel.numberOfLines = 0;
    headerLabel.minimumScaleFactor = 14/15;
    headerLabel.adjustsFontSizeToFitWidth = true;
    headerLabel.font = [UIFont systemFontOfSize:15];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor = [UIColor blackColor];
    headerLabel.text = @"Pera Pera\nBy Akira Education";
    [self.view addSubview:headerLabel];
    
    
    UILabel *bodyLabel1 = [UILabel new];
    bodyLabel1.frame = [GameUtil transformFitToScree:CGRectMake(20, 320, 280, 50) withScreenWidth:scrWidth withScreenHeight:scrHeight];
    //bodyLabel1.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width-280)/2, bodyLabel1.frame.origin.y, 280, bodyLabel1.frame.size.height);
    bodyLabel1.numberOfLines = 0;
    //bodyLabel1.minimumScaleFactor = 14/15;
    bodyLabel1.adjustsFontSizeToFitWidth = true;
    bodyLabel1.font = [UIFont boldSystemFontOfSize:15];
    bodyLabel1.textAlignment = NSTextAlignmentCenter;
    bodyLabel1.backgroundColor = [UIColor clearColor];
    bodyLabel1.textColor = [UIColor blackColor];
    bodyLabel1.text = @"Akira Learning Japanese là apps hỗ trợ học tập cho học viên trên website học tiếng Nhật của Akira";
    [self.view addSubview:bodyLabel1];
    
    UIButton *rateApp = [UIButton new];
    [rateApp setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:51/255.0 green:110/255.0 blue:180/255.0 alpha:1]] forState:UIControlStateHighlighted];
    [rateApp setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1]] forState:UIControlStateNormal];
    rateApp.titleLabel.font = [UIFont boldSystemFontOfSize:15.0];
    [rateApp setTitle:@"Đánh giá apps" forState:UIControlStateNormal];
    [rateApp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    rateApp.frame = [GameUtil transformFitToScree:CGRectMake(20, 405, 280, 30) withScreenWidth:scrWidth withScreenHeight:scrHeight];
    [rateApp addTarget:self action:@selector(rateApp) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:rateApp];
    
    
    UIButton *later = [UIButton new];
    [later setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:51/255.0 green:110/255.0 blue:180/255.0 alpha:1]] forState:UIControlStateHighlighted];
    [later setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1]] forState:UIControlStateNormal];
    later.titleLabel.font = [UIFont boldSystemFontOfSize:15.0];
    [later setTitle:@"Để sau" forState:UIControlStateNormal];
    [later setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    later.frame = [GameUtil transformFitToScree:CGRectMake(20, 440, 280, 30) withScreenWidth:scrWidth withScreenHeight:scrHeight];
    [later addTarget:self action:@selector(later) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:later];
    
    
    UIButton *feedback = [UIButton new];
    //[feedback setBackgroundColor:[UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1]];
    
    [feedback setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:51/255.0 green:110/255.0 blue:180/255.0 alpha:1]] forState:UIControlStateHighlighted];
    [feedback setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1]] forState:UIControlStateNormal];
    
    feedback.titleLabel.font = [UIFont boldSystemFontOfSize:15.0];
    [feedback setTitle:@"Phản hồi" forState:UIControlStateNormal];
    [feedback setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    feedback.frame = [GameUtil transformFitToScree:CGRectMake(20, 475, 280, 30) withScreenWidth:scrWidth withScreenHeight:scrHeight];
    [feedback addTarget:self action:@selector(feedBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:feedback];
    
    
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(void)rateApp
{
    NSLog(@"rate");
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"neverRate"];
    NSString *str = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=1046203855";
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        str = @"itms-apps://itunes.apple.com/app/id1046203855";
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)later
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)feedBack
{
    [Email mailTo:emailAccount
            Title:feedbackTitle
       andContent:@""];
}

-(IBAction)backViewController:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
