//
//  KanaGame6ViewController.h
//  Pera
//
//  Created by Le Trong Thao on 3/3/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KanaGame6ViewController : UIViewController


@property NSString *topicID;
@property NSString *gameID;
@property int type;


@end
