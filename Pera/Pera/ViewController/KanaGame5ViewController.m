//
//  KanaGame5ViewController.m
//  Pera
//
//  Created by Le Trong Thao on 3/3/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import "KanaGame5ViewController.h"
#import "ProgressView.h"
#import "CircularProgressView.h"
#import "GameUtil.h"
#import "GlobalVariable.h"
#import "LessonHandle.h"
#import "DataHandle.h"

#import "CustomAnimation.h"
#import "CustomImageView.h"

#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

#import "UIViewController+AMSlideMenu.h"

#import "HttpThread.h"

@interface KanaGame5ViewController ()<UIAlertViewDelegate,CircularProgressViewDelegate, AVAudioPlayerDelegate>

@end

@implementation KanaGame5ViewController
{
    NSMutableArray *questionArray;
    
    ProgressView *progressView;
    UIView *gameView;
    int currentIndex;
    int totalTrueAnswer;
    
    NSMutableArray *imgViewArray;
    
    CustomImageView *choosedImgView;
    
    
    
    AVAudioPlayer *readPlayer;
    AVAudioPlayer *resultPlayer;
    
    
    BOOL setCirclePlayer;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self disableSlidePanGestureForLeftMenu];
    
    
    [self initView];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    currentViewController = self;
}

-(void)viewWillDisappear:(BOOL)animated
{
    
}

#pragma mark initView

-(void)initView
{
    totalTrueAnswer = 0;
    currentIndex = 0;
    questionArray = [LessonHandle getQuestionSetsFrom:[DataHandle getAllLessonTestOfKanaTopic:[self.topicID intValue] Type:self.type] withAmount:4];
    
    UIButton *backButton = [UIButton new];
    float heightOfBt = 36;
    float widthOfBt = 36;
    backButton.frame = [GameUtil transformFitToScree:CGRectMake(8, 30 , widthOfBt, heightOfBt) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    [backButton setBackgroundImage:[UIImage imageNamed:@"abc_ic_ab_back_mtrl_am_alpha_blue.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backViewController) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:backButton];
    
    
    
    float xProgressView = backButton.frame.origin.x + backButton.frame.size.width + 10;
    float widthProgressView = [UIScreen mainScreen].bounds.size.width - xProgressView*2;
    float heightProgressView = 6;
    
    progressView = [[ProgressView alloc] initWithFrame:CGRectMake(xProgressView, backButton.frame.origin.y+backButton.frame.size.height/2-heightProgressView/2, widthProgressView, heightProgressView) withMax:questionArray.count];
    [self.view addSubview:progressView];
    
    
    UILabel *topLabel = [UILabel new];
    topLabel.frame = CGRectMake(10, progressView.frame.origin.y+progressView.frame.size.height+15, [UIScreen mainScreen].bounds.size.width-20,25);
    topLabel.font = [UIFont systemFontOfSize:15];
    topLabel.numberOfLines = 0;
    topLabel.textAlignment = NSTextAlignmentCenter;
    topLabel.textColor = [UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1];
    topLabel.backgroundColor = [UIColor clearColor];
    topLabel.text = @"Chọn cách viết đúng của";
    [self.view addSubview:topLabel];
    
    
    [self initGameView];
    
}


-(void)initGameView
{
    choosedImgView = NULL;
    
    QuestionSet *set = questionArray[currentIndex];
    
    gameView = [UIView new];
    float yGameView = 100;
    gameView.frame = [GameUtil transformFitToScree:CGRectMake(0, yGameView, 320, 568 - yGameView) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    gameView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:gameView];
    
    
    UILabel *quesLabel = [UILabel new];
    quesLabel.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,25);
    quesLabel.font = [UIFont boldSystemFontOfSize:15];
    quesLabel.numberOfLines = 0;
    quesLabel.textAlignment = NSTextAlignmentCenter;
    quesLabel.textColor = [UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1];
    quesLabel.backgroundColor = [UIColor clearColor];
    quesLabel.text = [NSString stringWithFormat:@"\"%@\"",set.answer.vietnamese];
    
    [gameView addSubview:quesLabel];
    
    
    
    float sizeOfCircleProgress = 45*[UIScreen mainScreen].bounds.size.width/320;
    
    
    NSMutableArray *numberAr = [[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4"]];
    int heightScreen= [UIScreen mainScreen].bounds.size.height;
    float sizeOfImg;
    if(heightScreen < 500){
        sizeOfImg = 100*[UIScreen mainScreen].bounds.size.height/568;
    }else{
        sizeOfImg = 110*[UIScreen mainScreen].bounds.size.height/568;
    }
    
    
    float between = 30;
    float leftPadding = (gameView.frame.size.width - 2*sizeOfImg - between)/2;
    
    CustomImageView *imgView1 = [[CustomImageView alloc] initWithFrame:CGRectMake(0,0, sizeOfImg, sizeOfImg)];
    imgView1.lesson = [self randomLesson:numberAr andSet:set];
    [imgView1.layer setBorderWidth:1];
    imgView1.layer.borderColor = [[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor];
    imgView1.image = [[UIImage alloc] initWithContentsOfFile:[DataHandle getImgPathOfKanaGame:imgView1.lesson.fileName Type:self.type]];
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectImgView:)];
    tap1.numberOfTouchesRequired = 1;
    [imgView1 addGestureRecognizer:tap1];
    imgView1.contentMode = UIViewContentModeScaleToFill;
    imgView1.userInteractionEnabled = true;
    
    UILabel *lb1 = [UILabel new];
    lb1.font = [UIFont systemFontOfSize:13];
    lb1.numberOfLines = 0;
    lb1.adjustsFontSizeToFitWidth = true;
    lb1.minimumScaleFactor  =11/13;
    lb1.textColor = [UIColor blackColor];
    lb1.textAlignment = NSTextAlignmentCenter;
    lb1.backgroundColor = [UIColor clearColor];
    lb1.text = imgView1.lesson.hiragana;
    CGSize constraint = CGSizeMake(imgView1.frame.size.width, 20000.0f);
    CGSize size = [lb1.text sizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:constraint lineBreakMode:UILineBreakModeWordWrap];
    lb1.frame = CGRectMake(1, imgView1.frame.origin.y+imgView1.frame.size.height+ 5, imgView1.frame.size.width - 2, size.height + 5);
    
    UIView *view1 = [UIView new];
    view1.backgroundColor = [UIColor whiteColor];
    view1.userInteractionEnabled = true;
    //view1.layer.borderWidth = 0;
    //view1.layer.borderColor = [[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor];
    view1.frame = CGRectMake(leftPadding, quesLabel.frame.origin.y+quesLabel.frame.size.height + 10 +sizeOfCircleProgress +10, imgView1.frame.size.width, imgView1.frame.size.height+5+lb1.frame.size.height);
    [view1 addSubview:imgView1];
    [view1 addSubview:lb1];
    [gameView addSubview:view1];
    
    
    
    
    CustomImageView *imgView2 = [[CustomImageView alloc] initWithFrame:CGRectMake(0,0, sizeOfImg, sizeOfImg)];
    imgView2.lesson = [self randomLesson:numberAr andSet:set];
    [imgView2.layer setBorderWidth:1];
    imgView2.layer.borderColor = [[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor];
    imgView2.image = [[UIImage alloc] initWithContentsOfFile:[DataHandle getImgPathOfKanaGame:imgView2.lesson.fileName Type:self.type]];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectImgView:)];
    tap2.numberOfTouchesRequired = 1;
    [imgView2 addGestureRecognizer:tap2];
    imgView2.contentMode = UIViewContentModeScaleToFill;
    imgView2.userInteractionEnabled = true;
    
    UILabel *lb2 = [UILabel new];
    lb2.font = [UIFont systemFontOfSize:13];
    lb2.numberOfLines = 0;
    lb2.adjustsFontSizeToFitWidth = true;
    lb2.minimumScaleFactor  =11/13;
    lb2.textColor = [UIColor blackColor];
    lb2.textAlignment = NSTextAlignmentCenter;
    lb2.backgroundColor = [UIColor clearColor];
    lb2.text = imgView2.lesson.hiragana;
    CGSize constraint2 = CGSizeMake(imgView2.frame.size.width, 20000.0f);
    CGSize size2 = [lb2.text sizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:constraint lineBreakMode:UILineBreakModeWordWrap];
    lb2.frame = CGRectMake(1, imgView2.frame.origin.y+imgView2.frame.size.height+ 5, imgView2.frame.size.width - 2, size.height + 5);
    
    UIView *view2 = [UIView new];
    view2.backgroundColor = [UIColor whiteColor];
    view2.userInteractionEnabled = true;
    //view1.layer.borderWidth = 0;
    //view1.layer.borderColor = [[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor];
    view2.frame = CGRectMake(view1.frame.origin.x+view1.frame.size.width+between, view1.frame.origin.y, imgView2.frame.size.width, imgView2.frame.size.height+5+lb2.frame.size.height);
    [view2 addSubview:imgView2];
    [view2 addSubview:lb2];
    [gameView addSubview:view2];
    
    
    
    
    
    CustomImageView *imgView3 = [[CustomImageView alloc] initWithFrame:CGRectMake(0,0, sizeOfImg, sizeOfImg)];
    imgView3.lesson = [self randomLesson:numberAr andSet:set];
    [imgView3.layer setBorderWidth:1];
    imgView3.layer.borderColor = [[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor];
    imgView3.image = [[UIImage alloc] initWithContentsOfFile:[DataHandle getImgPathOfKanaGame:imgView3.lesson.fileName Type:self.type]];
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectImgView:)];
    tap3.numberOfTouchesRequired = 1;
    [imgView3 addGestureRecognizer:tap3];
    imgView3.contentMode = UIViewContentModeScaleToFill;
    imgView3.userInteractionEnabled = true;
    
    UILabel *lb3 = [UILabel new];
    lb3.font = [UIFont systemFontOfSize:13];
    lb3.numberOfLines = 0;
    lb3.adjustsFontSizeToFitWidth = true;
    lb3.minimumScaleFactor  =11/13;
    lb3.textColor = [UIColor blackColor];
    lb3.textAlignment = NSTextAlignmentCenter;
    lb3.backgroundColor = [UIColor clearColor];
    lb3.text = imgView3.lesson.hiragana;
    CGSize constraint3 = CGSizeMake(imgView3.frame.size.width, 20000.0f);
    CGSize size3 = [lb3.text sizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:constraint lineBreakMode:UILineBreakModeWordWrap];
    lb3.frame = CGRectMake(1, imgView3.frame.origin.y+imgView3.frame.size.height+ 5, imgView3.frame.size.width - 2, size.height + 5);
    
    UIView *view3 = [UIView new];
    view3.backgroundColor = [UIColor whiteColor];
    view3.userInteractionEnabled = true;
    //view1.layer.borderWidth = 0;
    //view1.layer.borderColor = [[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor];
    view3.frame = CGRectMake(leftPadding, view1.frame.origin.y+view1.frame.size.height + 10, imgView3.frame.size.width, imgView3.frame.size.height+5+lb3.frame.size.height);
    [view3 addSubview:imgView3];
    [view3 addSubview:lb3];
    [gameView addSubview:view3];
    
    
    
    
    
    
    CustomImageView *imgView4 = [[CustomImageView alloc] initWithFrame:CGRectMake(0,0, sizeOfImg, sizeOfImg)];
    imgView4.lesson = [self randomLesson:numberAr andSet:set];
    [imgView4.layer setBorderWidth:1];
    imgView4.layer.borderColor = [[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor];
    imgView4.image = [[UIImage alloc] initWithContentsOfFile:[DataHandle getImgPathOfKanaGame:imgView4.lesson.fileName Type:self.type]];
    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectImgView:)];
    tap4.numberOfTouchesRequired = 1;
    [imgView4 addGestureRecognizer:tap4];
    imgView4.contentMode = UIViewContentModeScaleToFill;
    imgView4.userInteractionEnabled = true;
    
    UILabel *lb4 = [UILabel new];
    lb4.font = [UIFont systemFontOfSize:13];
    lb4.numberOfLines = 0;
    lb4.adjustsFontSizeToFitWidth = true;
    lb4.minimumScaleFactor  =11/13;
    lb4.textColor = [UIColor blackColor];
    lb4.textAlignment = NSTextAlignmentCenter;
    lb4.backgroundColor = [UIColor clearColor];
    lb4.text = imgView4.lesson.hiragana;
    CGSize constraint4 = CGSizeMake(imgView4.frame.size.width, 20000.0f);
    CGSize size4 = [lb4.text sizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:constraint lineBreakMode:UILineBreakModeWordWrap];
    lb4.frame = CGRectMake(1, imgView4.frame.origin.y+imgView4.frame.size.height+ 5, imgView4.frame.size.width - 2, size.height + 5);
    
    UIView *view4 = [UIView new];
    view4.backgroundColor = [UIColor whiteColor];
    view4.userInteractionEnabled = true;
    //view1.layer.borderWidth = 0;
    //view1.layer.borderColor = [[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor];
    view4.frame = CGRectMake(view2.frame.origin.x, view3.frame.origin.y, imgView4.frame.size.width, imgView4.frame.size.height+5+lb4.frame.size.height);
    [view4 addSubview:imgView4];
    [view4 addSubview:lb4];
    [gameView addSubview:view4];
    
    imgViewArray = [[NSMutableArray alloc] initWithArray:@[view1,view2,view3,view4]];
    
    
    
    float heightOfNextButton = 30;
    
    if([UIScreen mainScreen].bounds.size.height == 480)
    {
        heightOfNextButton = 25;
    }
    
    UIButton *nextButton = [UIButton new];
    [nextButton setTitle:@"Kiểm tra" forState:UIControlStateNormal];
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setBackgroundColor:[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1]];
    nextButton.frame = CGRectMake((gameView.frame.size.width-120)/2, gameView.frame.size.height - heightOfNextButton - 5, 120, heightOfNextButton);
    [nextButton addTarget:self action:@selector(checkButton:) forControlEvents:UIControlEventTouchUpInside];
    [gameView addSubview:nextButton];
    
    
    setCirclePlayer = false;
    
    [CustomAnimation upperAnimationDuration1:0.5
                              WithViewsArray:imgViewArray
                               andCompletion:^(BOOL finish)
     {
         if(!setCirclePlayer)
         {
             setCirclePlayer = true;
             
             CircularProgressView *circleProgress = [[CircularProgressView alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-sizeOfCircleProgress)/2, quesLabel.frame.origin.x + quesLabel.frame.size.height + 10, sizeOfCircleProgress, sizeOfCircleProgress)
                                                                                      backColor:[UIColor colorWithRed:236.0 / 255.0 green:236.0 / 255.0 blue:236.0 / 255.0 alpha:1.0] progressColor:[UIColor colorWithRed:82.0 / 255.0 green:135.0 / 255.0 blue:237.0 / 255.0 alpha:1.0]
                                                                                      lineWidth:10
                                                                                       audioURL:[[NSURL alloc] initFileURLWithPath:[DataHandle getAudioPathOfKanaGame:set.answer.fileName Type:self.type]]
                                                                                        imgPlay:@"icon_play.png"
                                                                                        imgStop:@"icon_stop.png"];
             circleProgress.delegate = self;
             [circleProgress.btnPlay setImage:[UIImage imageNamed:@"icon_stop.png"] forState:UIControlStateNormal];
             [circleProgress setTag:1];
             [gameView addSubview:circleProgress];
             [circleProgress play];

         }
         
     }];
    
}


-(void)initEndingViewWithStars:(int)starNumber
{
    
    UIView *endingView = [UIView new];
    endingView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    endingView.userInteractionEnabled = true;
    endingView.backgroundColor = [UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1];
    [endingView setTag:1];
    [self.view addSubview:endingView];
    
    
    UILabel *infoLabel = [UILabel new];
    infoLabel.frame = CGRectMake(0, 100*[UIScreen mainScreen].bounds.size.height/568.0, [UIScreen mainScreen].bounds.size.width, 40);
    infoLabel.font = [UIFont boldSystemFontOfSize:25];
    infoLabel.textAlignment = NSTextAlignmentCenter;
    infoLabel.textColor = [UIColor whiteColor];
    infoLabel.backgroundColor = [UIColor clearColor];
    if(starNumber == 3)
    {
        infoLabel.text = @"Tuyệt vời";
    }
    else if (starNumber == 2)
    {
        infoLabel.text = @"Tốt";
    }
    else if (starNumber == 1)
    {
        infoLabel.text = @"Cố lên nào";
    }
    else if (starNumber == 0)
    {
        infoLabel.text = @"Cố lên nào";
    }
    
    [endingView addSubview:infoLabel];
    
    
    float sizeOfImg = 40*[UIScreen mainScreen].bounds.size.width/320.0;
    float xStart = ([UIScreen mainScreen].bounds.size.width - (sizeOfImg*3 + 10*2))/2;
    UIImageView *imgViewTemp;
    
    for(int i = 1; i<= 3; i++)
    {
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(xStart, infoLabel.frame.origin.y+infoLabel.frame.size.height+20, sizeOfImg, sizeOfImg)];
        imgView.image = [UIImage imageNamed:@"favorite.png"];
        
        if(i==1&& starNumber==1)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        else if((i==1 ||i ==2) && starNumber == 2)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        else if((i==1 ||i ==2 || i==3) && starNumber == 3)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        
        xStart += sizeOfImg + 10;
        [endingView addSubview:imgView];
        imgViewTemp = imgView;
    }
    
    
    float sizeOfBt = 50*[UIScreen mainScreen].bounds.size.width/320.0;
    
    UIButton *replayButton = [UIButton new];
    float xReplayBt = ([UIScreen mainScreen].bounds.size.width - sizeOfBt*2 - 40)/2;
    replayButton.frame = CGRectMake(xReplayBt, imgViewTemp.frame.origin.y+imgViewTemp.frame.size.height + 30, sizeOfBt, sizeOfBt);
    [replayButton setBackgroundImage:[UIImage imageNamed:@"icon_redo.png"] forState:UIControlStateNormal];
    [replayButton addTarget:self action:@selector(replay) forControlEvents:UIControlEventTouchUpInside];
    [endingView addSubview:replayButton];
    
    
    
    UIButton *nextButton = [UIButton new];
    nextButton.frame = CGRectMake(replayButton.frame.origin.x+sizeOfBt+40, imgViewTemp.frame.origin.y+imgViewTemp.frame.size.height + 30, sizeOfBt, sizeOfBt);
    [nextButton setBackgroundImage:[UIImage imageNamed:@"next_icon.png"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(backViewControllerWhenFinish) forControlEvents:UIControlEventTouchUpInside];
    [endingView addSubview:nextButton];
    
    
    if(starNumber > [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d",self.type,[self.topicID intValue],[self.gameID intValue]]])
    {
        [[NSUserDefaults standardUserDefaults] setInteger:starNumber forKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d",self.type,[self.topicID intValue],[self.gameID intValue]]];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            
            NSString *course = @"";
            if(self.type==1)
            {
                course = @"hiragana";
            }
            else
            {
                course = @"katakana";
            }
            
            [HttpThread upDateToServerGame:[self.gameID intValue] Topic:[self.topicID intValue] Course:course Sub:1 Star:starNumber];
        });
        
    }
    
    if(starNumber >= 1)
    {
        if([[[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d_islock",self.type,[self.topicID intValue],[self.gameID intValue]+1]] isEqualToString:@"lock"])
        {
            [[NSUserDefaults standardUserDefaults] setValue:@"unlock" forKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d_islock",self.type,[self.topicID intValue],[self.gameID intValue]+1]];
        }
    }
    
}


-(void)showResViewWithRes:(BOOL)res
{
    float heightOfResView = 50;
    float heightOfNextButton = 30;
    
    if([UIScreen mainScreen].bounds.size.height == 480)
    {
        heightOfResView = 40;
        heightOfNextButton = 25;
    }
    
    UIView *resView = [UIView new];
    resView.frame = CGRectMake(0, gameView.frame.size.height - 5 - heightOfNextButton - 5 -heightOfResView, gameView.frame.size.width, heightOfResView);
    if(!res) resView.backgroundColor = [UIColor colorWithRed:253.0/255.0 green:235.0/255.0 blue:218.0/255.0 alpha:1];
    else resView.backgroundColor = [UIColor colorWithRed:221.0/255.0 green:230.0/255.0 blue:244.0/255.0 alpha:1];
    [gameView addSubview:resView];
    
    UIImageView *check_wrongImgView;
    if(!res) check_wrongImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"check_wrong.png"]];
    else check_wrongImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"check_right.png"]];
    check_wrongImgView.frame = CGRectMake(10, (resView.frame.size.height - 20)/2, 20, 20);
    [check_wrongImgView setAlpha:1];
    [resView addSubview:check_wrongImgView];
    
    UILabel *resLb = [UILabel new];
    resLb.frame = CGRectMake(check_wrongImgView.frame.origin.x+check_wrongImgView.frame.size.width+10, 0, resView.frame.size.width - (check_wrongImgView.frame.origin.x+check_wrongImgView.frame.size.width+10), resView.frame.size.height);
    resLb.font = [UIFont boldSystemFontOfSize:17];
    resLb.textAlignment = NSTextAlignmentLeft;
    resLb.textColor = [UIColor blackColor];
    resLb.backgroundColor = [UIColor clearColor];
    QuestionSet *set = questionArray[currentIndex];
    resLb.text = set.answer.hiragana;
    [resView addSubview:resLb];
    
    UIButton *nextButton = [UIButton new];
    [nextButton setTitle:@"Tiếp tục" forState:UIControlStateNormal];
    if(currentIndex == questionArray.count-1)
    {
        [nextButton setTitle:@"Kết thúc" forState:UIControlStateNormal];
    }
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setBackgroundColor:[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1]];
    nextButton.frame = CGRectMake((gameView.frame.size.width-100)/2, gameView.frame.size.height - heightOfNextButton - 5, 100, heightOfNextButton);
    [nextButton addTarget:self action:@selector(nextLesson) forControlEvents:UIControlEventTouchUpInside];
    [gameView addSubview:nextButton];
    
    
    NSString *audioFilePath;
    if(!res) audioFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"fail.mp3"];
    else audioFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"true.mp3"];
    NSURL *url = [[NSURL alloc] initFileURLWithPath:audioFilePath];
    resultPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    resultPlayer.numberOfLoops = 0;
    resultPlayer.delegate = self;
    [resultPlayer play];
}


#pragma mark init button

-(void)backViewController
{
    NSString *mes = @"Bạn có chắc chắn muốn thoát game này ?";
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:mes
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    
    [alert show];
}

-(IBAction)didSelectImgView:(UITapGestureRecognizer*)sender
{
    
    
    choosedImgView.layer.borderColor = sender.view.layer.borderColor;
    
    CustomImageView *img = (CustomImageView*)sender.view;
    choosedImgView = img;
    
    [img.layer setBorderWidth:1];
    img.layer.borderColor = [[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1] CGColor];
    
//    NSURL *audioUrl = [LessonHandle getAudioUrlOfFileName:img.lesson.fileName TopicID:img.lesson.topic Level:g_course];
//    readPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioUrl error:nil];
//    readPlayer.numberOfLoops = 0;
//    readPlayer.delegate = self;
//    [readPlayer play];
    
}

-(void)backViewControllerWhenFinish
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) replay
{
    for(UIView *view in self.view.subviews)
    {
        [view removeFromSuperview];
    }
    
    [self initView];
    
}

-(IBAction)checkButton:(UIButton*)sender
{
    
    
    QuestionSet *set  = questionArray[currentIndex];
    
    if(choosedImgView.lesson.lessonID == set.answer.lessonID)
    {
        NSLog(@"true");
        [self handleWhenTrueAnswer:nil];
    }
    else
    {
        NSLog(@"false");
        [self handleWhenFalseAnswer:nil];
    }
    
}

-(void)nextLesson
{
    
    CircularProgressView *circleProgress = (CircularProgressView*)[gameView viewWithTag:1];
    
    [circleProgress stop];
    
    
    if(currentIndex < questionArray.count)
    {
        
        [gameView removeFromSuperview];
        
        [self initGameView];
    }
    else
    {
        //        double delayInSeconds = 1.0;
        //        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        //        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //
        //        });
        
        [self initEndingViewWithStars:[self getStars]];
    }
}


#pragma mark private method

-(void)updateRateDay
{
    NSDate *previousDay = [[NSUserDefaults standardUserDefaults] objectForKey:@"rateDay"];
    if(!previousDay)
    {
        showRateView = true;
        
    }
    else
    {
        NSTimeInterval time = [[NSDate date] timeIntervalSinceDate:previousDay];
        if(time > 24*3600)
        {
            showRateView = true;
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"rateDay"];
    
}


-(BOOL)checkView:(UIView*)view inContainView:(UIView*)containView
{
    BOOL flag = false;
    
    for(UIView *viewTemp in containView.subviews)
    {
        if(viewTemp == view)
        {
            flag = true;
            break;
        }
    }
    
    
    return flag;
}


-(Lesson*)randomLesson:(NSMutableArray*)numberArray andSet:(QuestionSet*)set
{
    Lesson *ls;
    
    int random = arc4random_uniform(numberArray.count);
    NSString *str = numberArray[random];
    [numberArray removeObject:str];
    
    if([str isEqualToString:@"1"])
    {
        ls = set.answer;
    }
    else
    {
        ls = set.falseAnswerArray[[str intValue] - 2];
    }
    
    return ls;
}


-(void)handleWhenFalseAnswer:(CustomImageView*)imgView
{
    NSTimer *t;
    t = [NSTimer scheduledTimerWithTimeInterval:0.0
                                         target:self
                                       selector:@selector(timerWhenFalse:)
                                       userInfo:imgView
                                        repeats:NO];
}

-(void)handleWhenTrueAnswer:(CustomImageView*)imgView
{
    NSTimer *t;
    t = [NSTimer scheduledTimerWithTimeInterval:0.0
                                         target:self
                                       selector:@selector(timerWhenTrue:)
                                       userInfo:imgView
                                        repeats:NO];
}

-(int)getStars
{
    int stars = 0;
    
    if((float)totalTrueAnswer/questionArray.count >= 0.8)
    {
        stars = 3;
    }
    else if((float)totalTrueAnswer/questionArray.count >= 0.5)
    {
        stars = 2;
    }
    else if((float)totalTrueAnswer/questionArray.count >= 0.3)
    {
        stars = 1;
    }
    
    return stars;
    
}

#pragma mark- timer

-(void)timerWhenFalse:(NSTimer*)timer
{
    //choosedImgView = NULL;
    
    
    [self showResViewWithRes:false];
    
    currentIndex++;
    
    [progressView setPercentComplete:currentIndex];
    
    
}

-(void)timerWhenTrue:(NSTimer*)timer
{
    //CustomImageView *imgView = (CustomImageView*)[timer userInfo];
    //choosedImgView = NULL;
    
    [self showResViewWithRes:true];
    
    totalTrueAnswer++;
    currentIndex++;
    
    [progressView setPercentComplete:currentIndex];
    
}

#pragma mark- alert view delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
#pragma mark- CircularProgressViewDelegate


-(void)playerDidFinishPlaying
{
    NSLog(@"playerDidFinishPlaying");
    CircularProgressView *circleProgress = (CircularProgressView*)[gameView viewWithTag:1];
    
    [circleProgress.btnPlay setImage:[UIImage imageNamed:@"icon_play.png"] forState:UIControlStateNormal];
    
}

-(void)updateProgressViewWithPlayer:(AVAudioPlayer *)player
{
    
}

#pragma mark- avaudio delegate

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [player stop];
    
    if(player == readPlayer)
    {
        
    }
    else if(player == resultPlayer)
    {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
