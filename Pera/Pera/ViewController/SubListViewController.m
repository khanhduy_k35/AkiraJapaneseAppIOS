//
//  SubListViewController.m
//  Pera
//
//  Created by Le Trong Thao on 2/27/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import "SubListViewController.h"
#import "UIViewController+AMSlideMenu.h"
#import "GameListViewController.h"
#import "GlobalVariable.h"
#import "DataHandle.h"
#import "GameUtil.h"
#import "ProgressView.h"


@interface SubListViewController ()<UITableViewDataSource, UITableViewDelegate>

@end

@implementation SubListViewController
{
    float heightOfCell;
    
    NSArray *subTitleArray;
    NSArray *imgFileName;
    UIView *topView;
    UIView *topLabelView;
    
    UITableView *subTable;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    NSLog(@"%@",self.topic);
    
    topView = [UIView new];
    [topView setBackgroundColor:[UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1]];
    [self.view addSubview:topView];
    
    topLabelView = [UIView new];
    [topView addSubview:topLabelView];
    
    subTable = [UITableView new];
    [subTable setTag:25];
    [self.view addSubview:subTable];
    subTable.delegate = self;
    subTable.dataSource = self;
    subTable.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];
    subTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    subTable.contentInset = UIEdgeInsetsZero;
    [subTable setShowsVerticalScrollIndicator:NO];
    
    heightOfCell = 100*[UIScreen mainScreen].bounds.size.height/568.0;
    
    subTitleArray = @[@"Từ vựng 1",@"Từ vựng 2",@"Từ vựng 3",@"Từ vựng 4"];
    
    imgFileName = @[@"1.png",@"2.png",@"3.png",@"4.png"];
    
    [self initView];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self disableSlidePanGestureForLeftMenu];
    
    currentViewController = self;
    
    [subTable reloadData];
    
}


-(void)initView
{
    topView.frame = [GameUtil transformFitToScree:CGRectMake(0, 0, 320, 60) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    
    topLabelView.frame = [GameUtil transformFitToScreen:topView.frame.size.width height:topView.frame.size.height WithOriginFrame:CGRectMake(40, 15, 240, 44) originScreenWidth:320 andOriginScreenHeight:60];
    
    
    subTable.frame = CGRectMake(0, topView.frame.origin.y+topView.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-topView.frame.size.height);
    
    
    UILabel *topLabel = [[UILabel alloc] initWithFrame:topLabelView.frame];
    topLabel.font = [UIFont boldSystemFontOfSize:17];
    topLabel.backgroundColor = [UIColor clearColor];
    topLabel.textAlignment = NSTextAlignmentCenter;
    topLabel.textColor = [UIColor whiteColor];
    topLabel.text = [NSString stringWithFormat:@"Bài %@",self.topic];
    [topView addSubview:topLabel];
    
    
    UIButton *backButton = [UIButton new];
    //float heightOfBt = 28;
    //float widthOfBt = 28;
    backButton.frame = [GameUtil transformFitToScreen:topView.frame.size.width height:topView.frame.size.height WithOriginFrame:CGRectMake(10, 25, 40, 40) originScreenWidth:320 andOriginScreenHeight:60];
    backButton.frame = CGRectMake(backButton.frame.origin.x, topLabelView.frame.origin.y+topLabelView.frame.size.height/2- backButton.frame.size.height/2, backButton.frame.size.width, backButton.frame.size.height);
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backViewController) forControlEvents:UIControlEventTouchUpInside];
    
    [topView addSubview:backButton];
    
}


#pragma mark- button

-(void)backViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark- table view datasource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return heightOfCell;
}

//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return subTitleArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIView *view = [UIView new];
    view.frame = CGRectMake(5, 5, [UIScreen mainScreen].bounds.size.width - 10, heightOfCell-10);
    view.userInteractionEnabled = true;
    view.backgroundColor = [UIColor whiteColor];
    [cell addSubview:view];
    
    float sizeOfImg = view.frame.size.height*3/4;
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, (view.frame.size.height - sizeOfImg)/2, sizeOfImg, sizeOfImg)];
    NSString *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true)[0];
    NSString *filePath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/category/%@",imgFileName[indexPath.row]]];
    imgView.image = [UIImage imageWithContentsOfFile:filePath];
    imgView.layer.masksToBounds = true;
    imgView.layer.cornerRadius = imgView.frame.size.width/2;
    [view addSubview:imgView];
    
    
    UILabel *topicLabel = [UILabel new];
    topicLabel.frame = CGRectMake(imgView.frame.origin.x + imgView.frame.size.width +25, imgView.frame.origin.y, view.frame.size.width -(imgView.frame.origin.x + imgView.frame.size.width + 25 +60) , imgView.frame.size.height/2);
    topicLabel.font = [UIFont boldSystemFontOfSize:15];
    topicLabel.textAlignment = NSTextAlignmentCenter;
    topicLabel.backgroundColor = [UIColor clearColor];
    topicLabel.textColor = [UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1];
    NSString *topicTitle = subTitleArray[indexPath.row];
    topicLabel.text = [topicTitle stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:[[topicTitle substringWithRange:NSMakeRange(0, 1)] uppercaseString]];
    [view addSubview:topicLabel];
    
    
    int stars = [DataHandle getStarsOfSub:indexPath.row+1 Topic:[self.topic intValue] Level:g_course Type:0];
    
    int sumStars = 3*6;
    if(indexPath.row == 3)
    {
        sumStars = 3*3;
    }
    
    ProgressView *progressBarView = [[ProgressView alloc] initWithFrame:CGRectMake(topicLabel.frame.origin.x, topicLabel.frame.origin.y+topicLabel.frame.size.height+5, topicLabel.frame.size.width, 5) withMax:100];
    [progressBarView setPercentComplete:stars*100/sumStars];
    [view addSubview:progressBarView];
    
    return cell;
}

#pragma mark- tableView delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"gamelistSegue" sender:indexPath];
}


#pragma mark- segue method

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = (NSIndexPath*)sender;
    
    GameListViewController *DVC = (GameListViewController*)segue.destinationViewController;
    DVC.topicID = [self.topic intValue];
    DVC.sub = indexPath.row + 1;
    
}


@end
