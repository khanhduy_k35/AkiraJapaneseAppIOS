//
//  RateAppView.m
//  Pera
//
//  Created by Le Trong Thao on 12/6/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import "RateAppView.h"
#import "JsonFileHandle.h"
#import "GlobalVariable.h"

@implementation RateAppView



-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    
    float sizeOfContainView = 280;//*[UIScreen mainScreen].bounds.size.width/320.0;
    UIView *containView = [UIView new];
    containView.backgroundColor = [UIColor whiteColor];
    containView.userInteractionEnabled = true;
    containView.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width - sizeOfContainView)/2,([UIScreen mainScreen].bounds.size.height- sizeOfContainView)/2, sizeOfContainView, sizeOfContainView);
    [self addSubview:containView];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, containView.frame.size.width, containView.frame.size.height/3)];
    headerView.backgroundColor = [UIColor colorWithRed:138.0/255.0 green:200.0/255.0 blue:85.0/255.0 alpha:1];
    [containView addSubview:headerView];
    
    
    UIImageView *lionImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lion_rate_screen.png"]];
    lionImgView.frame = CGRectMake(headerView.frame.size.width-headerView.frame.size.height, 0, headerView.frame.size.height-25, headerView.frame.size.height);
    [headerView addSubview:lionImgView];
    
    UILabel *headerLabel = [UILabel new];
    headerLabel.frame = CGRectMake(30, 0, headerView.frame.size.width - 30-lionImgView.frame.size.width-20, headerView.frame.size.height);
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.font = [UIFont boldSystemFontOfSize:18];
    headerLabel.textAlignment = NSTextAlignmentLeft;
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.numberOfLines = 0;
    headerLabel.text = [JsonFileHandle getLocalTitleWithType:@"like_pera" andLanguageKey:g_language];
    [headerView addSubview:headerLabel];
    
    
    UILabel *bodyLabel = [UILabel new];
    bodyLabel.frame = CGRectMake(10, headerView.frame.origin.y+headerView.frame.size.height+10, containView.frame.size.width - 20, 40);
    bodyLabel.backgroundColor = [UIColor clearColor];
    bodyLabel.font = [UIFont systemFontOfSize:15];
    bodyLabel.textAlignment = NSTextAlignmentLeft;
    bodyLabel.textColor = [UIColor blackColor];
    bodyLabel.numberOfLines = 0;
    bodyLabel.text = [JsonFileHandle getLocalTitleWithType:@"introduce" andLanguageKey:g_language];
    [containView addSubview:bodyLabel];
    
    
    float heightOfBt = 35;
    UIButton *rateBt = [UIButton new];
    rateBt.frame = CGRectMake(containView.frame.size.width-250 - 20, containView.frame.size.height-3*heightOfBt, 250, heightOfBt);
    [rateBt addTarget:self action:@selector(rateAppButton) forControlEvents:UIControlEventTouchUpInside];
    rateBt.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [rateBt setTitle:[JsonFileHandle getLocalTitleWithType:@"rateapp" andLanguageKey:g_language] forState:UIControlStateNormal];
    [rateBt setTitleColor:[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1] forState:UIControlStateNormal];
    [containView addSubview:rateBt];
    
    
    UIButton *feedBackBt = [UIButton new];
    feedBackBt.frame = CGRectMake(containView.frame.size.width-250 - 20, containView.frame.size.height-2*heightOfBt, 250, heightOfBt);
    [feedBackBt addTarget:self action:@selector(feedbackButton) forControlEvents:UIControlEventTouchUpInside];
    feedBackBt.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [feedBackBt setTitle:[JsonFileHandle getLocalTitleWithType:@"send_feed" andLanguageKey:g_language] forState:UIControlStateNormal];
    [feedBackBt setTitleColor:[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1] forState:UIControlStateNormal];
    [containView addSubview:feedBackBt];
    
    
    
    UIButton *ignoreBt = [UIButton new];
    ignoreBt.frame = CGRectMake(containView.frame.size.width-250 - 20, containView.frame.size.height-1*heightOfBt, 250, heightOfBt);
    [ignoreBt addTarget:self action:@selector(ignoreButton) forControlEvents:UIControlEventTouchUpInside];
    ignoreBt.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [ignoreBt setTitle:[JsonFileHandle getLocalTitleWithType:@"dismiss" andLanguageKey:g_language] forState:UIControlStateNormal];
    [ignoreBt setTitleColor:[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1] forState:UIControlStateNormal];
    [containView addSubview:ignoreBt];

    
    
    
    return self;
}



-(void)rateAppButton
{
    if([self.delegate respondsToSelector:@selector(rateAppTouched:)])
    {
        [self.delegate rateAppTouched:self];
    }
}

-(void)feedbackButton
{
    if([self.delegate respondsToSelector:@selector(sendFeedbackTouched:)])
    {
        [self.delegate sendFeedbackTouched:self];
    }
}

-(void)ignoreButton
{
    if([self.delegate respondsToSelector:@selector(ignoreTouched:)])
    {
        [self.delegate ignoreTouched:self];
    }
}

@end
