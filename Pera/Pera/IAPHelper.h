//
//  IAPHelper.h
//  InAppRage
//
//  Created by Ray Wenderlich on 2/28/11.
//  Copyright 2011 Ray Wenderlich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StoreKit/StoreKit.h"

#define kProductsLoadedNotification         @"ProductsLoaded"
#define kProductsFailLoadedNotification     @"ProductsFailLoaded"
#define kProductPurchasedNotification       @"ProductPurchased"
#define kProductPurchaseFailedNotification  @"ProductPurchaseFailed"
#define kProductsRestoredNotification       @"ProductsRestored"
#define kProductsRestoredFailedNotification @"ProductsRestoredFailed"

@interface IAPHelper : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver> {
    NSSet * _productIdentifiers;    
    NSArray * _products;
    NSMutableSet * _purchasedProducts;
    SKProductsRequest * _request;
}

@property (retain) NSSet *productIdentifiers;
@property (retain) NSMutableDictionary *productPrices;
@property (retain) NSDictionary *productCoins;
@property (retain) NSArray * products;
@property (retain) NSMutableSet *purchasedProducts;
@property (retain) SKProductsRequest *request;

- (void)requestProducts;
- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers prices:(NSMutableDictionary*)pD coins:(NSDictionary*)cD;
- (void)buyProductIdentifier:(NSString *)productIdentifier;
- (BOOL)isProductPurchased:(NSString *)productIdentifier;
- (void)restoreCompletedTransactions;
- (NSString*)getPriceStringForProductID:(NSString*)pid;
- (int)getCoinsForProductID:(NSString*)pid;

- (void)setInAppsPurchased:(BOOL)on;
- (BOOL)areInAppsPurchased;

@end

