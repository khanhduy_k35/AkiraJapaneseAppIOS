//
//  CircularProgressView.m
//  CircularProgressView
//
//  Created by nijino saki on 13-3-2.
//  Copyright (c) 2013年 nijino. All rights reserved.
//  QQ:20118368
//  http://www.nijino.cn

#import "CircularProgressView.h"
#import <QuartzCore/QuartzCore.h>

@interface CircularProgressView ()<AVAudioPlayerDelegate>

@property (nonatomic) CADisplayLink *displayLink;
@property (nonatomic) AVAudioPlayer *player;//an AVAudioPlayer instance
@property (nonatomic) CAShapeLayer *progressLayer;
@property (nonatomic) float progress;
@property (nonatomic) CGFloat angle;//angle between two lines
@property (nonatomic) NSString *imgPlay;
@property (nonatomic) NSString  *imgPause;
@property (nonatomic) BOOL isPlaying;

@end

@implementation CircularProgressView

- (id)initWithFrame:(CGRect)frame
          backColor:(UIColor *)backColor
      progressColor:(UIColor *)progressColor
          lineWidth:(CGFloat)lineWidth
           audioURL:(NSURL *)audioURL
            imgPlay:(NSString *)imgplay imgStop:(NSString *)imgStop{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
        _backColor = backColor;
        _progressColor = progressColor;
        self.lineWidth = lineWidth;
        _player = [[AVAudioPlayer alloc] initWithContentsOfURL:audioURL error:nil];
        _player.numberOfLoops = 0;
        _audioURL = audioURL;
        _player.delegate = self;
        _btnPlay  = [UIButton buttonWithType:UIButtonTypeCustom];
        float distance= lineWidth/ 4;
        _imgPlay = [imgplay copy];
        _imgPause = [imgStop copy];
        _btnPlay.frame = CGRectMake(distance, distance, self.frame.size.width - 2*distance, self.frame.size.height - 2*distance);
        [_btnPlay setImage:[UIImage imageNamed:imgplay] forState:UIControlStateNormal];
        [_btnPlay addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        _isPlaying = false;
        [self addSubview:_btnPlay];
        [_player prepareToPlay];
    }
    return self;
}

-(void) buttonClicked:(UIButton*)sender
{
    if (!_isPlaying) {
        [self play];
        [_btnPlay setImage:[UIImage imageNamed:_imgPause] forState:UIControlStateNormal];
        _isPlaying = YES;
        _playOrPauseButtonIsPlaying = YES;
    }else{
        [self pause];
        [_btnPlay setImage:[UIImage imageNamed:_imgPlay] forState:UIControlStateNormal];
        _isPlaying = NO;
        _playOrPauseButtonIsPlaying = NO;
    }
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp{
    self.backgroundColor = [UIColor clearColor];
}

- (void)setLineWidth:(CGFloat)lineWidth{
    CAShapeLayer *backgroundLayer = [self createRingLayerWithCenter:CGPointMake(CGRectGetWidth(self.frame) / 2, CGRectGetHeight(self.frame) / 2) radius:CGRectGetWidth(self.bounds) / 2 - lineWidth / 2 lineWidth:lineWidth color:self.backColor];
    _lineWidth = lineWidth;
    [self.layer addSublayer:backgroundLayer];
    _progressLayer = [self createRingLayerWithCenter:CGPointMake(CGRectGetWidth(self.frame) / 2, CGRectGetHeight(self.frame) / 2) radius:CGRectGetWidth(self.bounds) / 2 - lineWidth / 2 lineWidth:lineWidth color:self.progressColor];
    _progressLayer.strokeEnd = 0;
    [self.layer addSublayer:_progressLayer];
}

- (void)setAudioURL:(NSURL *)audioURL{
    if (audioURL) {
        self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:audioURL error:nil];
        self.player.delegate = self;
        self.duration = self.player.duration;
        [self.player prepareToPlay];
    }
    _audioURL = audioURL;
}

- (CAShapeLayer *)createRingLayerWithCenter:(CGPoint)center radius:(CGFloat)radius lineWidth:(CGFloat)lineWidth color:(UIColor *)color {
    UIBezierPath *smoothedPath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(radius, radius) radius:radius startAngle:- M_PI_2 endAngle:(M_PI + M_PI_2) clockwise:YES];
    CAShapeLayer *slice = [CAShapeLayer layer];
    slice.contentsScale = [[UIScreen mainScreen] scale];
    slice.frame = CGRectMake(center.x - radius, center.y - radius, radius * 2, radius * 2);
    slice.fillColor = [UIColor clearColor].CGColor;
    slice.strokeColor = color.CGColor;
    slice.lineWidth = lineWidth;
    slice.lineCap = kCALineJoinBevel;
    slice.lineJoin = kCALineJoinBevel;
    slice.path = smoothedPath.CGPath;
    return slice;
}

- (void)setProgress:(float)progress{
    if (progress == 0) {
        self.progressLayer.hidden = YES;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.progressLayer.strokeEnd = 0;
        });
    }else {
        self.progressLayer.hidden = NO;
        self.progressLayer.strokeEnd = progress;
    }
}

- (void)updateProgressCircle{
    //update progress value
    self.progress = (float) (self.player.currentTime / self.player.duration);
    if (self.delegate && [self.delegate conformsToProtocol:@protocol(CircularProgressViewDelegate)]) {
        [self.delegate updateProgressViewWithPlayer:self.player];
    }
}

- (void)play{
    if (!self.player.playing) {
        if (!self.displayLink) {
            self.displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(updateProgressCircle)];
            self.displayLink.frameInterval = 2;
            [self.displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
        } else {
            self.displayLink.paused = NO;
        }
        [self.player play];
    }
}

- (void)pause{
    if (self.player.playing) {
        self.displayLink.paused = YES;
        [self.player pause];
    }
}

- (void)stop{
    [self.player stop];
    self.progress = 0;
    self.player.currentTime = 0;
    [self.displayLink invalidate];
    self.displayLink = nil;
}

#pragma mark AVAudioPlayerDelegate method
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    if (flag) {
        [self.displayLink invalidate];
        self.displayLink = nil;
        //restore progress value
        self.progress = 0;
        [self.delegate playerDidFinishPlaying];
        [_btnPlay setImage:[UIImage imageNamed:_imgPlay] forState:UIControlStateNormal];
        _isPlaying = NO;
        //_playOrPauseButtonIsPlaying = NO;
    }
}
@end