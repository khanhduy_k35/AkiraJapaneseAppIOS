//
//  LoginViewController.m
//  Pera
//
//  Created by Le Trong Thao on 3/6/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import "LoginViewController.h"
#import "HttpThread.h"
#import "GameUtil.h"
#import "Reachability.h"
#import "Toast.h"
#import "UIViewController+AMSlideMenu.h"
#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface LoginViewController ()<NSURLConnectionDelegate, UITextFieldDelegate, GIDSignInDelegate, GIDSignInUIDelegate, FBSDKLoginButtonDelegate>

@end

@implementation LoginViewController
{
    UITextField *emailTF;
    UITextField *passTF;
    UIImageView *logo;
    BOOL animation;
    float delta;
}

//652383913232-4osqdhuknflg4rqeu6945nl2c6gckekh.apps.googleusercontent.com

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    animation = false;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self initView];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    
    [self disableSlidePanGestureForLeftMenu];
    
}



#pragma mark init view

-(void)initView
{
    
    float scrWidth = [UIScreen mainScreen].bounds.size.width;
    float scrHeight = [UIScreen mainScreen].bounds.size.height;
    
    logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_akira.png"]];
    float logoHeight = 140;
    float logoWidth = 270;
    //logo.frame = [GameUtil transformFitToScree:CGRectMake((320-logoWidth)/2, 120, logoWidth, logoHeight) withScreenWidth:scrWidth withScreenHeight:scrHeight];
    logo.frame = CGRectMake((scrWidth - logoWidth)/2, 100, logoWidth, logoHeight);
    [self.view addSubview:logo];
    
    
    emailTF = [UITextField new];
    emailTF.frame = CGRectMake(20, logo.frame.origin.y+logo.frame.size.height + 20, scrWidth-40, 40);
    emailTF.layer.cornerRadius = 4;
    emailTF.layer.borderWidth = 1;
    emailTF.layer.borderColor = [[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor];
    emailTF.placeholder = @"Email";
    emailTF.delegate = self;
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    emailTF.leftView = paddingView;
    emailTF.leftViewMode = UITextFieldViewModeAlways;
    [self .view addSubview:emailTF];
    
    passTF = [UITextField new];
    passTF.frame = CGRectMake(emailTF.frame.origin.x, emailTF.frame.origin.y+emailTF.frame.size.height+5, emailTF.frame.size.width, emailTF.frame.size.height);
    passTF.layer.cornerRadius = 4;
    passTF.layer.borderWidth = 1;
    passTF.layer.borderColor = [[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor];
    passTF.placeholder = @"Password";
    passTF.delegate = self;
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    passTF.leftView = paddingView1;
    passTF.leftViewMode = UITextFieldViewModeAlways;
    passTF.secureTextEntry = true;
    [self.view addSubview:passTF];
    
    
    UIButton *loginBt = [UIButton new];
    loginBt.frame = CGRectMake(emailTF.frame.origin.x, passTF.frame.origin.y+passTF.frame.size.height+5, emailTF.frame.size.width, emailTF.frame.size.height);
    [loginBt setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:82.0/255.0 green:128.0/255.0 blue:194.0/255.0 alpha:1]] forState:UIControlStateNormal];
    [loginBt setTitle:@"Login" forState:UIControlStateNormal];
    [loginBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    loginBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [loginBt addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginBt];
    
    
//    UIButton *loginfbBt = [UIButton new];
//    loginfbBt.frame = CGRectMake(emailTF.frame.origin.x, loginBt.frame.origin.y+loginBt.frame.size.height+5, emailTF.frame.size.width, emailTF.frame.size.height);
//    [loginfbBt setTitle:@"Log in with Facebook" forState:UIControlStateNormal];
//    [loginfbBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    loginfbBt.titleLabel.font = [UIFont systemFontOfSize:15];
//    loginfbBt.backgroundColor = [UIColor greenColor];
//    [loginfbBt addTarget:self action:@selector(loginFb) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:loginfbBt];
    
    
    FBSDKLoginButton *loginFbButton = [[FBSDKLoginButton alloc] init];
    loginFbButton.frame = CGRectMake(emailTF.frame.origin.x, loginBt.frame.origin.y+loginBt.frame.size.height+5, emailTF.frame.size.width, emailTF.frame.size.height);
    loginFbButton.delegate = self;
    [self.view addSubview:loginFbButton];
    
    
//    UIButton *loginggBt = [UIButton new];
//    loginggBt.frame = CGRectMake(emailTF.frame.origin.x, loginfbBt.frame.origin.y+loginfbBt.frame.size.height+5, emailTF.frame.size.width, emailTF.frame.size.height);
//    [loginggBt setTitle:@"Sign in" forState:UIControlStateNormal];
//    [loginggBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    loginggBt.titleLabel.font = [UIFont systemFontOfSize:15];
//    loginggBt.backgroundColor = [UIColor greenColor];
//    [loginggBt addTarget:self action:@selector(loginGG) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:loginggBt];
    
    
    [[GIDSignIn sharedInstance] signOut];
    [GIDSignIn sharedInstance].clientID = @"652383913232-4osqdhuknflg4rqeu6945nl2c6gckekh.apps.googleusercontent.com";
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;
    GIDSignInButton *signInButton = [GIDSignInButton new];
    signInButton.frame = CGRectMake(emailTF.frame.origin.x-3, loginFbButton.frame.origin.y+loginFbButton.frame.size.height+5, emailTF.frame.size.width+6, emailTF.frame.size.height);
    signInButton.style = 1;
    
    [self.view addSubview:signInButton];
    
    
    float totalHeight = signInButton.frame.origin.y+signInButton.frame.size.height-logo.frame.origin.y;
    float ydelta = logo.frame.origin.y - (scrHeight-totalHeight)/2;
    for(UIView *view in self.view.subviews)
    {
        view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y - ydelta, view.frame.size.width, view.frame.size.height);
    }
    
}

#pragma mark init button

-(void)login
{
    //@"hailinhnguyen93@gmail.com"
    //@"07061993"
    
    if([self currentNetworkStatus])
    {
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        indicator.center = self.view.center;
        [self.view addSubview:indicator];
        [indicator startAnimating];
        self.view.userInteractionEnabled = false;
        
        double delayInSeconds = 0.1;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            if([HttpThread loginWithEmail:emailTF.text andPass:passTF.text])
            {
                UINavigationController *slideNavigtion = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"slideNavigation"];
                
                [indicator stopAnimating];
                self.view.userInteractionEnabled = true;
                
                [self presentViewController:slideNavigtion animated:true completion:nil];
                
                //emailTF.text = @"";
                //passTF.text = @"";
            }
            else
            {
                [indicator stopAnimating];
                self.view.userInteractionEnabled = true;
            }
            
        });
    }
    else
    {
        [Toast showToastOnView:self.view withMessage:@"Kiểm tra lại mạng"];
    }
    
}


-(void)loginGG
{
    if([self currentNetworkStatus])
    {
        self.view.userInteractionEnabled = false;
        
        [[GIDSignIn sharedInstance] signOut];
        
        [GIDSignIn sharedInstance].clientID = @"652383913232-4osqdhuknflg4rqeu6945nl2c6gckekh.apps.googleusercontent.com";
        
        [GIDSignIn sharedInstance].delegate = self;
        [GIDSignIn sharedInstance].uiDelegate = self;
        [[GIDSignIn sharedInstance] signIn];
        
    }
    else
    {
        [Toast showToastOnView:self.view withMessage:@"Kiểm tra lại mạng"];
    }

}


-(void)loginFb
{
    if([self currentNetworkStatus])
    {
        
        self.view.userInteractionEnabled = false;
        
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logOut];
        
        [login logInWithReadPermissions: @[@"public_profile"]
                     fromViewController:self
                                handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
         {
             if (error) {
                 NSLog(@"Process error");
                 self.view.userInteractionEnabled = true;
                 
             } else if (result.isCancelled) {
                 NSLog(@"Cancelled");
                 self.view.userInteractionEnabled = true;
                 
             } else {
                 NSLog(@"Logged in");
                 
                 NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
                 [parameters setValue:@"id,name,email" forKey:@"fields"];
                 
                 [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
                  startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                               id publicResult, NSError *error)
                  {
                      
                      
                      self.view.userInteractionEnabled = true;
                      
                      if([self currentNetworkStatus])
                      {
                          UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                          indicator.center = self.view.center;
                          [self.view addSubview:indicator];
                          [indicator startAnimating];
                          self.view.userInteractionEnabled = false;
                          
                          double delayInSeconds = 0.1;
                          dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                          dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                              
                              if([HttpThread loginFbWith:result andProfile:publicResult])
                              {
                                  UINavigationController *slideNavigtion = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"slideNavigation"];
                                  
                                  [indicator stopAnimating];
                                  self.view.userInteractionEnabled = true;
                                  
                                  [self presentViewController:slideNavigtion animated:true completion:nil];
                                  
                              }
                              else
                              {
                                  [indicator stopAnimating];
                                  self.view.userInteractionEnabled = true;
                              }
                              
                          });
                      }
                      else
                      {
                          [Toast showToastOnView:self.view withMessage:@"Kiểm tra lại mạng"];
                      }
                      
                      
                      NSLog(@"");
                      
                  }];
                 
             }
         }];
    }
    else
    {
        [Toast showToastOnView:self.view withMessage:@"Kiểm tra lại mạng"];
    }

}

#pragma mark textfield delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark keyboard

- (void)keyboardDidShow: (NSNotification *) notif{
    // Do something here
    
    NSDictionary* keyboardInfo = [notif userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    
    if(!animation)
    {
        if(passTF.frame.origin.y+passTF.frame.size.height > self.view.frame.size.height - keyboardFrameBeginRect.size.height)
        {
            delta = (passTF.frame.origin.y+passTF.frame.size.height) - (self.view.frame.size.height - keyboardFrameBeginRect.size.height) - 10;
            
            animation = true;
            
            [UIView animateWithDuration:0.5 animations:^{
                
                for(UIView *view in self.view.subviews)
                {
                    if(![view isKindOfClass:[UIButton class]] && ![view isKindOfClass:[GIDSignInButton class]])
                    {
                        view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y - delta - 15, view.frame.size.width, view.frame.size.height);
                    }
                    
                }
                
            }];
        }

    }
}

- (void)keyboardDidHide: (NSNotification *) notif{
    // Do something here
        
        if(animation)
        {
            animation = false;
            
            [UIView animateWithDuration:0.5 animations:^{
                
                for(UIView *view in self.view.subviews)
                {
                    //self.view.frame.size.height - keyboardFrameBeginRect.size.height - 45
                    if(![view isKindOfClass:[UIButton class]] && ![view isKindOfClass:[GIDSignInButton class]])
                    {
                        view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y + delta + 15, view.frame.size.width, view.frame.size.height);
                    }
                    
                }
                
            }];
            
        }
    
}

#pragma mark gg login delegate

-(void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error
{
    if([self currentNetworkStatus])
    {
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        indicator.center = self.view.center;
        [self.view addSubview:indicator];
        [indicator startAnimating];
        self.view.userInteractionEnabled = false;
        
        double delayInSeconds = 0.1;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            if([HttpThread loginGoogleWithUser:user])
            {
                UINavigationController *slideNavigtion = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"slideNavigation"];
                
                [indicator stopAnimating];
                self.view.userInteractionEnabled = true;
                
                [self presentViewController:slideNavigtion animated:true completion:nil];
                
            }
            else
            {
                [indicator stopAnimating];
                self.view.userInteractionEnabled = true;
            }
            
        });
    }
    else
    {
        [Toast showToastOnView:self.view withMessage:@"Kiểm tra lại mạng"];
    }
    
}


- (void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user withError:(NSError *)error
{
    // Perform any operations when the user disconnects from app here.
    // ...
    
    self.view.userInteractionEnabled = true;
    
    NSLog(@"login gg fail");
}


- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    //[myActivityIndicator stopAnimating];
}


- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController
{
    [self presentViewController:viewController animated:YES completion:nil];
}

- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController
{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark fb login delegate


-(void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error
{
    if (error) {
        NSLog(@"Process error");
        self.view.userInteractionEnabled = true;
        
    } else if (result.isCancelled) {
        NSLog(@"Cancelled");
        self.view.userInteractionEnabled = true;
        
    } else {
        NSLog(@"Logged in");
        
        NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@"id,name,email" forKey:@"fields"];
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                      id publicResult, NSError *error)
         {
             
             
             self.view.userInteractionEnabled = true;
             
             if([self currentNetworkStatus])
             {
                 UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                 indicator.center = self.view.center;
                 [self.view addSubview:indicator];
                 [indicator startAnimating];
                 self.view.userInteractionEnabled = false;
                 
                 double delayInSeconds = 0.1;
                 dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                 dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                     
                     if([HttpThread loginFbWith:result andProfile:publicResult])
                     {
                         UINavigationController *slideNavigtion = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"slideNavigation"];
                         
                         [indicator stopAnimating];
                         self.view.userInteractionEnabled = true;
                         
                         [self presentViewController:slideNavigtion animated:true completion:nil];
                         
                     }
                     else
                     {
                         [indicator stopAnimating];
                         self.view.userInteractionEnabled = true;
                     }
                     
                 });
             }
             else
             {
                 [Toast showToastOnView:self.view withMessage:@"Kiểm tra lại mạng"];
             }
             
             
             NSLog(@"");
             
         }];
        
    }

}

-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton
{
    NSLog(@"logout fb");
}


-(BOOL)currentNetworkStatus
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}


- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
