//
//  SqlManager.h
//  Pera
//
//  Created by Le Trong Thao on 12/4/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface SqlManager : NSObject


+(void)copyFileToDocumentPath;
+(BOOL) checkTopicExistWithTopic:(NSString*)topicID Language:(NSString*)language Course:(NSString*)course;
+(BOOL) checkGameExistWithGame:(NSString*)gameID Language:(NSString*)language Course:(NSString*)course andTopicID:(NSString*)topicID;
+(void) unlockTopicId: (NSString *)lg Course:(NSString *)cs andTopicID:(NSString *)topicID;
+(int)getsTarsOfCourse:(NSString*)course andLanguage:(NSString*)language;
+(int)getStarsOfTopic:(NSString*)topicID Language:(NSString*)language AndCourse:(NSString*)course;
+(int) getStarsOfGame:(NSString*)gameID Language:(NSString*)language Course:(NSString*)course andTopicID:(NSString*)topicID;
+(void)updateTopicTableStarsWithLanguage:(NSString*)lg Course:(NSString*)cs andTopicID:(NSString*)topicID;

+(void)updateTopicWithTopic:(NSString*)topicID Language:(NSString*)language Course:(NSString*)course andStars:(NSString*)stars;
+(void)updateGameWithGame:(NSString*)gameID Language:(NSString*)language Course:(NSString*)course andTopicID:(NSString*)topicID andStars:(NSString*)stars;


+(void)insertGameTableWithGameID:(NSString*)gameID Language:(NSString*)lg Course:(NSString*)cs TopicID:(NSString*)topicID andStars:(NSString*)star;

+(BOOL)checkExistLanguageAndCourse;
+(void)insertSettingTableWith:(NSString*)lang andCourse:(NSString*)cs;
+(void)updateSettingTableWith:(NSString*)lang andCourse:(NSString*)cs;
+(NSString*)getLanguage;
+(NSString*)getCourse;

+(BOOL)checkChoosedTopic;
+(void)updateChoosedTopic;

@end
