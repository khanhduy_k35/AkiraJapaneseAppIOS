//
//  KanaGame4ViewController.m
//  Pera
//
//  Created by Le Trong Thao on 3/5/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import "KanaGame4ViewController.h"
#import "ProgressView.h"
#import "CircularProgressView.h"
#import "UIViewController+AMSlideMenu.h"
#import "GameUtil.h"
#import "GlobalVariable.h"

#import "LessonHandle.h"
#import "DataHandle.h"

#import "CustomAnimation.h"
#import "TGDrawSvgPathView.h"
#import "Toast.h"
#import "HttpThread.h"

@interface KanaGame4ViewController ()<CircularProgressViewDelegate, UIAlertViewDelegate, UITextFieldDelegate, AVAudioPlayerDelegate>

@end

@implementation KanaGame4ViewController
{
    ProgressView *progressView;
    UIView *gameView;
    
    NSMutableArray *lessonArray;
    
    
    int currentIndex;
    
    UIView *wordView;
    
    int drawIndex;
    float drawTime;
    NSMutableArray *svgFileAr;
    NSTimer *drawTimer;
    
    
    int totalTrueAnswer;
    
    
    AVAudioPlayer *readPlayer;
    AVAudioPlayer *resultPlayer;
    
    UITextField *ansTextField;
    CGRect ansStandardFrame;
    float yAnimationOrigin;
    
    UIButton *checkButton;
    CGRect checkBtStandardFrame;
    
    UIButton *nextButton;
    UIView *resView;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self disableSlidePanGestureForLeftMenu];
    
    [self initView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];

    
}

-(void)viewDidAppear:(BOOL)animated
{
    currentViewController = self;
}

-(void)viewWillDisappear:(BOOL)animated
{
    
}

#pragma mark init View

-(void)initView
{
    drawTime = 1.5;
    
    currentIndex = 0;
    totalTrueAnswer = 0;
    
    lessonArray = [DataHandle getAllKanaLessonTopic:self.topicID Type:self.type];
    lessonArray = [LessonHandle rearrangeElementRandomly:lessonArray];
    
    UIButton *backButton = [UIButton new];
    float heightOfBt = 36;
    float widthOfBt = 36;
    backButton.frame = [GameUtil transformFitToScree:CGRectMake(8, 30 , widthOfBt, heightOfBt) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    [backButton setBackgroundImage:[UIImage imageNamed:@"abc_ic_ab_back_mtrl_am_alpha_blue.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backViewController) forControlEvents:UIControlEventTouchUpInside];
    //backButton.backgroundColor = [UIColor blackColor];
    [self.view addSubview:backButton];
    
    
    
    float xProgressView = backButton.frame.origin.x + backButton.frame.size.width + 10;
    float widthProgressView = [UIScreen mainScreen].bounds.size.width - xProgressView*2;
    float heightProgressView = 6;
    
    progressView = [[ProgressView alloc] initWithFrame:CGRectMake(xProgressView, backButton.frame.origin.y+backButton.frame.size.height/2-heightProgressView/2, widthProgressView, heightProgressView) withMax:lessonArray.count];
    [self.view addSubview:progressView];
    
    
    UILabel *topLabel = [UILabel new];
    topLabel.frame = CGRectMake(10, backButton.frame.origin.y+backButton.frame.size.height+5, [UIScreen mainScreen].bounds.size.width-20,35);
    topLabel.font = [UIFont boldSystemFontOfSize:15];
    topLabel.numberOfLines = 0;
    topLabel.textAlignment = NSTextAlignmentCenter;
    topLabel.textColor = [UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1];
    topLabel.backgroundColor = [UIColor clearColor];
    topLabel.text = @"Viết cách đọc của chữ";
    [self.view addSubview:topLabel];
    
    [self initGameView];
}


-(void)initGameView
{
    drawIndex = 0;
    Lesson *ls = lessonArray[currentIndex];
    
    gameView = [UIView new];
    float yGameView = 110;
    gameView.frame = [GameUtil transformFitToScree:CGRectMake(0, yGameView, 320, 568 - yGameView) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    gameView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:gameView];
    
    
    NSArray *allFileName = [ls.fileName componentsSeparatedByString:@","];
    NSString *audioFileName = allFileName[0];
    svgFileAr = [NSMutableArray new];
    
    if(allFileName.count > 1)
    {
        for(int i = 1; i < allFileName.count; i++)
        {
            [svgFileAr addObject:allFileName[i]];
        }
    }
    else
    {
        [svgFileAr addObject:allFileName[0]];
    }
    
    
    NSMutableArray *wordArray = [NSMutableArray new];
    for(int i = 0; i < ls.hiragana.length; i++)
    {
        [wordArray addObject:[ls.hiragana substringWithRange:NSMakeRange(i, 1)]];
    }
    
    float sizeOfWordBt = 35*[UIScreen mainScreen].bounds.size.width/320;
    float distance = 10;
    float xStart = (gameView.frame.size.width - sizeOfWordBt*svgFileAr.count - distance*(svgFileAr.count-1))/2;
    
    for (int i = 0; i < svgFileAr.count; i++)
    {
        UIButton *wordBt = [UIButton new];
        wordBt.frame = CGRectMake(xStart, 0, sizeOfWordBt, sizeOfWordBt);
        wordBt.tag = 20 + i;
        [wordBt setBackgroundImage:[self imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [wordBt setTitle:wordArray[i] forState:UIControlStateNormal];
        [wordBt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        wordBt.layer.borderWidth = 1;
        wordBt.layer.borderColor = [[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor];
        wordBt.titleLabel.font = [UIFont systemFontOfSize:15];
        [wordBt addTarget:self action:@selector(reDrawWord:) forControlEvents:UIControlEventTouchUpInside];
        [gameView addSubview:wordBt];
        
        xStart += distance + sizeOfWordBt;
        
        yAnimationOrigin = wordBt.frame.origin.y + wordBt.frame.size.height + 5;
    }
    
    float sizeOfCircleProgress = 55*[UIScreen mainScreen].bounds.size.width/320;
    if([UIScreen mainScreen].bounds.size.height == 480)
        sizeOfCircleProgress = 40;
    
    float sizeOfImg = 200*[UIScreen mainScreen].bounds.size.width/320;
    if([UIScreen mainScreen].bounds.size.height == 480)
        sizeOfImg = 150;
    float yImgView = sizeOfWordBt + 10 + sizeOfCircleProgress/2;
    wordView = [UIView new];
    wordView.frame = CGRectMake((gameView.frame.size.width - sizeOfImg)/2, yImgView, sizeOfImg, sizeOfImg);
    wordView.backgroundColor = [UIColor whiteColor];
    wordView.layer.borderWidth = 1;
    wordView.layer.borderColor = [[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor];
    [gameView addSubview:wordView];
    
    
    CircularProgressView *circleProgress = [[CircularProgressView alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-sizeOfCircleProgress)/2, wordView.frame.origin.y - sizeOfCircleProgress/2, sizeOfCircleProgress, sizeOfCircleProgress)
                                                                             backColor:[UIColor colorWithRed:236.0 / 255.0 green:236.0 / 255.0 blue:236.0 / 255.0 alpha:1.0] progressColor:[UIColor colorWithRed:82.0 / 255.0 green:135.0 / 255.0 blue:237.0 / 255.0 alpha:1.0]
                                                                             lineWidth:10
                                                                              audioURL:[LessonHandle getKanaAudioUrlOfFileName:audioFileName]
                                                                               imgPlay:@"icon_play.png"
                                                                               imgStop:@"icon_stop.png"];
    circleProgress.delegate = self;
    [circleProgress.btnPlay setImage:[UIImage imageNamed:@"icon_stop.png"] forState:UIControlStateNormal];
    [circleProgress setTag:1];
    [gameView addSubview:circleProgress];
    [circleProgress play];
    
    
    xStart = 0;
    for (int i = 0; i < svgFileAr.count; i++)
    {
        float sizeOfSvgView = [self getSizeOfWord:wordArray[i] onView:wordView withWordArray:wordArray];
        TGDrawSvgPathView *svgView = [[TGDrawSvgPathView alloc] initWithFrame:CGRectMake(xStart, (wordView.frame.size.height - sizeOfSvgView)/2, sizeOfSvgView, sizeOfSvgView)];
        svgView.tag = 20 + i;
        if(i==0)[svgView setFilePathFromSvg:[LessonHandle getSvgFilePathOf:svgFileAr[i] Type:self.type] strokeColor:[UIColor blackColor] duration:drawTime];
        [wordView addSubview:svgView];
        xStart += sizeOfSvgView;
    }
    
    
    [self adjustSubFrameOnView:wordView];
    
    drawIndex++;
    drawTimer = [NSTimer scheduledTimerWithTimeInterval:drawTime target:self selector:@selector(drawWord) userInfo:nil repeats:true];
    
    
    
    ansTextField = [UITextField new];
    ansTextField.frame = CGRectMake(15, wordView.frame.size.height + wordView.frame.origin.y + 20, gameView.frame.size.width - 30,35);
    ansTextField.font = [UIFont systemFontOfSize:18];
    ansTextField.textAlignment = NSTextAlignmentLeft;
    ansTextField.textColor = [UIColor blackColor];
    ansTextField.backgroundColor = [UIColor whiteColor];
    ansTextField.placeholder = @"Viết cách đọc của chữ";
    ansTextField.layer.cornerRadius = 4;
    ansTextField.layer.borderWidth = 1;
    ansTextField.layer.borderColor = wordView.layer.borderColor;
    ansTextField.delegate = self;
    
    ansStandardFrame = ansTextField.frame;
    
    [gameView addSubview:ansTextField];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    ansTextField.leftView = paddingView;
    ansTextField.leftViewMode = UITextFieldViewModeAlways;
    
    float heightOfNextButton = 35;
    
    checkButton = [UIButton new];
    [checkButton setTitle:@"Kiểm tra" forState:UIControlStateNormal];
    [checkButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    checkButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [checkButton setBackgroundColor:[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1]];
    checkButton.frame = CGRectMake((gameView.frame.size.width-120)/2, gameView.frame.size.height - heightOfNextButton - 5, 120, heightOfNextButton);
    [checkButton addTarget:self action:@selector(checkButton:) forControlEvents:UIControlEventTouchUpInside];
    [gameView addSubview:checkButton];
    
    checkBtStandardFrame = checkButton.frame;
    
}


- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(void)initEndingViewWithStars:(int)starNumber
{
    
    UIView *endingView = [UIView new];
    endingView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    endingView.userInteractionEnabled = true;
    endingView.backgroundColor = [UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1];
    [endingView setTag:1];
    [self.view addSubview:endingView];
    
    
    UILabel *infoLabel = [UILabel new];
    infoLabel.frame = CGRectMake(0, 100*[UIScreen mainScreen].bounds.size.height/568.0, [UIScreen mainScreen].bounds.size.width, 40);
    infoLabel.font = [UIFont boldSystemFontOfSize:25];
    infoLabel.textAlignment = NSTextAlignmentCenter;
    infoLabel.textColor = [UIColor whiteColor];
    infoLabel.backgroundColor = [UIColor clearColor];
    if(starNumber == 3)
    {
        infoLabel.text = @"Tuyệt vời";
    }
    else if (starNumber == 2)
    {
        infoLabel.text = @"Tốt";
    }
    else if (starNumber == 1)
    {
        infoLabel.text = @"Cố lên nào";
    }
    else if (starNumber == 0)
    {
        infoLabel.text = @"Cố lên nào";
    }
    
    [endingView addSubview:infoLabel];
    
    
    float sizeOfImg = 40*[UIScreen mainScreen].bounds.size.width/320.0;
    float xStart = ([UIScreen mainScreen].bounds.size.width - (sizeOfImg*3 + 10*2))/2;
    UIImageView *imgViewTemp;
    
    for(int i = 1; i<= 3; i++)
    {
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(xStart, infoLabel.frame.origin.y+infoLabel.frame.size.height+20, sizeOfImg, sizeOfImg)];
        imgView.image = [UIImage imageNamed:@"favorite.png"];
        
        if(i==1&& starNumber==1)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        else if((i==1 ||i ==2) && starNumber == 2)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        else if((i==1 ||i ==2 || i==3) && starNumber == 3)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        
        xStart += sizeOfImg + 10;
        [endingView addSubview:imgView];
        imgViewTemp = imgView;
    }
    
    
    float sizeOfBt = 50*[UIScreen mainScreen].bounds.size.width/320.0;
    
    UIButton *replayButton = [UIButton new];
    float xReplayBt = ([UIScreen mainScreen].bounds.size.width - sizeOfBt*2 - 40)/2;
    replayButton.frame = CGRectMake(xReplayBt, imgViewTemp.frame.origin.y+imgViewTemp.frame.size.height + 30, sizeOfBt, sizeOfBt);
    [replayButton setBackgroundImage:[UIImage imageNamed:@"icon_redo.png"] forState:UIControlStateNormal];
    [replayButton addTarget:self action:@selector(replay) forControlEvents:UIControlEventTouchUpInside];
    [endingView addSubview:replayButton];
    
    
    
    UIButton *nextButton1 = [UIButton new];
    nextButton1.frame = CGRectMake(replayButton.frame.origin.x+sizeOfBt+40, imgViewTemp.frame.origin.y+imgViewTemp.frame.size.height + 30, sizeOfBt, sizeOfBt);
    [nextButton1 setBackgroundImage:[UIImage imageNamed:@"next_icon.png"] forState:UIControlStateNormal];
    [nextButton1 addTarget:self action:@selector(backViewControllerWhenFinish) forControlEvents:UIControlEventTouchUpInside];
    [endingView addSubview:nextButton1];
    
    
    if(starNumber > [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d",self.type,[self.topicID intValue],[self.gameID intValue]]])
    {
        [[NSUserDefaults standardUserDefaults] setInteger:starNumber forKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d",self.type,[self.topicID intValue],[self.gameID intValue]]];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            
            NSString *course = @"";
            if(self.type==1)
            {
                course = @"hiragana";
            }
            else
            {
                course = @"katakana";
            }
            
            [HttpThread upDateToServerGame:[self.gameID intValue] Topic:[self.topicID intValue] Course:course Sub:1 Star:starNumber];
        });
    }
    
    if(starNumber >= 1)
    {
        if([[[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d_islock",self.type,[self.topicID intValue],[self.gameID intValue]+1]] isEqualToString:@"lock"])
        {
            [[NSUserDefaults standardUserDefaults] setValue:@"unlock" forKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d_islock",self.type,[self.topicID intValue],[self.gameID intValue]+1]];
        }
    }
    
}

-(void)showResViewWithRes:(BOOL)res
{
    float heightOfResView = 40;
    float heightOfNextButton = 35;
    
    
    resView = [UIView new];
    resView.frame = CGRectMake(0, gameView.frame.size.height - 5 - heightOfNextButton - 5 -heightOfResView, gameView.frame.size.width, heightOfResView);
    if(!res) resView.backgroundColor = [UIColor colorWithRed:253.0/255.0 green:235.0/255.0 blue:218.0/255.0 alpha:1];
    else resView.backgroundColor = [UIColor colorWithRed:221.0/255.0 green:230.0/255.0 blue:244.0/255.0 alpha:1];
    [gameView addSubview:resView];
    
    UIImageView *check_wrongImgView;
    if(!res) check_wrongImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"check_wrong.png"]];
    else check_wrongImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"check_right.png"]];
    check_wrongImgView.frame = CGRectMake(10, (resView.frame.size.height - 20)/2, 20, 20);
    [check_wrongImgView setAlpha:1];
    [resView addSubview:check_wrongImgView];
    
    UILabel *resLb = [UILabel new];
    resLb.frame = CGRectMake(check_wrongImgView.frame.origin.x+check_wrongImgView.frame.size.width+10, 0, resView.frame.size.width - (check_wrongImgView.frame.origin.x+check_wrongImgView.frame.size.width+10), resView.frame.size.height);
    resLb.font = [UIFont boldSystemFontOfSize:17];
    resLb.textAlignment = NSTextAlignmentLeft;
    resLb.textColor = [UIColor blackColor];
    resLb.backgroundColor = [UIColor clearColor];
    Lesson *ls = lessonArray[currentIndex];
    resLb.text = [NSString stringWithFormat:@"%@: %@",ls.hiragana,ls.vietnamese];
    [resView addSubview:resLb];
    
    nextButton = [UIButton new];
    [nextButton setTitle:@"Tiếp tục" forState:UIControlStateNormal];
    if(currentIndex == lessonArray.count-1)
    {
        [nextButton setTitle:@"Kết thúc" forState:UIControlStateNormal];
    }
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setBackgroundColor:[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1]];
    //nextButton.frame = CGRectMake((gameView.frame.size.width-100)/2, gameView.frame.size.height - heightOfNextButton - 5, 100, heightOfNextButton);
    nextButton.frame = checkButton.frame;
    [nextButton addTarget:self action:@selector(nextLesson) forControlEvents:UIControlEventTouchUpInside];
    [gameView addSubview:nextButton];
    
    resView.frame = CGRectMake(resView.frame.origin.x, nextButton.frame.origin.y - 5 - resView.frame.size.height, resView.frame.size.width, heightOfResView);
    
    NSString *audioFilePath;
    if(!res) audioFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"fail.mp3"];
    else audioFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"true.mp3"];
    NSURL *url = [[NSURL alloc] initFileURLWithPath:audioFilePath];
    resultPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    resultPlayer.numberOfLoops = 0;
    resultPlayer.delegate = self;
    [resultPlayer play];
}

#pragma mark- button

-(void)backViewController
{
    NSString *mes = @"Bạn có chắc chắn muốn thoát game này ?";
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:mes
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    
    [alert show];
}

-(IBAction)replayAudio:(UITapGestureRecognizer*)sender
{
    CircularProgressView *button  = (CircularProgressView*)[gameView viewWithTag:1];
    [button.btnPlay setImage:[UIImage imageNamed:@"icon_stop.png"] forState:UIControlStateNormal];
    [button play];
}

-(IBAction)checkButton:(UIButton*)sender
{
    
    if([ansTextField.text isEqualToString:@""])
    {
        [Toast showToastOnView:self.view withMessage:@"Vui lòng viết cách đọc"];
    }
    else
    {
        Lesson *ls  = lessonArray[currentIndex];
        
        if([[ansTextField.text lowercaseString] isEqualToString:ls.romaji])
        {
            NSLog(@"true");
            [self handleWhenTrueAnswer];
        }
        else
        {
            NSLog(@"false");
            [self handleWhenFalseAnswer];
        }
    }
    
    
}

-(void)nextLesson
{
    if(currentIndex < lessonArray.count)
    {
        
        [gameView removeFromSuperview];
        
        [self initGameView];
    }
    else
    {
        //        double delayInSeconds = 1.0;
        //        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        //        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //
        //        });
        
        [self initEndingViewWithStars:[self getStars]];
    }
}


-(void)backViewControllerWhenFinish
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) replay
{
    for(UIView *view in self.view.subviews)
    {
        [view removeFromSuperview];
    }
    
    [self initView];
    
}

#pragma mark- avaudio delegate

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [player stop];
    
}


#pragma mark- CircularProgressViewDelegate

-(void)playerDidFinishPlaying
{
    NSLog(@"playerDidFinishPlaying");
    CircularProgressView *circleProgress = (CircularProgressView*)[gameView viewWithTag:1];
    
    [circleProgress.btnPlay setImage:[UIImage imageNamed:@"icon_play.png"] forState:UIControlStateNormal];
    
    if(currentIndex <= lessonArray.count-2)
    {
        //        UIButton *nextButton = (UIButton*)[gameView viewWithTag:3];
        //        nextButton.hidden = false;
    }
    else
    {
        //        [UIView animateWithDuration:0.5
        //                         animations:^{
        //                             [progressView setPercentComplete:currentIndex+1];
        //
        //                         }
        //                         completion:^(BOOL finish){
        //                             [self initEndingViewWithStars:3];
        //
        //                         }];
        
        
        NSLog(@"get 3 stars");
    }
}

-(void)updateProgressViewWithPlayer:(AVAudioPlayer *)player
{
    
}


#pragma mark- alert view delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark- private method

#pragma mark timer

-(void)handleWhenFalseAnswer
{
    NSTimer *t;
    t = [NSTimer scheduledTimerWithTimeInterval:0.0
                                         target:self
                                       selector:@selector(timerWhenFalse)
                                       userInfo:nil
                                        repeats:NO];
}

-(void)handleWhenTrueAnswer
{
    NSTimer *t;
    t = [NSTimer scheduledTimerWithTimeInterval:0.0
                                         target:self
                                       selector:@selector(timerWhenTrue)
                                       userInfo:nil
                                        repeats:NO];
}

-(int)getStars
{
    int stars = 0;
    
    if((float)totalTrueAnswer/lessonArray.count >= 0.8)
    {
        stars = 3;
    }
    else if((float)totalTrueAnswer/lessonArray.count >= 0.5)
    {
        stars = 2;
    }
    else if((float)totalTrueAnswer/lessonArray.count >= 0.3)
    {
        stars = 1;
    }
    
    return stars;
    
}

#pragma mark- timer

-(void)timerWhenFalse
{
    //choosedImgView = NULL;
    
    
    [self showResViewWithRes:false];
    
    currentIndex++;
    
    [progressView setPercentComplete:currentIndex];
    
    
}

-(void)timerWhenTrue
{
    //CustomImageView *imgView = (CustomImageView*)[timer userInfo];
    //choosedImgView = NULL;
    
    [self showResViewWithRes:true];
    
    totalTrueAnswer++;
    currentIndex++;
    
    [progressView setPercentComplete:currentIndex];
    
}


#pragma mark draw method

-(float)getSizeOfWord:(NSString*)word onView:(UIView*)containView withWordArray:(NSArray*)wordAr
{
    float size = 0;
    float wordRatio = 0;
    float totalRatio = 0;
    
    NSArray *smallWord = @[@"ょ",@"ゅ",@"ゃ",@"っ"];
    
    for(NSString *w in wordAr)
    {
        if([smallWord containsObject:w])
            totalRatio += 3;
        else
            totalRatio += 5;
    }
    
    if([smallWord containsObject:word])
        wordRatio = 3;
    else
        wordRatio = 5;
    
    
    size = wordRatio*containView.frame.size.width/totalRatio;
    
    return size;
}


-(void)adjustSubFrameOnView:(UIView*)containView
{
    if(containView.subviews.count > 1)
    {
        float max = 0;
        
        for(TGDrawSvgPathView *svgView in containView.subviews)
        {
            if(svgView.frame.origin.y+svgView.frame.size.height > max)
            {
                max = svgView.frame.origin.y+svgView.frame.size.height;
            }
            
        }
        
        for(TGDrawSvgPathView *svgView in containView.subviews)
        {
            svgView.frame = CGRectMake(svgView.frame.origin.x, max - svgView.frame.size.height, svgView.frame.size.width, svgView.frame.size.height);
            
        }
    }
}

-(void)drawWord
{
    if(drawIndex == svgFileAr.count)
    {
        
        [drawTimer invalidate];
    }
    else
    {
        TGDrawSvgPathView *svgView = (TGDrawSvgPathView*)[wordView viewWithTag:20+drawIndex];
        [svgView setFilePathFromSvg:[LessonHandle getSvgFilePathOf:svgFileAr[drawIndex] Type:self.type] strokeColor:[UIColor blackColor] duration:drawTime];
        drawIndex++;
        
    }
}

-(IBAction)reDrawWord:(UIButton*)sender
{
    if(drawIndex == svgFileAr.count)
    {
        TGDrawSvgPathView *oldSvgView = (TGDrawSvgPathView*)[wordView viewWithTag:sender.tag];
        
        TGDrawSvgPathView *newSvgView = [TGDrawSvgPathView new];
        newSvgView.frame = oldSvgView.frame;
        newSvgView.tag = oldSvgView.tag;
        [wordView addSubview:newSvgView];
        [oldSvgView removeFromSuperview];
        [newSvgView setFilePathFromSvg:[LessonHandle getSvgFilePathOf:svgFileAr[newSvgView.tag - 20] Type:self.type] strokeColor:[UIColor blackColor] duration:drawTime];
    }
}


#pragma mark text field delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}


#pragma mark keyboard

- (void)keyboardDidShow: (NSNotification *) notif{
    // Do something here
    
    NSDictionary* keyboardInfo = [notif userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    
    [UIView animateWithDuration:0.5 animations:^{
        
        checkButton.frame = CGRectMake(checkButton.frame.origin.x, gameView.frame.size.height - keyboardFrameBeginRect.size.height - 45, checkButton.frame.size.width, checkButton.frame.size.height);
        
        ansTextField.frame = CGRectMake(ansTextField.frame.origin.x, yAnimationOrigin, ansTextField.frame.size.width, ansTextField.frame.size.height);
        
        [wordView setAlpha:0];
        CircularProgressView *circleProgress = (CircularProgressView*)[gameView viewWithTag:1];
        [circleProgress setAlpha:0];
        
    }];
    
    
}

- (void)keyboardDidHide: (NSNotification *) notif{
    // Do something here
    
    [UIView animateWithDuration:0.5 animations:^{
        
        ansTextField.frame = ansStandardFrame;
        checkButton.frame = checkBtStandardFrame;
        if(nextButton) nextButton.frame = checkBtStandardFrame;
        
        resView.frame = CGRectMake(resView.frame.origin.x, nextButton.frame.origin.y - 5 - resView.frame.size.height, resView.frame.size.width, resView.frame.size.height);
        
        [wordView setAlpha:1];
        CircularProgressView *circleProgress = (CircularProgressView*)[gameView viewWithTag:1];
        [circleProgress setAlpha:1];
        
    }];
    
}



@end
