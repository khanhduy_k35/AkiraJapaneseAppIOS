//
//  LessonHandle.m
//  Pera
//
//  Created by Le Trong Thao on 12/5/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import "LessonHandle.h"
#import "Lesson.h"
#import "QuestionSet.h"
#import "GlobalVariable.h"

@implementation LessonHandle

+(NSMutableArray*)rearrangeElementRandomly:(NSMutableArray *)array
{
    NSMutableArray *result = [NSMutableArray new];
    
    int count = array.count;
    for(int i = 0; i < count ; i++)
    {
        int random = arc4random_uniform(array.count);
        
        id ls  = array[random];
        
        [result addObject:ls];
        [array removeObject:ls];
        
    }
    
    
    return result;
}

+(NSMutableArray*)getQuestionSetsFrom:(NSMutableArray *)lessonArray withAmount:(int)amount
{
    NSMutableArray *result = [NSMutableArray new];
    
    lessonArray = [LessonHandle rearrangeElementRandomly:lessonArray];
    
    for(Lesson *ls in lessonArray)
    {
        NSMutableArray *tempArray  = [[NSMutableArray alloc] initWithArray:lessonArray];
        [tempArray removeObject:ls];
        
        QuestionSet *set = [QuestionSet new];
        set.falseAnswerArray  = [NSMutableArray new];
        set.answer = ls;
        
        for(int i=1; i <= amount-1; i++)
        {
            int random = arc4random_uniform(tempArray.count);
            Lesson *ls1 = tempArray[random];
            [tempArray removeObject:ls1];
            [set.falseAnswerArray addObject:ls1];
        }
        
        //[set setSequenceNummberForLesson];
        
        [result addObject:set];
        
    }
    
    
    return result;
}


+(NSMutableArray*)getRandomlyLessonSets:(int)amount fromArray:(NSMutableArray *)lessonAr
{
    NSMutableArray *res = [NSMutableArray new];
    
    
    for(int i = 1; i <= amount; i++)
    {
        int random = arc4random_uniform(lessonAr.count);
        
        [res addObject:lessonAr[random]];
        
        [lessonAr removeObjectAtIndex:random];
    }
    
    
    return res;
}

+(NSString*)getImgFilePathOfFileName:(NSString *)fileName andTopic:(int)topicID Level:(NSString *)level
{
    NSString *filePath;
    
    NSArray *paths  = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    filePath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/courses/%@/%d/vocab/image/%@.png",level,topicID,fileName]];
    
    
    return filePath;
}

//+(NSString*)getKanaImgFilePathOfFileName:(NSString*)

+(NSURL*)getAudioUrlOfFileName:(NSString *)fileName TopicID:(int)topicID Level:(NSString *)level
{
    NSURL *audioUrl;
    
    NSArray *paths  = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *filePath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/courses/%@/%d/vocab/audio/%@.mp3",level,topicID,fileName]];

    audioUrl = [[NSURL alloc] initFileURLWithPath:filePath];
    
    return audioUrl;
}

+(BOOL)checkAudioUrlOfFileName:(NSString *)fileName TopicID:(int)topicID Level:(NSString *)level
{
    NSArray *paths  = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *filePath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/courses/%@/%d/vocab/audio/%@.mp3",level,topicID,fileName]];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        return true;
    }
    else
    {
        return false;
    }
}

#pragma mark Kana Lesson

+(NSString*)getSvgFilePathOf:(NSString *)fileName Type:(int)type
{
    NSString *filePath;
    
    fileName = [fileName stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *folder;
    if(type==1)
        folder = @"hiragana";
    else
    {
        folder = @"katakana";
        fileName = [fileName uppercaseString];
    }
    
    NSArray *paths  = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    filePath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/courses/kana/svg/%@/%@.svg",folder,fileName]];
    
    
    return filePath;
}

+(NSURL*)getKanaAudioUrlOfFileName:(NSString *)fileName
{
    NSURL *audioUrl;
    
    NSArray *paths  = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *filePath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/courses/kana/audio/%@.mp3",fileName]];
    
    audioUrl = [[NSURL alloc] initFileURLWithPath:filePath];
    
    return audioUrl;
}

@end
