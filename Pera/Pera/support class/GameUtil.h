//
//  GameUtil.h
//  TestGame
//
//  Created by DuyLe on 7/16/15.
//  Copyright (c) 2015 DuyLe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GameUtil : NSObject
+(CGRect) transformFitToScree: (CGRect)original withScreenWidth:(int)screenWidth withScreenHeight: (int)screenHeight;
+(CGRect) transformfitToHintView: (CGRect)original withHintViewWidth:(float)viewWidth andViewHeight:(float)viewHeight;
+(CGRect)transformfitToGetHintBt:(CGRect)original withHintViewWidth:(float)viewWidth andViewHeight:(float)viewHeight;
+(CGRect)transformFitToScreen:(float)screenWidth height:(float)screenHeight WithOriginFrame:(CGRect)frame originScreenWidth:(float)width andOriginScreenHeight:(float)height;

@end
