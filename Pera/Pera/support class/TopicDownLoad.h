//
//  TopicDownLoad.h
//  Pera
//
//  Created by Le Trong Thao on 12/7/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "TCBlobDownload.h"
#import "MBProgressHUD.h"

@interface TopicDownLoad : NSObject<TCBlobDownloaderDelegate,MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}
@property (nonatomic , strong) TCBlobDownloadManager *sharedDownloadManager;
@property TCBlobDownloader *downloader;


-(void)downloadDataForTopic:(NSString*)topic Level:(NSString*)level onView:(UIView*)view completion:(void(^)(BOOL finished))complete;


@end
