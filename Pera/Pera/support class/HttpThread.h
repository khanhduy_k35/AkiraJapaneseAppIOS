//
//  HttpThread.h
//  Pera
//
//  Created by Le Trong Thao on 3/8/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface HttpThread : NSObject


+(BOOL)loginWithEmail:(NSString*)email andPass:(NSString*)pass;

+(BOOL)loginGoogleWithUser:(GIDGoogleUser*)user;
+(BOOL)loginFbWith:(FBSDKLoginManagerLoginResult*)token andProfile:(id)profile;

//+(void)loginFacebook;

+(void)upDateToServerGame:(int)game Topic:(int)topicID Course:(NSString*)course Sub:(int)sub Star:(int)star;
+(BOOL)getScore;
+(void)syncScore;

+(BOOL)logoutAccount;

@end
