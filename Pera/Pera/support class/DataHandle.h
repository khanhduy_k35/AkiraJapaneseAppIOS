//
//  DataHandle.h
//  Pera
//
//  Created by Le Trong Thao on 2/27/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Game.h"

@interface DataHandle : NSObject

+(void)initData;

+(int)getStarsOfGame:(int)game Sub:(int)sub Topic:(int)topic Level:(NSString*)course Type:(int)type;
+(int)getStarsOfSub:(int)sub Topic:(int)topic Level:(NSString*)level Type:(in)type;
+(int)getStarsOfTopic:(int)topic Level:(NSString*)level Type:(int)type;
+(int)getStarsOfType:(int)type Level:(NSString*)level;
+(int)getSumStarsOfType:(int)type Level:(NSString*)level;
+(int)getSumStarOfTopic:(int)topic Type:(int)type;


+(NSMutableArray*)getAllLessonOfTopic:(int)topic Level:(NSString*)level Sub:(int)sub;
+(NSMutableArray*)getAllGameOfKanaTopic:(int)topicId Type:(int)type;
+(NSMutableArray*)getAllLessonTestOfKanaTopic:(int)topicId Type:(int)type;
+(NSMutableArray*)getAllKanaLessonTopic:(NSString*)topic Type:(int)type;

+(NSString*)getAudioPathOfKanaGame:(NSString*)fileName Type:(int)type;
+(NSString*)getImgPathOfKanaGame:(NSString*)fileName Type:(int)type;

+(void)checkDayRemain;

+(void)reloadData;
+(void)clearData;


@end
