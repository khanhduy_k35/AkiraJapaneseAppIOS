//
//  RightTableViewController.m
//  Pera
//
//  Created by Le Trong Thao on 1/27/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import "RightTableViewController.h"
#import "JsonFileHandle.h"
#import "GlobalVariable.h"
#import "Tools.h"
#import "InAppWordsIAPHelper.h"
#import "AMSlideMenuMainViewController.h"
#import "MBProgressHUD.h"
#import "SqlManager.h"
@interface RightTableViewController ()<AMSlideMenuDelegate>

@end

@implementation RightTableViewController
{
    CGFloat heightOfCell;
    MBProgressHUD *HUD;
    NSString *keyBuyCource;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    heightOfCell = 140;
    
    self.tableView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:kProductPurchasedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchaseFailed:) name:kProductPurchaseFailedNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(restorePurchased:) name:kProductsRestoredNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(restorePurchaseFailed:) name:kProductsRestoredFailedNotification object: nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
}
#pragma mark table view datasource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1 || indexPath.row == 2) {
        return heightOfCell + 40;
    }
    else
        return heightOfCell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(indexPath.row == 0)
    {
        cell.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:200.0/255.0 blue:67.0/255.0 alpha:1];
        
        float heightOfImg = heightOfCell - 30;
        float widthOfImg = 90;
        
        UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lion_rate_screen.png"]];
        img.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - ([UIScreen mainScreen].bounds.size.width - leftMenuWidth) - widthOfImg - 5, 20 , widthOfImg, heightOfImg);
        [cell addSubview:img];
        
        
        UILabel *lb = [UILabel new];
        lb.frame = CGRectMake(30, 40, 150, 70);
        lb.textAlignment = NSTextAlignmentLeft;
        lb.font = [UIFont boldSystemFontOfSize:16];
        lb.textColor = [UIColor whiteColor];
        lb.backgroundColor = [UIColor clearColor];
        lb.text = [JsonFileHandle getLocalTitleWithType:@"title_premium" andLanguageKey:g_language];
        lb.numberOfLines = 0;
        [cell addSubview:lb];
        
    }
    else if (indexPath.row == 1 || indexPath.row == 2)
    {
        cell.backgroundColor = [UIColor clearColor];
        
        UIView *containView = [UIView new];
        containView.frame = CGRectMake(10, 20, [UIScreen mainScreen].bounds.size.width - ([UIScreen mainScreen].bounds.size.width - leftMenuWidth) - 20, heightOfCell+20);
        containView.userInteractionEnabled = true;
        containView.backgroundColor = [UIColor whiteColor];
        [cell addSubview:containView];
        
        UILabel *priceLb = [UILabel new];
        priceLb.frame = CGRectMake(0, 0, containView.frame.size.width, 40);
        priceLb.textColor = [UIColor whiteColor];
        priceLb.font = [UIFont boldSystemFontOfSize:17];
        priceLb.backgroundColor = [UIColor colorWithRed:131.0/255.0 green:190.0/255.0 blue:85.0/255.0 alpha:1];
        priceLb.textAlignment = NSTextAlignmentCenter;
        priceLb.text = @"4.99$";
        if(indexPath.row == 2)
        {
            priceLb.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:200.0/255.0 blue:67.0/255.0 alpha:1];
            priceLb.text = @"9.99$";
        }
        [containView addSubview:priceLb];
        
        
        UILabel *bodyLb = [UILabel new];
        bodyLb.frame = CGRectMake(10, priceLb.frame.size.height, containView.frame.size.width - 20, 80);
        bodyLb.textColor = [UIColor blackColor];
        bodyLb.font = [UIFont systemFontOfSize:17];
        bodyLb.backgroundColor = [UIColor clearColor];
        bodyLb.textAlignment = NSTextAlignmentCenter;
        bodyLb.text = [JsonFileHandle getLocalTitleWithType:[NSString stringWithFormat:@"title_premium_%@",g_course] andLanguageKey:g_language];
        if (indexPath.row == 2) {
            bodyLb.text = [JsonFileHandle getLocalTitleWithType:@"title_premium_2" andLanguageKey:g_language];
        }
        bodyLb.numberOfLines = 0;
        [containView addSubview:bodyLb];
        
        
        float heightOfBt = containView.frame.size.height  - (bodyLb.frame.origin.y + bodyLb.frame.size.height + 10);
        float widthOfBt = 80;
        UIButton *buyBt = [UIButton new];
        buyBt.frame = CGRectMake((containView.frame.size.width-widthOfBt*2 - 10)/2, bodyLb.frame.origin.y+bodyLb.frame.size.height+5, widthOfBt, heightOfBt);
        [buyBt setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:131.0/255.0 green:190.0/255.0 blue:85.0/255.0 alpha:1]] forState:UIControlStateNormal];
        //[UIColor colorWithRed:131.0/255.0 green:190.0/255.0 blue:85.0/255.0 alpha:1] forState:UIControlStateNormal];
        //buyBt.backgroundColor = [UIColor colorWithRed:131.0/255.0 green:190.0/255.0 blue:85.0/255.0 alpha:1];
        if(indexPath.row == 2)
        {
            [buyBt setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:255.0/255.0 green:200.0/255.0 blue:67.0/255.0 alpha:1]] forState:UIControlStateNormal];
            //buyBt.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:200.0/255.0 blue:67.0/255.0 alpha:1];
        }
        [buyBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        buyBt.titleLabel.font = [UIFont systemFontOfSize:15];
        [buyBt setTitle:[JsonFileHandle getLocalTitleWithType:@"title_upgrade" andLanguageKey:g_language] forState:UIControlStateNormal];
        if (indexPath.row == 1)
        {
            [buyBt addTarget:self action:@selector(buyButton9:) forControlEvents:UIControlEventTouchUpInside];
        }
        else if (indexPath.row == 2) {
            [buyBt addTarget:self action:@selector(buyButton19:) forControlEvents:UIControlEventTouchUpInside];
        }
        buyBt.layer.cornerRadius = 4;
        [containView addSubview:buyBt];
        
        UIButton *restoreBt = [UIButton new];
        restoreBt.frame = CGRectMake(buyBt.frame.origin.x + 10 + widthOfBt, bodyLb.frame.origin.y+bodyLb.frame.size.height+5, widthOfBt, heightOfBt);
        [restoreBt setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:131.0/255.0 green:190.0/255.0 blue:85.0/255.0 alpha:1]] forState:UIControlStateNormal];
        //[UIColor colorWithRed:131.0/255.0 green:190.0/255.0 blue:85.0/255.0 alpha:1] forState:UIControlStateNormal];
        //buyBt.backgroundColor = [UIColor colorWithRed:131.0/255.0 green:190.0/255.0 blue:85.0/255.0 alpha:1];
        if(indexPath.row == 2)
        {
            [restoreBt setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:255.0/255.0 green:200.0/255.0 blue:67.0/255.0 alpha:1]] forState:UIControlStateNormal];
            //buyBt.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:200.0/255.0 blue:67.0/255.0 alpha:1];
        }
        [restoreBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        restoreBt.titleLabel.font = [UIFont systemFontOfSize:15];
        [restoreBt setTitle:@"Restore" forState:UIControlStateNormal];
        if (indexPath.row == 1)
        {
            [restoreBt addTarget:self action:@selector(restore:) forControlEvents:UIControlEventTouchUpInside];
        }
        else if (indexPath.row == 2) {
            [restoreBt addTarget:self action:@selector(restore:) forControlEvents:UIControlEventTouchUpInside];
        }
        restoreBt.layer.cornerRadius = 4;
        [containView addSubview:restoreBt];
        
    }
    
    return cell;
}

-(UIImage *) imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%d",indexPath.row);
    
    //[[self mainVC] closeRightMenuAnimated:true];
}

- (IBAction)restore:(id)sender {
    [[InAppWordsIAPHelper sharedHelper] restoreCompletedTransactions];
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    
    HUD.delegate = self;
    HUD.labelText = @"Loading";
    HUD.dimBackground = YES;
    [HUD show:YES];
    [self performSelector:@selector(timeout:) withObject:nil afterDelay:90];
}


-(IBAction)buyButton9:(id)sender
{
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    keyBuyCource = [NSString stringWithFormat:@"course_%@",g_course];
    [[InAppWordsIAPHelper sharedHelper] buyProductIdentifier:keyBuyCource];
    HUD.delegate = self;
    HUD.labelText = @"Loading";
    HUD.dimBackground = YES;
    [HUD show:YES];
    [self performSelector:@selector(timeout:) withObject:nil afterDelay:90];
    NSLog(@"4.99$");
}

-(IBAction)buyButton19:(id)sender
{
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    keyBuyCource = @"cource_all";
    [[InAppWordsIAPHelper sharedHelper] buyProductIdentifier:keyBuyCource];
    HUD.delegate = self;
    HUD.labelText = @"Loading";
    HUD.dimBackground = YES;
    [HUD show:YES];
    [self performSelector:@selector(timeout:) withObject:nil afterDelay:90];
    NSLog(@"19.99$");
}

- (void)timeout:(id)arg
{
    if (HUD!=nil) {
        [HUD hide:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)productPurchased:(NSNotification *)notification
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
   // NSString* msg = @"You successfully purchased PRO-version! Please restart the application to take effect ";
    [Tools setDisAd:YES];
    if ([keyBuyCource isEqualToString:@"cource_all"]) {
        for(int i= 1 ; i< 45 ; i++ ){
            [SqlManager unlockTopicId:@"en" Course:@"vi" andTopicID:[NSString stringWithFormat:@"%d",i]];
            [SqlManager unlockTopicId:@"en" Course:@"jp" andTopicID:[NSString stringWithFormat:@"%d",i]];
            [SqlManager unlockTopicId:@"en" Course:@"kr" andTopicID:[NSString stringWithFormat:@"%d",i]];
            [SqlManager unlockTopicId:@"en" Course:@"zh" andTopicID:[NSString stringWithFormat:@"%d",i]];
            
            [SqlManager unlockTopicId:@"vi" Course:@"zh" andTopicID:[NSString stringWithFormat:@"%d",i]];
            [SqlManager unlockTopicId:@"vi" Course:@"en" andTopicID:[NSString stringWithFormat:@"%d",i]];
            [SqlManager unlockTopicId:@"vi" Course:@"kr" andTopicID:[NSString stringWithFormat:@"%d",i]];
            [SqlManager unlockTopicId:@"vi" Course:@"jp" andTopicID:[NSString stringWithFormat:@"%d",i]];
            
            [SqlManager unlockTopicId:@"jp" Course:@"en" andTopicID:[NSString stringWithFormat:@"%d",i]];
            [SqlManager unlockTopicId:@"jp" Course:@"vi" andTopicID:[NSString stringWithFormat:@"%d",i]];
        }
    }else{
        for(int i= 1 ; i< 45 ; i++ ){
            if ([g_course isEqualToString:@"vi"]) {
                [SqlManager unlockTopicId:@"en" Course:g_course andTopicID:[NSString stringWithFormat:@"%d",i]];
                [SqlManager unlockTopicId:@"jp" Course:g_course andTopicID:[NSString stringWithFormat:@"%d",i]];
            }else{
                [SqlManager unlockTopicId:g_language Course:g_course andTopicID:[NSString stringWithFormat:@"%d",i]];
            }
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loginComplete" object:nil];
    [(AMSlideMenuMainViewController*)mainMenuViewController closeRightMenuAnimated:YES];
    if ([Tools isDisad]){
        if (HUD!=nil) {
            [HUD hide:YES];
        }
        
    }
}

- (void)productPurchaseFailed:(NSNotification *)notification
{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    if (HUD!=nil) {
        [HUD hide:YES];
    }
    
    SKPaymentTransaction * transaction = (SKPaymentTransaction *) notification.object;
    if (transaction.error.code != SKErrorPaymentCancelled) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                        message:transaction.error.localizedDescription
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK", nil];
        [alert show];
    }
}

- (void)restorePurchased:(NSNotification *)noti{
    NSLog(@"restorePurchased");
    if (HUD!=nil) {
        [HUD hide:YES];
    }
    NSNumber *number = noti.object;
    if (number.intValue == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                        message:@"You haven’t bought a Pro-version yet.Please purchase a Pro-version !"
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK", nil];
        [alert show];
    }
}

- (void)restorePurchaseFailed:(NSNumber *)queue{
    NSLog(@"restorePurchaseFailed");
    if (HUD!=nil) {
        [HUD hide:YES];
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                    message:@"Please purchase a Pro-version!"
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"OK", nil];
    [alert show];
}
@end
