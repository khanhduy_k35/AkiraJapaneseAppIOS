//
//  KanaGame2ViewController.h
//  Pera
//
//  Created by Le Trong Thao on 3/5/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KanaGame2ViewController : UIViewController

@property NSString *topicID;
@property NSString *gameID;
@property int type;

@end
