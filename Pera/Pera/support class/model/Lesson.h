//
//  Lesson.h
//  Pera
//
//  Created by Le Trong Thao on 12/5/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Lesson : NSObject

@property int lessonID;
@property int topic;
@property int sub;
@property NSString *fileName;
@property NSString *hiragana;
@property NSString *kanji;
@property NSString *romaji;
@property NSString *vietnamese;
@property NSString *english;


@property int sequenceNumber;

@end
