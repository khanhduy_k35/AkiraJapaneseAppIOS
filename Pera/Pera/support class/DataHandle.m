//
//  DataHandle.m
//  Pera
//
//  Created by Le Trong Thao on 2/27/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import "DataHandle.h"
#import "Lesson.h"


@implementation DataHandle


+(void)initData
{
    [[NSUserDefaults standardUserDefaults] setValue:@"kana" forKey:@"level"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSMutableDictionary new] forKey:@"syncScore"];
    
    [self reloadData];
    
}

+(void)reloadData
{
    NSDictionary *dataDic = [[NSUserDefaults standardUserDefaults] objectForKey:@"data"];
    
    for(int i=3; i <= 5; i++)
    {
        int numberTopic = 0;
        
        if(i==3)
        {
            numberTopic = 36;
        }
        else
        {
            numberTopic = 24;
        }
        
        for (int j=1; j <= numberTopic; j++)
        {
            for(int k=1; k <= 4; k++)
            {
                int numberOfGame = 6;
                if(k==4)
                {
                    numberOfGame = 3;
                }
                
                for(int l=1; l <= numberOfGame; l++)
                {
                    NSString *gameID = @"";
                    
                    
                    if(k!=4)
                    {
                        if(l==1)
                        {
                            gameID = @"10";
                        }
                        else if (l==2)
                        {
                            gameID = @"2";
                        }
                        else if (l==3)
                        {
                            gameID = @"3";
                        }
                        else if (l==4)
                        {
                            gameID = @"5";
                        }
                        else if (l==5)
                        {
                            gameID = @"1";
                        }
                        else if (l==6)
                        {
                            gameID = @"4";
                        }
                    }
                    else
                    {
                        if(l==1)
                        {
                            gameID = @"10";
                        }
                        else if (l==2)
                        {
                            gameID = @"5";
                        }
                        else if (l==3)
                        {
                            gameID = @"4";
                        }
                        
                    }
                    
                    
                    
                    int star = [[[[[[dataDic objectForKey:[NSString stringWithFormat:@"n%d",i]] objectForKey:@"star"] objectForKey:[NSString stringWithFormat:@"%d",j]] objectForKey:[NSString stringWithFormat:@"%d",k]] valueForKey:gameID] intValue];
                    
                    
                    [[NSUserDefaults standardUserDefaults] setInteger:star forKey:[NSString stringWithFormat:@"n%d/topic%d/sub%d/game%d",i,j,k,l]];
                    
                    if((l == 1 || [[[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"n%d/topic%d/sub%d/game%d_islock",i,j,k,l]] isEqualToString:@"unlock"]) && star == 0)
                    {
                        [[NSUserDefaults standardUserDefaults] setValue:@"unlock" forKey:[NSString stringWithFormat:@"n%d/topic%d/sub%d/game%d_islock",i,j,k,l]];
                    }
                    else if(star == 0)
                    {
                        [[NSUserDefaults standardUserDefaults] setValue:@"lock" forKey:[NSString stringWithFormat:@"n%d/topic%d/sub%d/game%d_islock",i,j,k,l]];
                    }
                    else if(star >= 1)
                    {
                        [[NSUserDefaults standardUserDefaults] setValue:@"unlock" forKey:[NSString stringWithFormat:@"n%d/topic%d/sub%d/game%d_islock",i,j,k,l]];
                        
                        [[NSUserDefaults standardUserDefaults] setValue:@"unlock" forKey:[NSString stringWithFormat:@"n%d/topic%d/sub%d/game%d_islock",i,j,k,l+1]];
                    }
                    
                }
                
            }
        }
    }
    
    
    for(int i = 1; i <= 2; i++)
    {
        
        NSString *typeStr = @"";
        if(i==1)
            typeStr = @"hiragana";
        else
            typeStr = @"katakana";
        
        for(int j = 1; j <= 13; j++)
        {
            
            int numberOfGame = [DataHandle getAllGameOfKanaTopic:j Type:i].count;
            
            for(int k = 1; k <= numberOfGame; k++)
            {
                NSString *gameID = @"";
                
                
                if(k==1)
                {
                    gameID = @"10";
                }
                else if (k==2)
                {
                    gameID = @"3";
                }
                else if (k==3)
                {
                    gameID = @"2";
                }
                else if (k==4)
                {
                    gameID = @"1";
                }
                else if (k==5)
                {
                    gameID = @"8";
                }
                else if (k==6)
                {
                    gameID = @"6";
                }
                
                
                int star = [[[[[[dataDic objectForKey:typeStr] objectForKey:@"star"] objectForKey:[NSString stringWithFormat:@"%d",j]] objectForKey:@"1"] valueForKey:gameID] intValue];
                
                
                [[NSUserDefaults standardUserDefaults] setInteger:star forKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d",i,j,k]];
                
                if((k==1|| [[[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d_islock",i,j,k]] isEqualToString:@"unlock"]) && star==0)
                {
                    [[NSUserDefaults standardUserDefaults] setValue:@"unlock" forKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d_islock",i,j,k]];
                }
                else if(star == 0)
                {
                    [[NSUserDefaults standardUserDefaults] setValue:@"lock" forKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d_islock",i,j,k]];
                }
                else if(star >= 1)
                {
                    [[NSUserDefaults standardUserDefaults] setValue:@"unlock" forKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d_islock",i,j,k]];
                    
                    [[NSUserDefaults standardUserDefaults] setValue:@"unlock" forKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d_islock",i,j,k+1]];
                }
                
            }
        }
    }

}

+(int)getStarsOfGame:(int)game Sub:(int)sub Topic:(int)topic Level:(NSString *)course Type:(int)type
{
    int stars = 0;
    
    if([course isEqualToString:@"kana"])
    {
        stars = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d",type,topic,game]];
    }
    else
    {
        stars = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"%@/topic%d/sub%d/game%d",course,topic,sub,game]];
    }
    
    return stars;
}

+(int)getStarsOfSub:(int)sub Topic:(int)topic Level:(NSString *)level Type:(in id)type
{
    int stars = 0;
    
    int numberOfGame = 6;
    
    if(sub == 4)
    {
        numberOfGame = 3;
    }
   
    for(int i = 1; i <= numberOfGame; i++)
    {
        stars = stars + [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"%@/topic%d/sub%d/game%d",level,topic,sub,i]];
    }
    
    return stars;
}

+(int)getStarsOfTopic:(int)topic Level:(NSString *)level Type:(int)type
{
    int stars = 0;
    
    if([level isEqualToString:@"kana"])
    {
//        int numberOfGame = 6;
//        if(type == 2)
//        {
//            numberOfGame = 4;
//        }
        
        int numberOfGame = [DataHandle getAllGameOfKanaTopic:topic Type:type].count;
        
        for(int i = 1; i <= numberOfGame; i++)
        {
            stars = stars +  [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d",type,topic,i]];
        }
    }
    else
    {
        for(int i = 1; i <= 4 ; i++)
        {
            stars = stars + [self getStarsOfSub:i Topic:topic Level:level Type:0];
        }
    }
    
    return stars;
}

+(int)getStarsOfType:(int)type Level:(NSString *)level
{
    int stars = 0;
    
    if([level isEqualToString:@"kana"])
    {
        for(int i = 1; i <= 13; i++)
        {
            stars = stars + [self getStarsOfTopic:i Level:level Type:type];
        }
    }
    
    return stars;
}

+(int)getSumStarsOfType:(int)type Level:(NSString *)level
{
    int stars = 0;
    
    if([level isEqualToString:@"kana"])
    {
        for(int i = 1; i <= 13; i++)
        {
            stars = stars + 3*[self getAllGameOfKanaTopic:i Type:type].count;
        }
    }
    
    return stars;
}

+(int)getSumStarOfTopic:(int)topic Type:(int)type
{
    int numberOfGame = [self getAllGameOfKanaTopic:topic Type:type].count;
    
    return numberOfGame*3;
}


+(NSMutableArray*)getAllLessonOfTopic:(int)topic Level:(NSString *)level Sub:(int)sub
{
    NSMutableArray *ar = [NSMutableArray new];
    
    NSString *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true)[0];
    NSString *filePath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/courses/%@/%d/vocab/json/vocab.json",level,topic]];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        filePath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/courses/%@/%d/vocab/vocab/vocab.json",level,topic]];
    }
    
    NSString *jsContent = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    
    NSArray *jsAr = [NSJSONSerialization JSONObjectWithData:[jsContent dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    
    for(NSDictionary *dic in jsAr)
    {
        
        NSNumber *subTemp = [dic valueForKey:@"sub"];
        NSNumber *topicTemp = [dic valueForKey:@"topic"];
        
        if([subTemp intValue] == sub  && [topicTemp intValue] == topic)
        {
            Lesson *ls = [Lesson new];
            ls.topic =  topic; //[[dic valueForKey:@"topic"] intValue];
            ls.sub = [[dic valueForKey:@"sub"] intValue];
            ls.lessonID = [[dic valueForKey:@"id"] intValue];
            ls.fileName = [dic valueForKey:@"filename"];
            ls.hiragana = [dic valueForKey:@"hiragana"];
            ls.kanji = [dic valueForKey:@"kanji"];
            ls.romaji = [dic valueForKey:@"romaji"];
            ls.vietnamese = [dic valueForKey:@"vietnamese"];
            ls.english = [dic valueForKey:@"english"];
            
            [ar addObject:ls];
        }
    }
    
    return ar;
}


+(NSMutableArray*)getAllGameOfKanaTopic:(int)topicId Type:(int)type
{
    NSMutableArray *ar = [NSMutableArray new];
    
    NSString *fileName = @"";
    if(type == 1) fileName = @"learn_hira.json";
    else fileName = @"learn_kana.json";
    
    NSString *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true)[0];
    NSString *filePath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/courses/kana/json/%@",fileName]];
    
    NSString *jsonContent = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSArray *jsonDic = [NSJSONSerialization JSONObjectWithData:[jsonContent dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    
    for (NSDictionary *dic in jsonDic) {
        if( [[dic valueForKey:@"id"] intValue]== topicId)
        {
            NSArray *gameAr = [dic objectForKey:@"skill"];
            
            for(NSDictionary *gameDic in gameAr)
            {
                Game *game = [Game new];
                game.gameID = [[gameDic valueForKey:@"id"] intValue];
                game.title = [gameDic valueForKey:@"title"];
                game.key = [gameDic valueForKey:@"key"];
                [ar addObject:game];
            }
        }
    }
    
    return ar;
}

+(NSMutableArray*)getAllLessonTestOfKanaTopic:(int)topicId Type:(int)type
{
    NSMutableArray *ar = [NSMutableArray new];
    
    NSString *fileName = @"";
    if(type == 1) fileName = @"hiragana-test.json";
    else fileName = @"katakana-test.json";
    
    NSString *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true)[0];
    NSString *filePath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/courses/kana/json/%@",fileName]];
    
    NSString *jsonContent = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSArray *jsonDic = [NSJSONSerialization JSONObjectWithData:[jsonContent dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    
    for (NSDictionary *dic in jsonDic)
    {
        if([[dic valueForKey:@"topic"] intValue] == topicId)
        {
            Lesson *ls = [Lesson new];
            ls.topic = [[dic valueForKey:@"topic"] intValue];
            ls.sub = [[dic valueForKey:@"sub"] intValue];
            ls.lessonID = [[dic valueForKey:@"id"] intValue];
            ls.fileName = [dic valueForKey:@"filename"];
            ls.hiragana = [dic valueForKey:@"hiragana"];
            ls.kanji = [dic valueForKey:@"kanji"];
            ls.romaji = [dic valueForKey:@"romaji"];
            ls.vietnamese = [dic valueForKey:@"vietnamese"];
            ls.english = [dic valueForKey:@"english"];
            
            [ar addObject:ls];
        }
    }
    
    return ar;
}


+(NSMutableArray*)getAllKanaLessonTopic:(NSString *)topic Type:(int)type
{
    NSMutableArray *ar = [NSMutableArray new];
    
    NSString *fileName = @"";
    if(type == 1) fileName = @"hiragana.json";
    else fileName = @"katakana.json";
    
    NSString *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true)[0];
    NSString *filePath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/courses/kana/json/%@",fileName]];
    
    NSString *jsonContent = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSArray *jsonDic = [NSJSONSerialization JSONObjectWithData:[jsonContent dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    
    for (NSDictionary *dic in jsonDic)
    {
        if([[dic valueForKey:@"topic"] isEqualToString:topic])
        {
            Lesson *ls = [Lesson new];
            ls.topic = [[dic valueForKey:@"topic"] intValue];
            ls.sub = [[dic valueForKey:@"sub"] intValue];
            ls.lessonID = [[dic valueForKey:@"id"] intValue];
            ls.fileName = [dic valueForKey:@"filename"];
            ls.hiragana = [dic valueForKey:@"hiragana"];
            ls.kanji = [dic valueForKey:@"kanji"];
            ls.romaji = [dic valueForKey:@"romaji"];
            ls.vietnamese = [dic valueForKey:@"vietnamese"];
            ls.english = [dic valueForKey:@"english"];
            
            [ar addObject:ls];
        }
    }
    
    return ar;
}

+(NSString*)getAudioPathOfKanaGame:(NSString *)fileName Type:(int)type
{
    NSString *filePath = @"";
    
    NSString *typeFolder = @"katakana-test-audio";
    if(type == 1)
    {
        typeFolder = @"hiragana-test-audio";
    }
    
    
    NSString *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true)[0];
    filePath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/courses/kana/%@/%@.mp3",typeFolder,fileName]];
    
    return filePath;
}

+(NSString*)getImgPathOfKanaGame:(NSString *)fileName Type:(int)type
{
    NSString *filePath = @"";
    
    NSString *typeFolder = @"katakana-test-picture";
    if(type == 1)
    {
        typeFolder = @"hiragana-test-picture";
    }
    
    
    NSString *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true)[0];
    filePath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/courses/kana/%@/%@.jpg",typeFolder,fileName]];
    
    return filePath;
}


+(void)clearData
{
    for(int i=3; i <= 5; i++)
    {
        int numberTopic = 0;
        
        if(i==3)
        {
            numberTopic = 36;
        }
        else
        {
            numberTopic = 24;
        }
        
        for (int j=1; j <= numberTopic; j++)
        {
            for(int k=1; k <= 4; k++)
            {
                int numberOfGame = 6;
                if(k==4)
                {
                    numberOfGame = 3;
                }
                
                for(int l=1; l <= numberOfGame; l++)
                {
                    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:[NSString stringWithFormat:@"n%d/topic%d/sub%d/game%d",i,j,k,l]];
                    
                    [[NSUserDefaults standardUserDefaults] setValue:@"lock" forKey:[NSString stringWithFormat:@"n%d/topic%d/sub%d/game%d_islock",i,j,k,l]];
                }
                
            }
        }
    }
    
    
    for(int i = 1; i <= 2; i++)
    {
        
        for(int j = 1; j <= 13; j++)
        {
            
            int numberOfGame = (int)[DataHandle getAllGameOfKanaTopic:j Type:i].count;
            
            for(int k = 1; k <= numberOfGame; k++)
            {
                
                [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d",i,j,k]];
                [[NSUserDefaults standardUserDefaults] setValue:@"lock" forKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d_islock",i,j,k]];
                
                
            }
        }
    }
    
    
    
    NSMutableDictionary *dic = [[[NSUserDefaults standardUserDefaults] objectForKey:@"syncScore"] mutableCopy];
    [dic removeAllObjects];
    
    [[NSUserDefaults standardUserDefaults] setObject:dic forKey:@"syncScore"];

}

+(void)checkDayRemain
{
    NSDate *recentDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"recentDate"];
    NSDate *now = [NSDate date];
    
    NSComparisonResult result = [now compare:recentDate];
    
    if(result != NSOrderedSame)
    {
        NSTimeInterval time = [now timeIntervalSinceDate:recentDate];
        int day = time/(24*60*60);
        
        if(day>0)
        {
            int remainDay = [[NSUserDefaults standardUserDefaults] integerForKey:@"day_remain"];
            remainDay = remainDay - day;
            
            if(remainDay < 0)
                remainDay = 0;
            
            [[NSUserDefaults standardUserDefaults] setInteger:remainDay forKey:@"day_remain"];
            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"recentDate"];
        }
        
    }
}

@end
