//
//  Game.h
//  Pera
//
//  Created by Le Trong Thao on 3/3/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Game : NSObject

@property int gameID;
@property NSString *title;
@property NSString *key;

@end
