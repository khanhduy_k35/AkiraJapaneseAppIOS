//
//  Email.h
//  Pera
//
//  Created by Le Trong Thao on 12/6/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Social/Social.h>

@interface Email : NSObject

+(void)mailTo:(NSString*)destinationAccount Title:(NSString*)title andContent:(NSString*)content;
+(void)shareFacebookWithContent:(NSString*)content andViewController:(UIViewController*)vc;


@end
