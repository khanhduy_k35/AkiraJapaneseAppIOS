//
//  QuestionSet.h
//  Pera
//
//  Created by Le Trong Thao on 12/5/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Lesson.h"

@interface QuestionSet : NSObject


@property Lesson *answer;
@property NSMutableArray *falseAnswerArray;

-(void)setSequenceNummberForLesson;

@end
