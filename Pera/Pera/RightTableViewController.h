//
//  RightTableViewController.h
//  Pera
//
//  Created by Le Trong Thao on 1/27/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import "AMSlideMenuRightTableViewController.h"
#import "MBProgressHUD.h"
@interface RightTableViewController : AMSlideMenuRightTableViewController<MBProgressHUDDelegate>

@end
