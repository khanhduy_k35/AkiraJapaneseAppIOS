//
//  FirstViewController.m
//  AkiraLearning
//
//  Created by Le Trong Thao on 3/11/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initView
{
    float scrWidth = [UIScreen mainScreen].bounds.size.width;
    float scrHeight = [UIScreen mainScreen].bounds.size.height;
    
    UIImageView *logoImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_akira.png"]];
    float logoHeight = 140;
    float logoWidth = 270;
    logoImg.frame = CGRectMake((scrWidth - logoWidth)/2, 100, logoWidth, logoHeight);
    [self.view addSubview:logoImg];
    
    
    UILabel *bodyLabel = [UILabel new];
    bodyLabel.frame = CGRectMake(25, logoImg.frame.origin.y+logoImg.frame.size.height+10, scrWidth - 50, 70);
    bodyLabel.backgroundColor = [UIColor clearColor];
    bodyLabel.font = [UIFont boldSystemFontOfSize:14];
    bodyLabel.textAlignment = NSTextAlignmentCenter;
    bodyLabel.textColor = [UIColor colorWithRed:6.0/255.0 green:45.0/255.0 blue:113.0/255.0 alpha:1];
    bodyLabel.numberOfLines = 0;
    bodyLabel.text = @"Akira Learning Japanese là app hỗ trợ học tập cho học viên trên website học tiếng Nhật của Akira";
    [self.view addSubview:bodyLabel];
    
    /*
    UILabel *bodyLabel2 = [UILabel new];
    bodyLabel2.frame = CGRectMake(25, bodyLabel.frame.origin.y+bodyLabel.frame.size.height+5, scrWidth - 50, 90);
    bodyLabel2.backgroundColor = [UIColor clearColor];
    bodyLabel2.font = [UIFont systemFontOfSize:14];
    bodyLabel2.textAlignment = NSTextAlignmentCenter;
    bodyLabel2.textColor = [UIColor colorWithRed:6.0/255.0 green:45.0/255.0 blue:113.0/255.0 alpha:1];
    bodyLabel2.numberOfLines = 0;
    bodyLabel2.text = @"Toàn bộ các bài học sẽ được mở khóa khi bạn đăng nhập trên apps bằng chính tài khoản của bạn sử dụng để đăng nhập trên website: learn.akira.edu.vn";
    [self.view addSubview:bodyLabel2];*/
    
    /*
    UIButton *buyCardBt = [UIButton new];
    buyCardBt.frame = CGRectMake(25, bodyLabel2.frame.origin.y+bodyLabel2.frame.size.height + 20, scrWidth-50, 40);
    [buyCardBt setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:255.0/255.0 green:200.0/255.0 blue:67.0/255.0 alpha:1]] forState:UIControlStateNormal];
    [buyCardBt setTitle:@"TÌM HIỂU THÊM" forState:UIControlStateNormal];
    [buyCardBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    buyCardBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [buyCardBt addTarget:self action:@selector(buyCard) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buyCardBt];*/
    
    
    
    UIButton *loginBt = [UIButton new];
    loginBt.frame = CGRectMake(25, bodyLabel.frame.origin.y+bodyLabel.frame.size.height + 10, scrWidth-50, 40);
    [loginBt setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:82.0/255.0 green:128.0/255.0 blue:194.0/255.0 alpha:1]] forState:UIControlStateNormal];
    [loginBt setTitle:@"ĐĂNG NHẬP" forState:UIControlStateNormal];
    [loginBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    loginBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [loginBt addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginBt];
    
    
    float totalHeight = loginBt.frame.origin.y+loginBt.frame.size.height-logoImg.frame.origin.y;
    float ydelta = logoImg.frame.origin.y - (scrHeight-totalHeight)/2;
    for(UIView *view in self.view.subviews)
    {
        view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y - ydelta, view.frame.size.width, view.frame.size.height);
    }
    
}



-(void)buyCard
{
    NSURL *url = [NSURL URLWithString:@"http://akira.edu.vn/hoc-tieng-nhat-online/hoc-tieng-nhat-online-cung-akira-6/"];
    [[UIApplication sharedApplication] openURL:url];
}

-(void)login
{
    [self performSegueWithIdentifier:@"loginSegue" sender:nil];
}


- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
@end
