//
//  CustomButton.h
//  Pera
//
//  Created by Le Trong Thao on 12/5/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Lesson.h"

@interface CustomButton : UIButton


@property Lesson *lesson;
@property UIButton *button;

@end
