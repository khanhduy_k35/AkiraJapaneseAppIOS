//
//  HttpThread.m
//  Pera
//
//  Created by Le Trong Thao on 3/8/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import "HttpThread.h"
#import "DataHandle.h"
#import "GlobalVariable.h"
#import "Reachability.h"
#import "Toast.h"

@implementation HttpThread


#pragma mark login

+(BOOL)loginWithEmail:(NSString*)email andPass:(NSString*)pass
{
    BOOL res = false;
    
    NSString *post = [NSString stringWithFormat:@"email=%@&password=%@",email,pass];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d",[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://api.akirajapanese.com/v3/signin"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    
    NSURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    
    
    if(responseData)
    {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        
        if([[dic valueForKey:@"code"] intValue] == 1)
        {
            NSDictionary *user = [dic objectForKey:@"user"];
            NSString *userToken = [user valueForKey:@"user_token"];
            NSString *avatar = [user valueForKey:@"avatar"];
            NSString *displayName = [user valueForKey:@"display_name"];
            int dayRemain = [[user valueForKey:@"day_remain"] intValue];
            NSDictionary *data = [[dic objectForKey:@"data"] objectForKey:@"nihongo"];
            
            
            [[NSUserDefaults standardUserDefaults] setValue:userToken forKey:@"user_token"];
            [[NSUserDefaults standardUserDefaults] setValue:avatar forKey:@"avatar"];
            [[NSUserDefaults standardUserDefaults] setInteger:dayRemain forKey:@"day_remain"];
            [[NSUserDefaults standardUserDefaults] setValue:displayName forKey:@"display_name"];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"data"];
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"login"];
            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"recentDate"];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),^{
                [DataHandle initData];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    UITableView *tableView = [currentViewController.view viewWithTag:25];
                    if(tableView)
                    {
                        [tableView reloadData];
                    }
                });
                
                
            });
            
            
            res = true;
            
        }
        
        NSLog(@"");
    }
    
    
    return res;
}


+(BOOL)loginGoogleWithUser:(GIDGoogleUser *)user
{
    BOOL res = false;
    
    NSString *post;
    if(!user.profile.hasImage)
        post = [NSString stringWithFormat:@"email=%@&display_name=%@&social_type=google&social_id=%@&social_token=%@",user.profile.email,user.profile.name,user.authentication.idToken,user.authentication.accessToken];
    else
        post = [NSString stringWithFormat:@"email=%@&display_name=%@&social_type=google&social_id=%@&social_token=%@&avatar=%@",user.profile.email,user.profile.name,user.authentication.idToken,user.authentication.accessToken,[[user.profile imageURLWithDimension:30] absoluteString]];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d",[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://api.akirajapanese.com/v3/login"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    
    NSURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    
    
    if(responseData)
    {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        
        if([[dic valueForKey:@"code"] intValue] == 1)
        {
            NSDictionary *user = [dic objectForKey:@"user"];
            NSString *userToken = [user valueForKey:@"user_token"];
            NSString *avatar = [user valueForKey:@"avatar"];
            NSString *displayName = [user valueForKey:@"display_name"];
            int dayRemain = [[user valueForKey:@"day_remain"] intValue];
            NSDictionary *data = [[dic objectForKey:@"data"] objectForKey:@"nihongo"];
            
            
            [[NSUserDefaults standardUserDefaults] setValue:userToken forKey:@"user_token"];
            [[NSUserDefaults standardUserDefaults] setValue:avatar forKey:@"avatar"];
            [[NSUserDefaults standardUserDefaults] setInteger:dayRemain forKey:@"day_remain"];
            [[NSUserDefaults standardUserDefaults] setValue:displayName forKey:@"display_name"];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"data"];
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"login"];
            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"recentDate"];
            
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),^{
                    [DataHandle initData];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    UITableView *tableView = [currentViewController.view viewWithTag:25];
                    if(tableView)
                    {
                        [tableView reloadData];
                    }
                });
                
                
            });
            
            
            res = true;
            
        }
        
        NSLog(@"");
    }
    
    return res;

}

+(BOOL)loginFbWith:(FBSDKLoginManagerLoginResult *)token andProfile:(id)profile
{
    BOOL res = false;
    
    
    NSString *email = [profile valueForKey:@"email"];
    NSString *displayName = [profile valueForKey:@"name"];
    NSString *avatar = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",token.token.userID];
    
    
    NSString *post = [NSString stringWithFormat:@"email=%@&display_name=%@&social_type=google&social_id=%@&social_token=%@&avatar=%@",email,displayName,token.token.userID,token.token.tokenString,avatar];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d",[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://api.akirajapanese.com/v3/login"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    
    NSURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    
    
    if(responseData)
    {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        
        if([[dic valueForKey:@"code"] intValue] == 1)
        {
            NSDictionary *user = [dic objectForKey:@"user"];
            NSString *userToken = [user valueForKey:@"user_token"];
            NSString *avatar = [user valueForKey:@"avatar"];
            NSString *displayName = [user valueForKey:@"display_name"];
            int dayRemain = [[user valueForKey:@"day_remain"] intValue];
            NSDictionary *data = [[dic objectForKey:@"data"] objectForKey:@"nihongo"];
            
            
            [[NSUserDefaults standardUserDefaults] setValue:userToken forKey:@"user_token"];
            [[NSUserDefaults standardUserDefaults] setValue:avatar forKey:@"avatar"];
            [[NSUserDefaults standardUserDefaults] setInteger:dayRemain forKey:@"day_remain"];
            [[NSUserDefaults standardUserDefaults] setValue:displayName forKey:@"display_name"];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"data"];
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"login"];
            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"recentDate"];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),^{
                [DataHandle initData];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    UITableView *tableView = [currentViewController.view viewWithTag:25];
                    if(tableView)
                    {
                        [tableView reloadData];
                    }
                });
                
                
            });
            
            res = true;
            
        }
        
        NSLog(@"");
    }
    
    return res;
    
}

+(BOOL)getScore
{
    // http://api.akirajapanese.com/v3/scores-information?user_token=
    
    BOOL res = false;
    
    if([self currentNetworkStatus])
    {
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://api.akirajapanese.com/v3/scores-information?user_token=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"user_token"]]]];
        
        NSURLResponse *response;
        NSError *error;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request
                                                     returningResponse:&response
                                                                 error:&error];
        
        
        if(responseData)
        {
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
            
            if([[dic valueForKey:@"code"] intValue] == 1)
            {
                
                
                NSDictionary *DataDic = [[dic objectForKey:@"data"] objectForKey:@"nihongo"];
                [[NSUserDefaults standardUserDefaults] setObject:DataDic forKey:@"data"];
                [DataHandle reloadData];
                
                NSLog(@"reload data finish");
                
                res = true;
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    for(UITableView *view in currentViewController.view.subviews)
                    {
                        if([view isKindOfClass:[UITableView class]])
                        {
                            [view reloadData];
                        }
                    }
                });
                
            }
            
        }

    }
    else
    {
        //[Toast showToastOnView:currentViewController.view withMessage:@"Kiểm tra lại mạng"];
    }
    
    
    return res;
    
    
}

+(void)syncScore
{
    NSMutableDictionary *dic = [[NSUserDefaults standardUserDefaults] objectForKey:@"syncScore"];
    dic = [dic mutableCopy];
    NSMutableArray *deletedKeyAr = [NSMutableArray new];
    
    if([dic allKeys].count > 0)
    {
        for(NSString *key in [dic allKeys])
        {
            NSString *post = [dic valueForKey:key];
            
            if([self currentNetworkStatus])
            {
                NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
                NSString *postLength = [NSString stringWithFormat:@"%d",[postData length]];
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                [request setURL:[NSURL URLWithString:@"http://api.akirajapanese.com/v3/score"]];
                [request setHTTPMethod:@"POST"];
                [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
                [request setHTTPBody:postData];
                
                
                NSURLResponse *response;
                NSError *error;
                NSData *responseData = [NSURLConnection sendSynchronousRequest:request
                                                             returningResponse:&response
                                                                         error:&error];
                
                
                if(responseData)
                {
                    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
                    
                    if([[dic valueForKey:@"code"] intValue] == 1)
                    {
                        [deletedKeyAr addObject:key];
                    }
                    else
                    {
                        
                    }
                    
                }

            }
            
            
        }
    }
    
    
    for(NSString *key in deletedKeyAr)
    {
        [dic removeObjectForKey:key];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:dic forKey:@"syncScore"];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        for(UITableView *view in currentViewController.view.subviews)
        {
            if([view isKindOfClass:[UITableView class]])
            {
                [view reloadData];
            }
        }
        
    });
    
}


+(void)upDateToServerGame:(int)game Topic:(int)topicID Course:(NSString *)course Sub:(int)sub Star:(int)star
{
    //http://api.akirajapanese.com/v3/score
    
    
    NSString *gameID = @"";
    
    if([course isEqualToString:@"hiragana"] || [course isEqualToString:@"katakana"])
    {
        if(game==1)
        {
            gameID = @"10";
        }
        else if (game==2)
        {
            gameID = @"3";
        }
        else if (game==3)
        {
            gameID = @"2";
        }
        else if (game==4)
        {
            gameID = @"1";
        }
        else if (game==5)
        {
            gameID = @"8";
        }
        else if (game==6)
        {
            gameID = @"6";
        }

    }
    else
    {
        if(sub!=4)
        {
            if(game==1)
            {
                gameID = @"10";
            }
            else if (game==2)
            {
                gameID = @"2";
            }
            else if (game==3)
            {
                gameID = @"3";
            }
            else if (game==4)
            {
                gameID = @"5";
            }
            else if (game==5)
            {
                gameID = @"1";
            }
            else if (game==6)
            {
                gameID = @"4";
            }
        }
        else
        {
            if(game==1)
            {
                gameID = @"10";
            }
            else if (game==2)
            {
                gameID = @"5";
            }
            else if (game==3)
            {
                gameID = @"4";
            }
            
        }

    }
    
    NSString *post = [NSString stringWithFormat:@"course=%@&lesson=%d&sub_topic=%d&game=%@&star=%d&exp=0&user_token=%@",course,topicID,sub,gameID,star,[[NSUserDefaults standardUserDefaults] valueForKey:@"user_token"]];
    
    if([self currentNetworkStatus])
    {
        
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%d",[postData length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"http://api.akirajapanese.com/v3/score"]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:postData];
        
        
        NSURLResponse *response;
        NSError *error;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request
                                                     returningResponse:&response
                                                                 error:&error];
        
        
        if(responseData)
        {
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
            
            if([[dic valueForKey:@"code"] intValue] == 1)
            {
                
            }
            
        }

    }
    else
    {
        NSMutableDictionary *dic = [[NSUserDefaults standardUserDefaults] objectForKey:@"syncScore"];
        
        NSMutableDictionary *dicTemp = [dic mutableCopy];
        [dicTemp setObject:post forKey:[NSString stringWithFormat:@"course%@/lesson%d/sub_topic%d/game%@",course,topicID,sub,gameID]];
        
        [[NSUserDefaults standardUserDefaults] setObject:dicTemp forKey:@"syncScore"];
        
    }
    
    
}

#pragma mark logout

+(BOOL)logoutAccount
{
    BOOL res = false;
    

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://api.akirajapanese.com/v3/logout?user_token=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"user_token"]]]];
    
    NSURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    
    
    if(responseData)
    {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        
        if([[dic valueForKey:@"code"] intValue] == 1)
        {
            res = true;
            
        }
        
        NSLog(@"");
    }
    
    return res;

}


+(BOOL)currentNetworkStatus
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}


@end
