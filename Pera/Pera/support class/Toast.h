//
//  Toast.h
//  Pera
//
//  Created by Le Trong Thao on 2/27/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Toast : NSObject

+(void)showToastOnView:(UIView*)view withMessage:(NSString*)message;

@end
