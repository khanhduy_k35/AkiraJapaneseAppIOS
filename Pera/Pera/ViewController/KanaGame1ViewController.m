//
//  KanaGame1ViewController.m
//  Pera
//
//  Created by Le Trong Thao on 3/4/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import "KanaGame1ViewController.h"
#import "ProgressView.h"
#import "CircularProgressView.h"
#import "UIViewController+AMSlideMenu.h"
#import "GameUtil.h"
#import "GlobalVariable.h"

#import "LessonHandle.h"
#import "DataHandle.h"

#import "CustomAnimation.h"
#import "TGDrawSvgPathView.h"

#import "HttpThread.h"

@interface KanaGame1ViewController ()<CircularProgressViewDelegate, UIAlertViewDelegate>

@end

@implementation KanaGame1ViewController
{
    ProgressView *progressView;
    UIView *gameView;
    
    NSMutableArray *lessonArray;
    
    
    int currentIndex;
    
    UIView *wordView;
    
    int drawIndex;
    float drawTime;
    NSMutableArray *svgFileAr;
    NSTimer *drawTimer;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self disableSlidePanGestureForLeftMenu];
    
    [self initView];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    currentViewController = self;
}

-(void)viewWillDisappear:(BOOL)animated
{
    
}

#pragma mark init View

-(void)initView
{
    drawTime = 1.5;
    
    currentIndex = 0;
    lessonArray = [DataHandle getAllKanaLessonTopic:self.topicID Type:self.type];
    lessonArray = [LessonHandle rearrangeElementRandomly:lessonArray];
    
    UIButton *backButton = [UIButton new];
    float heightOfBt = 36;
    float widthOfBt = 36;
    backButton.frame = [GameUtil transformFitToScree:CGRectMake(8, 30 , widthOfBt, heightOfBt) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    [backButton setBackgroundImage:[UIImage imageNamed:@"abc_ic_ab_back_mtrl_am_alpha_blue.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backViewController) forControlEvents:UIControlEventTouchUpInside];
    //backButton.backgroundColor = [UIColor blackColor];
    [self.view addSubview:backButton];
    
    
    
    float xProgressView = backButton.frame.origin.x + backButton.frame.size.width + 10;
    float widthProgressView = [UIScreen mainScreen].bounds.size.width - xProgressView*2;
    float heightProgressView = 6;
    
    progressView = [[ProgressView alloc] initWithFrame:CGRectMake(xProgressView, backButton.frame.origin.y+backButton.frame.size.height/2-heightProgressView/2, widthProgressView, heightProgressView) withMax:lessonArray.count];
    [self.view addSubview:progressView];
    
    
    UILabel *topLabel = [UILabel new];
    topLabel.frame = CGRectMake(10, backButton.frame.origin.y+backButton.frame.size.height+5, [UIScreen mainScreen].bounds.size.width-20,35);
    topLabel.font = [UIFont boldSystemFontOfSize:15];
    topLabel.numberOfLines = 0;
    topLabel.textAlignment = NSTextAlignmentCenter;
    topLabel.textColor = [UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1];
    topLabel.backgroundColor = [UIColor clearColor];
    topLabel.text = @"Học cách viết";
    [self.view addSubview:topLabel];
    
    [self initGameView];
}


-(void)initGameView
{
    [progressView setPercentComplete:currentIndex+1];
    
    drawIndex = 0;
    Lesson *ls = lessonArray[currentIndex];
    
    gameView = [UIView new];
    float yGameView = 110;
    gameView.frame = [GameUtil transformFitToScree:CGRectMake(0, yGameView, 320, 568 - yGameView) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    gameView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:gameView];
    
    
    NSArray *allFileName = [ls.fileName componentsSeparatedByString:@","];
    NSString *audioFileName = allFileName[0];
    svgFileAr = [NSMutableArray new];
    
    if(allFileName.count > 1)
    {
        for(int i = 1; i < allFileName.count; i++)
        {
            [svgFileAr addObject:allFileName[i]];
        }
    }
    else
    {
        [svgFileAr addObject:allFileName[0]];
    }
    
    
    NSMutableArray *wordArray = [NSMutableArray new];
    for(int i = 0; i < ls.hiragana.length; i++)
    {
        [wordArray addObject:[ls.hiragana substringWithRange:NSMakeRange(i, 1)]];
    }
    
    float sizeOfWordBt = 35*[UIScreen mainScreen].bounds.size.width/320;
    float distance = 10;
    float xStart = (gameView.frame.size.width - sizeOfWordBt*svgFileAr.count - distance*(svgFileAr.count-1))/2;
    
    for (int i = 0; i < svgFileAr.count; i++)
    {
        UIButton *wordBt = [UIButton new];
        wordBt.frame = CGRectMake(xStart, 0, sizeOfWordBt, sizeOfWordBt);
        wordBt.tag = 20 + i;
        [wordBt setBackgroundImage:[self imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [wordBt setTitle:wordArray[i] forState:UIControlStateNormal];
        [wordBt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        wordBt.layer.borderWidth = 1;
        wordBt.layer.borderColor = [[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor];
        wordBt.titleLabel.font = [UIFont systemFontOfSize:15];
        [wordBt addTarget:self action:@selector(reDrawWord:) forControlEvents:UIControlEventTouchUpInside];
        [gameView addSubview:wordBt];
        
        xStart += distance + sizeOfWordBt;
        
    }
    
    float sizeOfCircleProgress = 55*[UIScreen mainScreen].bounds.size.width/320;
    
    
    float sizeOfImg = 200*[UIScreen mainScreen].bounds.size.width/320;
    float yImgView = sizeOfWordBt + 10 + sizeOfCircleProgress/2;
    wordView = [UIView new];
    wordView.frame = CGRectMake((gameView.frame.size.width - sizeOfImg)/2, yImgView, sizeOfImg, sizeOfImg);
    wordView.backgroundColor = [UIColor whiteColor];
    wordView.layer.borderWidth = 1;
    wordView.layer.borderColor = [[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor];
    [gameView addSubview:wordView];
    
    
    CircularProgressView *circleProgress = [[CircularProgressView alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-sizeOfCircleProgress)/2, wordView.frame.origin.y - sizeOfCircleProgress/2, sizeOfCircleProgress, sizeOfCircleProgress)
                                                                             backColor:[UIColor colorWithRed:236.0 / 255.0 green:236.0 / 255.0 blue:236.0 / 255.0 alpha:1.0] progressColor:[UIColor colorWithRed:82.0 / 255.0 green:135.0 / 255.0 blue:237.0 / 255.0 alpha:1.0]
                                                                             lineWidth:10
                                                                              audioURL:[LessonHandle getKanaAudioUrlOfFileName:audioFileName]
                                                                               imgPlay:@"icon_play.png"
                                                                               imgStop:@"icon_stop.png"];
    circleProgress.delegate = self;
    [circleProgress.btnPlay setImage:[UIImage imageNamed:@"icon_stop.png"] forState:UIControlStateNormal];
    [circleProgress setTag:1];
    [gameView addSubview:circleProgress];
    [circleProgress play];
    
    
    xStart = 0;
    for (int i = 0; i < svgFileAr.count; i++)
    {
        float sizeOfSvgView = [self getSizeOfWord:wordArray[i] onView:wordView withWordArray:wordArray];
        TGDrawSvgPathView *svgView = [[TGDrawSvgPathView alloc] initWithFrame:CGRectMake(xStart, (wordView.frame.size.height - sizeOfSvgView)/2, sizeOfSvgView, sizeOfSvgView)];
        svgView.tag = 20 + i;
        if(i==0)[svgView setFilePathFromSvg:[LessonHandle getSvgFilePathOf:svgFileAr[i] Type:self.type] strokeColor:[UIColor blackColor] duration:drawTime];
        //[svgView setFilePathFromSvg:[LessonHandle getSvgFilePathOf:svgFileAr[i] Type:self.type] strokeColor:[UIColor blackColor] duration:drawTime];
        [wordView addSubview:svgView];
        
        xStart += sizeOfSvgView;
    }
    

    [self adjustSubFrameOnView:wordView];
    
    drawIndex++;
    drawTimer = [NSTimer scheduledTimerWithTimeInterval:drawTime target:self selector:@selector(drawWord) userInfo:nil repeats:true];
    
    [self initWordLabelsWithLesson:ls];
    
}


-(void)initWordLabelsWithLesson:(Lesson*)ls
{

    UILabel *learnedWordLabel = [UILabel new];
    learnedWordLabel.frame = CGRectMake(0, wordView.frame.origin.y+wordView.frame.size.height+5, [UIScreen mainScreen].bounds.size.width, 30);
    learnedWordLabel.font = [UIFont boldSystemFontOfSize:17];
    learnedWordLabel.backgroundColor = [UIColor clearColor];
    learnedWordLabel.textAlignment = NSTextAlignmentCenter;
    learnedWordLabel.textColor =  [UIColor blackColor];
    learnedWordLabel.text = ls.hiragana;
    [gameView addSubview:learnedWordLabel];
    
    
    UILabel *pronounLabel = [UILabel new];
    pronounLabel.frame = CGRectMake(0, learnedWordLabel.frame.origin.y+learnedWordLabel.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
    CGSize constraint = CGSizeMake([UIScreen mainScreen].bounds.size.width-30, 20000.0f);
    CGSize size ;
    
    pronounLabel.frame = CGRectMake(0, pronounLabel.frame.origin.y, [UIScreen mainScreen].bounds.size.width, 30);
    pronounLabel.font = [UIFont boldSystemFontOfSize:17];
    pronounLabel.backgroundColor = [UIColor clearColor];
    pronounLabel.textAlignment = NSTextAlignmentCenter;
    pronounLabel.textColor =  [UIColor blackColor];
    pronounLabel.numberOfLines = 0;
    pronounLabel.text = ls.romaji;
    size = [ls.romaji sizeWithFont:[UIFont boldSystemFontOfSize:17] constrainedToSize:constraint lineBreakMode:UILineBreakModeWordWrap];
    pronounLabel.frame =  CGRectMake(15, pronounLabel.frame.origin.y + 5, [UIScreen mainScreen].bounds.size.width-30, size.height + 10);
    
    [gameView addSubview:pronounLabel];
    
    
    UIButton *nexButton = [UIButton new];
    float widthOfNextButton = 130*[UIScreen mainScreen].bounds.size.width/320.0;
    float heightOfNextButton;
    
    if([UIScreen mainScreen].bounds.size.height == 480)
        heightOfNextButton = (gameView.frame.size.height - pronounLabel.frame.origin.y - pronounLabel.frame.size.height)*3/5;
    else
        heightOfNextButton = 40*[UIScreen mainScreen].bounds.size.height/568.0;

    
    nexButton.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width-widthOfNextButton)/2, pronounLabel.frame.origin.y+pronounLabel.frame.size.height+(gameView.frame.size.height - pronounLabel.frame.origin.y - pronounLabel.frame.size.height)/2-heightOfNextButton/2, widthOfNextButton, heightOfNextButton);
    
    [nexButton setBackgroundColor:[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1]];
    NSString *next =  @"Tiếp tục";
    [nexButton setTitle:next forState:UIControlStateNormal];
    [nexButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nexButton setTag:3];
    nexButton.hidden = true;
    [nexButton addTarget:self action:@selector(nextLesson) forControlEvents:UIControlEventTouchUpInside];
    [gameView addSubview:nexButton];
    
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(void)initEndingViewWithStars:(int)starNumber
{
    
    UIView *endingView = [UIView new];
    endingView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    endingView.userInteractionEnabled = true;
    endingView.backgroundColor = [UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1];
    [endingView setTag:1];
    [self.view addSubview:endingView];
    
    
    UILabel *infoLabel = [UILabel new];
    infoLabel.frame = CGRectMake(0, 100*[UIScreen mainScreen].bounds.size.height/568.0, [UIScreen mainScreen].bounds.size.width, 40);
    infoLabel.font = [UIFont boldSystemFontOfSize:25];
    infoLabel.textAlignment = NSTextAlignmentCenter;
    infoLabel.textColor = [UIColor whiteColor];
    infoLabel.backgroundColor = [UIColor clearColor];
    if(starNumber == 3)
    {
        infoLabel.text = @"Tuyệt vời";
    }
    else if (starNumber == 2)
    {
        infoLabel.text = @"Tốt";
    }
    else if (starNumber == 1)
    {
        infoLabel.text = @"Cố lên nào";
    }
    else if (starNumber == 0)
    {
        infoLabel.text = @"Cố lên nào";
    }
    
    [endingView addSubview:infoLabel];
    
    
    float sizeOfImg = 40*[UIScreen mainScreen].bounds.size.width/320.0;
    float xStart = ([UIScreen mainScreen].bounds.size.width - (sizeOfImg*3 + 10*2))/2;
    UIImageView *imgViewTemp;
    
    for(int i = 1; i<= 3; i++)
    {
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(xStart, infoLabel.frame.origin.y+infoLabel.frame.size.height+20, sizeOfImg, sizeOfImg)];
        imgView.image = [UIImage imageNamed:@"favorite.png"];
        
        if(i==1&& starNumber==1)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        else if((i==1 ||i ==2) && starNumber == 2)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        else if((i==1 ||i ==2 || i==3) && starNumber == 3)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        
        xStart += sizeOfImg + 10;
        [endingView addSubview:imgView];
        imgViewTemp = imgView;
    }
    
    
    float sizeOfBt = 50*[UIScreen mainScreen].bounds.size.width/320.0;
    
    UIButton *replayButton = [UIButton new];
    float xReplayBt = ([UIScreen mainScreen].bounds.size.width - sizeOfBt*2 - 40)/2;
    replayButton.frame = CGRectMake(xReplayBt, imgViewTemp.frame.origin.y+imgViewTemp.frame.size.height + 30, sizeOfBt, sizeOfBt);
    [replayButton setBackgroundImage:[UIImage imageNamed:@"icon_redo.png"] forState:UIControlStateNormal];
    [replayButton addTarget:self action:@selector(replay) forControlEvents:UIControlEventTouchUpInside];
    [endingView addSubview:replayButton];
    
    
    
    UIButton *nextButton = [UIButton new];
    nextButton.frame = CGRectMake(replayButton.frame.origin.x+sizeOfBt+40, imgViewTemp.frame.origin.y+imgViewTemp.frame.size.height + 30, sizeOfBt, sizeOfBt);
    [nextButton setBackgroundImage:[UIImage imageNamed:@"next_icon.png"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(backViewControllerWhenFinish) forControlEvents:UIControlEventTouchUpInside];
    [endingView addSubview:nextButton];
    
    
    if(starNumber > [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d",self.type,[self.topicID intValue],[self.gameID intValue]]])
    {
        [[NSUserDefaults standardUserDefaults] setInteger:starNumber forKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d",self.type,[self.topicID intValue],[self.gameID intValue]]];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            
            NSString *course = @"";
            if(self.type==1)
            {
                course = @"hiragana";
            }
            else
            {
                course = @"katakana";
            }
            
            [HttpThread upDateToServerGame:[self.gameID intValue] Topic:[self.topicID intValue] Course:course Sub:1 Star:starNumber];
        });
    }
    
    if(starNumber >= 1)
    {
        if([[[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d_islock",self.type,[self.topicID intValue],[self.gameID intValue]+1]] isEqualToString:@"lock"])
        {
            [[NSUserDefaults standardUserDefaults] setValue:@"unlock" forKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d_islock",self.type,[self.topicID intValue],[self.gameID intValue]+1]];
        }
    }
    
}

#pragma mark- button

-(void)backViewController
{
    NSString *mes = @"Bạn có chắc chắn muốn thoát game này ?";
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:mes
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    
    [alert show];
}

-(IBAction)replayAudio:(UITapGestureRecognizer*)sender
{
    CircularProgressView *button  = (CircularProgressView*)[gameView viewWithTag:1];
    [button.btnPlay setImage:[UIImage imageNamed:@"icon_stop.png"] forState:UIControlStateNormal];
    [button play];
}

-(void)nextLesson
{
    CircularProgressView *button  = (CircularProgressView*)[gameView viewWithTag:1];
    if(button!=nil){
        [button stop];
    }
    
    //UIButton *nextButton = (UIButton*)[gameView viewWithTag:3];
    
    if(currentIndex != lessonArray.count - 1)
    {
        //[progressView setPercentComplete:currentIndex+1];
        currentIndex = currentIndex + 1;
        [gameView removeFromSuperview];
        [self initGameView];
    }
    else
    {
        [self initEndingViewWithStars:3];
    }
}

-(void)backViewControllerWhenFinish
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) replay
{
    for(UIView *view in self.view.subviews)
    {
        [view removeFromSuperview];
    }
    
    [self initView];
    
}

#pragma mark- CircularProgressViewDelegate

-(void)playerDidFinishPlaying
{
    NSLog(@"playerDidFinishPlaying");
    CircularProgressView *circleProgress = (CircularProgressView*)[gameView viewWithTag:1];
    
    [circleProgress.btnPlay setImage:[UIImage imageNamed:@"icon_play.png"] forState:UIControlStateNormal];
    
    if(currentIndex <= lessonArray.count-2)
    {
//        UIButton *nextButton = (UIButton*)[gameView viewWithTag:3];
//        nextButton.hidden = false;
    }
    else
    {
//        [UIView animateWithDuration:0.5
//                         animations:^{
//                             [progressView setPercentComplete:currentIndex+1];
//                             
//                         }
//                         completion:^(BOOL finish){
//                             [self initEndingViewWithStars:3];
//                             
//                         }];
        
        
        NSLog(@"get 3 stars");
    }
}

-(void)updateProgressViewWithPlayer:(AVAudioPlayer *)player
{
    
}


#pragma mark- alert view delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark- private method

-(void)updateRateDay
{
    NSDate *previousDay = [[NSUserDefaults standardUserDefaults] objectForKey:@"rateDay"];
    if(!previousDay)
    {
        showRateView = true;
        
    }
    else
    {
        NSTimeInterval time = [[NSDate date] timeIntervalSinceDate:previousDay];
        if(time > 24*3600)
        {
            showRateView = true;
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"rateDay"];
    
}

#pragma mark draw method

-(float)getSizeOfWord:(NSString*)word onView:(UIView*)containView withWordArray:(NSArray*)wordAr
{
    float size = 0;
    float wordRatio = 0;
    float totalRatio = 0;
    
    NSArray *smallWord = @[@"ょ",@"ゅ",@"ゃ",@"っ"];
    
    for(NSString *w in wordAr)
    {
        if([smallWord containsObject:w])
            totalRatio += 3;
        else
            totalRatio += 5;
    }
    
    if([smallWord containsObject:word])
        wordRatio = 3;
    else
        wordRatio = 5;
    
    
    size = wordRatio*containView.frame.size.width/totalRatio;
    
    return size;
}


-(void)adjustSubFrameOnView:(UIView*)containView
{
    if(containView.subviews.count > 1)
    {
        float max = 0;
        
        for(TGDrawSvgPathView *svgView in containView.subviews)
        {
            if(svgView.frame.origin.y+svgView.frame.size.height > max)
            {
                max = svgView.frame.origin.y+svgView.frame.size.height;
            }
            
        }
        
        for(TGDrawSvgPathView *svgView in containView.subviews)
        {
            svgView.frame = CGRectMake(svgView.frame.origin.x, max - svgView.frame.size.height, svgView.frame.size.width, svgView.frame.size.height);
            
        }
    }
}

-(void)drawWord
{
    if(drawIndex == svgFileAr.count)
    {
        UIButton *nextButton = (UIButton*)[gameView viewWithTag:3];
        nextButton.hidden = false;
        
        
        if(currentIndex == lessonArray.count - 1)
        {
            [nextButton setTitle:@"Kết thúc" forState:UIControlStateNormal];
        }
        
        
        [drawTimer invalidate];
    }
    else
    {
        TGDrawSvgPathView *svgView = (TGDrawSvgPathView*)[wordView viewWithTag:20+drawIndex];
        [svgView setFilePathFromSvg:[LessonHandle getSvgFilePathOf:svgFileAr[drawIndex] Type:self.type] strokeColor:[UIColor blackColor] duration:drawTime];
        drawIndex++;
        
    }
}

-(IBAction)reDrawWord:(UIButton*)sender
{
    if(drawIndex == svgFileAr.count)
    {
        TGDrawSvgPathView *oldSvgView = (TGDrawSvgPathView*)[wordView viewWithTag:sender.tag];
        
        TGDrawSvgPathView *newSvgView = [TGDrawSvgPathView new];
        newSvgView.frame = oldSvgView.frame;
        newSvgView.tag = oldSvgView.tag;
        [wordView addSubview:newSvgView];
        [oldSvgView removeFromSuperview];
        [newSvgView setFilePathFromSvg:[LessonHandle getSvgFilePathOf:svgFileAr[newSvgView.tag - 20] Type:self.type] strokeColor:[UIColor blackColor] duration:drawTime];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
