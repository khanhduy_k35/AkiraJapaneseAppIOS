//
//  Toast.m
//  Pera
//
//  Created by Le Trong Thao on 2/27/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import "Toast.h"

@implementation Toast


+(void)showToastOnView:(UIView *)view withMessage:(NSString *)message
{
    UILabel *need2Label = [UILabel new];
    need2Label.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-100, 40);
    need2Label.numberOfLines = 0;
    need2Label.font = [UIFont boldSystemFontOfSize:15];
    need2Label.textAlignment = NSTextAlignmentCenter;
    need2Label.backgroundColor = [UIColor clearColor];
    need2Label.textColor = [UIColor whiteColor];
    need2Label.text = message;
    [need2Label sizeToFit];
    
    UIView *need2View = [UIView new];
    need2View.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.8];
    need2View.frame = CGRectMake(40, [UIScreen mainScreen].bounds.size.height-(need2Label.frame.size.height+10)-50, [UIScreen mainScreen].bounds.size.width-80, need2Label.frame.size.height+10);
    
    need2View.layer.cornerRadius = need2View.frame.size.height/2;
    
    
    need2Label.frame = CGRectMake((need2View.frame.size.width-need2Label.frame.size.width)/2, (need2View.frame.size.height-need2Label.frame.size.height)/2, need2Label.frame.size.width, need2Label.frame.size.height);
    
    [need2View addSubview:need2Label];
    
    [view addSubview:need2View];
    
    [UILabel animateWithDuration:2 animations:^{
        [need2View setAlpha:0];
    }completion:^(BOOL finish){
        [need2View removeFromSuperview];
    }];

}


@end
