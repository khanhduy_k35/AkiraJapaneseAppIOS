//
//  AppDelegate.m
//  Pera
//
//  Created by Le Trong Thao on 12/1/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import "AppDelegate.h"
#import "SqlManager.h"
#import "SSZipArchive.h"
#import "GlobalVariable.h"
#import "JsonFileHandle.h"
#import <AVFoundation/AVFoundation.h>
#import "Tools.h"
#import "InAppWordsIAPHelper.h"
#import "Reachability.h"
#import "DataHandle.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSLog(@"%@",documentPath);
    if(![[NSFileManager defaultManager] fileExistsAtPath:[documentPath stringByAppendingPathComponent:@"resources"]])
    {
        NSString *zipDataFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"resources.zip"];
        [SSZipArchive unzipFileAtPath:zipDataFilePath toDestination:documentPath];
    }
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *error = nil;
    BOOL result = NO;
    if ([audioSession respondsToSelector:@selector(setActive:withOptions:error:)]) {
        result = [audioSession setActive:YES withOptions:0 error:&error]; // iOS6+
    } else {
        [audioSession setActive:YES withFlags:0 error:&error]; // iOS5 and below
    }
    if (!result && error) {
        // deal with the error
    }
    
    error = nil;
   // [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    result = [audioSession setCategory:AVAudioSessionCategoryPlayback error:&error];
    
    if (!result && error) {
        // deal with the error
    }
    
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    UINavigationController *nvc;
    
    if(![[NSUserDefaults standardUserDefaults] valueForKey:@"level"])
    {
        //[[NSUserDefaults standardUserDefaults] setBool:true forKey:@"first"];
        
        nvc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"firstNavigation"];
    }
    else
    {
        nvc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"slideNavigation"];
    }
    
    self.window.rootViewController = nvc;
    [self.window makeKeyAndVisible];
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    if([[url absoluteString] rangeOfString:@"google"].location != NSNotFound)
        return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:sourceApplication
                                      annotation:annotation];
    else
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation
                ];
}


@end
