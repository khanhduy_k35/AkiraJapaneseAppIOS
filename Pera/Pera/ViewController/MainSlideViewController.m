//
//  MainSlideViewController.m
//  Pera
//
//  Created by Le Trong Thao on 12/2/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import "MainSlideViewController.h"
#import "GlobalVariable.h"

@interface MainSlideViewController ()<AMSlideMenuDelegate>

@end

@implementation MainSlideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    mainMenuViewController = self;
    //self.slideMenuDelegate = self;
}


- (NSString *)segueIdentifierForIndexPathInLeftMenu:(NSIndexPath *)indexPath
{
    NSString *identifier=@"topic";
    switch (indexPath.row) {
        case 1:
            identifier  = @"course";
            
            break;
            
        case 2:
            identifier = @"course";
            
            break;
            
        case 3:
            identifier = @"course";
            break;
            
        default:
            break;
    }
    return identifier;
}


-(NSString*)segueIdentifierForIndexPathInRightMenu:(NSIndexPath *)indexPath
{
    NSString *identifier=@"first";
    switch (indexPath.row) {
        case 1:
            identifier  = @"first";
            
            break;
            
        case 2:
            identifier = @"second";
            
            break;
            
        default:
            break;
    }
    
    return identifier;
    
}


- (AMPrimaryMenu)primaryMenu
{
    return AMPrimaryMenuLeft;
}


- (void)configureLeftMenuButton:(UIButton *)button
{
    /* CGRect frame = button.frame;
     frame.origin = CGPointMake(0, 0);
     frame.size = CGSizeMake(28, 28);
     button.frame = frame;
     button.backgroundColor = [UIColor blueColor];
     [button setContentEdgeInsets:UIEdgeInsetsMake(0, -15, 0, 0)];
     [button setImage:[UIImage imageNamed:@"ic_action"] forState:UIControlStateNormal];*/
    
}

-(CGFloat)leftMenuWidth{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return leftMenuWidth = 330;
    }
    else{
        return leftMenuWidth = [UIScreen mainScreen].bounds.size.width - 40;
    }
}

-(CGFloat)rightMenuWidth
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return 330;
    }
    else{
        return [UIScreen mainScreen].bounds.size.width - 40;
    }
}



-(void)leftMenuDidClose
{
    NSLog(@"close");
}
@end
