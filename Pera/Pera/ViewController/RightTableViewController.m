//
//  RightTableViewController.m
//  Pera
//
//  Created by Le Trong Thao on 1/27/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import "RightTableViewController.h"
#import "JsonFileHandle.h"
#import "GlobalVariable.h"

#import "AMSlideMenuMainViewController.h"


@interface RightTableViewController ()<AMSlideMenuDelegate>

@end

@implementation RightTableViewController
{
    CGFloat heightOfCell;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    heightOfCell = 140;
    
    self.tableView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

}

#pragma mark table view datasource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1 || indexPath.row == 2) {
        return heightOfCell + 40;
    }
    else
        return heightOfCell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(indexPath.row == 0)
    {
        cell.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:200.0/255.0 blue:67.0/255.0 alpha:1];
        
        float heightOfImg = heightOfCell - 30;
        float widthOfImg = 90;
        
        UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lion_rate_screen.png"]];
        img.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - ([UIScreen mainScreen].bounds.size.width - leftMenuWidth) - widthOfImg - 5, 20 , widthOfImg, heightOfImg);
        [cell addSubview:img];
        
        
        UILabel *lb = [UILabel new];
        lb.frame = CGRectMake(30, 40, 150, 70);
        lb.textAlignment = NSTextAlignmentLeft;
        lb.font = [UIFont boldSystemFontOfSize:16];
        lb.textColor = [UIColor whiteColor];
        lb.backgroundColor = [UIColor clearColor];
        lb.text = @"Upgrade to unlock the course";
        lb.numberOfLines = 0;
        [cell addSubview:lb];
        
    }
    else if (indexPath.row == 1 || indexPath.row == 2)
    {
        cell.backgroundColor = [UIColor clearColor];
        
        UIView *containView = [UIView new];
        containView.frame = CGRectMake(10, 20, [UIScreen mainScreen].bounds.size.width - ([UIScreen mainScreen].bounds.size.width - leftMenuWidth) - 20, heightOfCell+20);
        containView.userInteractionEnabled = true;
        containView.backgroundColor = [UIColor whiteColor];
        [cell addSubview:containView];
        
        UILabel *priceLb = [UILabel new];
        priceLb.frame = CGRectMake(0, 0, containView.frame.size.width, 40);
        priceLb.textColor = [UIColor whiteColor];
        priceLb.font = [UIFont boldSystemFontOfSize:17];
        priceLb.backgroundColor = [UIColor colorWithRed:131.0/255.0 green:190.0/255.0 blue:85.0/255.0 alpha:1];
        priceLb.textAlignment = NSTextAlignmentCenter;
        priceLb.text = @"9.99$";
        if(indexPath.row == 2)
        {
            priceLb.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:200.0/255.0 blue:67.0/255.0 alpha:1];
            priceLb.text = @"19.99$";
            
        }
        [containView addSubview:priceLb];
        
        
        UILabel *bodyLb = [UILabel new];
        bodyLb.frame = CGRectMake(10, priceLb.frame.size.height, containView.frame.size.width - 20, 80);
        bodyLb.textColor = [UIColor blackColor];
        bodyLb.font = [UIFont systemFontOfSize:17];
        bodyLb.backgroundColor = [UIColor clearColor];
        bodyLb.textAlignment = NSTextAlignmentCenter;
        bodyLb.text = @"Unlock all the lesson of Japanese course";
        if (indexPath.row == 2) {
            bodyLb.text = @"Unlock all the courses (Japanese, Chinese, Korean, Vietnamese)";
        }
        bodyLb.numberOfLines = 0;
        [containView addSubview:bodyLb];
        
        
        float heightOfBt = containView.frame.size.height  - (bodyLb.frame.origin.y + bodyLb.frame.size.height + 10);
        float widthOfBt = 80;
        UIButton *buyBt = [UIButton new];
        buyBt.frame = CGRectMake((containView.frame.size.width-widthOfBt)/2, bodyLb.frame.origin.y+bodyLb.frame.size.height+5, widthOfBt, heightOfBt);
        buyBt.backgroundColor = [UIColor colorWithRed:131.0/255.0 green:190.0/255.0 blue:85.0/255.0 alpha:1];
        if(indexPath.row == 2)
        {
            buyBt.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:200.0/255.0 blue:67.0/255.0 alpha:1];
        }
        [buyBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        buyBt.titleLabel.font = [UIFont systemFontOfSize:15];
        [buyBt setTitle:@"Upgrade" forState:UIControlStateNormal];
        [buyBt addTarget:self action:@selector(buyButton9:) forControlEvents:UIControlEventTouchUpInside];
        if (indexPath.row == 2) {
            [buyBt addTarget:self action:@selector(buyButton19:) forControlEvents:UIControlEventTouchUpInside];
        }
        buyBt.layer.cornerRadius = 4;
        [containView addSubview:buyBt];
        
    }
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%d",indexPath.row);
    
    //[[self mainVC] closeRightMenuAnimated:true];
}

-(IBAction)buyButton9:(id)sender
{
    NSLog(@"9.99$");
}

-(IBAction)buyButton19:(id)sender
{
    NSLog(@"19.99$");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
