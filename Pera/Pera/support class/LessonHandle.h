//
//  LessonHandle.h
//  Pera
//
//  Created by Le Trong Thao on 12/5/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Lesson.h"
#import "QuestionSet.h"


@interface LessonHandle : NSObject

+(NSMutableArray*)rearrangeElementRandomly:(NSMutableArray*)array;
+(NSMutableArray*)getQuestionSetsFrom:(NSMutableArray*)lessonArray withAmount:(int)amount;
+(NSMutableArray*)getRandomlyLessonSets:(int)amount fromArray:(NSMutableArray*)lessonAr;


+(NSString*)getImgFilePathOfFileName:(NSString*)fileName andTopic:(int)topicID Level:(NSString*)level;
+(NSURL*)getAudioUrlOfFileName:(NSString*)fileName TopicID:(int)topicID Level:(NSString*)level;

+(BOOL)checkAudioUrlOfFileName:(NSString *)fileName TopicID:(int)topicID Level:(NSString *)level;

// Kana

+(NSString*)getSvgFilePathOf:(NSString*)fileName Type:(int)type;
+(NSURL*)getKanaAudioUrlOfFileName:(NSString *)fileName;

@end
