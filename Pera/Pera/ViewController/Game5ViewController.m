//
//  Game5ViewController.m
//  Pera
//
//  Created by Le Trong Thao on 12/4/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import "Game5ViewController.h"
#import "ProgressView.h"
#import "CircularProgressView.h"
#import "GameUtil.h"
#import "GlobalVariable.h"
#import "LessonHandle.h"
#import "SCAudioMeter.h"
#import "CustomAnimation.h"
#import "CustomButton.h"
#import "DataHandle.h"

#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

#import "UIViewController+AMSlideMenu.h"
#import "HttpThread.h"


@interface Game5ViewController ()<CircularProgressViewDelegate,UIAlertViewDelegate,AVAudioPlayerDelegate>

@end

@implementation Game5ViewController
{
    NSMutableArray *lessonArray;
    
    ProgressView *progressView;
    UIView *gameView;
    int currentIndex;
    int totalTrueAnswer;
    
    NSMutableArray *answerArray;
    
    AVAudioPlayer *resultPlayer;
    
    //UIButton *nexButton;
    
    
    
    NSMutableArray *characterArray;
    NSArray *fullCharArray;
    
    NSMutableArray *btInBoardArray;
    NSMutableArray *hintBtArray;
    
    int minEmptyIndex;
}

// い,う,え,お,か,き,く,け,こ,さ,し,す,せ,そ,な,に,ぬ,ね,の,た,ち,つ,て,と,は,ひ,ふ,へ,ほ,や,ゆ,よ,ま,み,む,め,も,ら,り,る,れ,ろ,わ,を,ん

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self disableSlidePanGestureForLeftMenu];
    
    UIView *downloadView = [self.navigationController.view viewWithTag:10];
    if(downloadView)
    {
        [downloadView setAlpha:0];
    }
    
     ((AppDelegate *)[[UIApplication sharedApplication] delegate]).isGame5Success = NO;
    
    fullCharArray = @[@"い",@"う",@"え",@"お",@"か",@"き",@"く",@"け",@"こ",@"さ",@"し",@"す",@"せ",@"そ",@"な",@"に",@"ぬ",@"ね",@"の",@"た",@"ち",@"つ",@"て",@"と",@"は",@"ひ",@"ふ",@"へ",@"ほ",@"や",@"ゆ",@"よ",@"ま",@"み",@"む",@"め",@"も",@"ら",@"り",@"る",@"れ",@"ろ",@"わ",@"を",@"ん"];
    
    
    [self initView];
    
}


-(void)viewDidAppear:(BOOL)animated
{
    currentViewController = self;
}

-(void)viewWillDisappear:(BOOL)animated
{
    
}

#pragma mark initView

-(void)initView
{
    totalTrueAnswer = 0;
    currentIndex = 0;
    lessonArray = [DataHandle getAllLessonOfTopic:[self.topicID intValue] Level:g_course Sub:self.sub];
    lessonArray = [LessonHandle rearrangeElementRandomly:lessonArray];
    
    UIButton *backButton = [UIButton new];
    float heightOfBt = 36;
    float widthOfBt = 36;
    backButton.frame = [GameUtil transformFitToScree:CGRectMake(8, 30 , widthOfBt, heightOfBt) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    [backButton setBackgroundImage:[UIImage imageNamed:@"abc_ic_ab_back_mtrl_am_alpha_blue.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backViewController) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:backButton];
    
    
    
    float xProgressView = backButton.frame.origin.x + backButton.frame.size.width + 10;
    float widthProgressView = [UIScreen mainScreen].bounds.size.width - xProgressView*2;
    float heightProgressView = 6;
    
    progressView = [[ProgressView alloc] initWithFrame:CGRectMake(xProgressView, backButton.frame.origin.y+backButton.frame.size.height/2-heightProgressView/2, widthProgressView, heightProgressView) withMax:lessonArray.count];
    [self.view addSubview:progressView];
    
    
    UILabel *topLabel = [UILabel new];
    topLabel.frame = CGRectMake(10, backButton.frame.origin.y+backButton.frame.size.height+5, [UIScreen mainScreen].bounds.size.width-20,35);
    topLabel.font = [UIFont boldSystemFontOfSize:15];
    topLabel.numberOfLines = 0;
    topLabel.textAlignment = NSTextAlignmentCenter;
    topLabel.textColor = [UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1];
    topLabel.backgroundColor = [UIColor clearColor];
    topLabel.text = @"Điền từ tương ứng với tranh";
    [self.view addSubview:topLabel];
    
    
    [self initGameView];
    
}


-(void)initGameView
{
    
    //nexButton = nil;
    minEmptyIndex = 0;
    Lesson *ls = lessonArray[currentIndex];
    
    [progressView setPercentComplete:currentIndex];
    
    gameView = [UIView new];
    float yGameView = 110;
    gameView.frame = [GameUtil transformFitToScree:CGRectMake(0, yGameView, 320, 568 - yGameView) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    gameView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:gameView];
    
    float sizeOfImg = 170*[UIScreen mainScreen].bounds.size.width/320;
    float yImgView = 0*gameView.frame.size.height/448;
    NSString *imgFilePath = [LessonHandle getImgFilePathOfFileName:ls.fileName andTopic:ls.topic Level:g_course];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:imgFilePath]];
    imgView.userInteractionEnabled = true;
    [imgView setTag:2];
    imgView.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width-sizeOfImg)/2, yImgView, sizeOfImg, sizeOfImg);
    [imgView.layer setBorderWidth:1];
    [imgView.layer setBorderColor:[[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor]];
    [gameView addSubview:imgView];
    
    
    characterArray = [self getCharacterArray];
    
    float sizeOfCharLb = 40;
    float distanceBetweenCharLB = 5;
    float edgePadding = 20;
    float widthOfBoard = [UIScreen mainScreen].bounds.size.width -  2*15;
    float heightOfBoard = 5*2+sizeOfCharLb;
    
    // determine number of charlb for each row
    
    int numberOf1RowLbChar = (int)(([UIScreen mainScreen].bounds.size.width - edgePadding*2 + distanceBetweenCharLB)/(distanceBetweenCharLB+sizeOfCharLb));
    int numberOf2RowLbChar = 0;
    if(characterArray.count < numberOf1RowLbChar)
    {
        numberOf1RowLbChar = characterArray.count;
    }
    else if(characterArray.count > numberOf1RowLbChar)
    {
        numberOf2RowLbChar = characterArray.count - numberOf1RowLbChar;
    }
    
    if(numberOf2RowLbChar > 0)
    {
        heightOfBoard = 5*3+sizeOfCharLb*2;
    }
    
    
    UIView *board = [UIView new];
    board.userInteractionEnabled = true;
    board.frame = CGRectMake((gameView.frame.size.width - widthOfBoard)/2, imgView.frame.origin.y+imgView.frame.size.height + 20, widthOfBoard, heightOfBoard);
    board.backgroundColor = [UIColor whiteColor];
    board.layer.borderWidth = 1;
    [board.layer setBorderColor:[[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor]];
    [gameView addSubview:board];
     
    
    float xStart = ([UIScreen mainScreen].bounds.size.width - numberOf1RowLbChar*sizeOfCharLb - (numberOf1RowLbChar-1)*distanceBetweenCharLB)/2;
    float yStart = board.frame.origin.y + board.frame.size.height + 15;
    
    int count = 0;
    
    btInBoardArray = [NSMutableArray new];
    hintBtArray = [NSMutableArray new];
    
    for(int i=1; i<= numberOf1RowLbChar; i++)
    {
        CustomButton *btInBoard = [CustomButton new];
        btInBoard.frame = CGRectMake(xStart-15, 5, sizeOfCharLb, sizeOfCharLb);
        btInBoard.layer.borderWidth = 1;
        [btInBoard.layer setBorderColor:[[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor]];
        [btInBoard setBackgroundImage:[self imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        btInBoard.titleLabel.font = [UIFont systemFontOfSize:15];
        [btInBoard setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btInBoard addTarget:self action:@selector(chooseCharButton:) forControlEvents:UIControlEventTouchUpInside];
        [board addSubview:btInBoard];
        //btInBoard.hidden = true;
        
        [btInBoardArray addObject:btInBoard];
        
        UIButton *bthint = [UIButton new];
        bthint.frame = CGRectMake(xStart, yStart, sizeOfCharLb, sizeOfCharLb);
        bthint.layer.borderWidth = 1;
        [bthint.layer setBorderColor:[[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor]];
        [bthint setBackgroundImage:[self imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        bthint.titleLabel.font = [UIFont systemFontOfSize:15];
        [bthint setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [bthint setTitle:characterArray[count] forState:UIControlStateNormal];
        [bthint addTarget:self action:@selector(chooseCharButton:) forControlEvents:UIControlEventTouchUpInside];
        [gameView addSubview:bthint];
        btInBoard.hidden = true;
        
        [hintBtArray addObject:bthint];
        
        xStart += sizeOfCharLb + distanceBetweenCharLB;
        count++;
        
    }
    
    if(numberOf2RowLbChar > 0)
    {
        xStart = ([UIScreen mainScreen].bounds.size.width - numberOf2RowLbChar*sizeOfCharLb - (numberOf2RowLbChar-1)*distanceBetweenCharLB)/2;
        yStart = yStart + sizeOfCharLb+5;
        
        for(int i=1; i<= numberOf2RowLbChar; i++)
        {
            CustomButton *btInBoard = [CustomButton new];
            btInBoard.frame = CGRectMake(xStart-15, 5*2+sizeOfCharLb, sizeOfCharLb, sizeOfCharLb);
            btInBoard.layer.borderWidth = 1;
            [btInBoard.layer setBorderColor:[[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor]];
            [btInBoard setBackgroundImage:[self imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
            btInBoard.titleLabel.font = [UIFont systemFontOfSize:15];
            [btInBoard setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [btInBoard addTarget:self action:@selector(chooseCharButton:) forControlEvents:UIControlEventTouchUpInside];
            [board addSubview:btInBoard];
            btInBoard.hidden = true;
            
            [btInBoardArray addObject:btInBoard];
            
            UIButton *bthint = [UIButton new];
            bthint.frame = CGRectMake(xStart, yStart, sizeOfCharLb, sizeOfCharLb);
            bthint.layer.borderWidth = 1;
            [bthint.layer setBorderColor:[[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor]];
            [bthint setBackgroundImage:[self imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
            bthint.titleLabel.font = [UIFont systemFontOfSize:15];
            [bthint setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [bthint setTitle:characterArray[count] forState:UIControlStateNormal];
            [bthint addTarget:self action:@selector(chooseCharButton:) forControlEvents:UIControlEventTouchUpInside];
            [gameView addSubview:bthint];
            //btInBoard.hidden = true;
            
            [hintBtArray addObject:bthint];
            
            xStart += sizeOfCharLb + distanceBetweenCharLB;
            count++;
            
        }
    }
    

    float heightOfNextButton = 30;
    
    if([UIScreen mainScreen].bounds.size.height == 480)
    {
        heightOfNextButton = 25;
    }
    
    UIButton *nextButton = [UIButton new];
    [nextButton setTitle:@"Kiểm tra" forState:UIControlStateNormal];
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setBackgroundColor:[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1]];
    nextButton.frame = CGRectMake((gameView.frame.size.width-100)/2, gameView.frame.size.height - heightOfNextButton - 5, 100, heightOfNextButton);
    [nextButton addTarget:self action:@selector(checkButton:) forControlEvents:UIControlEventTouchUpInside];
    [gameView addSubview:nextButton];
    
    [CustomAnimation upperAnimationDuration:0.4
                                   WithView:imgView
                              andCompletion:^(BOOL finish)
     {
         
         
     }];
    
    
}


-(void)initEndingViewWithStars:(int)starNumber
{
    
    UIView *endingView = [UIView new];
    endingView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    endingView.userInteractionEnabled = true;
    endingView.backgroundColor = [UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1];
    [endingView setTag:1];
    [self.view addSubview:endingView];
    
    
    UILabel *infoLabel = [UILabel new];
    infoLabel.frame = CGRectMake(0, 100*[UIScreen mainScreen].bounds.size.height/568.0, [UIScreen mainScreen].bounds.size.width, 40);
    infoLabel.font = [UIFont boldSystemFontOfSize:25];
    infoLabel.textAlignment = NSTextAlignmentCenter;
    infoLabel.textColor = [UIColor whiteColor];
    infoLabel.backgroundColor = [UIColor clearColor];
    if(starNumber == 3)
    {
        infoLabel.text = @"Tuyệt vời";
    }
    else if (starNumber == 2)
    {
        infoLabel.text = @"Tốt";
    }
    else if (starNumber == 1)
    {
        infoLabel.text = @"Cố lên nào";
    }
    else if (starNumber == 0)
    {
        infoLabel.text = @"Cố lên nào";
    }
    
    [endingView addSubview:infoLabel];
    
    
    float sizeOfImg = 40*[UIScreen mainScreen].bounds.size.width/320.0;
    float xStart = ([UIScreen mainScreen].bounds.size.width - (sizeOfImg*3 + 10*2))/2;
    UIImageView *imgViewTemp;
    
    for(int i = 1; i<= 3; i++)
    {
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(xStart, infoLabel.frame.origin.y+infoLabel.frame.size.height+20, sizeOfImg, sizeOfImg)];
        imgView.image = [UIImage imageNamed:@"favorite.png"];
        
        if(i==1&& starNumber==1)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        else if((i==1 ||i ==2) && starNumber == 2)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        else if((i==1 ||i ==2 || i==3) && starNumber == 3)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        
        xStart += sizeOfImg + 10;
        [endingView addSubview:imgView];
        imgViewTemp = imgView;
    }
    
    
    float sizeOfBt = 50*[UIScreen mainScreen].bounds.size.width/320.0;
    
    UIButton *replayButton = [UIButton new];
    float xReplayBt = ([UIScreen mainScreen].bounds.size.width - sizeOfBt*2 - 40)/2;
    replayButton.frame = CGRectMake(xReplayBt, imgViewTemp.frame.origin.y+imgViewTemp.frame.size.height + 30, sizeOfBt, sizeOfBt);
    [replayButton setBackgroundImage:[UIImage imageNamed:@"icon_redo.png"] forState:UIControlStateNormal];
    [replayButton addTarget:self action:@selector(replay) forControlEvents:UIControlEventTouchUpInside];
    [endingView addSubview:replayButton];
    
    
    
    UIButton *nextButton = [UIButton new];
    nextButton.frame = CGRectMake(replayButton.frame.origin.x+sizeOfBt+40, imgViewTemp.frame.origin.y+imgViewTemp.frame.size.height + 30, sizeOfBt, sizeOfBt);
    [nextButton setBackgroundImage:[UIImage imageNamed:@"next_icon.png"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(backViewControllerWhenFinish) forControlEvents:UIControlEventTouchUpInside];
    [endingView addSubview:nextButton];
    

    if(starNumber > [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"%@/topic%d/sub%d/game%d",g_course,[self.topicID intValue],self.sub,[self.gameID intValue]]])
    {
        [[NSUserDefaults standardUserDefaults] setInteger:starNumber forKey:[NSString stringWithFormat:@"%@/topic%d/sub%d/game%d",g_course,[self.topicID intValue],self.sub,[self.gameID intValue]]];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [HttpThread upDateToServerGame:[self.gameID intValue] Topic:[self.topicID intValue] Course:g_course Sub:self.sub Star:starNumber];
        });
    }
    if(starNumber >= 1)
    {
        if([[[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"%@/topic%d/sub%d/game%d_islock",g_course,[self.topicID intValue],self.sub,[self.gameID intValue]+1]] isEqualToString:@"lock"])
        {
            [[NSUserDefaults standardUserDefaults] setValue:@"unlock" forKey:[NSString stringWithFormat:@"%@/topic%d/sub%d/game%d_islock",g_course,[self.topicID intValue],self.sub,[self.gameID intValue]+1]];
        }
    }
    
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


-(void)showResViewWithRes:(BOOL)res
{
    float heightOfResView = 50;
    float heightOfNextButton = 30;
    
    if([UIScreen mainScreen].bounds.size.height == 480)
    {
        heightOfResView = 40;
        heightOfNextButton = 25;
    }
    
    UIView *resView = [UIView new];
    resView.frame = CGRectMake(0, gameView.frame.size.height - 5 - heightOfNextButton - 5 -heightOfResView, gameView.frame.size.width, heightOfResView);
    if(!res) resView.backgroundColor = [UIColor colorWithRed:253.0/255.0 green:235.0/255.0 blue:218.0/255.0 alpha:1];
    else resView.backgroundColor = [UIColor colorWithRed:221.0/255.0 green:230.0/255.0 blue:244.0/255.0 alpha:1];
    [gameView addSubview:resView];
    
    UIImageView *check_wrongImgView;
    if(!res) check_wrongImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"check_wrong.png"]];
    else check_wrongImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"check_right.png"]];
    check_wrongImgView.frame = CGRectMake(10, (resView.frame.size.height - 20)/2, 20, 20);
    [check_wrongImgView setAlpha:1];
    [resView addSubview:check_wrongImgView];
    
    Lesson *ls = lessonArray[currentIndex];
    
    UILabel *resLb = [UILabel new];
    resLb.frame = CGRectMake(check_wrongImgView.frame.origin.x+check_wrongImgView.frame.size.width+10, 0, resView.frame.size.width - (check_wrongImgView.frame.origin.x+check_wrongImgView.frame.size.width+10), resView.frame.size.height);
    resLb.font = [UIFont boldSystemFontOfSize:17];
    resLb.textAlignment = NSTextAlignmentLeft;
    resLb.textColor = [UIColor blackColor];
    resLb.backgroundColor = [UIColor clearColor];
    resLb.text = [NSString stringWithFormat:@"%@: %@",ls.hiragana, ls.vietnamese];
    [resView addSubview:resLb];
    
    UIButton *nextButton = [UIButton new];
    [nextButton setTitle:@"Tiếp tục" forState:UIControlStateNormal];
    if(currentIndex == lessonArray.count-1)
    {
        [nextButton setTitle:@"Kết thúc" forState:UIControlStateNormal];
    }
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setBackgroundColor:[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1]];
    nextButton.frame = CGRectMake((gameView.frame.size.width-100)/2, gameView.frame.size.height - heightOfNextButton - 5, 100, heightOfNextButton);
    [nextButton addTarget:self action:@selector(nextLesson) forControlEvents:UIControlEventTouchUpInside];
    [gameView addSubview:nextButton];
    
    
    NSString *audioFilePath;
    if(!res) audioFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"fail.mp3"];
    else audioFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"true.mp3"];
    NSURL *url = [[NSURL alloc] initFileURLWithPath:audioFilePath];
    resultPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    resultPlayer.numberOfLoops = 0;
    resultPlayer.delegate = self;
    [resultPlayer play];
}



#pragma mark- init button

-(IBAction)checkButton:(UIButton*)sender
{
    
    NSString *guessStr = @"";
    
    for(CustomButton *bt in btInBoardArray)
    {
        if(bt.hidden == false)
        {
            guessStr = [guessStr stringByAppendingString:bt.titleLabel.text];
        }
    }
    
    Lesson *ls = lessonArray[currentIndex];
    
    if([guessStr isEqualToString:ls.hiragana])
    {
        NSLog(@"true");
        [self handleWhenTrueAnswer];
    }
    else
    {
        NSLog(@"false");
        [self handleWhenFalseAnswer];
    }
    
}


-(IBAction)chooseCharButton:(id)sender
{
    
    if([sender isKindOfClass:[CustomButton class]])
    {
        CustomButton *bt = (CustomButton*)sender;
        bt.button.hidden = false;
        bt.button = NULL;
        bt.hidden = true;
        
    }
    else
    {
        UIButton *bt = (UIButton*)sender;
        bt.hidden = true;
        CustomButton *customBt = btInBoardArray[[self getMinEmptyIndex]];
        customBt.button = bt;
        [customBt setTitle:bt.titleLabel.text forState:UIControlStateNormal];
        customBt.hidden = false;
    }
    
}


-(IBAction)replayAudio:(UITapGestureRecognizer*)sender
{
    CircularProgressView *button  = (CircularProgressView*)[gameView viewWithTag:1];
    [button.btnPlay setImage:[UIImage imageNamed:@"icon_stop.png"] forState:UIControlStateNormal];
    [button play];
}

-(void)backViewController
{
    NSString *mes = @"Bạn có chắc chắn muốn thoát game này ?";
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:mes
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    
    [alert show];
}


-(void)backViewControllerWhenFinish
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) replay
{
    for(UIView *view in self.view.subviews)
    {
        [view removeFromSuperview];
    }
    
    [self initView];
    
}

-(void)nextLesson
{
    if(currentIndex < lessonArray.count)
    {
        
        [gameView removeFromSuperview];
        
        [self initGameView];
    }
    else
    {
        //        double delayInSeconds = 1.0;
        //        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        //        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //
        //        });
        
        [self initEndingViewWithStars:[self getStars]];
    }
}



#pragma mark private method

-(int)getMinEmptyIndex
{
    int index = 0;
    
    for(UIButton *bt in btInBoardArray)
    {
        if(bt.hidden == true)
        {
            index = [btInBoardArray indexOfObject:bt];
            break;
        }
    }
    
    return index;
}


-(void)updateRateDay
{
    NSDate *previousDay = [[NSUserDefaults standardUserDefaults] objectForKey:@"rateDay"];
    if(!previousDay)
    {
        showRateView = true;
        
    }
    else
    {
        NSTimeInterval time = [[NSDate date] timeIntervalSinceDate:previousDay];
        if(time > 24*3600)
        {
            showRateView = true;
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"rateDay"];
    
}



-(void)handleWhenFalseAnswer
{
    NSTimer *t;
    t = [NSTimer scheduledTimerWithTimeInterval:0.0
                                         target:self
                                       selector:@selector(timerWhenFalse:)
                                       userInfo:nil
                                        repeats:NO];
}

-(void)handleWhenTrueAnswer
{
    NSTimer *t;
    t = [NSTimer scheduledTimerWithTimeInterval:0.0
                                         target:self
                                       selector:@selector(timerWhenTrue:)
                                       userInfo:nil
                                        repeats:NO];
}

-(int)getStars
{
    int stars = 0;
    
    if((float)totalTrueAnswer/lessonArray.count >= 0.8)
    {
        stars = 3;
    }
    else if((float)totalTrueAnswer/lessonArray.count >= 0.5)
    {
        stars = 2;
    }
    else if((float)totalTrueAnswer/lessonArray.count >= 0.3)
    {
        stars = 1;
    }
    
    return stars;

}


-(void)timerWhenFalse:(NSTimer*)timer
{
    
    [self showResViewWithRes:false];
    
    currentIndex++;
    
    [progressView setPercentComplete:currentIndex];
    
    
}

-(void)timerWhenTrue:(NSTimer*)timer
{
    
    [self showResViewWithRes:true];
    
    totalTrueAnswer++;
    currentIndex++;
    
    [progressView setPercentComplete:currentIndex];
    
}


-(NSMutableArray*)getCharacterArray
{
    Lesson *ls = lessonArray[currentIndex];
    NSMutableArray *ar = [NSMutableArray new];
    
    for(int i = 0; i < ls.hiragana.length; i++)
    {
        [ar addObject:[ls.hiragana substringWithRange:NSMakeRange(i, 1)]];
    }
    
    int count  = 1;
    
    for(NSString *ch in fullCharArray)
    {
        if(![ar containsObject:ch])
        {
            [ar addObject:ch];
            count ++;
        }
        
        if (count == 3) break;
    }
    
    NSMutableArray *chAr = [NSMutableArray new];

    
    count = ar.count;
    
    for(int i = 0; i < count ; i++)
    {
        int random = arc4random_uniform(ar.count);
        
        id ls  = ar[random];
        
        [chAr addObject:ls];
        [ar removeObjectAtIndex:random];
    }
    
    return chAr;
}


#pragma mark- avaudio delegate

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [player stop];
    
    if(player == resultPlayer)
    {
        
    }
}


#pragma mark- alert view delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
