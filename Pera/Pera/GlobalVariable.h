//
//  GlobalVariable.h
//  Pera
//
//  Created by Le Trong Thao on 12/2/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GlobalVariable : NSObject

FOUNDATION_EXPORT NSString *g_language;
FOUNDATION_EXPORT NSString *g_course;
FOUNDATION_EXPORT UIViewController *mainMenuViewController;

FOUNDATION_EXPORT NSString *emailAccount;
FOUNDATION_EXPORT NSString *feedbackTitle;
FOUNDATION_EXPORT NSString *appLink;
FOUNDATION_EXPORT NSString *fbShare;

FOUNDATION_EXPORT BOOL showRateView;

FOUNDATION_EXPORT UIViewController *currentViewController;

FOUNDATION_EXPORT UITableViewCell *preCell;

FOUNDATION_EXPORT CGFloat leftMenuWidth;
@end
