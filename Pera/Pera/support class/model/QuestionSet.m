//
//  QuestionSet.m
//  Pera
//
//  Created by Le Trong Thao on 12/5/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import "QuestionSet.h"
#import "LessonHandle.h"

@implementation QuestionSet


-(void)setSequenceNummberForLesson
{
    int total = 1 + self.falseAnswerArray.count;
    
    NSMutableArray *sequenceNumberArray = [NSMutableArray new];
    
    for(int i = 1; i <= total; i++)
    {
        [sequenceNumberArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    sequenceNumberArray  = [LessonHandle rearrangeElementRandomly:sequenceNumberArray];
    
    self.answer.sequenceNumber = [sequenceNumberArray[0] intValue];
    
    for(int i = 2; i<=total;i++)
    {
        Lesson *ls = self.falseAnswerArray[i-2];
        ls.sequenceNumber = [sequenceNumberArray[i-1] intValue];
    }
    
}

@end
