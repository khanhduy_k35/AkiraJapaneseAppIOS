//
//  ProgressView.h
//  testAudioRecode
//
//  Created by LeDuy on 12/3/15.
//  Copyright © 2015 LeDuy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressView : UIView

@property float currently;
@property float maxValue;
@property (strong,nonatomic) UIView *viewComplete;

- (id)initWithFrame:(CGRect)frame withMax : (float) maxValue;
- (void) setPercentComplete : (float) currentValue;
@end
