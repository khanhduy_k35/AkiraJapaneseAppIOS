//
//  CustomProgressBarView.h
//  Pera
//
//  Created by Le Trong Thao on 12/3/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomProgressBarView : UIView

@property UIView *processView;
@property CGFloat maxProcess;

-(CustomProgressBarView*)initWithFrame:(CGRect)rect andCornerRadius:(CGFloat)radious;
-(void)setMaxProcessWith:(CGFloat)max;
-(void)setProcessViewColor:(UIColor*)processColor;
-(void)updateProcessWithCurrentProcess:(CGFloat)current;

@end
