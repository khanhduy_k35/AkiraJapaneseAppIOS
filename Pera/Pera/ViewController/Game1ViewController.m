//
//  Game1ViewController.m
//  Pera
//
//  Created by Le Trong Thao on 12/4/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import "Game1ViewController.h"
#import "ProgressView.h"
#import "CircularProgressView.h"
#import "UIViewController+AMSlideMenu.h"
#import "GameUtil.h"
#import "GlobalVariable.h"

#import "LessonHandle.h"
#import "DataHandle.h"

#import "CustomAnimation.h"
#import "HttpThread.h"

@interface Game1ViewController ()<CircularProgressViewDelegate, UIAlertViewDelegate>
{
    ProgressView *progressView;
    UIView *gameView;
    
    NSMutableArray *lessonArray;
    
    
    int currentIndex;
    
}

@end

@implementation Game1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIView *downloadView = [self.navigationController.view viewWithTag:10];
    if(downloadView)
    {
        [downloadView setAlpha:0];
    }
    
    [self disableSlidePanGestureForLeftMenu];
    
    [self initView];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    currentViewController = self;
}

-(void)viewWillDisappear:(BOOL)animated
{
   
}

#pragma mark init View

-(void)initView
{
    currentIndex = 0;
    lessonArray = [DataHandle getAllLessonOfTopic:[self.topicID intValue] Level:g_course Sub:self.sub];
    lessonArray = [LessonHandle rearrangeElementRandomly:lessonArray];
    
    UIButton *backButton = [UIButton new];
    float heightOfBt = 36;
    float widthOfBt = 36;
    backButton.frame = [GameUtil transformFitToScree:CGRectMake(8, 30 , widthOfBt, heightOfBt) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    [backButton setBackgroundImage:[UIImage imageNamed:@"abc_ic_ab_back_mtrl_am_alpha_blue.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backViewController) forControlEvents:UIControlEventTouchUpInside];
    //backButton.backgroundColor = [UIColor blackColor];
    [self.view addSubview:backButton];
    
    
    
    float xProgressView = backButton.frame.origin.x + backButton.frame.size.width + 10;
    float widthProgressView = [UIScreen mainScreen].bounds.size.width - xProgressView*2;
    float heightProgressView = 6;
    
    progressView = [[ProgressView alloc] initWithFrame:CGRectMake(xProgressView, backButton.frame.origin.y+backButton.frame.size.height/2-heightProgressView/2, widthProgressView, heightProgressView) withMax:lessonArray.count];
    [self.view addSubview:progressView];
    
    
    UILabel *topLabel = [UILabel new];
    topLabel.frame = CGRectMake(10, backButton.frame.origin.y+backButton.frame.size.height+5, [UIScreen mainScreen].bounds.size.width-20,35);
    topLabel.font = [UIFont boldSystemFontOfSize:15];
    topLabel.numberOfLines = 0;
    topLabel.textAlignment = NSTextAlignmentCenter;
    topLabel.textColor = [UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1];
    topLabel.backgroundColor = [UIColor clearColor];
    topLabel.text = @"Học từ mới";
    [self.view addSubview:topLabel];
    
    [self initGameView];
}


-(void)initGameView
{
    Lesson *ls = lessonArray[currentIndex];
    
    gameView = [UIView new];
    float yGameView = 125;
    gameView.frame = [GameUtil transformFitToScree:CGRectMake(0, yGameView, 320, 568 - yGameView) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    gameView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:gameView];
    
    
    float sizeOfImg = 200*[UIScreen mainScreen].bounds.size.width/320;
    float yImgView = 35*gameView.frame.size.height/448;
    NSString *imgFilePath = [LessonHandle getImgFilePathOfFileName:ls.fileName andTopic:[self.topicID intValue] Level:g_course];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:imgFilePath]];
    if(self.sub == 4)imgView.image = NULL;
    imgView.userInteractionEnabled = true;
    [imgView setTag:2];
    imgView.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width-sizeOfImg)/2, yImgView, sizeOfImg, sizeOfImg);
    [imgView.layer setBorderWidth:1];
    [imgView.layer setBorderColor:[[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor]];
    [gameView addSubview:imgView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(replayAudio:)];
    tap.numberOfTouchesRequired = 1;
    [imgView addGestureRecognizer:tap];
    
    
    //UILabel *

    [CustomAnimation upperAnimationDuration:0.4
                        WithView:imgView
                   andCompletion:^(BOOL finish)
     {
         float sizeOfCircleProgress = 55*[UIScreen mainScreen].bounds.size.width/320;
         CircularProgressView *circleProgress = [[CircularProgressView alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-sizeOfCircleProgress)/2, 0, sizeOfCircleProgress, sizeOfCircleProgress)
                                                                                  backColor:[UIColor colorWithRed:236.0 / 255.0 green:236.0 / 255.0 blue:236.0 / 255.0 alpha:1.0] progressColor:[UIColor colorWithRed:82.0 / 255.0 green:135.0 / 255.0 blue:237.0 / 255.0 alpha:1.0]
                                                                                  lineWidth:10
                                                                                   audioURL:[LessonHandle getAudioUrlOfFileName:ls.fileName TopicID:[self.topicID intValue] Level:g_course]
                                                                                    imgPlay:@"icon_play.png"
                                                                                    imgStop:@"icon_stop.png"];
         circleProgress.delegate = self;
         [circleProgress.btnPlay setImage:[UIImage imageNamed:@"icon_stop.png"] forState:UIControlStateNormal];
         [circleProgress setTag:1];
         [gameView addSubview:circleProgress];
         [circleProgress play];
         
         
         
         if(self.sub == 4)
         {
             float heightOfLB = (imgView.frame.origin.y + imgView.frame.size.height - (circleProgress.frame.origin.y+circleProgress.frame.size.height) - 10*3 - 3*2)/4 - 1;
             
             UILabel *hiLb = [UILabel new];
             hiLb.frame = CGRectMake(imgView.frame.origin.x, circleProgress.frame.origin.y+circleProgress.frame.size.height+3, imgView.frame.size.width, heightOfLB);
             hiLb.font = [UIFont systemFontOfSize:15];
             hiLb.textAlignment = NSTextAlignmentCenter;
             hiLb.textColor = [UIColor blackColor];
             hiLb.backgroundColor = [UIColor clearColor];
             hiLb.text = ls.hiragana;
             [gameView addSubview:hiLb];
             
             UILabel *rule1 = [UILabel new];
             rule1.frame = CGRectMake(imgView.frame.origin.x + 15, hiLb.frame.origin.y+hiLb.frame.size.height, imgView.frame.size.width-30, 1);
             rule1.backgroundColor = [UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1];
             [gameView addSubview:rule1];
             
             
             UILabel *kaLb = [UILabel new];
             kaLb.frame = CGRectMake(imgView.frame.origin.x, rule1.frame.origin.y+rule1.frame.size.height+10, imgView.frame.size.width, heightOfLB);
             kaLb.font = [UIFont systemFontOfSize:15];
             kaLb.textAlignment = NSTextAlignmentCenter;
             kaLb.textColor = [UIColor blackColor];
             kaLb.backgroundColor = [UIColor clearColor];
             kaLb.text = ls.kanji;
             [gameView addSubview:kaLb];
             
             UILabel *rule2 = [UILabel new];
             rule2.frame = CGRectMake(imgView.frame.origin.x + 15, kaLb.frame.origin.y+kaLb.frame.size.height, imgView.frame.size.width-30, 1);
             rule2.backgroundColor = [UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1];
             [gameView addSubview:rule2];
             
             
             UILabel *roLb = [UILabel new];
             roLb.frame = CGRectMake(imgView.frame.origin.x, rule2.frame.origin.y+rule2.frame.size.height+10, imgView.frame.size.width, heightOfLB);
             roLb.font = [UIFont systemFontOfSize:15];
             roLb.textAlignment = NSTextAlignmentCenter;
             roLb.textColor = [UIColor blackColor];
             roLb.backgroundColor = [UIColor clearColor];
             roLb.text = ls.romaji;
             [gameView addSubview:roLb];
             
             UILabel *rule3 = [UILabel new];
             rule3.frame = CGRectMake(imgView.frame.origin.x + 15, roLb.frame.origin.y+roLb.frame.size.height, imgView.frame.size.width-30, 1);
             rule3.backgroundColor = [UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1];
             [gameView addSubview:rule3];
             
             
             UILabel *viLb = [UILabel new];
             viLb.frame = CGRectMake(imgView.frame.origin.x, rule3.frame.origin.y+rule3.frame.size.height+10, imgView.frame.size.width, heightOfLB);
             viLb.font = [UIFont systemFontOfSize:15];
             viLb.textAlignment = NSTextAlignmentCenter;
             viLb.textColor = [UIColor blackColor];
             viLb.backgroundColor = [UIColor clearColor];
             viLb.text = ls.vietnamese;
             [gameView addSubview:viLb];
             
         }
         
         
         
         [self initWordLabelsWithLesson:ls];
         
     }];
    
}


-(void)initWordLabelsWithLesson:(Lesson*)ls
{
    UIImageView *imgView = (UIImageView*)[gameView viewWithTag:2];
    
    UILabel *learnedWordLabel = [UILabel new];
    learnedWordLabel.frame = CGRectMake(0, imgView.frame.origin.y+imgView.frame.size.height+5, [UIScreen mainScreen].bounds.size.width, 30);
    learnedWordLabel.font = [UIFont boldSystemFontOfSize:17];
    learnedWordLabel.backgroundColor = [UIColor clearColor];
    learnedWordLabel.textAlignment = NSTextAlignmentCenter;
    learnedWordLabel.textColor =  [UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1];
    if(self.sub != 4) learnedWordLabel.text = ls.hiragana;
    
    [gameView addSubview:learnedWordLabel];
    
    
    UILabel *pronounLabel = [UILabel new];
    pronounLabel.frame = CGRectMake(0, learnedWordLabel.frame.origin.y+learnedWordLabel.frame.size.height, [UIScreen mainScreen].bounds.size.width, 0);
    CGSize constraint = CGSizeMake([UIScreen mainScreen].bounds.size.width-30, 20000.0f);
    CGSize size ;
    
    pronounLabel.frame = CGRectMake(0, pronounLabel.frame.origin.y, [UIScreen mainScreen].bounds.size.width, 30);
    pronounLabel.font = [UIFont boldSystemFontOfSize:17];
    pronounLabel.backgroundColor = [UIColor clearColor];
    pronounLabel.textAlignment = NSTextAlignmentCenter;
    pronounLabel.textColor =  [UIColor blackColor];
    pronounLabel.numberOfLines = 0;
    if(self.sub != 4)pronounLabel.text  = ls.romaji;
    size = [ls.romaji sizeWithFont:[UIFont boldSystemFontOfSize:17] constrainedToSize:constraint lineBreakMode:UILineBreakModeWordWrap];
    pronounLabel.frame =  CGRectMake(15, pronounLabel.frame.origin.y + 5, [UIScreen mainScreen].bounds.size.width-30, size.height + 10);
    
    [gameView addSubview:pronounLabel];
    
    UILabel *meanLabel = [UILabel new];
    meanLabel.frame = CGRectMake(0, pronounLabel.frame.origin.y+pronounLabel.frame.size.height, [UIScreen mainScreen].bounds.size.width, 30);
    meanLabel.font = [UIFont systemFontOfSize:17];
    meanLabel.backgroundColor = [UIColor clearColor];
    meanLabel.textAlignment = NSTextAlignmentCenter;
    meanLabel.textColor =  [UIColor blackColor];
    if(self.sub != 4) meanLabel.text = ls.vietnamese;
    
    [gameView addSubview:meanLabel];
    
    
    UIButton *nexButton = [UIButton new];
    float widthOfNextButton = 130*[UIScreen mainScreen].bounds.size.width/320.0;
    //float heightOfNextButton = 40*[UIScreen mainScreen].bounds.size.height/568.0;
    float heightOfNextButton;
    
    if(self.sub != 4)
    {
        if([UIScreen mainScreen].bounds.size.height == 480)
            heightOfNextButton = (gameView.frame.size.height - meanLabel.frame.origin.y - meanLabel.frame.size.height)*3/5;
        else
            heightOfNextButton = 40*[UIScreen mainScreen].bounds.size.height/568.0;
    }
    else
    {
        heightOfNextButton = 40;
    }
    
    nexButton.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width-widthOfNextButton)/2, meanLabel.frame.origin.y+meanLabel.frame.size.height+(gameView.frame.size.height - meanLabel.frame.origin.y - meanLabel.frame.size.height)/2-heightOfNextButton/2, widthOfNextButton, heightOfNextButton);
    //nexButton.layer.cornerRadius = 4;
    
    if(self.sub == 4)
    {
        nexButton.frame = CGRectMake(nexButton.frame.origin.x, nexButton.frame.origin.y - 5, nexButton.frame.size.width, nexButton.frame.size.height);
    }
    
    [nexButton setBackgroundColor:[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1]];
    NSString *next =  @"Tiếp tục";
    [nexButton setTitle:next forState:UIControlStateNormal];
    [nexButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nexButton setTag:3];
    nexButton.hidden = true;
    [nexButton addTarget:self action:@selector(nextLesson) forControlEvents:UIControlEventTouchUpInside];
    [gameView addSubview:nexButton];
    
    if(![LessonHandle checkAudioUrlOfFileName:ls.fileName TopicID:[self.topicID intValue] Level:g_course])
    {
        nexButton.hidden = false;
    }
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(void)initEndingViewWithStars:(int)starNumber
{
    
    UIView *endingView = [UIView new];
    endingView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    endingView.userInteractionEnabled = true;
    endingView.backgroundColor = [UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1];
    [endingView setTag:1];
    [self.view addSubview:endingView];
    
    
    UILabel *infoLabel = [UILabel new];
    infoLabel.frame = CGRectMake(0, 100*[UIScreen mainScreen].bounds.size.height/568.0, [UIScreen mainScreen].bounds.size.width, 40);
    infoLabel.font = [UIFont boldSystemFontOfSize:25];
    infoLabel.textAlignment = NSTextAlignmentCenter;
    infoLabel.textColor = [UIColor whiteColor];
    infoLabel.backgroundColor = [UIColor clearColor];
    if(starNumber == 3)
    {
        infoLabel.text = @"Tuyệt vời";
    }
    else if (starNumber == 2)
    {
        infoLabel.text = @"Tốt";
    }
    else if (starNumber == 1)
    {
        infoLabel.text = @"Cố lên nào";
    }
    else if (starNumber == 0)
    {
        infoLabel.text = @"Cố lên nào";
    }
    
    [endingView addSubview:infoLabel];
    
    
    float sizeOfImg = 40*[UIScreen mainScreen].bounds.size.width/320.0;
    float xStart = ([UIScreen mainScreen].bounds.size.width - (sizeOfImg*3 + 10*2))/2;
    UIImageView *imgViewTemp;
    
    for(int i = 1; i<= 3; i++)
    {
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(xStart, infoLabel.frame.origin.y+infoLabel.frame.size.height+20, sizeOfImg, sizeOfImg)];
        imgView.image = [UIImage imageNamed:@"favorite.png"];
        
        if(i==1&& starNumber==1)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        else if((i==1 ||i ==2) && starNumber == 2)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        else if((i==1 ||i ==2 || i==3) && starNumber == 3)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        
        xStart += sizeOfImg + 10;
        [endingView addSubview:imgView];
        imgViewTemp = imgView;
    }
    
    
    float sizeOfBt = 50*[UIScreen mainScreen].bounds.size.width/320.0;
    
    UIButton *replayButton = [UIButton new];
    float xReplayBt = ([UIScreen mainScreen].bounds.size.width - sizeOfBt*2 - 40)/2;
    replayButton.frame = CGRectMake(xReplayBt, imgViewTemp.frame.origin.y+imgViewTemp.frame.size.height + 30, sizeOfBt, sizeOfBt);
    [replayButton setBackgroundImage:[UIImage imageNamed:@"icon_redo.png"] forState:UIControlStateNormal];
    [replayButton addTarget:self action:@selector(replay) forControlEvents:UIControlEventTouchUpInside];
    [endingView addSubview:replayButton];
    
    
    
    UIButton *nextButton = [UIButton new];
    nextButton.frame = CGRectMake(replayButton.frame.origin.x+sizeOfBt+40, imgViewTemp.frame.origin.y+imgViewTemp.frame.size.height + 30, sizeOfBt, sizeOfBt);
    [nextButton setBackgroundImage:[UIImage imageNamed:@"next_icon.png"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(backViewControllerWhenFinish) forControlEvents:UIControlEventTouchUpInside];
    [endingView addSubview:nextButton];
    
    
    if(starNumber > [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"%@/topic%d/sub%d/game%d",g_course,[self.topicID intValue],self.sub,[self.gameID intValue]]])
    {
        [[NSUserDefaults standardUserDefaults] setInteger:starNumber forKey:[NSString stringWithFormat:@"%@/topic%d/sub%d/game%d",g_course,[self.topicID intValue],self.sub,[self.gameID intValue]]];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [HttpThread upDateToServerGame:[self.gameID intValue] Topic:[self.topicID intValue] Course:g_course Sub:self.sub Star:starNumber];
        });
    }
    if([[[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"%@/topic%d/sub%d/game%d_islock",g_course,[self.topicID intValue],self.sub,[self.gameID intValue]+1]] isEqualToString:@"lock"])
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"unlock" forKey:[NSString stringWithFormat:@"%@/topic%d/sub%d/game%d_islock",g_course,[self.topicID intValue],self.sub,[self.gameID intValue]+1]];
    }
    
}

#pragma mark- button

-(void)backViewController
{
    NSString *mes = @"Bạn có chắc chắn muốn thoát game này ?";

    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:mes
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    
    [alert show];
}

-(IBAction)replayAudio:(UITapGestureRecognizer*)sender
{
    CircularProgressView *button  = (CircularProgressView*)[gameView viewWithTag:1];
    [button.btnPlay setImage:[UIImage imageNamed:@"icon_stop.png"] forState:UIControlStateNormal];
    [button play];
}

-(void)nextLesson
{
    CircularProgressView *button  = (CircularProgressView*)[gameView viewWithTag:1];
    if(button!=nil){
        [button stop];
    }
    [progressView setPercentComplete:currentIndex+1];
    currentIndex = currentIndex + 1;
    [gameView removeFromSuperview];
    
    if(currentIndex < lessonArray.count)
        [self initGameView];
    else
    {
        [self initEndingViewWithStars:3];
    }
}

-(void)backViewControllerWhenFinish
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) replay
{
    for(UIView *view in self.view.subviews)
    {
        [view removeFromSuperview];
    }
    
    [self initView];
    
}

#pragma mark- CircularProgressViewDelegate

-(void)playerDidFinishPlaying
{
    NSLog(@"playerDidFinishPlaying");
    CircularProgressView *circleProgress = (CircularProgressView*)[gameView viewWithTag:1];
    
    [circleProgress.btnPlay setImage:[UIImage imageNamed:@"icon_play.png"] forState:UIControlStateNormal];
    
    if(currentIndex <= lessonArray.count-2)
    {
        UIButton *nextButton = (UIButton*)[gameView viewWithTag:3];
        nextButton.hidden = false;
    }
    else
    {
        [UIView animateWithDuration:0.5
                         animations:^{
                             [progressView setPercentComplete:currentIndex+1];
            
        }
                         completion:^(BOOL finish){
                             [self initEndingViewWithStars:3];
            
        }];
        
        
        NSLog(@"get 3 stars");
    }
}

-(void)updateProgressViewWithPlayer:(AVAudioPlayer *)player
{
    
}


#pragma mark- alert view delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark- private method

-(void)updateRateDay
{
    NSDate *previousDay = [[NSUserDefaults standardUserDefaults] objectForKey:@"rateDay"];
    if(!previousDay)
    {
        showRateView = true;
        
    }
    else
    {
        NSTimeInterval time = [[NSDate date] timeIntervalSinceDate:previousDay];
        if(time > 24*3600)
        {
            showRateView = true;
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"rateDay"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
