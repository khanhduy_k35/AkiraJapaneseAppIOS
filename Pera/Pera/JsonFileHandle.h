//
//  JsonFileHandle.h
//  Pera
//
//  Created by Le Trong Thao on 12/1/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Lesson.h"

@interface JsonFileHandle : NSObject


+(NSString*)getLocalTitleWithType:(NSString*)type andLanguageKey:(NSString*)languageKey;
+(NSMutableArray*)getTopicTitlesWithLanguageKey:(NSString*)languageKey;
+(NSMutableArray*)getAllLessonOfTopic:(NSString*)topicID andCourse:(NSString*)cource;


@end
