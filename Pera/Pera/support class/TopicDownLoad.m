//
//  TopicDownLoad.m
//  Pera
//
//  Created by Le Trong Thao on 12/7/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import "TopicDownLoad.h"
#import "SSZipArchive.h"
#import "GlobalVariable.h"
#import "AppDelegate.h"
#import "ProgressView.h"
@implementation TopicDownLoad


-(void)downloadDataForTopic:(NSString *)topic Level:(NSString *)level onView:(UIView *)view completion:(void (^)(BOOL))complete
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *downloadFolder = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/courses/%@/%@",level,topic]];
    
    NSString *urlStr = [NSString stringWithFormat:@"http://data.akirajapanese.com/%@/%@/vocab.zip",level,topic];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    self.sharedDownloadManager = [TCBlobDownloadManager sharedInstance];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIView *downLoadView = [UIView new];
        downLoadView.backgroundColor = [UIColor whiteColor];
        downLoadView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 50, [UIScreen mainScreen].bounds.size.width, 50);
        downLoadView.tag = 10;
        
        
        UILabel *downLoadLb = [UILabel new];
        downLoadLb.tag = 1;
        downLoadLb.frame = CGRectMake(10, 0, downLoadView.frame.size.width - 10, 25);
        downLoadLb.font = [UIFont systemFontOfSize:15];
        downLoadLb.textAlignment = NSTextAlignmentLeft;
        downLoadLb.textColor = [UIColor blackColor];
        downLoadLb.text = @"Download: 0%";
        [downLoadView addSubview:downLoadLb];
        
        ProgressView *progressView = [[ProgressView alloc] initWithFrame:CGRectMake(10, downLoadLb.frame.origin.y+downLoadLb.frame.size.height+7, downLoadView.frame.size.width - 20, 7) withMax:100];
        progressView.viewComplete.backgroundColor = [UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1];
        progressView.tag = 2;
        [downLoadView addSubview:progressView];
        
        [view addSubview:downLoadView];
        
    });
    
    
    
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isDownload = true;
    
    [self.sharedDownloadManager startDownloadWithURL:url
                                          customPath:downloadFolder
                                       firstResponse:NULL
                                            progress:^(uint64_t receivedLength, uint64_t totalLength, NSInteger remainingTime, float progress) {
                                                if (remainingTime != -1) {
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        UIView *downLoadView = [view viewWithTag:10];
                                                        
                                                        UILabel *downLoadLb = (UILabel*)[downLoadView viewWithTag:1];
                                                        
                                                        ProgressView *progressView = (ProgressView*)[downLoadView viewWithTag:2];
                                                        
                                                        [progressView setPercentComplete:(int) (progress*100)];
                                                        downLoadLb.text = [NSString stringWithFormat:@"Downloading: %d%%",(int) (progress*100)];
                                                        
                                                    });
                                                    
                                                    //NSLog(@"%d%%",(int)(progress*100));
                                                }
                                                
                                            }
                                               error:^(NSError *error) {
                                                   NSLog(@"fail to download with error: %@", error);
                                               }
                                            complete:^(BOOL downloadFinished, NSString *pathToFile) {
                                                
                                                
                                                if(downloadFinished)
                                                {
                                                    NSLog(@"complete download");
                                                    
                                                    NSString *zipFilePath = [downloadFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"vocab.zip"]];
                                                    [SSZipArchive unzipFileAtPath:zipFilePath toDestination:downloadFolder];
                                                    
                                                    
                                                }
                                                else
                                                {
                                                    NSLog(@"fail to download ");
                                                    [[[UIAlertView alloc] initWithTitle:@"Thông báo"
                                                                                message:@"Mạng bị lỗi"
                                                                               delegate:nil cancelButtonTitle:@"OK"
                                                                      otherButtonTitles: nil]
                                                     show];
                                                    
                                                    
                                                }
                                                
                                                [[NSFileManager defaultManager] removeItemAtPath:[downloadFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"vocab.zip"]]
                                                                                           error:nil];
                                                
                                                ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isDownload = false;
                                                
                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                    
                                                    UIView *downLoadView = [view viewWithTag:10];
                                                    [downLoadView removeFromSuperview];
                                                    
                                                    complete(downloadFinished);
                                                    
                                                });
                                                
                                                
                                            }];

}

#pragma mark check the network

-(BOOL)currentNetworkStatus
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}


@end
