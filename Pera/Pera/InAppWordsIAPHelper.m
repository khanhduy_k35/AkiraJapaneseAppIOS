#import "InAppWordsIAPHelper.h"


@implementation InAppWordsIAPHelper

static InAppWordsIAPHelper * _sharedHelper;

+ (InAppWordsIAPHelper *) sharedHelper {
    
    if (_sharedHelper != nil) {
        return _sharedHelper;
    }
    _sharedHelper = [[InAppWordsIAPHelper alloc] init];
    return _sharedHelper;
    
}

- (id)init {

    NSSet *productIdentifiers = [NSSet setWithObjects:
                                 @"course_vi", @"course_zh", @"course_kr", @"course_en", @"course_jp", @"cource_all",
                                 nil];
    
    NSMutableDictionary *prices = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"$4.99",  @"course_vi",
                                   @"$4.99",  @"course_zh",
                                   @"$4.99",  @"course_kr",
                                   @"$4.99",  @"course_en",
                                   @"$4.99",  @"course_jp",
                                   @"$9.99",  @"cource_all",
                                   nil];
    
    NSDictionary *coins = [NSDictionary dictionaryWithObjectsAndKeys:
                           [NSNumber numberWithInt:0],  @"course_vi",
                           [NSNumber numberWithInt:0],  @"course_zh",
                           [NSNumber numberWithInt:0],  @"course_kr",
                           [NSNumber numberWithInt:0],  @"course_en",
                           [NSNumber numberWithInt:0],  @"course_jp",
                           [NSNumber numberWithInt:0],  @"cource_all",
                           nil];
   
    if ((self = [super initWithProductIdentifiers:productIdentifiers prices:prices coins:coins])) {
        
    }
    return self;
    
}

@end
