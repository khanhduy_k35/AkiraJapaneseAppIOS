//
//  Game6ViewController.h
//  Pera
//
//  Created by Le Trong Thao on 3/2/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface Game6ViewController : UIViewController

@property NSString *topicID;
@property NSString *gameID;
@property int sub;

@end
