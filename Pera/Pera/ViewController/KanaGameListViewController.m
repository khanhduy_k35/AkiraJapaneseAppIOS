//
//  KanaGameListViewController.m
//  Pera
//
//  Created by Le Trong Thao on 3/3/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import "KanaGameListViewController.h"
#import "UIViewController+AMSlideMenu.h"
#import "GlobalVariable.h"
#import "DataHandle.h"
#import "GameUtil.h"
#import "StarBar.h"
#import "Toast.h"


#import "KanaGame1ViewController.h"
#import "KanaGame5ViewController.h"
#import "KanaGame6ViewController.h"
#import "KanaGame2ViewController.h"
#import "KanaGame3ViewController.h"
#import "KanaGame4ViewController.h"

@interface KanaGameListViewController ()<UITableViewDataSource, UITableViewDelegate>

@end

@implementation KanaGameListViewController
{
    NSMutableArray *gameAr;
    
    float heightOfCell;
    
    UIView *topView;
    UIView *topLabelView;
    
    UITableView *gameTable;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    topView = [UIView new];
    [topView setBackgroundColor:[UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1]];
    [self.view addSubview:topView];
    
    topLabelView = [UIView new];
    [topView addSubview:topLabelView];
    
    gameTable = [UITableView new];
    [gameTable setTag:25];
    [self.view addSubview:gameTable];
    gameTable.delegate = self;
    gameTable.dataSource = self;
    gameTable.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];
    gameTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [gameTable setShowsVerticalScrollIndicator:NO];
    
    heightOfCell = 100*[UIScreen mainScreen].bounds.size.height/568.0;
    
    gameAr = [DataHandle getAllGameOfKanaTopic:self.topicId Type:self.type];
    
    [self initView];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self disableSlidePanGestureForLeftMenu];
    
    currentViewController = self;
    
    [gameTable reloadData];
    
}


-(void)initView
{
    topView.frame = [GameUtil transformFitToScree:CGRectMake(0, 0, 320, 60) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    
    topLabelView.frame = [GameUtil transformFitToScreen:topView.frame.size.width height:topView.frame.size.height WithOriginFrame:CGRectMake(40, 15, 240, 44) originScreenWidth:320 andOriginScreenHeight:60];
    
    
    gameTable.frame = CGRectMake(0, topView.frame.origin.y+topView.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-topView.frame.size.height);
    
    
    UILabel *topLabel = [[UILabel alloc] initWithFrame:topLabelView.frame];
    topLabel.font = [UIFont boldSystemFontOfSize:17];
    topLabel.backgroundColor = [UIColor clearColor];
    topLabel.textAlignment = NSTextAlignmentCenter;
    topLabel.textColor = [UIColor whiteColor];
    topLabel.text = @"Chọn kĩ năng";
    [topView addSubview:topLabel];
    
    
    UIButton *backButton = [UIButton new];
    //float heightOfBt = 28;
    //float widthOfBt = 28;
    backButton.frame = [GameUtil transformFitToScreen:topView.frame.size.width height:topView.frame.size.height WithOriginFrame:CGRectMake(10, 25, 40, 40) originScreenWidth:320 andOriginScreenHeight:60];
    backButton.frame = CGRectMake(backButton.frame.origin.x, topLabelView.frame.origin.y+topLabelView.frame.size.height/2- backButton.frame.size.height/2, backButton.frame.size.width, backButton.frame.size.height);
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backViewController) forControlEvents:UIControlEventTouchUpInside];
    
    [topView addSubview:backButton];
    
}


#pragma mark- button

-(void)backViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark- table view datasource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return heightOfCell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return gameAr.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Game *game  = gameAr[indexPath.row];
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIView *view = [UIView new];
    view.frame = CGRectMake(5, 5, [UIScreen mainScreen].bounds.size.width - 10, heightOfCell-10);
    view.userInteractionEnabled = true;
    view.backgroundColor = [UIColor whiteColor];
    [cell addSubview:view];
    
    float sizeOfImg = view.frame.size.height*3/4;
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, (view.frame.size.height - sizeOfImg)/2, sizeOfImg, sizeOfImg)];
    NSString *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true)[0];
    NSString *filePath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/skills/%@.png",game.key]];
    imgView.image = [UIImage imageWithContentsOfFile:filePath];
    imgView.layer.masksToBounds = true;
    imgView.layer.cornerRadius = imgView.frame.size.width/2;
    [view addSubview:imgView];
    
    
    UILabel *topicLabel = [UILabel new];
    topicLabel.frame = CGRectMake(imgView.frame.origin.x + imgView.frame.size.width +25, imgView.frame.origin.y, view.frame.size.width -(imgView.frame.origin.x + imgView.frame.size.width + 25 +10) , imgView.frame.size.height/2);
    topicLabel.font = [UIFont boldSystemFontOfSize:15];
    topicLabel.textAlignment = NSTextAlignmentCenter;
    topicLabel.backgroundColor = [UIColor clearColor];
    topicLabel.textColor = [UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1];
    NSString *topicTitle = game.title;
    topicLabel.text = [topicTitle stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:[[topicTitle substringWithRange:NSMakeRange(0, 1)] uppercaseString]];
    [view addSubview:topicLabel];
    
    
    int stars = [DataHandle getStarsOfGame:indexPath.row+1 Sub:0 Topic:self.topicId Level:g_course Type:self.type];
    
    float heightOfStarBar = view.frame.size.height - (topicLabel.frame.origin.y+topicLabel.frame.size.height+20);
    StarBar *starBar = [[StarBar alloc] initWithFrame:CGRectMake(topicLabel.frame.origin.x+topicLabel.frame.size.width/2-heightOfStarBar*3/2, topicLabel.frame.origin.y+topicLabel.frame.size.height, heightOfStarBar*3, heightOfStarBar) andNumberOfStars:stars];
    [view addSubview:starBar];
    
    //[[NSUserDefaults standardUserDefaults] setValue:@"unlock" forKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d",i,j,k]];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d_islock",self.type,self.topicId,indexPath.row+1]] isEqualToString:@"lock"])
    {
        float widthOfLock = 25*[UIScreen mainScreen].bounds.size.height/568.0;
        float heightOfLock = 25*[UIScreen mainScreen].bounds.size.height/568.0;
        
        UIImageView *lockImgView = [[UIImageView alloc] initWithFrame:CGRectMake(view.frame.size.width-widthOfLock-15, starBar.frame.origin.y+starBar.frame.size.height/2-heightOfLock/2, widthOfLock, heightOfLock)];
        lockImgView.image = [UIImage imageNamed:@"iconkhoa.png"];
        [view addSubview:lockImgView];
    }
    
    return cell;
}

#pragma mark- tableView delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"login"])
    {
        if([[[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d_islock",self.type,self.topicId,indexPath.row+1]] isEqualToString:@"unlock"])
        {
            [self performSegueWithIdentifier:[NSString stringWithFormat:@"kanagame%d",indexPath.row+1] sender:indexPath];
        }
        else
        {
            [Toast showToastOnView:self.view withMessage:@"Bạn cần thêm sao ở game trước để chơi game này"];
        }
    }
    else
    {
        [Toast showToastOnView:self.view withMessage:@"Vui lòng đăng nhập để sử dụng"];
    }
}


#pragma mark- segue method

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    NSIndexPath *indexPath = (NSIndexPath*)sender;
    
    if([segue.identifier isEqualToString:@"kanagame1"])
    {
        KanaGame1ViewController *dvc = (KanaGame1ViewController*)segue.destinationViewController;
        dvc.topicID = [NSString stringWithFormat:@"%d",self.topicId];
        dvc.gameID  = [NSString stringWithFormat:@"%d",indexPath.row+1];
        dvc.type = self.type;
    }

    if([segue.identifier isEqualToString:@"kanagame2"])
    {
        KanaGame2ViewController *dvc = (KanaGame2ViewController*)segue.destinationViewController;
        dvc.topicID = [NSString stringWithFormat:@"%d",self.topicId];
        dvc.gameID  = [NSString stringWithFormat:@"%d",indexPath.row+1];
        dvc.type = self.type;
    }

    if([segue.identifier isEqualToString:@"kanagame3"])
    {
        KanaGame3ViewController *dvc = (KanaGame3ViewController*)segue.destinationViewController;
        dvc.topicID = [NSString stringWithFormat:@"%d",self.topicId];
        dvc.gameID  = [NSString stringWithFormat:@"%d",indexPath.row+1];
        dvc.type = self.type;
    }
    
    if([segue.identifier isEqualToString:@"kanagame4"])
    {
        KanaGame4ViewController *dvc = (KanaGame4ViewController*)segue.destinationViewController;
        dvc.topicID = [NSString stringWithFormat:@"%d",self.topicId];
        dvc.gameID  = [NSString stringWithFormat:@"%d",indexPath.row+1];
        dvc.type = self.type;
    }

    if([segue.identifier isEqualToString:@"kanagame5"])
    {
        KanaGame5ViewController *dvc = (KanaGame5ViewController*)segue.destinationViewController;
        dvc.topicID = [NSString stringWithFormat:@"%d",self.topicId];
        dvc.gameID  = [NSString stringWithFormat:@"%d",indexPath.row+1];
        dvc.type = self.type;
    }

    if([segue.identifier isEqualToString:@"kanagame6"])
    {
        KanaGame6ViewController *dvc = (KanaGame6ViewController*)segue.destinationViewController;
        dvc.topicID = [NSString stringWithFormat:@"%d",self.topicId];
        dvc.gameID  = [NSString stringWithFormat:@"%d",indexPath.row+1];
        dvc.type = self.type;
    }
    
}

@end
