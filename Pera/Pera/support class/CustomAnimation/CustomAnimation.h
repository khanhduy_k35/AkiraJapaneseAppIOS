//
//  CustomAnimation.h
//  Pera
//
//  Created by Le Trong Thao on 12/5/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAnimation : UIView

+(void)upperAnimationDuration:(NSTimeInterval)time WithView:(UIView*)view andCompletion:(void (^)(BOOL finished))completion;
+(void)upperAnimationDuration:(NSTimeInterval)time WithViewsArray:(NSArray*)array andCompletion:(void (^)(BOOL finished))completion;
+(void)upperAnimationDuration1:(NSTimeInterval)time WithViewsArray:(NSArray*)array andCompletion:(void (^)(BOOL finished))completion;

@end
