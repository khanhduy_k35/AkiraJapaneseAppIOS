//
//  GameListViewController.m
//  Pera
//
//  Created by Le Trong Thao on 12/4/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import "GameListViewController.h"
#import "Game1ViewController.h"
#import "Game2ViewController.h"
#import "Game3ViewController.h"
#import "Game4ViewController.h"
#import "Game5ViewController.h"
#import "Game6ViewController.h"

#import "Email.h"
#import "StarBar.h"

#import "GlobalVariable.h"
#import "GameUtil.h"
#import "UIViewController+AMSlideMenu.h"

#import "DataHandle.h"
#import "Toast.h"

@interface GameListViewController ()<UITableViewDataSource, UITableViewDelegate>



@end

@implementation GameListViewController
{
    float heightOfCell;
    
    NSArray *gameTitleArray;
    NSArray *imgFileName;
    UIView *topView;
    UIView *topLabelView;
    
    UITableView *gameTable;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    NSLog(@"%d",self.topicID);
    
    gameTable = [UITableView new];
    [gameTable setTag:25];
    [self.view addSubview:gameTable];
    
    topView = [UIView new];
    [topView setBackgroundColor:[UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1]];
    [self.view addSubview:topView];
    
    topLabelView = [UIView new];
    [topView addSubview:topLabelView];
    
    gameTable.delegate = self;
    gameTable.dataSource = self;
    gameTable.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];
    gameTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [gameTable setShowsVerticalScrollIndicator:NO];
    
    heightOfCell = 100*[UIScreen mainScreen].bounds.size.height/568.0;
    
    if(self.sub == 4)
    {
        gameTitleArray = @[@"Học từ mới",@"Nối từ với nghĩa tiếng việt",@"Chọn nghĩa từ nghe được"];
        imgFileName = @[@"write-word.png",@"connect.png",@"practice.png"];
    }
    else
    {
        gameTitleArray = @[@"Học từ mới",@"Chọn ảnh phù hợp với từ",@"Chọn từ phù hợp với tranh",@"Nối từ với nghĩa tiếng việt",@"Điền từ tiếng Nhật", @"Chọn nghĩa từ nghe được"];
        imgFileName = @[@"write-word.png",@"choose-word-by-word.png",@"picture-by-word-before.png",@"connect.png",@"write-word-by-picture.png",@"practice.png"];
    }
    
    [self initView];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self disableSlidePanGestureForLeftMenu];
    
    UIView *downloadView = [self.navigationController.view viewWithTag:10];
    if(downloadView)
    {
        [downloadView setAlpha:1];
    }
    
    currentViewController = self;
    
    [gameTable reloadData];
    
}


-(void)initView
{
    topView.frame = [GameUtil transformFitToScree:CGRectMake(0, 0, 320, 60) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    
    topLabelView.frame = [GameUtil transformFitToScreen:topView.frame.size.width height:topView.frame.size.height WithOriginFrame:CGRectMake(40, 15, 240, 44) originScreenWidth:320 andOriginScreenHeight:60];

    
    gameTable.frame = CGRectMake(0, topView.frame.origin.y+topView.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-topView.frame.size.height);
    
    
    UILabel *topLabel = [[UILabel alloc] initWithFrame:topLabelView.frame];
    topLabel.font = [UIFont boldSystemFontOfSize:17];
    topLabel.backgroundColor = [UIColor clearColor];
    topLabel.textAlignment = NSTextAlignmentCenter;
    topLabel.textColor = [UIColor whiteColor];
    topLabel.text = @"Chọn kĩ năng";
    [topView addSubview:topLabel];
    
    
    UIButton *backButton = [UIButton new];
    //float heightOfBt = 28;
    //float widthOfBt = 28;
    backButton.frame = [GameUtil transformFitToScreen:topView.frame.size.width height:topView.frame.size.height WithOriginFrame:CGRectMake(10, 25, 40, 40) originScreenWidth:320 andOriginScreenHeight:60];
    backButton.frame = CGRectMake(backButton.frame.origin.x, topLabelView.frame.origin.y+topLabelView.frame.size.height/2- backButton.frame.size.height/2, backButton.frame.size.width, backButton.frame.size.height);
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backViewController) forControlEvents:UIControlEventTouchUpInside];
    
    [topView addSubview:backButton];
    
}


#pragma mark- button

-(void)backViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark- table view datasource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return heightOfCell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return gameTitleArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIView *view = [UIView new];
    view.frame = CGRectMake(5, 5, [UIScreen mainScreen].bounds.size.width - 10, heightOfCell-10);
    view.userInteractionEnabled = true;
    view.backgroundColor = [UIColor whiteColor];
    [cell addSubview:view];
    
    float sizeOfImg = view.frame.size.height*3/4;
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, (view.frame.size.height - sizeOfImg)/2, sizeOfImg, sizeOfImg)];
    NSString *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true)[0];
    NSString *filePath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/skills/%@",imgFileName[indexPath.row]]];
    imgView.image = [UIImage imageWithContentsOfFile:filePath];
    imgView.layer.masksToBounds = true;
    imgView.layer.cornerRadius = imgView.frame.size.width/2;
    [view addSubview:imgView];
    
    
    UILabel *topicLabel = [UILabel new];
    topicLabel.frame = CGRectMake(imgView.frame.origin.x + imgView.frame.size.width +25, imgView.frame.origin.y, view.frame.size.width -(imgView.frame.origin.x + imgView.frame.size.width + 25 +10) , imgView.frame.size.height/2);
    topicLabel.font = [UIFont boldSystemFontOfSize:15];
    topicLabel.textAlignment = NSTextAlignmentCenter;
    topicLabel.backgroundColor = [UIColor clearColor];
    topicLabel.textColor = [UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1];
    NSString *topicTitle = gameTitleArray[indexPath.row];
    topicLabel.text = [topicTitle stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:[[topicTitle substringWithRange:NSMakeRange(0, 1)] uppercaseString]];
    [view addSubview:topicLabel];
    
    
    int stars = [DataHandle getStarsOfGame:indexPath.row+1 Sub:self.sub Topic:self.topicID Level:g_course Type:0];
    
    float heightOfStarBar = view.frame.size.height - (topicLabel.frame.origin.y+topicLabel.frame.size.height+20);
    StarBar *starBar = [[StarBar alloc] initWithFrame:CGRectMake(topicLabel.frame.origin.x+topicLabel.frame.size.width/2-heightOfStarBar*3/2, topicLabel.frame.origin.y+topicLabel.frame.size.height, heightOfStarBar*3, heightOfStarBar) andNumberOfStars:stars];
    [view addSubview:starBar];
    
    //[NSString stringWithFormat:@"n%d/topic%d/sub%d/game%d_islock",i,j,k,l]
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"%@/topic%d/sub%d/game%d_islock",g_course,self.topicID,self.sub,indexPath.row+1]] isEqualToString:@"lock"])
    {
        float widthOfLock = 25*[UIScreen mainScreen].bounds.size.height/568.0;
        float heightOfLock = 25*[UIScreen mainScreen].bounds.size.height/568.0;
        
        UIImageView *lockImgView = [[UIImageView alloc] initWithFrame:CGRectMake(view.frame.size.width-widthOfLock-15, starBar.frame.origin.y+starBar.frame.size.height/2-heightOfLock/2, widthOfLock, heightOfLock)];
        lockImgView.image = [UIImage imageNamed:@"iconkhoa.png"];
        [view addSubview:lockImgView];
    }
    
    return cell;
}

#pragma mark- tableView delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"login"])
    {
        if([[[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"%@/topic%d/sub%d/game%d_islock",g_course,self.topicID,self.sub,indexPath.row+1]] isEqualToString:@"unlock"])
        {
            if(self.sub != 4)
            {
                [self performSegueWithIdentifier:[NSString stringWithFormat:@"game%d",indexPath.row+1] sender:indexPath];
            }
            else
            {
                if(indexPath.row == 0)
                {
                    [self performSegueWithIdentifier:@"game1" sender:indexPath];
                }
                else if (indexPath.row == 1)
                {
                    [self performSegueWithIdentifier:@"game4" sender:indexPath];
                }
                else if (indexPath.row == 2)
                {
                    [self performSegueWithIdentifier:@"game6" sender:indexPath];
                }
            }
        }
        else
        {
            [Toast showToastOnView:self.view withMessage:@"Bạn cần thêm sao ở game trước để chơi game này"];
        }

    }
    else
    {
        [Toast showToastOnView:self.view withMessage:@"Vui lòng đăng nhập để sử dụng"];
    }
}


#pragma mark- segue method

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    NSIndexPath *indexPath = (NSIndexPath*)sender;
    
    if([segue.identifier isEqualToString:@"game1"])
    {
        Game1ViewController *dvc = (Game1ViewController*)segue.destinationViewController;
        dvc.topicID = [NSString stringWithFormat:@"%d",self.topicID];
        dvc.gameID  = [NSString stringWithFormat:@"%d",indexPath.row+1];
        dvc.sub = self.sub;
    }
    
    if([segue.identifier isEqualToString:@"game2"])
    {
        Game2ViewController *dvc = (Game2ViewController*)segue.destinationViewController;
        dvc.topicID = [NSString stringWithFormat:@"%d",self.topicID];
        dvc.gameID  = [NSString stringWithFormat:@"%d",indexPath.row+1];
        dvc.sub = self.sub;
    }
    
    if([segue.identifier isEqualToString:@"game3"])
    {
        Game3ViewController *dvc = (Game3ViewController*)segue.destinationViewController;
        dvc.topicID = [NSString stringWithFormat:@"%d",self.topicID];
        dvc.gameID  = [NSString stringWithFormat:@"%d",indexPath.row+1];
        dvc.sub = self.sub;
    }
    
    if([segue.identifier isEqualToString:@"game4"])
    {
        Game4ViewController *dvc = (Game4ViewController*)segue.destinationViewController;
        dvc.topicID = [NSString stringWithFormat:@"%d",self.topicID];
        dvc.gameID  = [NSString stringWithFormat:@"%d",indexPath.row+1];
        dvc.sub = self.sub;
    }
    
    
    if([segue.identifier isEqualToString:@"game5"])
    {
        Game5ViewController *dvc = (Game5ViewController*)segue.destinationViewController;
        dvc.topicID = [NSString stringWithFormat:@"%d",self.topicID];
        dvc.gameID  = [NSString stringWithFormat:@"%d",indexPath.row+1];
        dvc.sub = self.sub;
    }
    
    if([segue.identifier isEqualToString:@"game6"])
    {
        Game6ViewController *dvc = (Game6ViewController*)segue.destinationViewController;
        dvc.topicID = [NSString stringWithFormat:@"%d",self.topicID];
        dvc.gameID  = [NSString stringWithFormat:@"%d",indexPath.row+1];
        dvc.sub = self.sub;
    }

}

#pragma mark- rate app delegate

-(void)rateAppTouched:rateView
{
    NSLog(@"rateAppTouched");
    
    [rateView removeFromSuperview];
    NSString *str = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=1046203855";
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        str = @"itms-apps://itunes.apple.com/app/id1046203855";
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

-(void)sendFeedbackTouched:rateView
{
    NSLog(@"sendFeedbackTouched");
    
    [rateView removeFromSuperview];
    
    [Email mailTo:emailAccount
            Title:feedbackTitle
       andContent:@""];
}

-(void)ignoreTouched:(UIView *)rateView
{
    NSLog(@"ignoreTouched");
    
    [rateView removeFromSuperview];
}

@end
