//
//  CustomProgressBarView.m
//  Pera
//
//  Created by Le Trong Thao on 12/3/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import "CustomProgressBarView.h"

@implementation CustomProgressBarView
{
//    UIView *processView;
//@property (nonatomic) loat maxProcess;
}


-(CustomProgressBarView*)initWithFrame:(CGRect)rect andCornerRadius:(CGFloat)radious
{
    self = [super initWithFrame:rect];
    
    self.layer.cornerRadius = radious;
    
    _processView = [UIView new];
    _processView.frame = CGRectMake(0, 0, 0, rect.size.height);
    _processView.layer.cornerRadius = self.layer.cornerRadius;
    
    [self addSubview:_processView];

    return self;
}


-(void)setMaxProcessWith:(CGFloat)max
{
    self.maxProcess = max;
}

-(void)setProcessViewColor:(UIColor *)processColor
{
    _processView.backgroundColor = processColor;
}

-(void)updateProcessWithCurrentProcess:(CGFloat)current
{
    _processView.frame = CGRectMake(0, 0, current*self.frame.size.width/_maxProcess, _processView.frame.size.height);
}



@end
