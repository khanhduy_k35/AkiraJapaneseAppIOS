//  TopicListViewController.m
//  Pera
//
//  Created by Le Trong Thao on 12/3/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import "TopicListViewController.h"
#import "AMSlideMenuMainViewController.h"
#import "SubListViewController.h"
#import "KanaTopicViewController.h"
#import "ProgressView.h"
#import "GameUtil.h"
#import "GlobalVariable.h"
#import "TopicDownLoad.h"
#import "AppDelegate.h"
#import "DataHandle.h"
#import "Toast.h"
#import "HttpThread.h"

@interface TopicListViewController ()<UITableViewDataSource, UITableViewDelegate>
//@property (weak, nonatomic) IBOutlet UIView *topView;


@end

@implementation TopicListViewController
{
    UIView *chooseCourseView;
    BOOL downFlag;
    
    
    UITableView *topicTable;
    
    UIView *learnView;
    UILabel *learnLabel;
    UIButton *downButton;
    UIView *topView;
    
    NSArray *topicTitleArray;
    
    UIButton *backButton;
    //UIButton *storeButton;
    float heightOfCell;
    BOOL isShowToat;
    
    UIView *buyCardBgView;
    UIView *buyCardContainView;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"login"])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            [HttpThread syncScore];
            [HttpThread getScore];
            
        });
    }
    
    downFlag = false;
    g_course = [[NSUserDefaults standardUserDefaults] valueForKey:@"level"];
    
    heightOfCell = 95*[UIScreen mainScreen].bounds.size.height/568.0;
    
    
    
    topicTable = [UITableView new];
    topView = [UIView new];
    learnView = [UIView new];
    learnLabel = [UILabel new];
    downButton = [UIButton new];
    
    
    
    topicTable.delegate = self;
    topicTable.dataSource = self;
    topicTable.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];
    topicTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    self.view.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(buysuccess:)
                                                 name:@"loginComplete" object:nil];

    [self adjustView];
    
}

- (void)buysuccess:(NSNotification *)note {
    [self reloadTable];
}

-(void)viewWillAppear:(BOOL)animated
{
       
    [self disableSlidePanGestureForRightMenu];
    
    currentViewController =  self;
    
    
    [topicTable reloadData];
}

-(void)viewDidAppear:(BOOL)animated
{
    //[self adjustView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark view method

-(void)adjustView
{
    topView.frame = [GameUtil transformFitToScree:CGRectMake(0, 0, 320, 60) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    [topView setBackgroundColor:[UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1]];
    [self.view addSubview:topView];
    
    
    topicTable.frame = CGRectMake(0, topView.frame.origin.y+topView.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-topView.frame.size.height);
    topicTable.contentInset = UIEdgeInsetsZero;
    [topicTable setTag:25];
    
    [self.view addSubview:topicTable];
    
    
    learnView.backgroundColor = [UIColor clearColor];
    learnView.userInteractionEnabled = true;
    learnView.frame = [GameUtil transformFitToScreen:topView.frame.size.width height:topView.frame.size.height WithOriginFrame:CGRectMake(40, 15, 240, 44) originScreenWidth:320 andOriginScreenHeight:60];
    
    [topView addSubview:learnView];
    
    float sizeOfDownBt = 20;
    
    
    learnLabel.frame = CGRectMake(0, 0, 30, 20);
    learnLabel.font = [UIFont boldSystemFontOfSize:15];
    learnLabel.textColor = [UIColor whiteColor];
    learnLabel.textAlignment = NSTextAlignmentCenter;
    learnLabel.backgroundColor = [UIColor clearColor];
    learnLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTap)];
    [learnLabel addGestureRecognizer:tapGesture];
    if([g_course isEqualToString:@"n5"])
    {
        learnLabel.text = @"Sơ cấp 1 (N5)";
    }
    else if ([g_course isEqualToString:@"n4"])
    {
        learnLabel.text = @"Sơ cấp 2 (N4)";
    }
    else if ([g_course isEqualToString:@"n3"])
    {
        learnLabel.text = @"Trung cấp (N3)";
    }
    else if ([g_course isEqualToString:@"kana"])
    {
        learnLabel.text = @"Dành cho người mới bắt đầu";
    }
    [learnLabel sizeToFit];
    learnLabel.frame = CGRectMake((learnView.frame.size.width - learnLabel.frame.size.width-5-sizeOfDownBt)/2, (learnView.frame.size.height  - learnLabel.frame.size.height)/2, learnLabel.frame.size.width, learnLabel.frame.size.height);
    [learnView addSubview:learnLabel];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(downButton:)];
    tap.numberOfTouchesRequired = 1;
    [learnView addGestureRecognizer:tap];
    
    
    downButton.frame = CGRectMake(learnLabel.frame.origin.x+learnLabel.frame.size.width+5, (learnView.frame.size.height - sizeOfDownBt)/2, sizeOfDownBt, sizeOfDownBt);
    [downButton setBackgroundImage:[UIImage imageNamed:@"down_arrow.png"] forState:UIControlStateNormal];
    [downButton addTarget:self action:@selector(downButton:) forControlEvents:UIControlEventTouchUpInside];
    [learnView addSubview:downButton];
    
    backButton = [UIButton new];
    float heightOfBt = 40;
    float widthOfBt = 40;
    backButton.frame = CGRectMake(10, learnView.frame.origin.y+learnView.frame.size.height/2-heightOfBt/2, widthOfBt, heightOfBt);
    [backButton setBackgroundImage:[UIImage imageNamed:@"ic_action_navigation_drawer.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backToLeftSlideMenu) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:backButton];
    
}


-(void)showPopup
{
    
    buyCardBgView = [UIView new];
    buyCardBgView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    buyCardBgView.userInteractionEnabled = true;
    buyCardBgView.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.5];
    [self.view addSubview:buyCardBgView];
    
    
    float wOfContainView = 280;//*[UIScreen mainScreen].bounds.size.width/320.0;
    float hOfContainView = 320;
    buyCardContainView = [UIView new];
    buyCardContainView.backgroundColor = [UIColor whiteColor];
    buyCardContainView.userInteractionEnabled = true;
    buyCardContainView.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width - wOfContainView)/2,- hOfContainView, wOfContainView, hOfContainView);
    [buyCardBgView addSubview:buyCardContainView];
    
    
    float wOfLogoImg = 260;
    float hOfLogoImg = 140;
    UIImageView *logoImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_akira.png"]];
    logoImg.frame = CGRectMake((buyCardContainView.frame.size.width-wOfLogoImg)/2, 5, wOfLogoImg, hOfLogoImg);
    [buyCardContainView addSubview:logoImg];
    
    
    UILabel *bodyLabel = [UILabel new];
    bodyLabel.frame = CGRectMake(20, logoImg.frame.origin.y+logoImg.frame.size.height+10, buyCardContainView.frame.size.width - 40, 50);
    bodyLabel.backgroundColor = [UIColor clearColor];
    bodyLabel.font = [UIFont boldSystemFontOfSize:12];
    bodyLabel.textAlignment = NSTextAlignmentCenter;
    bodyLabel.textColor = [UIColor colorWithRed:6.0/255.0 green:45.0/255.0 blue:113.0/255.0 alpha:1];
    bodyLabel.numberOfLines = 0;
    bodyLabel.text = @"Akira Learning Japanense là app hỗ trợ học tập cho học viên trên website học tiếng Nhật của Akira";
    [buyCardContainView addSubview:bodyLabel];
    
    
    UILabel *bodyLabel2 = [UILabel new];
    bodyLabel2.frame = CGRectMake(20, bodyLabel.frame.origin.y+bodyLabel.frame.size.height+5, buyCardContainView.frame.size.width - 40, 50);
    bodyLabel2.backgroundColor = [UIColor clearColor];
    bodyLabel2.font = [UIFont systemFontOfSize:12];
    bodyLabel2.textAlignment = NSTextAlignmentCenter;
    bodyLabel2.textColor = [UIColor colorWithRed:6.0/255.0 green:45.0/255.0 blue:113.0/255.0 alpha:1];
    bodyLabel2.numberOfLines = 0;
    bodyLabel2.text = @"Bạn đã hết số ngày sử dụng, vui lòng mua thẻ học để tiếp tục sử dụng ứng dụng";
    [buyCardContainView addSubview:bodyLabel2];
    
    
    float widthOfBt = 150;
    float heightOfBt = 30;
    
    UIButton *okBt = [UIButton new];
    okBt.frame = CGRectMake((buyCardContainView.frame.size.width-widthOfBt)/2, bodyLabel2.frame.origin.y+bodyLabel2.frame.size.height + (buyCardContainView.frame.size.height-(bodyLabel2.frame.origin.y+bodyLabel2.frame.size.height)-heightOfBt)/2, widthOfBt, heightOfBt);
    [okBt setTitle:@"MUA THẺ HỌC" forState:UIControlStateNormal];
    okBt.titleLabel.font = [UIFont systemFontOfSize:15];
    okBt.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:200.0/255.0 blue:67.0/255.0 alpha:1];
    okBt.layer.cornerRadius = 4;
    [okBt addTarget:self action:@selector(okButton:) forControlEvents:UIControlEventTouchUpInside];
    [buyCardContainView addSubview:okBt];
    
    
    [UIView animateWithDuration:0.5 animations:^{
        buyCardContainView.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width - wOfContainView)/2,([UIScreen mainScreen].bounds.size.height- hOfContainView)/2, wOfContainView, hOfContainView);
    }completion:^(BOOL finish){
        
    }];
    
}


#pragma mark- button

-(IBAction)okButton:(id)sender
{
    UIButton *okBt = (UIButton*)sender;
    UIView *containView = okBt.superview;
    UIView *bgView = containView.superview;
    
    [UIView animateWithDuration:0.5 animations:^{
        containView.frame = CGRectMake(containView.frame.origin.x,-containView.frame.size.height, containView.frame.size.width , containView.frame.size.height);
    }completion:^(BOOL finish){
        [bgView removeFromSuperview];
        //showBuyCard = false;
    }];
    
    
}

-(void) labelTap{
    [self downButton:nil];
}


- (IBAction)downButton:(id)sender {
    
    if(!downFlag)
    {
        downFlag = true;
        
        UIView *courseBgView = [[UIView alloc] initWithFrame:CGRectMake(0, topView.frame.size.height, [UIScreen mainScreen].bounds.size.width, ([UIScreen mainScreen].bounds.size.height - topView.frame.size.height))];
        courseBgView.userInteractionEnabled = true;
        courseBgView.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.6];
        
        [self.view addSubview:courseBgView];
        
        
        int numberOfCourse = 4;
        
        
        chooseCourseView = [[UIView alloc] initWithFrame:CGRectMake(learnView.frame.origin.x, -learnView.frame.size.height*(numberOfCourse+1), learnView.frame.size.width, learnView.frame.size.height*(numberOfCourse+1))];
        chooseCourseView.userInteractionEnabled = true;
        chooseCourseView.backgroundColor = [UIColor whiteColor];
        chooseCourseView.layer.zPosition = 0;
        topView.layer.zPosition = 1;
        [courseBgView addSubview:chooseCourseView];
        
        
        UILabel *lb1 = [UILabel new];
        lb1.frame = CGRectMake(0, 0, chooseCourseView.frame.size.width, learnView.frame.size.height-1);
        lb1.font = [UIFont boldSystemFontOfSize:15];
        lb1.textAlignment = NSTextAlignmentCenter;
        lb1.backgroundColor = [UIColor clearColor];
        lb1.textColor = [UIColor blackColor];
        lb1.text = @"Chọn khóa học";
        [chooseCourseView addSubview:lb1];
        
        UILabel *rule1 = [[UILabel alloc] initWithFrame:CGRectMake(0, lb1.frame.size.height, chooseCourseView.frame.size.width, 2)];
        rule1.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];
        [chooseCourseView addSubview:rule1];
        
        
        UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(0, rule1.frame.origin.y + rule1.frame.size.height, chooseCourseView.frame.size.width, learnView.frame.size.height)];
        view1.userInteractionEnabled = true;
        UIImageView *img1 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, view1.frame.size.height-10, view1.frame.size.height-10)];
        img1.layer.masksToBounds = true;
        img1.layer.cornerRadius = img1.frame.size.width/2;
        
        UILabel *lb2 = [UILabel new];
        lb2.frame = CGRectMake(img1.frame.origin.x+img1.frame.size.width+ 5, 0, view1.frame.size.width - (img1.frame.origin.x+img1.frame.size.width+ 5), view1.frame.size.height);
        lb2.font = [UIFont systemFontOfSize:14];
        lb2.textAlignment = NSTextAlignmentLeft;
        lb2.backgroundColor = [UIColor clearColor];
        lb2.textColor = [UIColor blackColor];
        [view1 addSubview:img1];
        [view1 addSubview:lb2];
        [chooseCourseView addSubview:view1];
        
        UILabel *rule2 = [[UILabel alloc] initWithFrame:CGRectMake(0, view1.frame.size.height+view1.frame.origin.y, chooseCourseView.frame.size.width, 2)];
        rule2.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];
        [chooseCourseView addSubview:rule2];
        
        
        UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(0, rule2.frame.origin.y + rule2.frame.size.height, chooseCourseView.frame.size.width, learnView.frame.size.height)];
        view2.userInteractionEnabled = true;
        UIImageView *img2 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, view2.frame.size.height-10, view2.frame.size.height-10)];
        img2.layer.masksToBounds = true;
        img2.layer.cornerRadius = img2.frame.size.width/2;
        
        UILabel *lb3 = [UILabel new];
        lb3.frame = CGRectMake(img2.frame.origin.x+img2.frame.size.width+ 5, 0, view2.frame.size.width - (img2.frame.origin.x+img2.frame.size.width+ 5), view2.frame.size.height);
        lb3.font = [UIFont systemFontOfSize:14];
        lb3.textAlignment = NSTextAlignmentLeft;
        lb3.backgroundColor = [UIColor clearColor];
        lb3.textColor = [UIColor blackColor];
        
        //view2.frame = CGRectMake(view2.frame.origin.x, view2.frame.origin.y, view2.frame.size.width, view2.frame.size.height+4);
        
        [view2 addSubview:img2];
        [view2 addSubview:lb3];
        
        [chooseCourseView addSubview:view2];
        
        chooseCourseView.frame = CGRectMake(chooseCourseView.frame.origin.x, chooseCourseView.frame.origin.y, chooseCourseView.frame.size.width, view2.frame.origin.y+view2.frame.size.height + 4);
        
        UIView *view3;
        UIView *view4;
        
        UILabel *lb4;
        UILabel *lb5;
        
        UIImageView *img3;
        UIImageView *img4;
        
        UILabel *rule3 = [[UILabel alloc] initWithFrame:CGRectMake(0, view2.frame.size.height+view2.frame.origin.y, chooseCourseView.frame.size.width, 2)];
        rule3.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];
        [chooseCourseView addSubview:rule3];
        
        
        view3 = [[UIView alloc] initWithFrame:CGRectMake(0, rule3.frame.origin.y + rule3.frame.size.height, chooseCourseView.frame.size.width, learnView.frame.size.height)];
        view3.userInteractionEnabled = true;
        img3 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, view3.frame.size.height-10, view3.frame.size.height-10)];
        img3.layer.masksToBounds = true;
        img3.layer.cornerRadius = img3.frame.size.width/2;
        
        lb4 = [UILabel new];
        lb4.frame = CGRectMake(img3.frame.origin.x+img3.frame.size.width+ 5, 0, view3.frame.size.width - (img3.frame.origin.x+img3.frame.size.width+ 5), view3.frame.size.height);
        lb4.font = [UIFont systemFontOfSize:14];
        lb4.textAlignment = NSTextAlignmentLeft;
        lb4.backgroundColor = [UIColor clearColor];
        lb4.textColor = [UIColor blackColor];
        
        //view3.frame = CGRectMake(view3.frame.origin.x, view3.frame.origin.y, view3.frame.size.width, view3.frame.size.height+4);
        
        [view3 addSubview:img3];
        [view3 addSubview:lb4];
        
        [chooseCourseView addSubview:view3];
        
        
        
        UILabel *rule4 = [[UILabel alloc] initWithFrame:CGRectMake(0, view3.frame.size.height+view3.frame.origin.y, chooseCourseView.frame.size.width, 2)];
        rule4.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];
        [chooseCourseView addSubview:rule4];
        
        
        view4 = [[UIView alloc] initWithFrame:CGRectMake(0, rule4.frame.origin.y + rule4.frame.size.height, chooseCourseView.frame.size.width, learnView.frame.size.height)];
        view4.userInteractionEnabled = true;
        img4 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, view4.frame.size.height-10, view4.frame.size.height-10)];
        img4.layer.masksToBounds = true;
        img4.layer.cornerRadius = img4.frame.size.width/2;
        
        lb5 = [UILabel new];
        lb5.frame = CGRectMake(img4.frame.origin.x+img4.frame.size.width+ 5, 0, view4.frame.size.width - (img4.frame.origin.x+img4.frame.size.width+ 5), view4.frame.size.height);
        lb5.font = [UIFont systemFontOfSize:14];
        lb5.textAlignment = NSTextAlignmentLeft;
        lb5.backgroundColor = [UIColor clearColor];
        lb5.textColor = [UIColor blackColor];
        
        //view4.frame = CGRectMake(view4.frame.origin.x, view4.frame.origin.y, view4.frame.size.width, view4.frame.size.height+4);
        
        [view4 addSubview:img4];
        [view4 addSubview:lb5];
        
        [chooseCourseView addSubview:view4];
        
        chooseCourseView.frame = CGRectMake(chooseCourseView.frame.origin.x, chooseCourseView.frame.origin.y, chooseCourseView.frame.size.width, view4.frame.origin.y+view4.frame.size.height + 4);
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentPath = [paths firstObject];
        
        img1.image = [UIImage imageNamed:@"kana.png"];
        img2.image = [UIImage imageNamed:@"n5.png"];
        img3.image = [UIImage imageNamed:@"n4.png"];
        img4.image = [UIImage imageNamed:@"n3.png"];
        
        lb2.text  = @"Dành cho người mới bắt đầu";
        lb3.text  = @"Sơ cấp 1 (N5)";
        lb4.text  = @"Sơ cấp 2 (N4)";
        lb5.text  = @"Trung cấp (N3)";
        
        UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseCourse:)];
        tap1.numberOfTouchesRequired = 1;
        [view1 setTag:1];
        [view1 addGestureRecognizer:tap1];
        
        
        UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseCourse:)];
        tap2.numberOfTouchesRequired = 1;
        [view2 setTag:2];
        [view2 addGestureRecognizer:tap2];
        
        
        UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseCourse:)];
        tap3.numberOfTouchesRequired = 1;
        [view3 setTag:3];
        [view3 addGestureRecognizer:tap3];
        
        UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseCourse:)];
        tap4.numberOfTouchesRequired = 1;
        [view4 setTag:4];
        [view4 addGestureRecognizer:tap4];

        
        
        [UIView animateWithDuration:0.5 animations:^{
            chooseCourseView.frame = CGRectMake(chooseCourseView.frame.origin.x, self.view.frame.size.height/2 - chooseCourseView.frame.size.height/2 - 60, chooseCourseView.frame.size.width, chooseCourseView.frame.size.height);
        }];
        
    }
    else
    {
        downFlag = false;
        
        [UIView animateWithDuration:0.5 animations:^{
            chooseCourseView.frame = CGRectMake(chooseCourseView.frame.origin.x, - chooseCourseView.frame.size.height, chooseCourseView.frame.size.width, chooseCourseView.frame.size.height);
        }completion:^(BOOL finish)
         {
             [chooseCourseView.superview removeFromSuperview];
         }];
        
    }
}


-(IBAction)chooseCourse:(UITapGestureRecognizer*)sender
{
    UIView *view  = sender.view;
    
    int tag = (int)view.tag;
    
    if(tag==1)
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"kana" forKey:@"level"];
        g_course = @"kana";
    }
    else if(tag == 2)
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"n5" forKey:@"level"];
        g_course = @"n5";
    }
    else if(tag == 3)
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"n4" forKey:@"level"];
        g_course = @"n4";
    }
    else if(tag == 4)
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"n3" forKey:@"level"];
        g_course = @"n3";
    }
    
    downFlag = false;
    
    [UIView animateWithDuration:0.5 animations:^{
        chooseCourseView.frame = CGRectMake(chooseCourseView.frame.origin.x, - chooseCourseView.frame.size.height, chooseCourseView.frame.size.width, chooseCourseView.frame.size.height);
    }completion:^(BOOL finish)
     {
         [chooseCourseView.superview removeFromSuperview];
         
         if([g_course isEqualToString:@"n5"])
         {
             learnLabel.text = @"Sơ cấp 1 (N5)";
         }
         else if ([g_course isEqualToString:@"n4"])
         {
             learnLabel.text = @"Sơ cấp 2 (N4)";
         }
         else if ([g_course isEqualToString:@"n3"])
         {
             learnLabel.text = @"Trung cấp (N3)";
         }
         else if ([g_course isEqualToString:@"kana"])
         {
             learnLabel.text = @"Dành cho người mới bắt đầu";
         }
         
         [learnLabel sizeToFit];
         
         float sizeOfDownBt = 20;
         
         learnLabel.frame = CGRectMake((learnView.frame.size.width - learnLabel.frame.size.width-5-sizeOfDownBt)/2, (learnView.frame.size.height  - learnLabel.frame.size.height)/2, learnLabel.frame.size.width, learnLabel.frame.size.height);
         downButton.frame = CGRectMake(learnLabel.frame.origin.x+learnLabel.frame.size.width+5, (learnView.frame.size.height - sizeOfDownBt)/2, sizeOfDownBt, sizeOfDownBt);
         
         
         [self reloadTable];
         
         
     }];
    
    
}


-(void)backToLeftSlideMenu
{
    [(AMSlideMenuMainViewController*)mainMenuViewController openLeftMenuAnimated:YES];
}

-(void)storeButton
{
    //[(AMSlideMenuMainViewController*)mainMenuViewController openRightMenuAnimated:YES];
    // [(AMSlideMenuMainViewController*)mainMenuViewController openRightMenuAnimated:YES];
}

#pragma mark table method

-(void)reloadTable
{
//    if([g_language isEqualToString:@"en"])
//    {
//        topicTitleArray = [JsonFileHandle getTopicTitlesWithLanguageKey:@"eng"];
//    }
//    else
//    {
//        topicTitleArray = [JsonFileHandle getTopicTitlesWithLanguageKey:g_language];
//    }
    [topicTable reloadData];
}


#pragma mark- tableView data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return heightOfCell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([g_course isEqualToString:@"n5"])
    {
        return 24;
    }
    else if ([g_course isEqualToString:@"n4"])
    {
        return 24;
    }
    else if ([g_course isEqualToString:@"n3"])
    {
        return 36;
    }
    else if ([g_course isEqualToString:@"kana"])
    {
        return 2;
    }
    
    return 0;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    cell.backgroundColor =[UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIView *view = [UIView new];
    view.frame = CGRectMake(5, 5, [UIScreen mainScreen].bounds.size.width - 10, heightOfCell-10);
    view.userInteractionEnabled = true;
    view.backgroundColor = [UIColor whiteColor];
    [cell addSubview:view];
    
    ProgressView *progressBarView;

    if([g_course isEqualToString:@"kana"])
    {
        float sizeOfImg = view.frame.size.height*3/4;
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, (view.frame.size.height - sizeOfImg)/2, sizeOfImg, sizeOfImg)];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentPath = [paths firstObject];
            
            NSString *filePath = @"";
            
            if(indexPath.row == 0)
            {
                filePath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/courses/kana/category/hiragana/1.jpg"]];
            }
            else if (indexPath.row == 1)
            {
                filePath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/courses/kana/category/katakana/1.jpg"]];
            }
            
            UIImage *image = [UIImage imageWithContentsOfFile:filePath];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.5 animations:^{
                    [imgView setImage:image];
                }];
                
            });
            
        });
        [view addSubview:imgView];
        
        
        UILabel *topicLabel = [UILabel new];
        topicLabel.frame = CGRectMake(imgView.frame.origin.x + imgView.frame.size.width +25, imgView.frame.origin.y, view.frame.size.width -(imgView.frame.origin.x + imgView.frame.size.width + 25 +60) , imgView.frame.size.height/2);
        topicLabel.font = [UIFont boldSystemFontOfSize:15];
        topicLabel.numberOfLines  = 0;
        topicLabel.textAlignment = NSTextAlignmentCenter;
        topicLabel.backgroundColor = [UIColor clearColor];
        topicLabel.textColor = [UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1];
        
        if(indexPath.row == 0)
        {
            topicLabel.text = @"Bảng Hiragana";
        }
        else if (indexPath.row == 1)
        {
            topicLabel.text = @"Bảng Katakana";
        }
        
        [view addSubview:topicLabel];
        
        
        int stars = [DataHandle getStarsOfType:indexPath.row+1 Level:g_course];
        int sumStars = [DataHandle getSumStarsOfType:indexPath.row+1 Level:g_course];
        
        progressBarView = [[ProgressView alloc] initWithFrame:CGRectMake(topicLabel.frame.origin.x, topicLabel.frame.origin.y+topicLabel.frame.size.height+5, topicLabel.frame.size.width, 5) withMax:100];
        [progressBarView setPercentComplete:stars*100/sumStars];
        [view addSubview:progressBarView];
        
    }
    else
    {
        float sizeOfImg = view.frame.size.height*3/4;
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, (view.frame.size.height - sizeOfImg)/2, sizeOfImg, sizeOfImg)];
        //imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d.jpg",indexPath.row+1]];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentPath = [paths firstObject];
            
            
            UIImage *image = [UIImage imageWithContentsOfFile:[documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/courses/%@/lesson/%d.png",g_course,indexPath.row+1]]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.5 animations:^{
                    [imgView setImage:image];
                }];
                
            });
            
        });
        [view addSubview:imgView];
        
        
        UILabel *topicLabel = [UILabel new];
        topicLabel.frame = CGRectMake(imgView.frame.origin.x + imgView.frame.size.width +25, imgView.frame.origin.y, view.frame.size.width -(imgView.frame.origin.x + imgView.frame.size.width + 25 +60) , imgView.frame.size.height/2);
        topicLabel.font = [UIFont boldSystemFontOfSize:15];
        topicLabel.numberOfLines  = 0;
        topicLabel.textAlignment = NSTextAlignmentCenter;
        topicLabel.backgroundColor = [UIColor clearColor];
        topicLabel.textColor = [UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1];
        topicLabel.text = [NSString stringWithFormat:@"Bài %d",indexPath.row+1];
        [view addSubview:topicLabel];
        
        
        int stars = [DataHandle getStarsOfTopic:indexPath.row+1 Level:g_course Type:0];
        
        
        progressBarView = [[ProgressView alloc] initWithFrame:CGRectMake(topicLabel.frame.origin.x, topicLabel.frame.origin.y+topicLabel.frame.size.height+5, topicLabel.frame.size.width, 5) withMax:100];
        [progressBarView setPercentComplete:stars*100/(3*6*3+3*3)];
        [view addSubview:progressBarView];
    }
    
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"login"] || [[NSUserDefaults standardUserDefaults] integerForKey:@"day_remain"] == 0)
    {
        float widthOfLock = 25*[UIScreen mainScreen].bounds.size.height/568.0;
        float heightOfLock = 25*[UIScreen mainScreen].bounds.size.height/568.0;
        
        UIImageView *lockImgView = [[UIImageView alloc] initWithFrame:CGRectMake(view.frame.size.width-widthOfLock-10, (view.frame.size.height - heightOfLock)/2, widthOfLock, heightOfLock)];
        lockImgView.image = [UIImage imageNamed:@"iconkhoa.png"];
        [view addSubview:lockImgView];
    }
    
    return cell;
}

#pragma mark- tableView delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"login"])
    {
        if([[NSUserDefaults standardUserDefaults] integerForKey:@"day_remain"] > 0)
        {
            if([g_course isEqualToString:@"kana"])
            {
                [self performSegueWithIdentifier:@"kanatopicSegue" sender:indexPath];
            }
            else
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true);
                NSString *documentPath = paths[0];
                NSString *topicPath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"resources/courses/%@/%d/vocab",g_course,indexPath.row+1]];
                
                if([[NSFileManager defaultManager] fileExistsAtPath:topicPath])
                {
                    NSLog(@"exist");
                    
                    
                    [self performSegueWithIdentifier:@"sublistSegue" sender:indexPath];
                    
                    
                }
                else
                {
                    if(((AppDelegate*)[[UIApplication sharedApplication] delegate]).isDownload)
                    {
                        [Toast showToastOnView:self.navigationController.view withMessage:@"Đang download. Vui lòng đợi !"];
                    }
                    else
                    {
                        if([self currentNetworkStatus])
                        {
                            
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                                
                                [[TopicDownLoad new] downloadDataForTopic:[NSString stringWithFormat:@"%d",indexPath.row+1]
                                                                    Level:g_course
                                                                   onView:self.navigationController.view
                                                               completion:^(BOOL finish)
                                 {
                                     
                                 }];
                                
                            });
                            
                        }
                        else
                        {
                            [Toast showToastOnView:self.navigationController.view withMessage:@"Kiểm tra network"];
                        }
                    }
                }
            }

        }
        else
        {
            [self showPopup];
        }
    }
    else
    {
        [Toast showToastOnView:self.view withMessage:@"Vui lòng đăng nhập để sử dụng"];
    }
    
}


-(BOOL)currentNetworkStatus
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}


#pragma mark segue method

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
//    [SqlManager updateChoosedTopic];
//    NSIndexPath *indexPath = (NSIndexPath*)sender;
//    GameListViewController *dvc = (GameListViewController*)segue.destinationViewController;
//    dvc.topicID  = indexPath.row+1;
    
    NSIndexPath *indexPath = (NSIndexPath*)sender;
    
    if(![g_course isEqualToString:@"kana"])
    {
        
        SubListViewController *DVC = (SubListViewController*)segue.destinationViewController;
        DVC.topic = [NSString stringWithFormat:@"%d",indexPath.row+1];
    }
    else
    {
        KanaTopicViewController *dvc = (KanaTopicViewController*)segue.destinationViewController;
        dvc.type = indexPath.row+1;
    }
    
    
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event;
{
    UITouch *touch = [touches anyObject];
    if(touch.view == chooseCourseView.superview )
    {
        downFlag = false;
        
        [UIView animateWithDuration:0.5 animations:^{
            chooseCourseView.frame = CGRectMake(chooseCourseView.frame.origin.x, - chooseCourseView.frame.size.height, chooseCourseView.frame.size.width, chooseCourseView.frame.size.height);
        }completion:^(BOOL finish)
         {
             [chooseCourseView.superview removeFromSuperview];
         }];
    }
    else if (touch.view == buyCardBgView)
    {
        [UIView animateWithDuration:0.5 animations:^{
            buyCardContainView.frame = CGRectMake(buyCardContainView.frame.origin.x,-buyCardContainView.frame.size.height, buyCardContainView.frame.size.width , buyCardContainView.frame.size.height);
        }completion:^(BOOL finish){
            [buyCardBgView removeFromSuperview];
            //showBuyCard = false;
        }];

    }
    
    
}

@end
