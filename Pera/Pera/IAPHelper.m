#import "IAPHelper.h"
#import "GlobalVariable.h"
#import "SqlManager.h"
#define INAPPS_PUCHASED_KEY   @"inaps_purchased"

@implementation IAPHelper
@synthesize productIdentifiers = _productIdentifiers;
@synthesize productPrices = _productPrices;
@synthesize productCoins = _productCoins;
@synthesize products = _products;
@synthesize purchasedProducts = _purchasedProducts;
@synthesize request = _request;

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers prices:(NSMutableDictionary*)pD coins:(NSDictionary*)cD
{
    if ((self = [super init])) {
        
        // Store product identifiers
        _productIdentifiers = [productIdentifiers retain];
        
        // Store prices and coins
        _productPrices = [pD retain];
        _productCoins = [cD retain];
        
        // Check for previously purchased products
        NSMutableSet * purchasedProducts = [NSMutableSet set];
        /*
        for (NSString * productIdentifier in _productIdentifiers) {
            BOOL productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:productIdentifier];
            if (productPurchased) {
                [purchasedProducts addObject:productIdentifier];
                NSLog(@"Previously purchased: %@", productIdentifier);
            }
            NSLog(@"Not purchased: %@", productIdentifier);
        }*/
        self.purchasedProducts = purchasedProducts;
                        
    }
    return self;
}

- (BOOL)isProductPurchased:(NSString *)productIdentifier {
    return [[NSUserDefaults standardUserDefaults] boolForKey:productIdentifier];
}

- (void)requestProducts {
    
    self.request = [[[SKProductsRequest alloc] initWithProductIdentifiers:_productIdentifiers] autorelease];
    _request.delegate = self;
    [_request start];
    
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    
    NSLog(@"Received in-apps products");   
    self.products = response.products;
    self.request = nil;    
    
    [self updatePrices];
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductsLoadedNotification object:_products];    
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {

	[[NSNotificationCenter defaultCenter] postNotificationName:kProductsFailLoadedNotification object:_products]; 
}

- (void)updatePrices
{
    if (!self.products)
        return;
    
    for ( SKProduct *product  in self.products )
    {
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [numberFormatter setLocale:product.priceLocale];
        
        NSString* price = [numberFormatter stringFromNumber:product.price];
        [numberFormatter release];
        [self.productPrices setValue:price forKey:product.productIdentifier];
        //NSLog(@"Price:%@",price);
	}
}
- (NSString*)getPriceStringForProductID:(NSString*)pid
{
    return [self.productPrices objectForKey:pid];
}
- (int)getCoinsForProductID:(NSString*)pid
{
    return [[self.productCoins objectForKey:pid] intValue];
}

- (void)recordTransaction:(SKPaymentTransaction *)transaction {    
    // TODO: Record the transaction on the server side...    
}

- (void)provideContent:(NSString *)productIdentifier {
    
    NSLog(@"Provide content for: %@", productIdentifier);
    
    if ([productIdentifier isEqualToString:@"cource_all"]) {
        for(int i= 1 ; i< 45 ; i++ ){
            [SqlManager unlockTopicId:@"en" Course:@"vi" andTopicID:[NSString stringWithFormat:@"%d",i]];
            [SqlManager unlockTopicId:@"en" Course:@"jp" andTopicID:[NSString stringWithFormat:@"%d",i]];
            [SqlManager unlockTopicId:@"en" Course:@"kr" andTopicID:[NSString stringWithFormat:@"%d",i]];
            [SqlManager unlockTopicId:@"en" Course:@"zh" andTopicID:[NSString stringWithFormat:@"%d",i]];
            
            [SqlManager unlockTopicId:@"vi" Course:@"zh" andTopicID:[NSString stringWithFormat:@"%d",i]];
            [SqlManager unlockTopicId:@"vi" Course:@"en" andTopicID:[NSString stringWithFormat:@"%d",i]];
            [SqlManager unlockTopicId:@"vi" Course:@"kr" andTopicID:[NSString stringWithFormat:@"%d",i]];
            [SqlManager unlockTopicId:@"vi" Course:@"jp" andTopicID:[NSString stringWithFormat:@"%d",i]];
            
            [SqlManager unlockTopicId:@"jp" Course:@"en" andTopicID:[NSString stringWithFormat:@"%d",i]];
            [SqlManager unlockTopicId:@"jp" Course:@"vi" andTopicID:[NSString stringWithFormat:@"%d",i]];
        }
    }else{
        for(int i= 1 ; i< 45 ; i++ ){
            if ([g_course isEqualToString:@"vi"]) {
                [SqlManager unlockTopicId:@"en" Course:g_course andTopicID:[NSString stringWithFormat:@"%d",i]];
                [SqlManager unlockTopicId:@"jp" Course:g_course andTopicID:[NSString stringWithFormat:@"%d",i]];
            }else{
                [SqlManager unlockTopicId:g_language Course:g_course andTopicID:[NSString stringWithFormat:@"%d",i]];
            }
        }
    }
    
    [self setInAppsPurchased:YES];
   
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductPurchasedNotification object:productIdentifier];
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    
    NSLog(@"completeTransaction...");
    
    [self recordTransaction: transaction];
    [self provideContent: transaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    
    NSLog(@"restoreTransaction...");
    
    [self recordTransaction: transaction];
    [self provideContent: transaction.originalTransaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        NSLog(@"Transaction error: %@", transaction.error.localizedDescription);
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductPurchaseFailedNotification object:transaction];
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    }
}

- (void)buyProductIdentifier:(NSString *)productIdentifier {
    
    NSLog(@"Buying %@...", productIdentifier);
    
    SKPayment *payment = [SKPayment paymentWithProductIdentifier:productIdentifier];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
    
}

//------------------------------------------------------------------------------------------
// Restore Transactions

- (void)restoreCompletedTransactions {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

//Handling Restored Transactions
- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    NSNumber* number = [NSNumber numberWithInt:queue.transactions.count];
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductsRestoredNotification object:number];
}
- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductsRestoredFailedNotification object:error];
}

//------------------------------------------------------------------------------------------
- (void)setInAppsPurchased:(BOOL)on
{
    [[NSUserDefaults standardUserDefaults] setBool:on forKey:INAPPS_PUCHASED_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)areInAppsPurchased
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:INAPPS_PUCHASED_KEY];
}
//------------------------------------------------------------------------------------------
- (void)dealloc
{
    [_productIdentifiers release];
    _productIdentifiers = nil;
    [_productPrices release];
    _productPrices = nil;
    [_productCoins release];
    _productCoins = nil;
    [_products release];
    _products = nil;
    [_purchasedProducts release];
    _purchasedProducts = nil;
    [_request release];
    _request = nil;
    [super dealloc];
}
//------------------------------------------------------------------------------------------
@end
