//
//  KanaGame3ViewController.m
//  Pera
//
//  Created by Le Trong Thao on 3/5/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import "KanaGame3ViewController.h"
#import "ProgressView.h"
#import "CircularProgressView.h"
#import "GameUtil.h"
#import "GlobalVariable.h"
#import "LessonHandle.h"
#import "DataHandle.h"

#import "CustomAnimation.h"
#import "CustomImageView.h"

#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

#import "UIViewController+AMSlideMenu.h"
#import "TGDrawSvgPathView.h"

#import "HttpThread.h"

@interface KanaGame3ViewController ()<CircularProgressViewDelegate, UIAlertViewDelegate, AVAudioPlayerDelegate>

@end

@implementation KanaGame3ViewController
{
    ProgressView *progressView;
    UIView *gameView;
    
    NSMutableArray *questionArray;
    
    
    int currentIndex;
    int totalTrueAnswer;
    
    NSMutableArray *answerArray;
    
    AVAudioPlayer *resultPlayer;
    CircularProgressView *circleProgress;
    
    CustomImageView *choosedImgView;
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self disableSlidePanGestureForLeftMenu];
    
    [self initView];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    currentViewController = self;
}

-(void)viewWillDisappear:(BOOL)animated
{
    
}

#pragma mark init View

-(void)initView
{
    
    currentIndex = 0;
    questionArray = [LessonHandle getQuestionSetsFrom:[DataHandle getAllKanaLessonTopic:self.topicID Type:self.type] withAmount:4];
    
    UIButton *backButton = [UIButton new];
    float heightOfBt = 36;
    float widthOfBt = 36;
    backButton.frame = [GameUtil transformFitToScree:CGRectMake(8, 30 , widthOfBt, heightOfBt) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    [backButton setBackgroundImage:[UIImage imageNamed:@"abc_ic_ab_back_mtrl_am_alpha_blue.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backViewController) forControlEvents:UIControlEventTouchUpInside];
    //backButton.backgroundColor = [UIColor blackColor];
    [self.view addSubview:backButton];
    
    
    
    float xProgressView = backButton.frame.origin.x + backButton.frame.size.width + 10;
    float widthProgressView = [UIScreen mainScreen].bounds.size.width - xProgressView*2;
    float heightProgressView = 6;
    
    progressView = [[ProgressView alloc] initWithFrame:CGRectMake(xProgressView, backButton.frame.origin.y+backButton.frame.size.height/2-heightProgressView/2, widthProgressView, heightProgressView) withMax:questionArray.count];
    [self.view addSubview:progressView];
    
    
    UILabel *topLabel = [UILabel new];
    topLabel.frame = CGRectMake(10, backButton.frame.origin.y+backButton.frame.size.height+5, [UIScreen mainScreen].bounds.size.width-20,35);
    topLabel.font = [UIFont boldSystemFontOfSize:15];
    topLabel.numberOfLines = 0;
    topLabel.textAlignment = NSTextAlignmentCenter;
    topLabel.textColor = [UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1];
    topLabel.backgroundColor = [UIColor clearColor];
    topLabel.text = @"Chọn cách viết đúng của chữ";
    [self.view addSubview:topLabel];
    
    [self initGameView];
}


-(void)initGameView
{
    choosedImgView = NULL;
    
    QuestionSet *set = questionArray[currentIndex];
    
    gameView = [UIView new];
    float yGameView = 110;
    gameView.frame = [GameUtil transformFitToScree:CGRectMake(0, yGameView, 320, 568 - yGameView) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    gameView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:gameView];
    
    
    NSArray *allFileName = [set.answer.fileName componentsSeparatedByString:@","];
    NSString *audioFileName = allFileName[0];
    
    float sizeOfCircleProgress = 45*[UIScreen mainScreen].bounds.size.width/320;
    
    circleProgress = [[CircularProgressView alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-sizeOfCircleProgress)/2, 0, sizeOfCircleProgress, sizeOfCircleProgress)
                                                                             backColor:[UIColor colorWithRed:236.0 / 255.0 green:236.0 / 255.0 blue:236.0 / 255.0 alpha:1.0] progressColor:[UIColor colorWithRed:82.0 / 255.0 green:135.0 / 255.0 blue:237.0 / 255.0 alpha:1.0]
                                                                             lineWidth:10
                                                                              audioURL:[LessonHandle getKanaAudioUrlOfFileName:audioFileName]
                                                                               imgPlay:@"icon_play.png"
                                                                               imgStop:@"icon_stop.png"];
    circleProgress.delegate = self;
    [circleProgress.btnPlay setImage:[UIImage imageNamed:@"icon_stop.png"] forState:UIControlStateNormal];
    [circleProgress setTag:1];
    [gameView addSubview:circleProgress];
    [circleProgress play];
    
    
    UILabel *quesLabel = [UILabel new];
    quesLabel.frame = CGRectMake(0, circleProgress.frame.origin.y+sizeOfCircleProgress + 5, [UIScreen mainScreen].bounds.size.width,25);
    quesLabel.font = [UIFont boldSystemFontOfSize:15];
    quesLabel.numberOfLines = 0;
    quesLabel.textAlignment = NSTextAlignmentCenter;
    quesLabel.textColor = [UIColor blackColor];
    quesLabel.backgroundColor = [UIColor clearColor];
    quesLabel.text = [NSString stringWithFormat:@"%@",set.answer.romaji];
    
    [gameView addSubview:quesLabel];
    
    
    
    
    NSMutableArray *numberAr = [[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4"]];
    int heightScreen= [UIScreen mainScreen].bounds.size.height;
    float sizeOfImg;
    if(heightScreen < 500){
        sizeOfImg = 100*[UIScreen mainScreen].bounds.size.height/568;
    }else{
        sizeOfImg = 110*[UIScreen mainScreen].bounds.size.height/568;
    }
    
    
    float between = 30;
    float leftPadding = (gameView.frame.size.width - 2*sizeOfImg - between)/2;
    
    CustomImageView *imgView1 = [[CustomImageView alloc] initWithFrame:CGRectMake(leftPadding,quesLabel.frame.origin.y+quesLabel.frame.size.height+10, sizeOfImg, sizeOfImg)];
    imgView1.lesson = [self randomLesson:numberAr andSet:set];
    [imgView1.layer setBorderWidth:1];
    imgView1.layer.borderColor = [[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor];
    [self drawWordOnView:imgView1];
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectImgView:)];
    tap1.numberOfTouchesRequired = 1;
    [imgView1 addGestureRecognizer:tap1];
    imgView1.contentMode = UIViewContentModeScaleToFill;
    imgView1.userInteractionEnabled = true;
    [gameView addSubview:imgView1];
    
    CustomImageView *imgView2 = [[CustomImageView alloc] initWithFrame:CGRectMake(imgView1.frame.origin.x+sizeOfImg+between,imgView1.frame.origin.y, sizeOfImg, sizeOfImg)];
    imgView2.lesson = [self randomLesson:numberAr andSet:set];
    [imgView2.layer setBorderWidth:1];
    imgView2.layer.borderColor = [[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor];
    [self drawWordOnView:imgView2];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectImgView:)];
    tap2.numberOfTouchesRequired = 1;
    [imgView2 addGestureRecognizer:tap2];
    imgView2.contentMode = UIViewContentModeScaleToFill;
    imgView2.userInteractionEnabled = true;
    [gameView addSubview:imgView2];
    
    
    CustomImageView *imgView3 = [[CustomImageView alloc] initWithFrame:CGRectMake(leftPadding,imgView1.frame.origin.y+imgView1.frame.size.height+10, sizeOfImg, sizeOfImg)];
    imgView3.lesson = [self randomLesson:numberAr andSet:set];
    [imgView3.layer setBorderWidth:1];
    imgView3.layer.borderColor = [[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor];
    [self drawWordOnView:imgView3];
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectImgView:)];
    tap3.numberOfTouchesRequired = 1;
    [imgView3 addGestureRecognizer:tap3];
    imgView3.contentMode = UIViewContentModeScaleToFill;
    imgView3.userInteractionEnabled = true;
    [gameView addSubview:imgView3];
    
    CustomImageView *imgView4 = [[CustomImageView alloc] initWithFrame:CGRectMake(imgView2.frame.origin.x,imgView3.frame.origin.y, sizeOfImg, sizeOfImg)];
    imgView4.lesson = [self randomLesson:numberAr andSet:set];
    [imgView4.layer setBorderWidth:1];
    imgView4.layer.borderColor = [[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor];
    [self drawWordOnView:imgView4];
    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectImgView:)];
    tap4.numberOfTouchesRequired = 1;
    [imgView4 addGestureRecognizer:tap4];
    imgView4.contentMode = UIViewContentModeScaleToFill;
    imgView4.userInteractionEnabled = true;
    [gameView addSubview:imgView4];
    
    
    NSMutableArray *imgViewArray = [[NSMutableArray alloc] initWithArray:@[imgView1,imgView2,imgView3,imgView4]];
    
    [CustomAnimation upperAnimationDuration:0.4
                              WithViewsArray:imgViewArray
                               andCompletion:^(BOOL finish)
     {
         
     }];

    
    
    

    float heightOfNextButton = 35;
    
    if([UIScreen mainScreen].bounds.size.height == 480)
    {
        heightOfNextButton = 30;
    }
    
    UIButton *nextButton = [UIButton new];
    [nextButton setTitle:@"Kiểm tra" forState:UIControlStateNormal];
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setBackgroundColor:[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1]];
    nextButton.frame = CGRectMake((gameView.frame.size.width-120)/2, gameView.frame.size.height - heightOfNextButton - 5, 120, heightOfNextButton);
    [nextButton addTarget:self action:@selector(checkButton:) forControlEvents:UIControlEventTouchUpInside];
    [gameView addSubview:nextButton];
    
    
}



- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(void)initEndingViewWithStars:(int)starNumber
{
    
    UIView *endingView = [UIView new];
    endingView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    endingView.userInteractionEnabled = true;
    endingView.backgroundColor = [UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1];
    [endingView setTag:1];
    [self.view addSubview:endingView];
    
    
    UILabel *infoLabel = [UILabel new];
    infoLabel.frame = CGRectMake(0, 100*[UIScreen mainScreen].bounds.size.height/568.0, [UIScreen mainScreen].bounds.size.width, 40);
    infoLabel.font = [UIFont boldSystemFontOfSize:25];
    infoLabel.textAlignment = NSTextAlignmentCenter;
    infoLabel.textColor = [UIColor whiteColor];
    infoLabel.backgroundColor = [UIColor clearColor];
    if(starNumber == 3)
    {
        infoLabel.text = @"Tuyệt vời";
    }
    else if (starNumber == 2)
    {
        infoLabel.text = @"Tốt";
    }
    else if (starNumber == 1)
    {
        infoLabel.text = @"Cố lên nào";
    }
    else if (starNumber == 0)
    {
        infoLabel.text = @"Cố lên nào";
    }
    
    [endingView addSubview:infoLabel];
    
    
    float sizeOfImg = 40*[UIScreen mainScreen].bounds.size.width/320.0;
    float xStart = ([UIScreen mainScreen].bounds.size.width - (sizeOfImg*3 + 10*2))/2;
    UIImageView *imgViewTemp;
    
    for(int i = 1; i<= 3; i++)
    {
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(xStart, infoLabel.frame.origin.y+infoLabel.frame.size.height+20, sizeOfImg, sizeOfImg)];
        imgView.image = [UIImage imageNamed:@"favorite.png"];
        
        if(i==1&& starNumber==1)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        else if((i==1 ||i ==2) && starNumber == 2)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        else if((i==1 ||i ==2 || i==3) && starNumber == 3)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        
        xStart += sizeOfImg + 10;
        [endingView addSubview:imgView];
        imgViewTemp = imgView;
    }
    
    
    float sizeOfBt = 50*[UIScreen mainScreen].bounds.size.width/320.0;
    
    UIButton *replayButton = [UIButton new];
    float xReplayBt = ([UIScreen mainScreen].bounds.size.width - sizeOfBt*2 - 40)/2;
    replayButton.frame = CGRectMake(xReplayBt, imgViewTemp.frame.origin.y+imgViewTemp.frame.size.height + 30, sizeOfBt, sizeOfBt);
    [replayButton setBackgroundImage:[UIImage imageNamed:@"icon_redo.png"] forState:UIControlStateNormal];
    [replayButton addTarget:self action:@selector(replay) forControlEvents:UIControlEventTouchUpInside];
    [endingView addSubview:replayButton];
    
    
    
    UIButton *nextButton = [UIButton new];
    nextButton.frame = CGRectMake(replayButton.frame.origin.x+sizeOfBt+40, imgViewTemp.frame.origin.y+imgViewTemp.frame.size.height + 30, sizeOfBt, sizeOfBt);
    [nextButton setBackgroundImage:[UIImage imageNamed:@"next_icon.png"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(backViewControllerWhenFinish) forControlEvents:UIControlEventTouchUpInside];
    [endingView addSubview:nextButton];
    
    
    if(starNumber > [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d",self.type,[self.topicID intValue],[self.gameID intValue]]])
    {
        [[NSUserDefaults standardUserDefaults] setInteger:starNumber forKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d",self.type,[self.topicID intValue],[self.gameID intValue]]];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            
            NSString *course = @"";
            if(self.type==1)
            {
                course = @"hiragana";
            }
            else
            {
                course = @"katakana";
            }
            
            [HttpThread upDateToServerGame:[self.gameID intValue] Topic:[self.topicID intValue] Course:course Sub:1 Star:starNumber];
        });
    }
    
    if(starNumber >= 1)
    {
        if([[[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d_islock",self.type,[self.topicID intValue],[self.gameID intValue]+1]] isEqualToString:@"lock"])
        {
            [[NSUserDefaults standardUserDefaults] setValue:@"unlock" forKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d_islock",self.type,[self.topicID intValue],[self.gameID intValue]+1]];
        }
    }
}

-(void)showResViewWithRes:(BOOL)res
{
    float heightOfResView = 50;
    float heightOfNextButton = 35;
    
    
    if([UIScreen mainScreen].bounds.size.height == 480)
    {
        heightOfResView = 40;
        heightOfNextButton = 30;
    }
    
    
    UIView *resView = [UIView new];
    resView.frame = CGRectMake(0, gameView.frame.size.height - 5 - heightOfNextButton - 10 -heightOfResView, gameView.frame.size.width, heightOfResView);
    if(!res) resView.backgroundColor = [UIColor colorWithRed:253.0/255.0 green:235.0/255.0 blue:218.0/255.0 alpha:1];
    else resView.backgroundColor = [UIColor colorWithRed:221.0/255.0 green:230.0/255.0 blue:244.0/255.0 alpha:1];
    [gameView addSubview:resView];
    
    UIImageView *check_wrongImgView;
    if(!res) check_wrongImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"check_wrong.png"]];
    else check_wrongImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"check_right.png"]];
    check_wrongImgView.frame = CGRectMake(10, (resView.frame.size.height - 20)/2, 20, 20);
    [check_wrongImgView setAlpha:1];
    [resView addSubview:check_wrongImgView];
    
    UILabel *resLb = [UILabel new];
    resLb.frame = CGRectMake(check_wrongImgView.frame.origin.x+check_wrongImgView.frame.size.width+10, 0, resView.frame.size.width - (check_wrongImgView.frame.origin.x+check_wrongImgView.frame.size.width+10), resView.frame.size.height);
    resLb.font = [UIFont boldSystemFontOfSize:17];
    resLb.textAlignment = NSTextAlignmentLeft;
    resLb.textColor = [UIColor blackColor];
    resLb.backgroundColor = [UIColor clearColor];
    QuestionSet *set = questionArray[currentIndex];
    resLb.text = [NSString stringWithFormat:@"%@",set.answer.hiragana];
    [resView addSubview:resLb];
    
    UIButton *nextButton = [UIButton new];
    [nextButton setTitle:@"Tiếp tục" forState:UIControlStateNormal];
    if(currentIndex == questionArray.count-1)
    {
        [nextButton setTitle:@"Kết thúc" forState:UIControlStateNormal];
    }
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setBackgroundColor:[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1]];
    nextButton.frame = CGRectMake((gameView.frame.size.width-120)/2, gameView.frame.size.height - heightOfNextButton - 5, 120, heightOfNextButton);
    [nextButton addTarget:self action:@selector(nextLesson) forControlEvents:UIControlEventTouchUpInside];
    [gameView addSubview:nextButton];
    
    
    NSString *audioFilePath;
    if(!res) audioFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"fail.mp3"];
    else audioFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"true.mp3"];
    NSURL *url = [[NSURL alloc] initFileURLWithPath:audioFilePath];
    resultPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    resultPlayer.numberOfLoops = 0;
    resultPlayer.delegate = self;
    [resultPlayer play];
}


#pragma mark- button

-(void)backViewController
{
    NSString *mes = @"Bạn có chắc chắn muốn thoát game này ?";
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:mes
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    
    [alert show];
}

-(IBAction)replayAudio:(UITapGestureRecognizer*)sender
{
    CircularProgressView *button  = (CircularProgressView*)[gameView viewWithTag:1];
    [button.btnPlay setImage:[UIImage imageNamed:@"icon_stop.png"] forState:UIControlStateNormal];
    [button play];
}

-(void)nextLesson
{
    [resultPlayer stop];
    [circleProgress stop];
    
    if(currentIndex < questionArray.count)
    {
        
        [gameView removeFromSuperview];
        
        [self initGameView];
    }
    else
    {
        
        
        [self initEndingViewWithStars:[self getStars]];
    }
}

-(IBAction)checkButton:(UIButton*)sender
{
    
    
    QuestionSet *set  = questionArray[currentIndex];
    
    if(choosedImgView.lesson.lessonID == set.answer.lessonID)
    {
        NSLog(@"true");
        [self handleWhenTrueAnswer:nil];
    }
    else
    {
        NSLog(@"false");
        [self handleWhenFalseAnswer:nil];
    }
    
}

-(IBAction)didSelectImgView:(UITapGestureRecognizer*)sender
{
    
    
    choosedImgView.layer.borderColor = sender.view.layer.borderColor;
    
    CustomImageView *img = (CustomImageView*)sender.view;
    choosedImgView = img;
    
    [img.layer setBorderWidth:1];
    img.layer.borderColor = [[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1] CGColor];
    
    
}


-(void)backViewControllerWhenFinish
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) replay
{
    for(UIView *view in self.view.subviews)
    {
        [view removeFromSuperview];
    }
    
    [self initView];
    
}

#pragma mark- CircularProgressViewDelegate

-(void)playerDidFinishPlaying
{
    NSLog(@"playerDidFinishPlaying");
    //CircularProgressView *circleProgress = (CircularProgressView*)[gameView viewWithTag:1];
    
    [circleProgress.btnPlay setImage:[UIImage imageNamed:@"icon_play.png"] forState:UIControlStateNormal];
    
}

-(void)updateProgressViewWithPlayer:(AVAudioPlayer *)player
{
    
}


#pragma mark- alert view delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}



#pragma mark- timer


-(void)timerWhenFalse:(NSTimer*)timer
{
    [self showResViewWithRes:false];
    
    currentIndex++;
    
    [progressView setPercentComplete:currentIndex];
    
    
}

-(void)timerWhenTrue:(NSTimer*)timer
{
    
    [self showResViewWithRes:true];
    
    totalTrueAnswer++;
    currentIndex++;
    
    [progressView setPercentComplete:currentIndex];
}


#pragma mark- private method

-(void)handleWhenFalseAnswer:(CustomImageView*)imgView
{
    NSTimer *t;
    t = [NSTimer scheduledTimerWithTimeInterval:0.0
                                         target:self
                                       selector:@selector(timerWhenFalse:)
                                       userInfo:imgView
                                        repeats:NO];
}

-(void)handleWhenTrueAnswer:(CustomImageView*)imgView
{
    NSTimer *t;
    t = [NSTimer scheduledTimerWithTimeInterval:0.0
                                         target:self
                                       selector:@selector(timerWhenTrue:)
                                       userInfo:imgView
                                        repeats:NO];
}


-(Lesson*)randomLesson:(NSMutableArray*)numberArray andSet:(QuestionSet*)set
{
    Lesson *ls;
    
    int random = arc4random_uniform(numberArray.count);
    NSString *str = numberArray[random];
    [numberArray removeObject:str];
    
    if([str isEqualToString:@"1"])
    {
        ls = set.answer;
    }
    else
    {
        ls = set.falseAnswerArray[[str intValue] - 2];
    }
    
    return ls;
}

-(int)getStars
{
    int stars = 0;
    
    if((float)totalTrueAnswer/questionArray.count >= 0.8)
    {
        stars = 3;
    }
    else if((float)totalTrueAnswer/questionArray.count >= 0.5)
    {
        stars = 2;
    }
    else if((float)totalTrueAnswer/questionArray.count >= 0.3)
    {
        stars = 1;
    }
    
    return stars;
}


#pragma mark draw method

-(float)getSizeOfWord:(NSString*)word onView:(UIView*)containView withWordArray:(NSArray*)wordAr
{
    float size = 0;
    float wordRatio = 0;
    float totalRatio = 0;
    
    NSArray *smallWord = @[@"ょ",@"ゅ",@"ゃ",@"っ"];
    
    for(NSString *w in wordAr)
    {
        if([smallWord containsObject:w])
            totalRatio += 3;
        else
            totalRatio += 5;
    }
    
    if([smallWord containsObject:word])
        wordRatio = 3;
    else
        wordRatio = 5;
    
    
    size = wordRatio*containView.frame.size.width/totalRatio;
    
    return size;
}


-(void)adjustSubFrameOnView:(UIView*)containView
{
    if(containView.subviews.count > 1)
    {
        float max = 0;
        
        for(TGDrawSvgPathView *svgView in containView.subviews)
        {
            if(svgView.frame.origin.y+svgView.frame.size.height > max)
            {
                max = svgView.frame.origin.y+svgView.frame.size.height;
            }
            
        }
        
        for(TGDrawSvgPathView *svgView in containView.subviews)
        {
            svgView.frame = CGRectMake(svgView.frame.origin.x, max - svgView.frame.size.height, svgView.frame.size.width, svgView.frame.size.height);
            
        }
    }
}

-(void)drawWordOnView:(CustomImageView*)imgView
{
    NSArray *allFileName = [imgView.lesson.fileName componentsSeparatedByString:@","];
    NSMutableArray *svgFileAr = [NSMutableArray new];
    
    if(allFileName.count > 1)
    {
        for(int i = 1; i < allFileName.count; i++)
        {
            [svgFileAr addObject:allFileName[i]];
        }
    }
    else
    {
        [svgFileAr addObject:allFileName[0]];
    }
    
    
    NSMutableArray *wordArray = [NSMutableArray new];
    for(int i = 0; i < imgView.lesson.hiragana.length; i++)
    {
        [wordArray addObject:[imgView.lesson.hiragana substringWithRange:NSMakeRange(i, 1)]];
    }
    
    int xStart = 0;
    for (int i = 0; i < svgFileAr.count; i++)
    {
        float sizeOfSvgView = [self getSizeOfWord:wordArray[i] onView:imgView withWordArray:wordArray];
        TGDrawSvgPathView *svgView = [[TGDrawSvgPathView alloc] initWithFrame:CGRectMake(xStart, (imgView.frame.size.height - sizeOfSvgView)/2, sizeOfSvgView, sizeOfSvgView)];
        svgView.tag = 20 + i;
        [svgView setFilePathFromSvg:[LessonHandle getSvgFilePathOf:svgFileAr[i] Type:self.type] strokeColor:[UIColor blackColor] duration:0 lineWidth:2];
        [imgView addSubview:svgView];
        xStart += sizeOfSvgView;
    }
    
    [self adjustSubFrameOnView:imgView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
