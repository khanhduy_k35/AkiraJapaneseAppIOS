//
//  KanaGame2ViewController.m
//  Pera
//
//  Created by Le Trong Thao on 3/5/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import "KanaGame2ViewController.h"
#import "ProgressView.h"
#import "CircularProgressView.h"
#import "UIViewController+AMSlideMenu.h"
#import "GameUtil.h"
#import "GlobalVariable.h"

#import "LessonHandle.h"
#import "DataHandle.h"

#import "CustomButton.h"
#import "CustomAnimation.h"
#import "TGDrawSvgPathView.h"


#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

#import "HttpThread.h"


@interface KanaGame2ViewController ()<CircularProgressViewDelegate, UIAlertViewDelegate, AVAudioPlayerDelegate>

@end

@implementation KanaGame2ViewController
{
    ProgressView *progressView;
    UIView *gameView;
    
    NSMutableArray *questionArray;
    
    
    int currentIndex;
    int totalTrueAnswer;
    
    NSMutableArray *answerArray;
    
    
    UIView *wordView;
    
    int drawIndex;
    float drawTime;
    NSMutableArray *svgFileAr;
    NSTimer *drawTimer;
    
    
    AVAudioPlayer *readPlayer;
    AVAudioPlayer *resultPlayer;
    CircularProgressView *circleProgress;
    
    CustomButton *choosedImgView;
    
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self disableSlidePanGestureForLeftMenu];
    
    [self initView];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    currentViewController = self;
}

-(void)viewWillDisappear:(BOOL)animated
{
    
}

#pragma mark init View

-(void)initView
{
    drawTime = 1.5;
    
    currentIndex = 0;
    questionArray = [LessonHandle getQuestionSetsFrom:[DataHandle getAllKanaLessonTopic:self.topicID Type:self.type] withAmount:3];
    
    UIButton *backButton = [UIButton new];
    float heightOfBt = 36;
    float widthOfBt = 36;
    backButton.frame = [GameUtil transformFitToScree:CGRectMake(8, 30 , widthOfBt, heightOfBt) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    [backButton setBackgroundImage:[UIImage imageNamed:@"abc_ic_ab_back_mtrl_am_alpha_blue.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backViewController) forControlEvents:UIControlEventTouchUpInside];
    //backButton.backgroundColor = [UIColor blackColor];
    [self.view addSubview:backButton];
    
    
    
    float xProgressView = backButton.frame.origin.x + backButton.frame.size.width + 10;
    float widthProgressView = [UIScreen mainScreen].bounds.size.width - xProgressView*2;
    float heightProgressView = 6;
    
    progressView = [[ProgressView alloc] initWithFrame:CGRectMake(xProgressView, backButton.frame.origin.y+backButton.frame.size.height/2-heightProgressView/2, widthProgressView, heightProgressView) withMax:questionArray.count];
    [self.view addSubview:progressView];
    
    
    UILabel *topLabel = [UILabel new];
    topLabel.frame = CGRectMake(10, backButton.frame.origin.y+backButton.frame.size.height, [UIScreen mainScreen].bounds.size.width-20,35);
    topLabel.font = [UIFont boldSystemFontOfSize:15];
    topLabel.numberOfLines = 0;
    topLabel.textAlignment = NSTextAlignmentCenter;
    topLabel.textColor = [UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1];
    topLabel.backgroundColor = [UIColor clearColor];
    topLabel.text = @"Chọn cách đọc đúng";
    [self.view addSubview:topLabel];
    
    [self initGameView];
}


-(void)initGameView
{
    choosedImgView = NULL;
    drawIndex = 0;
    QuestionSet *set = questionArray[currentIndex];
    
    gameView = [UIView new];
    float yGameView = 100;
    gameView.frame = [GameUtil transformFitToScree:CGRectMake(0, yGameView, 320, 568 - yGameView) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    gameView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:gameView];
    
    
    NSArray *allFileName = [set.answer.fileName componentsSeparatedByString:@","];
    NSString *audioFileName = allFileName[0];
    svgFileAr = [NSMutableArray new];
    
    if(allFileName.count > 1)
    {
        for(int i = 1; i < allFileName.count; i++)
        {
            [svgFileAr addObject:allFileName[i]];
        }
    }
    else
    {
        [svgFileAr addObject:allFileName[0]];
    }
    
    
    NSMutableArray *wordArray = [NSMutableArray new];
    for(int i = 0; i < set.answer.hiragana.length; i++)
    {
        [wordArray addObject:[set.answer.hiragana substringWithRange:NSMakeRange(i, 1)]];
    }
    
    float sizeOfWordBt = 35*[UIScreen mainScreen].bounds.size.width/320;
    float distance = 10;
    float xStart = (gameView.frame.size.width - sizeOfWordBt*svgFileAr.count - distance*(svgFileAr.count-1))/2;
    
    for (int i = 0; i < svgFileAr.count; i++)
    {
        UIButton *wordBt = [UIButton new];
        wordBt.frame = CGRectMake(xStart, 0, sizeOfWordBt, sizeOfWordBt);
        wordBt.tag = 20 + i;
        [wordBt setBackgroundImage:[self imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [wordBt setTitle:wordArray[i] forState:UIControlStateNormal];
        [wordBt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        wordBt.layer.borderWidth = 1;
        wordBt.layer.borderColor = [[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor];
        wordBt.titleLabel.font = [UIFont systemFontOfSize:15];
        [wordBt addTarget:self action:@selector(reDrawWord:) forControlEvents:UIControlEventTouchUpInside];
        [gameView addSubview:wordBt];
        
        xStart += distance + sizeOfWordBt;
        
    }
    
    float sizeOfCircleProgress = 40*[UIScreen mainScreen].bounds.size.width/320;
    
    
    float sizeOfImg = 165*[UIScreen mainScreen].bounds.size.width/320;
    if([UIScreen mainScreen].bounds.size.height == 480)
        sizeOfImg = 140;
    float yImgView = sizeOfWordBt + 5 + sizeOfCircleProgress/2;
    wordView = [UIView new];
    wordView.frame = CGRectMake((gameView.frame.size.width - sizeOfImg)/2, yImgView, sizeOfImg, sizeOfImg);
    wordView.backgroundColor = [UIColor whiteColor];
    wordView.layer.borderWidth = 1;
    wordView.layer.borderColor = [[UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1] CGColor];
    [gameView addSubview:wordView];
    
    
    circleProgress = [[CircularProgressView alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-sizeOfCircleProgress)/2, wordView.frame.origin.y - sizeOfCircleProgress/2, sizeOfCircleProgress, sizeOfCircleProgress)
                                                                             backColor:[UIColor colorWithRed:236.0 / 255.0 green:236.0 / 255.0 blue:236.0 / 255.0 alpha:1.0] progressColor:[UIColor colorWithRed:82.0 / 255.0 green:135.0 / 255.0 blue:237.0 / 255.0 alpha:1.0]
                                                                             lineWidth:10
                                                                              audioURL:[LessonHandle getKanaAudioUrlOfFileName:audioFileName]
                                                                               imgPlay:@"icon_play.png"
                                                                               imgStop:@"icon_stop.png"];
    circleProgress.delegate = self;
    [circleProgress.btnPlay setImage:[UIImage imageNamed:@"icon_stop.png"] forState:UIControlStateNormal];
    [circleProgress setTag:1];
    [gameView addSubview:circleProgress];
    [circleProgress play];
    
    
    xStart = 0;
    for (int i = 0; i < svgFileAr.count; i++)
    {
        float sizeOfSvgView = [self getSizeOfWord:wordArray[i] onView:wordView withWordArray:wordArray];
        TGDrawSvgPathView *svgView = [[TGDrawSvgPathView alloc] initWithFrame:CGRectMake(xStart, (wordView.frame.size.height - sizeOfSvgView)/2, sizeOfSvgView, sizeOfSvgView)];
        svgView.tag = 20 + i;
        if(i==0)[svgView setFilePathFromSvg:[LessonHandle getSvgFilePathOf:svgFileAr[i] Type:self.type] strokeColor:[UIColor blackColor] duration:drawTime];
        [wordView addSubview:svgView];
        xStart += sizeOfSvgView;
    }
    
    
    [self adjustSubFrameOnView:wordView];
    
    drawIndex++;
    drawTimer = [NSTimer scheduledTimerWithTimeInterval:drawTime target:self selector:@selector(drawWord) userInfo:nil repeats:true];
    
    
    
    
    
    
    
    NSMutableArray *numberAr = [[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3"]];
    
    float widthOfBt = 280*[UIScreen mainScreen].bounds.size.width/320;
    float heightOfBt = 35*[UIScreen mainScreen].bounds.size.height/568;
    float between = 5;
    
    CustomButton *bt1 = [[CustomButton alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width - widthOfBt)/2, wordView.frame.origin.y+wordView.frame.size.height+10, widthOfBt,    heightOfBt)];
    bt1.lesson = [self randomLesson:numberAr andSet:set];
    [bt1.layer setBorderWidth:1];
    bt1.layer.cornerRadius = 8;
    bt1.layer.borderColor = [[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1] CGColor];
    [bt1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [bt1.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
    [bt1 setBackgroundColor:[UIColor whiteColor]];
    [bt1 setTitle:bt1.lesson.romaji forState:UIControlStateNormal];
    [bt1 addTarget:self action:@selector(didSelectAmswerButton:) forControlEvents:UIControlEventTouchUpInside];
    [gameView addSubview:bt1];
    
    
    CustomButton *bt2 = [[CustomButton alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width - widthOfBt)/2, bt1.frame.origin.y+bt1.frame.size.height+between, widthOfBt,    heightOfBt)];
    bt2.lesson = [self randomLesson:numberAr andSet:set];
    [bt2.layer setBorderWidth:1];
    bt2.layer.cornerRadius = 8;
    bt2.layer.borderColor = [[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1] CGColor];
    [bt2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [bt2.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
    [bt2 setBackgroundColor:[UIColor whiteColor]];
    [bt2 setTitle:bt2.lesson.romaji forState:UIControlStateNormal];
    [bt2 addTarget:self action:@selector(didSelectAmswerButton:) forControlEvents:UIControlEventTouchUpInside];
    [gameView addSubview:bt2];
    
    CustomButton *bt3 = [[CustomButton alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width - widthOfBt)/2, bt2.frame.origin.y+bt2.frame.size.height+between, widthOfBt,    heightOfBt)];
    bt3.lesson = [self randomLesson:numberAr andSet:set];
    [bt3.layer setBorderWidth:1];
    bt3.layer.cornerRadius = 8;
    bt3.layer.borderColor = [[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1] CGColor];
    [bt3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [bt3.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
    [bt3 setBackgroundColor:[UIColor whiteColor]];
    [bt3 setTitle:bt3.lesson.romaji forState:UIControlStateNormal];
    [bt3 addTarget:self action:@selector(didSelectAmswerButton:) forControlEvents:UIControlEventTouchUpInside];
    [gameView addSubview:bt3];
    
    
    answerArray = [[NSMutableArray alloc] initWithArray:@[bt1,bt2,bt3]];
    
    
    float heightOfNextButton = 35;
    
    if([UIScreen mainScreen].bounds.size.height == 480)
    {
        heightOfNextButton = 25;
    }
    
    
    UIButton *nextButton = [UIButton new];
    [nextButton setTitle:@"Kiểm tra" forState:UIControlStateNormal];
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setBackgroundColor:[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1]];
    nextButton.frame = CGRectMake((gameView.frame.size.width-120)/2, gameView.frame.size.height - heightOfNextButton - 5, 120, heightOfNextButton);
    [nextButton addTarget:self action:@selector(checkButton:) forControlEvents:UIControlEventTouchUpInside];
    [gameView addSubview:nextButton];

    
}



- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(void)initEndingViewWithStars:(int)starNumber
{
    
    UIView *endingView = [UIView new];
    endingView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    endingView.userInteractionEnabled = true;
    endingView.backgroundColor = [UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1];
    [endingView setTag:1];
    [self.view addSubview:endingView];
    
    
    UILabel *infoLabel = [UILabel new];
    infoLabel.frame = CGRectMake(0, 100*[UIScreen mainScreen].bounds.size.height/568.0, [UIScreen mainScreen].bounds.size.width, 40);
    infoLabel.font = [UIFont boldSystemFontOfSize:25];
    infoLabel.textAlignment = NSTextAlignmentCenter;
    infoLabel.textColor = [UIColor whiteColor];
    infoLabel.backgroundColor = [UIColor clearColor];
    if(starNumber == 3)
    {
        infoLabel.text = @"Tuyệt vời";
    }
    else if (starNumber == 2)
    {
        infoLabel.text = @"Tốt";
    }
    else if (starNumber == 1)
    {
        infoLabel.text = @"Cố lên nào";
    }
    else if (starNumber == 0)
    {
        infoLabel.text = @"Cố lên nào";
    }
    
    [endingView addSubview:infoLabel];
    
    
    float sizeOfImg = 40*[UIScreen mainScreen].bounds.size.width/320.0;
    float xStart = ([UIScreen mainScreen].bounds.size.width - (sizeOfImg*3 + 10*2))/2;
    UIImageView *imgViewTemp;
    
    for(int i = 1; i<= 3; i++)
    {
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(xStart, infoLabel.frame.origin.y+infoLabel.frame.size.height+20, sizeOfImg, sizeOfImg)];
        imgView.image = [UIImage imageNamed:@"favorite.png"];
        
        if(i==1&& starNumber==1)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        else if((i==1 ||i ==2) && starNumber == 2)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        else if((i==1 ||i ==2 || i==3) && starNumber == 3)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        
        xStart += sizeOfImg + 10;
        [endingView addSubview:imgView];
        imgViewTemp = imgView;
    }
    
    
    float sizeOfBt = 50*[UIScreen mainScreen].bounds.size.width/320.0;
    
    UIButton *replayButton = [UIButton new];
    float xReplayBt = ([UIScreen mainScreen].bounds.size.width - sizeOfBt*2 - 40)/2;
    replayButton.frame = CGRectMake(xReplayBt, imgViewTemp.frame.origin.y+imgViewTemp.frame.size.height + 30, sizeOfBt, sizeOfBt);
    [replayButton setBackgroundImage:[UIImage imageNamed:@"icon_redo.png"] forState:UIControlStateNormal];
    [replayButton addTarget:self action:@selector(replay) forControlEvents:UIControlEventTouchUpInside];
    [endingView addSubview:replayButton];
    
    
    
    UIButton *nextButton = [UIButton new];
    nextButton.frame = CGRectMake(replayButton.frame.origin.x+sizeOfBt+40, imgViewTemp.frame.origin.y+imgViewTemp.frame.size.height + 30, sizeOfBt, sizeOfBt);
    [nextButton setBackgroundImage:[UIImage imageNamed:@"next_icon.png"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(backViewControllerWhenFinish) forControlEvents:UIControlEventTouchUpInside];
    [endingView addSubview:nextButton];
    
    
    if(starNumber > [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d",self.type,[self.topicID intValue],[self.gameID intValue]]])
    {
        [[NSUserDefaults standardUserDefaults] setInteger:starNumber forKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d",self.type,[self.topicID intValue],[self.gameID intValue]]];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            
            NSString *course = @"";
            if(self.type==1)
            {
                course = @"hiragana";
            }
            else
            {
                course = @"katakana";
            }
            
            [HttpThread upDateToServerGame:[self.gameID intValue] Topic:[self.topicID intValue] Course:course Sub:1 Star:starNumber];
        });
        
    }
    
    if(starNumber >= 1)
    {
        if([[[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d_islock",self.type,[self.topicID intValue],[self.gameID intValue]+1]] isEqualToString:@"lock"])
        {
            [[NSUserDefaults standardUserDefaults] setValue:@"unlock" forKey:[NSString stringWithFormat:@"kana/%d/topic%d/game%d_islock",self.type,[self.topicID intValue],[self.gameID intValue]+1]];
        }
    }
}

-(void)showResViewWithRes:(BOOL)res
{
    float heightOfResView = 50;
    float heightOfNextButton = 35;
    
    
    if([UIScreen mainScreen].bounds.size.height == 480)
    {
        heightOfResView = 40;
        heightOfNextButton = 25;
    }

    
    UIView *resView = [UIView new];
    resView.frame = CGRectMake(0, gameView.frame.size.height - 5 - heightOfNextButton - 10 -heightOfResView, gameView.frame.size.width, heightOfResView);
    if(!res) resView.backgroundColor = [UIColor colorWithRed:253.0/255.0 green:235.0/255.0 blue:218.0/255.0 alpha:1];
    else resView.backgroundColor = [UIColor colorWithRed:221.0/255.0 green:230.0/255.0 blue:244.0/255.0 alpha:1];
    [gameView addSubview:resView];
    
    UIImageView *check_wrongImgView;
    if(!res) check_wrongImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"check_wrong.png"]];
    else check_wrongImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"check_right.png"]];
    check_wrongImgView.frame = CGRectMake(10, (resView.frame.size.height - 20)/2, 20, 20);
    [check_wrongImgView setAlpha:1];
    [resView addSubview:check_wrongImgView];
    
    UILabel *resLb = [UILabel new];
    resLb.frame = CGRectMake(check_wrongImgView.frame.origin.x+check_wrongImgView.frame.size.width+10, 0, resView.frame.size.width - (check_wrongImgView.frame.origin.x+check_wrongImgView.frame.size.width+10), resView.frame.size.height);
    resLb.font = [UIFont boldSystemFontOfSize:17];
    resLb.textAlignment = NSTextAlignmentLeft;
    resLb.textColor = [UIColor blackColor];
    resLb.backgroundColor = [UIColor clearColor];
    QuestionSet *set = questionArray[currentIndex];
    resLb.text = [NSString stringWithFormat:@"%@",set.answer.romaji];
    [resView addSubview:resLb];
    
    UIButton *nextButton = [UIButton new];
    [nextButton setTitle:@"Tiếp tục" forState:UIControlStateNormal];
    if(currentIndex == questionArray.count-1)
    {
        [nextButton setTitle:@"Kết thúc" forState:UIControlStateNormal];
    }
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setBackgroundColor:[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1]];
    nextButton.frame = CGRectMake((gameView.frame.size.width-120)/2, gameView.frame.size.height - heightOfNextButton - 5, 120, heightOfNextButton);
    [nextButton addTarget:self action:@selector(nextLesson) forControlEvents:UIControlEventTouchUpInside];
    [gameView addSubview:nextButton];
    
    
    NSString *audioFilePath;
    if(!res) audioFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"fail.mp3"];
    else audioFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"true.mp3"];
    NSURL *url = [[NSURL alloc] initFileURLWithPath:audioFilePath];
    resultPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    resultPlayer.numberOfLoops = 0;
    resultPlayer.delegate = self;
    [resultPlayer play];
}


#pragma mark- button

-(void)backViewController
{
    NSString *mes = @"Bạn có chắc chắn muốn thoát game này ?";
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:mes
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    
    [alert show];
}

-(IBAction)replayAudio:(UITapGestureRecognizer*)sender
{
    CircularProgressView *button  = (CircularProgressView*)[gameView viewWithTag:1];
    [button.btnPlay setImage:[UIImage imageNamed:@"icon_stop.png"] forState:UIControlStateNormal];
    [button play];
}

-(void)nextLesson
{
    [drawTimer invalidate];
    
    [circleProgress stop];
    
    if(currentIndex < questionArray.count)
    {
        
        [gameView removeFromSuperview];
        
        [self initGameView];
    }
    else
    {
        
        
        [self initEndingViewWithStars:[self getStars]];
    }
}

-(IBAction)checkButton:(UIButton*)sender
{
    
    
    QuestionSet *set  = questionArray[currentIndex];
    
    if(choosedImgView.lesson.lessonID == set.answer.lessonID)
    {
        NSLog(@"true");
        [self handleWhenTrueAnswer:nil];
    }
    else
    {
        NSLog(@"false");
        [self handleWhenFalseAnswer:nil];
    }
    
}

-(IBAction)didSelectAmswerButton:(id)sender
{
    
    //    for(CustomButton *imgView in answerArray)
    //    {
    //        imgView.userInteractionEnabled = false;
    //    }
    
    if(choosedImgView)
    {
        [choosedImgView setBackgroundColor:[UIColor whiteColor]];
    }
    
    CustomButton *img = (CustomButton*)sender;
    choosedImgView = img;
    [img setBackgroundColor:[UIColor colorWithRed:233.0/255.0 green:233.0/255.0 blue:233.0/255.0 alpha:1]];
    [img.layer setBorderWidth:1];
    img.layer.borderColor = [[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1] CGColor];
    
    //    NSURL *audioUrl = [LessonHandle getAudioUrlOfFileName:img.lesson.fileName TopicID:img.lesson.topic Level:g_course];
    //    readPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioUrl error:nil];
    //    readPlayer.numberOfLoops = 0;
    //    readPlayer.delegate = self;
    //    [readPlayer play];
    
}



-(void)backViewControllerWhenFinish
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) replay
{
    for(UIView *view in self.view.subviews)
    {
        [view removeFromSuperview];
    }
    
    [self initView];
    
}

#pragma mark- CircularProgressViewDelegate

-(void)playerDidFinishPlaying
{
    NSLog(@"playerDidFinishPlaying");
    //CircularProgressView *circleProgress = (CircularProgressView*)[gameView viewWithTag:1];
    
    [circleProgress.btnPlay setImage:[UIImage imageNamed:@"icon_play.png"] forState:UIControlStateNormal];
    
}

-(void)updateProgressViewWithPlayer:(AVAudioPlayer *)player
{
    
}


#pragma mark- alert view delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}



#pragma mark- timer


-(void)timerWhenFalse:(NSTimer*)timer
{
    [self showResViewWithRes:false];
    
    currentIndex++;
    
    [progressView setPercentComplete:currentIndex];
    
    
}

-(void)timerWhenTrue:(NSTimer*)timer
{
    
    [self showResViewWithRes:true];
    
    totalTrueAnswer++;
    currentIndex++;
    
    [progressView setPercentComplete:currentIndex];
}


#pragma mark- private method

-(void)handleWhenFalseAnswer:(CustomButton*)imgView
{
    NSTimer *t;
    t = [NSTimer scheduledTimerWithTimeInterval:0.0
                                         target:self
                                       selector:@selector(timerWhenFalse:)
                                       userInfo:imgView
                                        repeats:NO];
}

-(void)handleWhenTrueAnswer:(CustomButton*)imgView
{
    NSTimer *t;
    t = [NSTimer scheduledTimerWithTimeInterval:0.0
                                         target:self
                                       selector:@selector(timerWhenTrue:)
                                       userInfo:imgView
                                        repeats:NO];
}


-(Lesson*)randomLesson:(NSMutableArray*)numberArray andSet:(QuestionSet*)set
{
    Lesson *ls;
    
    int random = arc4random_uniform(numberArray.count);
    NSString *str = numberArray[random];
    [numberArray removeObject:str];
    
    if([str isEqualToString:@"1"])
    {
        ls = set.answer;
    }
    else
    {
        ls = set.falseAnswerArray[[str intValue] - 2];
    }
    
    return ls;
}

-(int)getStars
{
    int stars = 0;
    
    if((float)totalTrueAnswer/questionArray.count >= 0.8)
    {
        stars = 3;
    }
    else if((float)totalTrueAnswer/questionArray.count >= 0.5)
    {
        stars = 2;
    }
    else if((float)totalTrueAnswer/questionArray.count >= 0.3)
    {
        stars = 1;
    }
    
    return stars;
}


#pragma mark draw method

-(float)getSizeOfWord:(NSString*)word onView:(UIView*)containView withWordArray:(NSArray*)wordAr
{
    float size = 0;
    float wordRatio = 0;
    float totalRatio = 0;
    
    NSArray *smallWord = @[@"ょ",@"ゅ",@"ゃ",@"っ"];
    
    for(NSString *w in wordAr)
    {
        if([smallWord containsObject:w])
            totalRatio += 3;
        else
            totalRatio += 5;
    }
    
    if([smallWord containsObject:word])
        wordRatio = 3;
    else
        wordRatio = 5;
    
    
    size = wordRatio*containView.frame.size.width/totalRatio;
    
    return size;
}


-(void)adjustSubFrameOnView:(UIView*)containView
{
    if(containView.subviews.count > 1)
    {
        float max = 0;
        
        for(TGDrawSvgPathView *svgView in containView.subviews)
        {
            if(svgView.frame.origin.y+svgView.frame.size.height > max)
            {
                max = svgView.frame.origin.y+svgView.frame.size.height;
            }
            
        }
        
        for(TGDrawSvgPathView *svgView in containView.subviews)
        {
            svgView.frame = CGRectMake(svgView.frame.origin.x, max - svgView.frame.size.height, svgView.frame.size.width, svgView.frame.size.height);
            
        }
    }
}

-(void)drawWord
{
    if(drawIndex == svgFileAr.count)
    {
        [drawTimer invalidate];
    }
    else
    {
        TGDrawSvgPathView *svgView = (TGDrawSvgPathView*)[wordView viewWithTag:20+drawIndex];
        [svgView setFilePathFromSvg:[LessonHandle getSvgFilePathOf:svgFileAr[drawIndex] Type:self.type] strokeColor:[UIColor blackColor] duration:drawTime];
        drawIndex++;
        
    }
}

-(IBAction)reDrawWord:(UIButton*)sender
{
    if(drawIndex == svgFileAr.count)
    {
        TGDrawSvgPathView *oldSvgView = (TGDrawSvgPathView*)[wordView viewWithTag:sender.tag];
        
        TGDrawSvgPathView *newSvgView = [TGDrawSvgPathView new];
        newSvgView.frame = oldSvgView.frame;
        newSvgView.tag = oldSvgView.tag;
        [wordView addSubview:newSvgView];
        [oldSvgView removeFromSuperview];
        [newSvgView setFilePathFromSvg:[LessonHandle getSvgFilePathOf:svgFileAr[newSvgView.tag - 20] Type:self.type] strokeColor:[UIColor blackColor] duration:drawTime];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
