//
//  RateAppView.h
//  Pera
//
//  Created by Le Trong Thao on 12/6/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol RateAppViewDelegate <NSObject>

@optional

-(void)rateAppTouched:(UIView*)rateView;
-(void)sendFeedbackTouched:(UIView*)rateView;
-(void)ignoreTouched:(UIView*)rateView;

@end

@interface RateAppView : UIView

@property id<RateAppViewDelegate> delegate;

-(id)initWithFrame:(CGRect)frame;

@end
