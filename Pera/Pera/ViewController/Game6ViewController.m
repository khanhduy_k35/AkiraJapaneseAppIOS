//
//  Game6ViewController.m
//  Pera
//
//  Created by Le Trong Thao on 3/2/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import "Game6ViewController.h"
#import "ProgressView.h"
#import "CircularProgressView.h"
#import "GameUtil.h"
#import "GlobalVariable.h"
//#import "JsonFileHandle.h"
//#import "SqlManager.h"
#import "LessonHandle.h"
#import "DataHandle.h"

#import "CustomAnimation.h"
#import "CustomButton.h"

#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

#import "UIViewController+AMSlideMenu.h"
#import "HttpThread.h"

@interface Game6ViewController ()<CircularProgressViewDelegate,UIAlertViewDelegate,AVAudioPlayerDelegate>

@end

@implementation Game6ViewController
{
    NSMutableArray *questionArray;
    
    ProgressView *progressView;
    UIView *gameView;
    int currentIndex;
    
    
    int totalTrueAnswer;
    int life;
    NSMutableArray *heartImgArray;
    
    NSMutableArray *answerArray;
    
    
    
    CircularProgressView *circleProgress;
    
    AVAudioPlayer *readPlayer;
    AVAudioPlayer *resultPlayer;
    
    CustomButton *choosedImgView;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self disableSlidePanGestureForLeftMenu];
    [self disableSlidePanGestureForRightMenu];
    
    ((AppDelegate *)[[UIApplication sharedApplication] delegate]).isGame3Success = NO;
    
    [self initView];
    
}


-(void)viewDidAppear:(BOOL)animated
{
    currentViewController = self;
}

-(void)viewWillDisappear:(BOOL)animated
{
    
}

#pragma mark initView

-(void)initView
{
    totalTrueAnswer = 0;
    currentIndex = 0;
    questionArray = [LessonHandle getQuestionSetsFrom:[DataHandle getAllLessonOfTopic:[self.topicID intValue] Level:g_course Sub:self.sub] withAmount:3];
    
    UIButton *backButton = [UIButton new];
    float heightOfBt = 36;
    float widthOfBt = 36;
    backButton.frame = [GameUtil transformFitToScree:CGRectMake(8, 30 , widthOfBt, heightOfBt) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    [backButton setBackgroundImage:[UIImage imageNamed:@"abc_ic_ab_back_mtrl_am_alpha_blue.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backViewController) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:backButton];
    
    
    float xProgressView = backButton.frame.origin.x + backButton.frame.size.width + 10;
    float widthProgressView = [UIScreen mainScreen].bounds.size.width - xProgressView*2;
    float heightProgressView = 6;
    
    progressView = [[ProgressView alloc] initWithFrame:CGRectMake(xProgressView, backButton.frame.origin.y+backButton.frame.size.height/2-heightProgressView/2, widthProgressView, heightProgressView) withMax:questionArray.count];
    [self.view addSubview:progressView];
    
    
    UILabel *topLabel = [UILabel new];
    topLabel.frame = CGRectMake(10, progressView.frame.origin.y+progressView.frame.size.height+15, [UIScreen mainScreen].bounds.size.width-20,25);
    topLabel.font = [UIFont systemFontOfSize:15];
    topLabel.numberOfLines = 0;
    topLabel.textAlignment = NSTextAlignmentCenter;
    topLabel.textColor = [UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1];
    topLabel.backgroundColor = [UIColor clearColor];
    topLabel.text = @"Chọn nghĩa của tử bạn vừa nghe được";
    [self.view addSubview:topLabel];
    
    
    [self initGameView];

    
}


-(void)initGameView
{
    QuestionSet *set = questionArray[currentIndex];
    
    choosedImgView = NULL;
    gameView = [UIView new];
    float yGameView = 100;
    gameView.frame = [GameUtil transformFitToScree:CGRectMake(0, yGameView, 320, 568 - yGameView) withScreenWidth:[UIScreen mainScreen].bounds.size.width withScreenHeight:[UIScreen mainScreen].bounds.size.height];
    gameView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:gameView];
    
    float sizeOfCircleProgress = 100*[UIScreen mainScreen].bounds.size.width/320;
    circleProgress = [[CircularProgressView alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-sizeOfCircleProgress - 10 - sizeOfCircleProgress*3/4)/2, 0, sizeOfCircleProgress, sizeOfCircleProgress)
                                                       backColor:[UIColor colorWithRed:236.0 / 255.0 green:236.0 / 255.0 blue:236.0 / 255.0 alpha:1.0] progressColor:[UIColor colorWithRed:82.0 / 255.0 green:135.0 / 255.0 blue:237.0 / 255.0 alpha:1.0]
                                                       lineWidth:10
                                                        audioURL:[LessonHandle getAudioUrlOfFileName:set.answer.fileName TopicID:[self.topicID intValue] Level:g_course]
                                                         imgPlay:@"icon_play.png"
                                                         imgStop:@"icon_stop.png"];
    circleProgress.delegate = self;
    [circleProgress setTag:1];
    [gameView addSubview:circleProgress];
    [circleProgress.btnPlay setImage:[UIImage imageNamed:@"icon_stop.png"] forState:UIControlStateNormal];
    [circleProgress play];
    
    
    UIView *slowView = [UIView new];
    slowView.userInteractionEnabled = true;
    slowView.frame = CGRectMake(circleProgress.frame.origin.x + circleProgress.frame.size.width + 10, circleProgress.frame.origin.y+circleProgress.frame.size.height-circleProgress.frame.size.height*3/4, sizeOfCircleProgress*3/4, sizeOfCircleProgress*3/4);
    slowView.layer.masksToBounds = true;
   // slowView.layer.cornerRadius = slowView.frame.size.width/2;
   // slowView.layer.borderWidth = 2;
    //[slowView.layer setBorderColor:[[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1] CGColor]];
    [gameView addSubview:slowView];
    
    UIButton *slowBt = [UIButton new];
    slowBt.frame = CGRectMake(5, 5, slowView.frame.size.width - 10, slowView.frame.size.height - 10);
    slowBt.layer.masksToBounds = true;
   // slowBt.layer.cornerRadius = slowBt.frame.size.width/2;
    [slowBt setBackgroundImage:[UIImage imageNamed:@"slow.png"] forState:UIControlStateNormal];
    [slowBt addTarget:self action:@selector(slowSpeed) forControlEvents:UIControlEventTouchUpInside];
    [slowView addSubview:slowBt];
    
    NSMutableArray *numberAr = [[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3"]];
    
    float widthOfBt = 280*[UIScreen mainScreen].bounds.size.width/320;
    float heightOfBt = 35*[UIScreen mainScreen].bounds.size.height/568;
    float between = 5;
    
    CustomButton *bt1 = [[CustomButton alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width - widthOfBt)/2, circleProgress.frame.origin.y+circleProgress.frame.size.height+25, widthOfBt,    heightOfBt)];
    bt1.lesson = [self randomLesson:numberAr andSet:set];
    [bt1.layer setBorderWidth:1];
    bt1.layer.cornerRadius = 8;
    bt1.layer.borderColor = [[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1] CGColor];
    [bt1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [bt1.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
    [bt1 setBackgroundColor:[UIColor whiteColor]];
    [bt1 setTitle:bt1.lesson.vietnamese forState:UIControlStateNormal];
    [bt1 addTarget:self action:@selector(didSelectAmswerButton:) forControlEvents:UIControlEventTouchUpInside];
    [gameView addSubview:bt1];
    
    
    CustomButton *bt2 = [[CustomButton alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width - widthOfBt)/2, bt1.frame.origin.y+bt1.frame.size.height+between, widthOfBt,    heightOfBt)];
    bt2.lesson = [self randomLesson:numberAr andSet:set];
    [bt2.layer setBorderWidth:1];
    bt2.layer.cornerRadius = 8;
    bt2.layer.borderColor = [[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1] CGColor];
    [bt2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [bt2.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
    [bt2 setBackgroundColor:[UIColor whiteColor]];
    [bt2 setTitle:bt2.lesson.vietnamese forState:UIControlStateNormal];
    [bt2 addTarget:self action:@selector(didSelectAmswerButton:) forControlEvents:UIControlEventTouchUpInside];
    [gameView addSubview:bt2];
    
    CustomButton *bt3 = [[CustomButton alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width - widthOfBt)/2, bt2.frame.origin.y+bt2.frame.size.height+between, widthOfBt,    heightOfBt)];
    bt3.lesson = [self randomLesson:numberAr andSet:set];
    [bt3.layer setBorderWidth:1];
    bt3.layer.cornerRadius = 8;
    bt3.layer.borderColor = [[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1] CGColor];
    [bt3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [bt3.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
    [bt3 setBackgroundColor:[UIColor whiteColor]];
    [bt3 setTitle:bt3.lesson.vietnamese forState:UIControlStateNormal];
    [bt3 addTarget:self action:@selector(didSelectAmswerButton:) forControlEvents:UIControlEventTouchUpInside];
    [gameView addSubview:bt3];
    
    
    answerArray = [[NSMutableArray alloc] initWithArray:@[bt1,bt2,bt3]];
    
    
    float heightOfNextButton = 40;
    
//    if([UIScreen mainScreen].bounds.size.height == 480)
//    {
//        heightOfNextButton = 25;
//    }

    
    UIButton *nextButton = [UIButton new];
    [nextButton setTitle:@"Kiểm tra" forState:UIControlStateNormal];
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setBackgroundColor:[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1]];
    nextButton.frame = CGRectMake((gameView.frame.size.width-120)/2, gameView.frame.size.height - heightOfNextButton - 5, 120, heightOfNextButton);
    [nextButton addTarget:self action:@selector(checkButton:) forControlEvents:UIControlEventTouchUpInside];
    [gameView addSubview:nextButton];

}


-(void)initEndingViewWithStars:(int)starNumber
{
    
    UIView *endingView = [UIView new];
    endingView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    endingView.userInteractionEnabled = true;
    endingView.backgroundColor = [UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1];
    [endingView setTag:1];
    [self.view addSubview:endingView];
    
    
    UILabel *infoLabel = [UILabel new];
    infoLabel.frame = CGRectMake(0, 100*[UIScreen mainScreen].bounds.size.height/568.0, [UIScreen mainScreen].bounds.size.width, 40);
    infoLabel.font = [UIFont boldSystemFontOfSize:25];
    infoLabel.textAlignment = NSTextAlignmentCenter;
    infoLabel.textColor = [UIColor whiteColor];
    infoLabel.backgroundColor = [UIColor clearColor];
    if(starNumber == 3)
    {
        infoLabel.text = @"Tuyệt vời";
    }
    else if (starNumber == 2)
    {
        infoLabel.text = @"Tốt";
    }
    else if (starNumber == 1)
    {
        infoLabel.text = @"Cố lên nào";
    }
    else if (starNumber == 0)
    {
        infoLabel.text = @"Cố lên nào";
    }
    
    [endingView addSubview:infoLabel];
    
    
    float sizeOfImg = 40*[UIScreen mainScreen].bounds.size.width/320.0;
    float xStart = ([UIScreen mainScreen].bounds.size.width - (sizeOfImg*3 + 10*2))/2;
    UIImageView *imgViewTemp;
    
    for(int i = 1; i<= 3; i++)
    {
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(xStart, infoLabel.frame.origin.y+infoLabel.frame.size.height+20, sizeOfImg, sizeOfImg)];
        imgView.image = [UIImage imageNamed:@"favorite.png"];
        
        if(i==1&& starNumber==1)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        else if((i==1 ||i ==2) && starNumber == 2)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        else if((i==1 ||i ==2 || i==3) && starNumber == 3)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        
        xStart += sizeOfImg + 10;
        [endingView addSubview:imgView];
        imgViewTemp = imgView;
    }
    
    
    float sizeOfBt = 50*[UIScreen mainScreen].bounds.size.width/320.0;
    
    UIButton *replayButton = [UIButton new];
    float xReplayBt = ([UIScreen mainScreen].bounds.size.width - sizeOfBt*2 - 40)/2;
    replayButton.frame = CGRectMake(xReplayBt, imgViewTemp.frame.origin.y+imgViewTemp.frame.size.height + 30, sizeOfBt, sizeOfBt);
    [replayButton setBackgroundImage:[UIImage imageNamed:@"icon_redo.png"] forState:UIControlStateNormal];
    [replayButton addTarget:self action:@selector(replay) forControlEvents:UIControlEventTouchUpInside];
    [endingView addSubview:replayButton];
    
    
    
    UIButton *nextButton = [UIButton new];
    nextButton.frame = CGRectMake(replayButton.frame.origin.x+sizeOfBt+40, imgViewTemp.frame.origin.y+imgViewTemp.frame.size.height + 30, sizeOfBt, sizeOfBt);
    [nextButton setBackgroundImage:[UIImage imageNamed:@"next_icon.png"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(backViewControllerWhenFinish) forControlEvents:UIControlEventTouchUpInside];
    [endingView addSubview:nextButton];
    
    if(starNumber > [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"%@/topic%d/sub%d/game%d",g_course,[self.topicID intValue],self.sub,[self.gameID intValue]]])
    {
        [[NSUserDefaults standardUserDefaults] setInteger:starNumber forKey:[NSString stringWithFormat:@"%@/topic%d/sub%d/game%d",g_course,[self.topicID intValue],self.sub,[self.gameID intValue]]];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [HttpThread upDateToServerGame:[self.gameID intValue] Topic:[self.topicID intValue] Course:g_course Sub:self.sub Star:starNumber];
        });
    }
    
}

-(void)showResViewWithRes:(BOOL)res
{
    float heightOfResView = 50;
    float heightOfNextButton = 40;
    
    
//    if(self.sub != 4)
//    {
//        if([UIScreen mainScreen].bounds.size.height == 480)
//        {
//            heightOfResView = 40;
//            heightOfNextButton = 25;
//        }
//
//    }

    UIView *resView = [UIView new];
    resView.frame = CGRectMake(0, gameView.frame.size.height - 5 - heightOfNextButton - 10 -heightOfResView, gameView.frame.size.width, heightOfResView);
    if(!res) resView.backgroundColor = [UIColor colorWithRed:253.0/255.0 green:235.0/255.0 blue:218.0/255.0 alpha:1];
    else resView.backgroundColor = [UIColor colorWithRed:221.0/255.0 green:230.0/255.0 blue:244.0/255.0 alpha:1];
    [gameView addSubview:resView];
    
    UIImageView *check_wrongImgView;
    if(!res) check_wrongImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"check_wrong.png"]];
    else check_wrongImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"check_right.png"]];
    check_wrongImgView.frame = CGRectMake(10, (resView.frame.size.height - 20)/2, 20, 20);
    [check_wrongImgView setAlpha:1];
    [resView addSubview:check_wrongImgView];
    
    UILabel *resLb = [UILabel new];
    resLb.frame = CGRectMake(check_wrongImgView.frame.origin.x+check_wrongImgView.frame.size.width+10, 0, resView.frame.size.width - (check_wrongImgView.frame.origin.x+check_wrongImgView.frame.size.width+10), resView.frame.size.height);
    resLb.font = [UIFont boldSystemFontOfSize:17];
    resLb.textAlignment = NSTextAlignmentLeft;
    resLb.textColor = [UIColor blackColor];
    resLb.backgroundColor = [UIColor clearColor];
    QuestionSet *set = questionArray[currentIndex];
    resLb.text = [NSString stringWithFormat:@"%@: %@",set.answer.hiragana, set.answer.vietnamese];
    [resView addSubview:resLb];
    
    UIButton *nextButton = [UIButton new];
    [nextButton setTitle:@"Tiếp tục" forState:UIControlStateNormal];
    if(currentIndex == questionArray.count-1)
    {
        [nextButton setTitle:@"Kết thúc" forState:UIControlStateNormal];
    }
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setBackgroundColor:[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1]];
    nextButton.frame = CGRectMake((gameView.frame.size.width-120)/2, gameView.frame.size.height - heightOfNextButton - 5, 120, heightOfNextButton);
    [nextButton addTarget:self action:@selector(nextLesson) forControlEvents:UIControlEventTouchUpInside];
    [gameView addSubview:nextButton];
    
    
    NSString *audioFilePath;
    if(!res) audioFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"fail.mp3"];
    else audioFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"true.mp3"];
    NSURL *url = [[NSURL alloc] initFileURLWithPath:audioFilePath];
    resultPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    resultPlayer.numberOfLoops = 0;
    resultPlayer.delegate = self;
    [resultPlayer play];
}

#pragma mark- init button


-(void)slowSpeed
{
    
    [circleProgress stop];
    
    [readPlayer stop];
    
    readPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:circleProgress.audioURL  error:nil];
    readPlayer.enableRate = true;
    readPlayer.rate = 0.5;
    readPlayer.numberOfLoops = 0;
    [readPlayer play];
}

-(IBAction)replayAudio:(UITapGestureRecognizer*)sender
{
    CircularProgressView *button  = (CircularProgressView*)[gameView viewWithTag:1];
    [button.btnPlay setImage:[UIImage imageNamed:@"icon_stop.png"] forState:UIControlStateNormal];
    [button play];
}

-(void)backViewController
{
    NSString *mes = @"Bạn có chắc chắn muốn thoát game này ?";
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:mes
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    
    [alert show];
    
}


-(IBAction)didSelectAmswerButton:(id)sender
{
    
//    for(CustomButton *imgView in answerArray)
//    {
//        imgView.userInteractionEnabled = false;
//    }

    if(choosedImgView)
    {
        [choosedImgView setBackgroundColor:[UIColor whiteColor]];
    }
    
    CustomButton *img = (CustomButton*)sender;
    choosedImgView = img;
    [img setBackgroundColor:[UIColor colorWithRed:233.0/255.0 green:233.0/255.0 blue:233.0/255.0 alpha:1]];
    [img.layer setBorderWidth:1];
    img.layer.borderColor = [[UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1] CGColor];
    
//    NSURL *audioUrl = [LessonHandle getAudioUrlOfFileName:img.lesson.fileName TopicID:img.lesson.topic Level:g_course];
//    readPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioUrl error:nil];
//    readPlayer.numberOfLoops = 0;
//    readPlayer.delegate = self;
//    [readPlayer play];
    
}

-(void)backViewControllerWhenFinish
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) replay
{
    for(UIView *view in self.view.subviews)
    {
        [view removeFromSuperview];
    }
    
    [self initView];
    
}

-(IBAction)checkButton:(UIButton*)sender
{
    
    
    QuestionSet *set  = questionArray[currentIndex];
    
    if(choosedImgView.lesson.lessonID == set.answer.lessonID)
    {
        NSLog(@"true");
        [self handleWhenTrueAnswer:nil];
    }
    else
    {
        NSLog(@"false");
        [self handleWhenFalseAnswer:nil];
    }
    
}


-(void)nextLesson
{
    if(currentIndex < questionArray.count)
    {
        
        [gameView removeFromSuperview];
        
        [self initGameView];
    }
    else
    {
        
        
        [self initEndingViewWithStars:[self getStars]];
    }
}


#pragma mark private method

-(void)updateRateDay
{
    NSDate *previousDay = [[NSUserDefaults standardUserDefaults] objectForKey:@"rateDay"];
    if(!previousDay)
    {
        showRateView = true;
        
    }
    else
    {
        NSTimeInterval time = [[NSDate date] timeIntervalSinceDate:previousDay];
        if(time > 24*3600)
        {
            showRateView = true;
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"rateDay"];
    
}


-(Lesson*)randomLesson:(NSMutableArray*)numberArray andSet:(QuestionSet*)set
{
    Lesson *ls;
    
    int random = arc4random_uniform(numberArray.count);
    NSString *str = numberArray[random];
    [numberArray removeObject:str];
    
    if([str isEqualToString:@"1"])
    {
        ls = set.answer;
    }
    else
    {
        ls = set.falseAnswerArray[[str intValue] - 2];
    }
    
    return ls;
}


-(void)handleWhenFalseAnswer:(CustomButton*)imgView
{
    NSTimer *t;
    t = [NSTimer scheduledTimerWithTimeInterval:0.0
                                         target:self
                                       selector:@selector(timerWhenFalse:)
                                       userInfo:imgView
                                        repeats:NO];
}

-(void)handleWhenTrueAnswer:(CustomButton*)imgView
{
    NSTimer *t;
    t = [NSTimer scheduledTimerWithTimeInterval:0.0
                                         target:self
                                       selector:@selector(timerWhenTrue:)
                                       userInfo:imgView
                                        repeats:NO];
}

-(int)getStars
{
    int stars = 0;
    
    if((float)totalTrueAnswer/questionArray.count >= 0.8)
    {
        stars = 3;
    }
    else if((float)totalTrueAnswer/questionArray.count >= 0.5)
    {
        stars = 2;
    }
    else if((float)totalTrueAnswer/questionArray.count >= 0.3)
    {
        stars = 1;
    }
    
    return stars;
}

#pragma mark- timer

-(void)updateLearnTime
{
    [[NSUserDefaults standardUserDefaults] setDouble:([[NSUserDefaults standardUserDefaults] doubleForKey:[NSString stringWithFormat:@"timeLearned_%@",g_course]]+1) forKey:[NSString stringWithFormat:@"timeLearned_%@",g_course]];
    
}

#pragma mark- timer


-(void)timerWhenFalse:(NSTimer*)timer
{
    [self showResViewWithRes:false];
    
    currentIndex++;
    
    [progressView setPercentComplete:currentIndex];
    
    
}

-(void)timerWhenTrue:(NSTimer*)timer
{
    
    [self showResViewWithRes:true];
    
    totalTrueAnswer++;
    currentIndex++;
    
    [progressView setPercentComplete:currentIndex];
}



#pragma mark- circle progress delegate

-(void)playerDidFinishPlaying
{
    NSLog(@"playerDidFinishPlaying");
    
    //CircularProgressView *circleProgress = (CircularProgressView*)[gameView viewWithTag:1];
    [circleProgress.btnPlay setImage:[UIImage imageNamed:@"icon_play.png"] forState:UIControlStateNormal];
    
}

-(void)updateProgressViewWithPlayer:(AVAudioPlayer *)player
{
    
}

#pragma mark- alert view delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark- avaudio delegate

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [player stop];
    
    if(player == resultPlayer)
    {
        //[circleProgress play];
    }
}



@end
