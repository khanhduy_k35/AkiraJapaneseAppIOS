//
//  LeftTableViewController.m
//  Pera
//
//  Created by Le Trong Thao on 1/27/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import "LeftTableViewController.h"
//#import "AMSlideMenuMainViewController.h"

#import "CourseViewController.h"
#import "JsonFileHandle.h"
#import "GlobalVariable.h"
#import "Email.h"

@interface LeftTableViewController ()<UIPickerViewDataSource, UIPickerViewDelegate, AMSlideMenuDelegate>

@end

@implementation LeftTableViewController

{
    
    //UITableViewCell *preCell;
    
    int indexOfChoosedCell;
    
    float heightOfCell;
    
    BOOL showTimePicker;
    
    
    NSMutableArray *hourArray;
    NSMutableArray *minuteArray;
    NSArray *typeArray;
    
    float widthOfPicker;
    
    UIPickerView *timePickerView;
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    showTimePicker = false;
    
    if(self.mainSlideMenu.leftMenuWidth > leftMenuWidth)
    {
        leftMenuWidth = self.mainSlideMenu.leftMenuWidth;
    }
    
    widthOfPicker = [UIScreen mainScreen].bounds.size.width - ([UIScreen mainScreen].bounds.size.width - leftMenuWidth) - 30;
    
    hourArray = [NSMutableArray new];
    minuteArray = [NSMutableArray new];
    typeArray = @[@"AM", @"PM"];
    
    NSString *strVal = [[NSString alloc] init];
    
    for(int i=0; i<61; i++)
    {
        strVal = [NSString stringWithFormat:@"%d", i];
        
        if(strVal.length == 1)
        {
            strVal = [NSString stringWithFormat:@"0%@",strVal];
        }
        
        if (i < 13 && i > 0)
        {
            [hourArray addObject:strVal];
        }
        
        [minuteArray addObject:strVal];
        
    }
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"view will appear");
    
    self.tableView.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    heightOfCell = 50*[UIScreen mainScreen].bounds.size.height/568.0;
    [self.tableView reloadData];
    
    AMSlideMenuMainViewController *mainMenu = [self mainVC];
    mainMenu.slideMenuDelegate = self;
}


#pragma mark - TableView Delegate -
/*----------------------------------------------------*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if(indexPath.row >= 1 && indexPath.row <= 3)
    {
        //                [tableView deselectRowAtIndexPath:indexPath animated:YES];
        //                [self performSegueWithIdentifier:segueIdentifier sender:indexPath];
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [[self mainVC] closeLeftMenuAnimated:YES];
        
        indexOfChoosedCell = indexPath.row;
        
        CourseViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"CourseViewController"];
        
        if(indexPath.row==1)
        {
            vc.language = @"en";
        }
        else if (indexPath.row==2)
        {
            vc.language = @"jp";
        }
        else if (indexPath.row==3)
        {
            vc.language = @"vi";
        }
        
        
        [currentViewController.navigationController pushViewController:vc animated:YES];
        
    }
    else if (indexPath.row == 5)
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [[self mainVC] closeLeftMenuAnimated:YES];
        
        [Email mailTo:emailAccount
                Title:feedbackTitle
           andContent:@""];
    }
    else if (indexPath.row == 6)
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [[self mainVC] closeLeftMenuAnimated:YES];
        
        [Email shareFacebookWithContent:fbShare
                      andViewController:self];
        
    }
    else if (indexPath.row == 7)
    {
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [[self mainVC] closeLeftMenuAnimated:YES];
        
        [Email mailTo:@""
                Title:@"Góp ý Pera Pera"
           andContent:appLink];
    }
    else if (indexPath.row == 8)
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [[self mainVC] closeLeftMenuAnimated:YES];
        
        UIViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AboutViewController"];
        [currentViewController.navigationController pushViewController:vc animated:YES];
    }
    else if (indexPath.row == 11)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"remind"] && !showTimePicker)
        {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            showTimePicker = true;
            
            [self.tableView reloadData];
            
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:12 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:true];
        }
        else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"remind"] && showTimePicker)
        {
            [self finishRemind];
        }
    }
    
}

#pragma mark- table view datasource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < 12)
        return heightOfCell;
    else
        return 240;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(!showTimePicker)
        return 12;
    else
        return 13;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    
    float sizeOfImg = 35*[UIScreen mainScreen].bounds.size.height/568.0;
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(20, (heightOfCell-sizeOfImg)/2, sizeOfImg, sizeOfImg)];
    imgView.layer.masksToBounds = true;
    imgView.layer.cornerRadius = imgView.frame.size.width/2;
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.frame = CGRectMake(30, 0, [UIScreen mainScreen].bounds.size.width-10, heightOfCell);
    //titleLabel.font = [UIFont boldSystemFontOfSize:17];
    titleLabel.font = [UIFont systemFontOfSize:17];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    //titleLabel.textColor = [UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    
    if(indexPath.row==0)
    {
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.font = [UIFont boldSystemFontOfSize:17];
        titleLabel.frame = CGRectMake(10, titleLabel.frame.origin.y, titleLabel.frame.size.width, titleLabel.frame.size.height);
        titleLabel.text = [JsonFileHandle getLocalTitleWithType:@"choose_language" andLanguageKey:g_language];
        cell.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];
    }
    else if(indexPath.row == 1)
    {
        imgView.image = [UIImage imageNamed:@"english.png"];
        titleLabel.frame = CGRectMake(imgView.frame.origin.x+imgView.frame.size.width+10, 0, [UIScreen mainScreen].bounds.size.width-25, heightOfCell);
        titleLabel.text = [JsonFileHandle getLocalTitleWithType:@"en" andLanguageKey:@"en"];
        [cell addSubview:imgView];
        
        if([g_language isEqualToString:@"en"])
            cell.backgroundColor = [UIColor colorWithRed:251.0/255.0 green:148.0/255.0 blue:84.0/255.0 alpha:1];
        
        
        //        UILabel *rule = [[UILabel alloc] initWithFrame:CGRectMake(0, heightOfCell-1, [UIScreen mainScreen].bounds.size.width, 1)];
        //        [rule setBackgroundColor:[UIColor grayColor]];
        //        [cell addSubview:rule];
        
    }
    else if(indexPath.row == 2)
    {
        imgView.image = [UIImage imageNamed:@"japan.png"];
        titleLabel.frame = CGRectMake(imgView.frame.origin.x+imgView.frame.size.width+10, 0, [UIScreen mainScreen].bounds.size.width-25, heightOfCell);
        titleLabel.text = [JsonFileHandle getLocalTitleWithType:@"jp" andLanguageKey:@"jp"];
        [cell addSubview:imgView];
        
        if([g_language isEqualToString:@"jp"])
            cell.backgroundColor = [UIColor colorWithRed:251.0/255.0 green:148.0/255.0 blue:84.0/255.0 alpha:1];
        //        UILabel *rule = [[UILabel alloc] initWithFrame:CGRectMake(0, heightOfCell-1, [UIScreen mainScreen].bounds.size.width, 1)];
        //        [rule setBackgroundColor:[UIColor grayColor]];
        //        [cell addSubview:rule];
    }
    else if(indexPath.row == 3)
    {
        imgView.image = [UIImage imageNamed:@"vn.png"];
        titleLabel.frame = CGRectMake(imgView.frame.origin.x+imgView.frame.size.width+10, 0, [UIScreen mainScreen].bounds.size.width-25, heightOfCell);
        titleLabel.text = [JsonFileHandle getLocalTitleWithType:@"vi" andLanguageKey:@"vi"];
        [cell addSubview:imgView];
        
        if([g_language isEqualToString:@"vi"])
            cell.backgroundColor = [UIColor colorWithRed:251.0/255.0 green:148.0/255.0 blue:84.0/255.0 alpha:1];
    }
    else if(indexPath.row==4)
    {
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.font = [UIFont boldSystemFontOfSize:17];
        titleLabel.frame = CGRectMake(10, titleLabel.frame.origin.y, titleLabel.frame.size.width, titleLabel.frame.size.height);
        titleLabel.text = [JsonFileHandle getLocalTitleWithType:@"tools" andLanguageKey:g_language];
        cell.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];
    }
    else if (indexPath.row == 5)
    {
        titleLabel.text = [JsonFileHandle getLocalTitleWithType:@"feedback" andLanguageKey:g_language];
        UILabel *rule = [[UILabel alloc] initWithFrame:CGRectMake(-30, heightOfCell-1, [UIScreen mainScreen].bounds.size.width, 1)];
        [rule setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1]];
        [titleLabel addSubview:rule];
        
    }
    else if (indexPath.row == 6)
    {
        titleLabel.text = [JsonFileHandle getLocalTitleWithType:@"share_on_Facebook" andLanguageKey:g_language];
        UILabel *rule = [[UILabel alloc] initWithFrame:CGRectMake(-30, heightOfCell-1, [UIScreen mainScreen].bounds.size.width, 1)];
        [rule setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1]];
        [titleLabel addSubview:rule];
        
    }
    else if (indexPath.row == 7)
    {
        titleLabel.text = [JsonFileHandle getLocalTitleWithType:@"share_via_email" andLanguageKey:g_language];
        UILabel *rule = [[UILabel alloc] initWithFrame:CGRectMake(-30, heightOfCell-1, [UIScreen mainScreen].bounds.size.width, 1)];
        [rule setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1]];
        [titleLabel addSubview:rule];
        
    }
    else if (indexPath.row == 8)
    {
        titleLabel.text = [JsonFileHandle getLocalTitleWithType:@"about_us" andLanguageKey:g_language];
        UILabel *rule = [[UILabel alloc] initWithFrame:CGRectMake(-30, heightOfCell-1, [UIScreen mainScreen].bounds.size.width, 1)];
        [rule setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1]];
        [titleLabel addSubview:rule];
        
    }
    else if(indexPath.row == 9)
    {
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.font = [UIFont boldSystemFontOfSize:17];
        titleLabel.frame = CGRectMake(10, titleLabel.frame.origin.y, titleLabel.frame.size.width, titleLabel.frame.size.height);
        titleLabel.text = [JsonFileHandle getLocalTitleWithType:@"remind" andLanguageKey:g_language];
        cell.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];
    }
    else if (indexPath.row == 10)
    {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        titleLabel.text = [JsonFileHandle getLocalTitleWithType:@"remind_title" andLanguageKey:g_language];
        UILabel *rule = [[UILabel alloc] initWithFrame:CGRectMake(-30, heightOfCell-1, [UIScreen mainScreen].bounds.size.width, 1)];
        [rule setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1]];
        [titleLabel addSubview:rule];
        titleLabel.userInteractionEnabled = true;
        
        //[UIScreen mainScreen].bounds.size.width - ([UIScreen mainScreen].bounds.size.width - self.mainSlideMenu.leftMenuWidth) - remindSwitch.frame.size.width - 35
        if(self.mainSlideMenu.leftMenuWidth > leftMenuWidth)
        {
            leftMenuWidth = self.mainSlideMenu.leftMenuWidth;
        }
        
        UISwitch *remindSwitch = [UISwitch new];
        remindSwitch.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - ([UIScreen mainScreen].bounds.size.width - leftMenuWidth) - remindSwitch.frame.size.width - 35, (titleLabel.frame.size.height-remindSwitch.frame.size.height)/2, remindSwitch.frame.size.width, remindSwitch.frame.size.height);
        
        [remindSwitch addTarget:self action:@selector(reminSwitch:) forControlEvents:UIControlEventValueChanged];
        
        [titleLabel addSubview:remindSwitch];
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"remind"] == false)
        {
            [remindSwitch setOn:false animated:false];
        }
        else
        {
            [remindSwitch setOn:true animated:false];
            
        }
        
        
    }
    else if (indexPath.row == 11)
    {
        titleLabel.text = [JsonFileHandle getLocalTitleWithType:@"remind_time" andLanguageKey:g_language];
        UILabel *rule = [[UILabel alloc] initWithFrame:CGRectMake(-30, heightOfCell-1, [UIScreen mainScreen].bounds.size.width, 1)];
        [rule setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1]];
        [titleLabel addSubview:rule];
        
        NSString *remindTime = [[NSUserDefaults standardUserDefaults] objectForKey:@"remindTime"];
        NSArray *timeComponents = [remindTime componentsSeparatedByString:@":"];
        
        float widthRmLb = 90;
        float heigthRmLb = 40;
        UILabel *remindTimeLabel = [UILabel new];
        remindTimeLabel.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - ([UIScreen mainScreen].bounds.size.width - leftMenuWidth) - widthRmLb - 35, (titleLabel.frame.size.height-heigthRmLb)/2, widthRmLb, heigthRmLb);
        remindTimeLabel.backgroundColor = [UIColor clearColor];
        remindTimeLabel.textAlignment = NSTextAlignmentRight;
        remindTimeLabel.font = [UIFont systemFontOfSize:15];
        remindTimeLabel.textColor = [UIColor blackColor];
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"remind"] && timeComponents.count == 3)
            remindTimeLabel.text = [NSString stringWithFormat:@"%@:%@ %@",timeComponents[0],timeComponents[1],timeComponents[2]];
        else
            remindTimeLabel.text = @"__:__";
        
        [titleLabel addSubview:remindTimeLabel];
        
        
    }
    else if (indexPath.row == 12)
    {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        timePickerView = [UIPickerView new];
        
        timePickerView.delegate = self;
        timePickerView.dataSource = self;
        timePickerView.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width - ([UIScreen mainScreen].bounds.size.width - leftMenuWidth) - widthOfPicker)/2, 10, widthOfPicker, 120);
        
        [cell addSubview:timePickerView];
        
        [timePickerView selectRow:7 inComponent:0 animated:YES];
        [timePickerView selectRow:30 inComponent:1 animated:YES];
        [timePickerView selectRow:0 inComponent:2 animated:YES];
        
        NSString *remindTime = [[NSUserDefaults standardUserDefaults] objectForKey:@"remindTime"];
        NSArray *timeComponents = [remindTime componentsSeparatedByString:@":"];
        
        if(timeComponents.count == 3)
        {
            [timePickerView selectRow:[hourArray indexOfObject:timeComponents[0]] inComponent:0 animated:YES];
            [timePickerView selectRow:[minuteArray indexOfObject:timeComponents[1]] inComponent:1 animated:YES];
            [timePickerView selectRow:[typeArray indexOfObject:timeComponents[2]] inComponent:2 animated:YES];
        }
        
        UIButton *xongButton = [UIButton new];
        xongButton.frame = CGRectMake(timePickerView.frame.origin.x+timePickerView.frame.size.width-110,timePickerView.frame.origin.y+timePickerView.frame.size.height+15,110,40);
        [xongButton setTitle:[JsonFileHandle getLocalTitleWithType:@"setting_time" andLanguageKey:g_language] forState:UIControlStateNormal];
        [xongButton addTarget:self action:@selector(finishRemind) forControlEvents:UIControlEventTouchUpInside];
        [xongButton setBackgroundColor:[UIColor colorWithRed:70.0/255.0 green:187.0/255.0 blue:23.0/255.0 alpha:1]];
        
        [cell addSubview:xongButton];
    }
    
    
    if(indexOfChoosedCell==1 && indexPath.row==1)
    {
        UIView *view = [UIView new];
        view.frame = CGRectMake(0, 0, 10, heightOfCell);
        view.backgroundColor = [UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1];
        [view setTag:1];
        [cell addSubview:view];
    }
    else if(indexOfChoosedCell==2 && indexPath.row==2)
    {
        UIView *view = [UIView new];
        view.frame = CGRectMake(0, 0, 10, heightOfCell);
        view.backgroundColor = [UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1];
        [view setTag:1];
        [cell addSubview:view];
    }
    else if(indexOfChoosedCell==3 && indexPath.row==3)
    {
        UIView *view = [UIView new];
        view.frame = CGRectMake(0, 0, 10, heightOfCell);
        view.backgroundColor = [UIColor colorWithRed:68.0/225.0 green:120.0/225.0 blue:201.0/255.0 alpha:1];
        [view setTag:1];
        [cell addSubview:view];
    }
    
    [cell addSubview:titleLabel];
    return cell;
}


#pragma mark button

-(IBAction)reminSwitch:(id)sender
{
    UISwitch *remindSwitch = (UISwitch*) sender;
    
    if ([remindSwitch isOn]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"remind"];
        showTimePicker = true;
        
        [self.tableView reloadData];
        
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:12 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:true];
        
    }
    else
    {
        
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"remind"];
        showTimePicker = false;
        
        [self.tableView reloadData];
        
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:11 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:true];
        
    }
}

-(void)finishRemind
{
    //@"remindTime"
    
    NSString *hour = hourArray[[timePickerView selectedRowInComponent:0]];
    NSString *minute = minuteArray[[timePickerView selectedRowInComponent:1]];
    NSString *typeHour = typeArray[[timePickerView selectedRowInComponent:2]];
    
    NSString *remindTime = [NSString stringWithFormat:@"%@:%@:%@",hour, minute,typeHour];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:remindTime forKey:@"remindTime"];
    
    showTimePicker = false;
    
    [self.tableView reloadData];
    
}

#pragma mark pickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}


// Method to define the numberOfRows in a component using the array.
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent :(NSInteger)component
{
    if (component==0)
    {
        return [hourArray count];
    }
    else if (component==1)
    {
        return [minuteArray count];
    }
    else
    {
        return 2;
    }
    
}


// Method to show the title of row for a component.
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    switch (component)
    {
        case 0:
            return [hourArray objectAtIndex:row];
            break;
        case 1:
            return [minuteArray objectAtIndex:row];
            break;
        case 2:
            return [typeArray objectAtIndex:row];
            break;
    }
    return nil;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return widthOfPicker/3;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40;
    //return 30;
}

#pragma mark picker view delegate

//-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
//{
//    remindTime = [NSString stringWithFormat:@""];
//}



-(void)leftMenuDidClose
{
    NSLog(@"close");
    
    [self.tableView reloadData];
    
}

@end
