//
//  SqlManager.m
//  Pera
//
//  Created by Le Trong Thao on 12/4/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import "SqlManager.h"
#import "JsonFileHandle.h"
#import "GlobalVariable.h"

@implementation SqlManager


+(void)copyFileToDocumentPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *sqlFilePath = [documentPath stringByAppendingPathComponent:@"pera.sqlite"];
    if(![[NSFileManager defaultManager] fileExistsAtPath:sqlFilePath])
    {
        NSString *bundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"pera.sqlite"];
        BOOL success =  [[NSFileManager defaultManager] copyItemAtPath:bundlePath toPath:sqlFilePath error:nil];
        if (success) {
            NSURL *url = [NSURL fileURLWithPath:sqlFilePath];
            [self addSkipBackupAttributeToItemAtURL:url];
            return;
        }
    }
}

+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

+(BOOL)checkTopicExistWithTopic:(NSString *)topicID Language:(NSString *)language Course:(NSString *)course
{
    BOOL flag = false;
    
    sqlite3 *sql;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *sqlFilePath = [documentPath stringByAppendingPathComponent:@"pera.sqlite"];
    
    const char *sqlFilePath_sql = [sqlFilePath UTF8String];
    if(sqlite3_open(sqlFilePath_sql , &sql)==SQLITE_OK)
    {
        sqlite3_stmt *statement;
        NSString *query = [NSString stringWithFormat:@"Select * from Topic Where Language = '%@' and Course = '%@' and TopicID = '%@'",language, course, topicID];
        const char *query_sql = [query UTF8String];
        if(sqlite3_prepare(sql, query_sql, -1, &statement, NULL)==SQLITE_OK)
        {
            if(sqlite3_step(statement)==SQLITE_ROW)
            {
                flag = true;
            }
        }
        
        sqlite3_finalize(statement);
    }
    
    sqlite3_close(sql);
    
    return flag;
}

+(BOOL)checkGameExistWithGame:(NSString *)gameID Language:(NSString *)language Course:(NSString *)course andTopicID:(NSString *)topicID
{
    BOOL flag = false;
    
    sqlite3 *sql;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *sqlFilePath = [documentPath stringByAppendingPathComponent:@"pera.sqlite"];
    
    const char *sqlFilePath_sql = [sqlFilePath UTF8String];
    if(sqlite3_open(sqlFilePath_sql , &sql)==SQLITE_OK)
    {
        sqlite3_stmt *statement;
        NSString *query = [NSString stringWithFormat:@"Select * from Game Where Language = '%@' and Course = '%@' and TopicID = '%@' and GameID = '%@'",language, course, topicID,gameID];
        const char *query_sql = [query UTF8String];
        if(sqlite3_prepare(sql, query_sql, -1, &statement, NULL)==SQLITE_OK)
        {
            if(sqlite3_step(statement)==SQLITE_ROW)
            {
                flag = true;
            }
        }
        sqlite3_finalize(statement);
    }
    
    sqlite3_close(sql);
    
    return flag;
}


+(int)getsTarsOfCourse:(NSString*)course andLanguage:(NSString*)language
{
    int stars = 0;
    
    sqlite3 *sql;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *sqlFilePath = [documentPath stringByAppendingPathComponent:@"pera.sqlite"];
    
    const char *sqlFilePath_sql = [sqlFilePath UTF8String];
    if(sqlite3_open(sqlFilePath_sql , &sql)==SQLITE_OK)
    {
        sqlite3_stmt *statement;
        NSString *query = [NSString stringWithFormat:@"Select * from Topic Where Language = '%@' and Course = '%@'",language, course];
        const char *query_sql = [query UTF8String];
        if(sqlite3_prepare(sql, query_sql, -1, &statement, NULL)==SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                stars =  stars + [[NSString stringWithUTF8String:(const char*)sqlite3_column_text(statement, 3)] intValue];
            }
        }
        sqlite3_finalize(statement);
    }
    
    sqlite3_close(sql);
    return stars;
}

+(int)getStarsOfTopic:(NSString *)topicID Language:(NSString *)language AndCourse:(NSString *)course
{
    int stars = 0;
    
    sqlite3 *sql;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *sqlFilePath = [documentPath stringByAppendingPathComponent:@"pera.sqlite"];
    
    const char *sqlFilePath_sql = [sqlFilePath UTF8String];
    if(sqlite3_open(sqlFilePath_sql , &sql)==SQLITE_OK)
    {
        sqlite3_stmt *statement;
        NSString *query = [NSString stringWithFormat:@"Select * from Topic Where Language = '%@' and Course = '%@' and TopicID = '%@'",language, course, topicID];
        const char *query_sql = [query UTF8String];
        if(sqlite3_prepare(sql, query_sql, -1, &statement, NULL)==SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                stars = [[NSString stringWithUTF8String:(const char*)sqlite3_column_text(statement, 3)] intValue];
            }
        }
        sqlite3_finalize(statement);
    }
    
    sqlite3_close(sql);
    return stars;
}


+(int)getStarsOfGame:(NSString *)gameID Language:(NSString *)language Course:(NSString *)course andTopicID:(NSString *)topicID
{
    int stars = 0;
    
    sqlite3 *sql;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *sqlFilePath = [documentPath stringByAppendingPathComponent:@"pera.sqlite"];
    
    const char *sqlFilePath_sql = [sqlFilePath UTF8String];
    if(sqlite3_open(sqlFilePath_sql , &sql)==SQLITE_OK)
    {
        sqlite3_stmt *statement;
        NSString *query = [NSString stringWithFormat:@"Select * from Game Where Language = '%@' and Course = '%@' and TopicID = '%@' and GameID = '%@'",language, course, topicID,gameID];
        const char *query_sql = [query UTF8String];
        if(sqlite3_prepare(sql, query_sql, -1, &statement, NULL)==SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                stars = [[NSString stringWithUTF8String:(const char*)sqlite3_column_text(statement, 4)] intValue];
            }
        }
        sqlite3_finalize(statement);
    }
    
    sqlite3_close(sql);
    
    return stars;
}


+(void)updateTopicWithTopic:(NSString *)topicID Language:(NSString *)language Course:(NSString *)course andStars:(NSString *)stars
{
    sqlite3 *sql;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *sqlFilePath = [documentPath stringByAppendingPathComponent:@"pera.sqlite"];
    
    const char *sqlFilePath_sql = [sqlFilePath UTF8String];
    if(sqlite3_open(sqlFilePath_sql , &sql)==SQLITE_OK)
    {
        sqlite3_stmt *statement;
        NSString *query = [NSString stringWithFormat:@"Update Topic Set Stars = '%@' Where Language = '%@' and Course = '%@' and TopicID = '%@'",stars,language, course, topicID];
        const char *query_sql = [query UTF8String];
        if(sqlite3_prepare(sql, query_sql, -1, &statement, NULL)==SQLITE_OK)
        {
            sqlite3_step(statement);
        }
        sqlite3_finalize(statement);
    }
    
    sqlite3_close(sql);

}

+(void)updateTopicTableStarsWithLanguage:(NSString *)lg Course:(NSString *)cs andTopicID:(NSString *)topicID
{
    int topicStar = 0;
    
    sqlite3 *sql;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *sqlFilePath = [documentPath stringByAppendingPathComponent:@"pera.sqlite"];
    
    const char *sqlFilePath_sql = [sqlFilePath UTF8String];
    if(sqlite3_open(sqlFilePath_sql , &sql)==SQLITE_OK)
    {
        sqlite3_stmt *statement;
        NSString *query = [NSString stringWithFormat:@"Select * from Game Where Language = '%@' and Course = '%@' and TopicID = '%@'",lg, cs, topicID];
        const char *query_sql = [query UTF8String];
        if(sqlite3_prepare(sql, query_sql, -1, &statement, NULL)==SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                topicStar += [[NSString stringWithUTF8String:(const char*)sqlite3_column_text(statement, 4)] intValue];
            }
        }
        
        query = [NSString stringWithFormat:@"Update Topic Set Stars = '%@' Where Language = '%@' and Course = '%@' and TopicID = '%@'",[NSString stringWithFormat:@"%d",topicStar],lg, cs, topicID];
        query_sql = [query UTF8String];
        if(sqlite3_prepare(sql, query_sql, -1, &statement, NULL)==SQLITE_OK)
        {
            sqlite3_step(statement);
        }
        
        int sumStar = [self getsTarsOfCourse:cs andLanguage:lg];
        NSString *sumStarStr = [NSString stringWithFormat:@"%d",sumStar];
        NSString *needUnlockTopic;
        if(sumStarStr.length>1)
            needUnlockTopic = [NSString stringWithFormat:@"%d",[[sumStarStr substringWithRange:NSMakeRange(0, sumStarStr.length-1)] intValue]+1];
        else
            needUnlockTopic = @"1";
        
        if([topicID intValue] < [JsonFileHandle getTopicTitlesWithLanguageKey:lg].count && ![SqlManager checkTopicExistWithTopic:needUnlockTopic Language:lg Course:cs])
        {
            
            query = [NSString stringWithFormat:@"Insert into Topic Values('%@','%@','%@','0')",lg,cs,needUnlockTopic];
            query_sql = [query UTF8String];
            if(sqlite3_prepare(sql, query_sql, -1, &statement, NULL)==SQLITE_OK)
            {
                sqlite3_step(statement);
            }
            
            [SqlManager insertGameTableWithGameID:@"1" Language:lg Course:cs TopicID:needUnlockTopic andStars:@"0"];

        }
        
        sqlite3_finalize(statement);
    }
    
    sqlite3_close(sql);
}

+(void) unlockTopicId: (NSString *)lg Course:(NSString *)cs andTopicID:(NSString *)topicID{
    sqlite3 *sql;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *sqlFilePath = [documentPath stringByAppendingPathComponent:@"pera.sqlite"];
    
    const char *sqlFilePath_sql = [sqlFilePath UTF8String];
 
    if(sqlite3_open(sqlFilePath_sql , &sql)==SQLITE_OK && ![SqlManager checkTopicExistWithTopic:[NSString stringWithFormat:@"%@",topicID] Language:lg Course:cs])
    {
        sqlite3_stmt *statement;
        
        NSString *query = [NSString stringWithFormat:@"Insert into Topic Values('%@','%@','%@','0')",lg,cs,topicID];
        const char * query_sql = [query UTF8String];
        if(sqlite3_prepare(sql, query_sql, -1, &statement, NULL)==SQLITE_OK)
        {
            sqlite3_step(statement);
        }
        
        [SqlManager insertGameTableWithGameID:@"1" Language:lg Course:cs TopicID:topicID andStars:@"0"];
        
        sqlite3_finalize(statement);
    }
    sqlite3_close(sql);
}

+(void)updateGameWithGame:(NSString *)gameID Language:(NSString *)language Course:(NSString *)course andTopicID:(NSString *)topicID andStars:(NSString *)stars
{
    sqlite3 *sql;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *sqlFilePath = [documentPath stringByAppendingPathComponent:@"pera.sqlite"];
    
    const char *sqlFilePath_sql = [sqlFilePath UTF8String];
    if(sqlite3_open(sqlFilePath_sql , &sql)==SQLITE_OK)
    {
        sqlite3_stmt *statement;
        NSString *query = [NSString stringWithFormat:@"Update Game Set Stars = '%@' Where Language = '%@' and Course = '%@' and TopicID = '%@' and GameID = '%@'",stars,language, course, topicID,gameID];
        const char *query_sql = [query UTF8String];
        if(sqlite3_prepare(sql, query_sql, -1, &statement, NULL)==SQLITE_OK)
        {
            sqlite3_step(statement);
        }
        sqlite3_finalize(statement);
    }
    
    sqlite3_close(sql);
}


+(void)insertGameTableWithGameID:(NSString*)gameID Language:(NSString*)lg Course:(NSString*)cs TopicID:(NSString*)topicID andStars:(NSString*)star
{
    sqlite3 *sql;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *sqlFilePath = [documentPath stringByAppendingPathComponent:@"pera.sqlite"];
    
    const char *sqlFilePath_sql = [sqlFilePath UTF8String];
    if(sqlite3_open(sqlFilePath_sql , &sql)==SQLITE_OK)
    {
        sqlite3_stmt *statement;
        NSString *query = [NSString stringWithFormat:@"Insert into Game Values('%@','%@','%@','%@','%@')",lg,cs,gameID,topicID,star];
        const char *query_sql = [query UTF8String];
        if(sqlite3_prepare(sql, query_sql, -1, &statement, NULL)==SQLITE_OK)
        {
            sqlite3_step(statement);
        }
        sqlite3_finalize(statement);
    }
    
    sqlite3_close(sql);
}


+(BOOL)checkExistLanguageAndCourse
{
    BOOL flag = false;
    
    sqlite3 *sql;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *sqlFilePath = [documentPath stringByAppendingPathComponent:@"pera.sqlite"];
    
    const char *sqlFilePath_sql = [sqlFilePath UTF8String];
    if(sqlite3_open(sqlFilePath_sql , &sql)==SQLITE_OK)
    {
        sqlite3_stmt *statement;
        NSString *query = [NSString stringWithFormat:@"Select count (*) from Setting"];
        const char *query_sql = [query UTF8String];
        if(sqlite3_prepare(sql, query_sql, -1, &statement, NULL)==SQLITE_OK)
        {
            while(sqlite3_step(statement)==SQLITE_ROW)
            {
                int count = sqlite3_column_int(statement, 0);
                if(count==0)
                    flag = false;
                else
                    flag = true;
            }
        }
        
        sqlite3_finalize(statement);
    }
    
    sqlite3_close(sql);
    
    return flag;

}

+(void)insertSettingTableWith:(NSString *)lang andCourse:(NSString *)cs
{
    sqlite3 *sql;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *sqlFilePath = [documentPath stringByAppendingPathComponent:@"pera.sqlite"];
    
    const char *sqlFilePath_sql = [sqlFilePath UTF8String];
    if(sqlite3_open(sqlFilePath_sql , &sql)==SQLITE_OK)
    {
        sqlite3_stmt *statement;
        NSString *query = [NSString stringWithFormat:@"Insert into Setting Values('%@','%@','0')",lang,cs];
        const char *query_sql = [query UTF8String];
        if(sqlite3_prepare(sql, query_sql, -1, &statement, NULL)==SQLITE_OK)
        {
            sqlite3_step(statement);
        }
        sqlite3_finalize(statement);
    }
    
    sqlite3_close(sql);
}

+(void)updateSettingTableWith:(NSString *)lang andCourse:(NSString *)cs
{
    sqlite3 *sql;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *sqlFilePath = [documentPath stringByAppendingPathComponent:@"pera.sqlite"];
    
    const char *sqlFilePath_sql = [sqlFilePath UTF8String];
    if(sqlite3_open(sqlFilePath_sql , &sql)==SQLITE_OK)
    {
        sqlite3_stmt *statement;
        NSString *query = [NSString stringWithFormat:@"Update Setting Set Language = '%@', Course = '%@'",lang,cs];
        const char *query_sql = [query UTF8String];
        if(sqlite3_prepare(sql, query_sql, -1, &statement, NULL)==SQLITE_OK)
        {
            sqlite3_step(statement);
        }
        sqlite3_finalize(statement);
    }
    
    sqlite3_close(sql);
}

+(NSString*)getLanguage
{
    
    NSString *lg;
    
    sqlite3 *sql;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *sqlFilePath = [documentPath stringByAppendingPathComponent:@"pera.sqlite"];
    
    const char *sqlFilePath_sql = [sqlFilePath UTF8String];
    if(sqlite3_open(sqlFilePath_sql , &sql)==SQLITE_OK)
    {
        sqlite3_stmt *statement;
        NSString *query = [NSString stringWithFormat:@"Select * from Setting"];
        const char *query_sql = [query UTF8String];
        if(sqlite3_prepare(sql, query_sql, -1, &statement, NULL)==SQLITE_OK)
        {
            while(sqlite3_step(statement)==SQLITE_ROW)
            {
                lg = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(statement, 0)];
            }
        }
        
        sqlite3_finalize(statement);
    }
    
    sqlite3_close(sql);
    
    return lg;
}

+(NSString*)getCourse
{
    NSString *lg;
    
    sqlite3 *sql;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *sqlFilePath = [documentPath stringByAppendingPathComponent:@"pera.sqlite"];
    
    const char *sqlFilePath_sql = [sqlFilePath UTF8String];
    if(sqlite3_open(sqlFilePath_sql , &sql)==SQLITE_OK)
    {
        sqlite3_stmt *statement;
        NSString *query = [NSString stringWithFormat:@"Select * from Setting"];
        const char *query_sql = [query UTF8String];
        if(sqlite3_prepare(sql, query_sql, -1, &statement, NULL)==SQLITE_OK)
        {
            while(sqlite3_step(statement)==SQLITE_ROW)
            {
                lg = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(statement, 1)];
            }
        }
        
        sqlite3_finalize(statement);
    }
    
    sqlite3_close(sql);
    
    return lg;
}

+(BOOL)checkChoosedTopic
{
    BOOL flag = false;
    
    sqlite3 *sql;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *sqlFilePath = [documentPath stringByAppendingPathComponent:@"pera.sqlite"];
    
    const char *sqlFilePath_sql = [sqlFilePath UTF8String];
    if(sqlite3_open(sqlFilePath_sql , &sql)==SQLITE_OK)
    {
        sqlite3_stmt *statement;
        NSString *query = [NSString stringWithFormat:@"Select * from Setting"];
        const char *query_sql = [query UTF8String];
        if(sqlite3_prepare(sql, query_sql, -1, &statement, NULL)==SQLITE_OK)
        {
            while(sqlite3_step(statement)==SQLITE_ROW)
            {
                NSString *choose = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(statement, 2)];
                if([choose isEqualToString:@"0"])
                {
                    flag  = flag;
                }
                else
                    flag = true;
            }
        }
        
        sqlite3_finalize(statement);
    }
    
    sqlite3_close(sql);
    
    return flag;
}

+(void)updateChoosedTopic
{
    sqlite3 *sql;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    NSString *sqlFilePath = [documentPath stringByAppendingPathComponent:@"pera.sqlite"];
    
    const char *sqlFilePath_sql = [sqlFilePath UTF8String];
    if(sqlite3_open(sqlFilePath_sql , &sql)==SQLITE_OK)
    {
        sqlite3_stmt *statement;
        NSString *query = [NSString stringWithFormat:@"Update Setting Set ChoosedTopic = '1'"];
        const char *query_sql = [query UTF8String];
        if(sqlite3_prepare(sql, query_sql, -1, &statement, NULL)==SQLITE_OK)
        {
            sqlite3_step(statement);
        }
        sqlite3_finalize(statement);
    }
    
    sqlite3_close(sql);
}

@end
