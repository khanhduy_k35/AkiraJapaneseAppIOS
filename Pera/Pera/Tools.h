#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define isIOS6 ( (NSClassFromString(@"UICollectionView")==nil) ? NO : YES  )

@interface Tools : NSObject


+ (NSString *)applicationDocumentsDirectory;

+ (void)setDisAd:(BOOL)on;
+ (BOOL)isDisad;

+ (void)setAppRun:(int)num;
@end
