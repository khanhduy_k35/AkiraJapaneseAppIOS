//
//  StarBar.m
//  Pera
//
//  Created by Le Trong Thao on 12/4/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import "StarBar.h"

@implementation StarBar


-(id)initWithFrame:(CGRect)frame andNumberOfStars:(int)starNumber
{
    self = [super initWithFrame:frame];
    
    float sizeOfImg = frame.size.height;
    
    for(int i = 1; i<= 3; i++)
    {
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake((i-1)*sizeOfImg, 0, sizeOfImg, sizeOfImg)];
        imgView.image = [UIImage imageNamed:@"favorite.png"];
        
        if(i==1&& starNumber==1)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        else if((i==1 ||i ==2) && starNumber == 2)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        else if((i==1 ||i ==2 || i==3) && starNumber == 3)
        {
            imgView.image = [UIImage imageNamed:@"favorite_active.png"];
        }
        
        [self addSubview:imgView];
    }
    
    return self;
}

@end
