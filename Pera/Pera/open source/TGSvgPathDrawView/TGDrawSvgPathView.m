//
//  TGDrawSvgPathView.m
//  SVGPathDrawing
//
//  Created by Thibault Guégan on 09/07/2014.
//  Copyright (c) 2014 Skyr. All rights reserved.
//

#import "TGDrawSvgPathView.h"

@implementation TGDrawSvgPathView
{
    CGMutablePathRef combinedPath;
    
    NSTimer *drawTimer;
    
    NSMutableArray *textArray;
    NSTimer *t;
    int drawIndex;
    int addTextIndex;
    
    
    float lineWidth;
    
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setPathFromSvg:(NSString*)fileName strokeColor:(UIColor *)color duration:(CFTimeInterval)time
{
    _shapeView = [[CAShapeLayer alloc] init];
    
    _strokeColor = color;
    _animationDuration = time;
    
    PocketSVG *pocketSVG = [PocketSVG initSVGFileNamed:fileName];
    NSMutableArray *bezier = pocketSVG.lstBezier;
    _lstShapeView = [[NSMutableArray alloc] init];
    for (int i = 0; i < bezier.count; i ++) {
        _shapeView = [[CAShapeLayer alloc] init];
        [_shapeView setPath:((UIBezierPath*)bezier[i]).CGPath];
        [_lstShapeView addObject:_shapeView];
    }
    combinedPath = CGPathCreateMutableCopy(((UIBezierPath*)bezier[0]).CGPath);
    CGPathAddPath(combinedPath, NULL, ((UIBezierPath*)bezier[1]).CGPath);
    CGPathAddPath(combinedPath, NULL, ((UIBezierPath*)bezier[2]).CGPath);
    [_shapeView setPath:combinedPath];
    
    [self scale: _shapeView duration:(time/_lstShapeView.count) delay:0];
}

-(void)setFilePathFromSvg:(NSString *)filePath strokeColor:(UIColor *)color duration:(CFTimeInterval)time
{
    @try{
        _shapeView = [[CAShapeLayer alloc] init];
        
        _strokeColor = color;
        _animationDuration = time;
        
        PocketSVG *pocketSVG = [PocketSVG initSVGFilePath:filePath];
        NSMutableArray *bezier = pocketSVG.lstBezier;
        _lstShapeView = [[NSMutableArray alloc] init];
        for (int i = 0; i < bezier.count; i ++) {
            _shapeView = [[CAShapeLayer alloc] init];
            [_shapeView setPath:((UIBezierPath*)bezier[i]).CGPath];
            [_lstShapeView addObject:_shapeView];
        }
        combinedPath = CGPathCreateMutableCopy(((UIBezierPath*)bezier[0]).CGPath);

        
//        for(int i = 1; i < bezier.count; i++)
//        {
//            CGPathAddPath(combinedPath, NULL, ((UIBezierPath*)bezier[i]).CGPath);
//        }
        
//        [_shapeView setPath:combinedPath];
        
        //[self scale: _shapeView duration:(time/_lstShapeView.count) delay:0];
        
        
        [self addTextWithFilePath:filePath andTotalDuration:time];
        
        
        [self scale: _lstShapeView[0] duration:time/_lstShapeView.count delay:0];
        drawIndex = 1;
        
        drawTimer = [NSTimer scheduledTimerWithTimeInterval:time/_lstShapeView.count target:self selector:@selector(drawWord) userInfo:nil repeats:true];
        
    }
    @catch (NSException *ex)
    {
        
    }
}

-(void)setFilePathFromSvg:(NSString *)filePath strokeColor:(UIColor *)color duration:(CFTimeInterval)time lineWidth:(float)width
{
    @try{
        _shapeView = [[CAShapeLayer alloc] init];
        
        _strokeColor = color;
        _animationDuration = time;
        
        PocketSVG *pocketSVG = [PocketSVG initSVGFilePath:filePath];
        NSMutableArray *bezier = pocketSVG.lstBezier;
        _lstShapeView = [[NSMutableArray alloc] init];
        for (int i = 0; i < bezier.count; i ++) {
            _shapeView = [[CAShapeLayer alloc] init];
            [_shapeView setPath:((UIBezierPath*)bezier[i]).CGPath];
            [_lstShapeView addObject:_shapeView];
        }
        combinedPath = CGPathCreateMutableCopy(((UIBezierPath*)bezier[0]).CGPath);
        
        //        for(int i = 1; i < bezier.count; i++)
        //        {
        //            CGPathAddPath(combinedPath, NULL, ((UIBezierPath*)bezier[i]).CGPath);
        //        }
        
        //        [_shapeView setPath:combinedPath];
        
        //[self scale: _shapeView duration:(time/_lstShapeView.count) delay:0];
        
        
        [self addTextWithFilePath:filePath andTotalDuration:time];
        
        
        [self scale: _lstShapeView[0] duration:time/_lstShapeView.count delay:0 lineWidth:width];
        drawIndex = 1;
        
        lineWidth = width;
        drawTimer = [NSTimer scheduledTimerWithTimeInterval:time/_lstShapeView.count target:self selector:@selector(drawWordWithLineWidth) userInfo:nil repeats:true];
        
        
    }
    @catch (NSException *ex)
    {
        
    }

}

- (void)scale : (CAShapeLayer*)shapeView duration:(CFTimeInterval)time delay: (CFTimeInterval)timeDelay
{
    // I'm assuming that the view and original shape layer is already created
    CGRect boundingBox = CGPathGetBoundingBox(shapeView.path);
    boundingBox.size.width = 109;
    boundingBox.size.height = 109;
    CGFloat boundingBoxAspectRatio = CGRectGetWidth(boundingBox)/CGRectGetHeight(boundingBox);
    CGFloat viewAspectRatio = CGRectGetWidth(self.frame)/CGRectGetHeight(self.frame);
    
    CGFloat scaleFactor = 1.0;
    if (boundingBoxAspectRatio > viewAspectRatio) {
        // Width is limiting factor
        scaleFactor = CGRectGetWidth(self.frame)/CGRectGetWidth(boundingBox);
    } else {
        // Height is limiting factor
        scaleFactor = CGRectGetHeight(self.frame)/CGRectGetHeight(boundingBox);
    }
    
    
    // Scaling the path ...
    CGAffineTransform scaleTransform = CGAffineTransformIdentity;
    // Scale down the path first
    scaleTransform = CGAffineTransformScale(scaleTransform, scaleFactor, scaleFactor);
        
    CGPathRef scaledPath = CGPathCreateCopyByTransformingPath(shapeView.path,
                                                              &scaleTransform);
    
    // Create a new shape layer and assign the new path
    CAShapeLayer *scaledShapeLayer = [CAShapeLayer layer];
    scaledShapeLayer.path = scaledPath;
    scaledShapeLayer.strokeColor = _strokeColor.CGColor;
    scaledShapeLayer.fillColor = [UIColor clearColor].CGColor;
    scaledShapeLayer.lineWidth = 3.0;
    
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration = time;
    pathAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    pathAnimation.toValue = [NSNumber numberWithFloat:1.0f];
    [scaledShapeLayer addAnimation:pathAnimation forKey:@"strokeEndAnimation"];
    
    //add the shape layer to our custom view
    [self.layer addSublayer:scaledShapeLayer];
    
    CGPathRelease(scaledPath); // release the copied path

}

- (void)scale : (CAShapeLayer*)shapeView duration:(CFTimeInterval)time delay: (CFTimeInterval)timeDelay lineWidth:(float)width
{
    // I'm assuming that the view and original shape layer is already created
    CGRect boundingBox = CGPathGetBoundingBox(shapeView.path);
    boundingBox.size.width = 109;
    boundingBox.size.height = 109;
    CGFloat boundingBoxAspectRatio = CGRectGetWidth(boundingBox)/CGRectGetHeight(boundingBox);
    CGFloat viewAspectRatio = CGRectGetWidth(self.frame)/CGRectGetHeight(self.frame);
    
    CGFloat scaleFactor = 1.0;
    if (boundingBoxAspectRatio > viewAspectRatio) {
        // Width is limiting factor
        scaleFactor = CGRectGetWidth(self.frame)/CGRectGetWidth(boundingBox);
    } else {
        // Height is limiting factor
        scaleFactor = CGRectGetHeight(self.frame)/CGRectGetHeight(boundingBox);
    }
    
    
    // Scaling the path ...
    CGAffineTransform scaleTransform = CGAffineTransformIdentity;
    // Scale down the path first
    scaleTransform = CGAffineTransformScale(scaleTransform, scaleFactor, scaleFactor);
    
    CGPathRef scaledPath = CGPathCreateCopyByTransformingPath(shapeView.path,
                                                              &scaleTransform);
    
    // Create a new shape layer and assign the new path
    CAShapeLayer *scaledShapeLayer = [CAShapeLayer layer];
    scaledShapeLayer.path = scaledPath;
    scaledShapeLayer.strokeColor = _strokeColor.CGColor;
    scaledShapeLayer.fillColor = [UIColor clearColor].CGColor;
    scaledShapeLayer.lineWidth = width;
    
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration = time;
    pathAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    pathAnimation.toValue = [NSNumber numberWithFloat:1.0f];
    [scaledShapeLayer addAnimation:pathAnimation forKey:@"strokeEndAnimation"];
    
    //add the shape layer to our custom view
    [self.layer addSublayer:scaledShapeLayer];
    
    CGPathRelease(scaledPath); // release the copied path
    
}


-(void)addTextWithFilePath:(NSString*)filePath andTotalDuration:(CFTimeInterval)time
{
    NSString *svgContent = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    
    NSArray *tempAr = [svgContent componentsSeparatedByString:@"<text transform=\"matrix"];

    textArray = [NSMutableArray new];
    
    for(NSString *Str in tempAr)
    {
        if([tempAr indexOfObject:Str] != 0)
        {
            NSArray *tempAr1 = [Str componentsSeparatedByString:@" "];
            CGPoint point = CGPointMake([tempAr1[4] floatValue], [tempAr1[5] floatValue]);
            
            UILabel *lb = [UILabel new];
            lb.frame = CGRectMake(point.x*self.frame.size.width/109 - 5, point.y*self.frame.size.height/109 -5, 10, 10);
            //lb.backgroundColor = [UIColor greenColor];
            lb.textColor = [UIColor blackColor];
            lb.font = [UIFont systemFontOfSize:8];
            lb.textAlignment = NSTextAlignmentCenter;
            lb.text = [NSString stringWithFormat:@"%d",[tempAr indexOfObject:Str]];
            
            [textArray addObject:lb];
            
            
            if([tempAr indexOfObject:Str]==1)
            {
                [self addSubview:lb];
                addTextIndex = 1;
            }
            
        }
        
    }
    
    t = [NSTimer scheduledTimerWithTimeInterval:time/textArray.count target:self selector:@selector(addTextLabel) userInfo:nil repeats:true];
    
}


#pragma mark- timer

-(void)drawWord
{
    if(drawIndex == _lstShapeView.count)
    {
        [drawTimer invalidate];
    }
    else
    {
        [self scale:_lstShapeView[drawIndex] duration:_animationDuration/_lstShapeView.count delay:0];
        drawIndex++;
    }
}

-(void)drawWordWithLineWidth
{
    
    if(drawIndex == _lstShapeView.count)
    {
        [drawTimer invalidate];
    }
    else
    {
        [self scale:_lstShapeView[drawIndex] duration:_animationDuration/_lstShapeView.count delay:0 lineWidth:lineWidth];
        drawIndex++;
    }
}

-(void)addTextLabel
{
    if(addTextIndex == textArray.count)
    {
        [t invalidate];
    }
    else
    {
        [self addSubview:textArray[drawIndex]];
        addTextIndex++;
    }
}

@end
