//
//  LeftTableViewController.m
//  Pera
//
//  Created by Le Trong Thao on 1/27/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import "LeftTableViewController.h"

#import "GlobalVariable.h"
#import "Email.h"
#import "UIViewController+AMSlideMenu.h"
#import "DataHandle.h"
#import "HttpThread.h"
#import "Reachability.h"
#import "Toast.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#define logout @"Đăng xuất"
#define login @"Đăng nhập"
#define phanHoi @"Phản hồi"
#define shareFb @"Chia sẻ Facebook"
#define shareEmail @"Chia sẻ qua Email"
#define about @"Về chúng tôi"

@interface LeftTableViewController ()<AMSlideMenuDelegate>

@end

@implementation LeftTableViewController

{
    
    //UITableViewCell *preCell;
    
    int indexOfChoosedCell;
    
    float heightOfCell;
    float heightOfHeader;
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    if(self.mainSlideMenu.leftMenuWidth > leftMenuWidth)
    {
        leftMenuWidth = self.mainSlideMenu.leftMenuWidth;
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"view will appear");
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    heightOfCell = 50*[UIScreen mainScreen].bounds.size.height/568.0;
    heightOfHeader = 80;
    [self.tableView reloadData];
    
    AMSlideMenuMainViewController *mainMenu = [self mainVC];
    mainMenu.slideMenuDelegate = self;
}


#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UILabel *titleLb = (UILabel*)[cell viewWithTag:1];
    
    
    if([titleLb.text isEqualToString:logout])
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [[self mainVC] closeLeftMenuAnimated:YES];
        
        
        if([self currentNetworkStatus])
        {
            FBSDKLoginManager *loginfb = [[FBSDKLoginManager alloc] init];
            [loginfb logOut];
            
            
            UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            indicator.center = self.view.center;
            [currentViewController.view addSubview:indicator];
            [indicator startAnimating];
            currentViewController.view.userInteractionEnabled = false;
            
            double delayInSeconds = 0.2;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                if([HttpThread logoutAccount])
                {
                    //[DataHandle clearData];
                    [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"login"];
                }
                else
                {
                    
                }
                
                [indicator stopAnimating];
                                
                currentViewController.view.userInteractionEnabled = true;
                
                for(UITableView *view in currentViewController.view.subviews)
                {
                    if([view isKindOfClass:[UITableView class]])
                    {
                        UITableView *table = (UITableView*)view;
                        [table reloadData];
                    }
                }
                
                
            });

            
            
        }
        else
        {
            [Toast showToastOnView:currentViewController.view withMessage:@"Kiểm tra lại mạng"];
        }
        
    }
    else if ([titleLb.text isEqualToString:login])
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [[self mainVC] closeLeftMenuAnimated:YES];
        
        
        UIViewController *loginViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginNVC"];
        
        double delayInSeconds = 0.1;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void)
        {
            [currentViewController.navigationController pushViewController:loginViewController animated:true];
        });
        
    }
    else if ([titleLb.text isEqualToString:phanHoi])
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [[self mainVC] closeLeftMenuAnimated:YES];
        
        [Email mailTo:emailAccount
                Title:feedbackTitle
           andContent:@""];
    }
    else if ([titleLb.text isEqualToString:shareFb])
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [[self mainVC] closeLeftMenuAnimated:YES];
        
        [Email shareFacebookWithContent:fbShare
                      andViewController:self];
        
    }
    else if ([titleLb.text isEqualToString:shareEmail])
    {
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [[self mainVC] closeLeftMenuAnimated:YES];
        
        [Email mailTo:@""
                Title:@"Góp ý Pera Pera"
           andContent:appLink];
    }
    else if ([titleLb.text isEqualToString:about])
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [[self mainVC] closeLeftMenuAnimated:YES];
        
        UIViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AboutViewController"];
        [currentViewController.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark- table view datasource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"login"])
    {
        if (indexPath.row > 0)
            return heightOfCell;
        else
            return heightOfHeader;

    }
    else
    {
        return heightOfCell;
    }

}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"login"])
    {
        return 7;
    }
    else
    {
        return 6;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.frame = CGRectMake(30, 0, [UIScreen mainScreen].bounds.size.width-10, heightOfCell);
    titleLabel.numberOfLines = 0;
    //titleLabel.font = [UIFont boldSystemFontOfSize:17];
    titleLabel.font = [UIFont systemFontOfSize:17];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    //titleLabel.textColor = [UIColor colorWithRed:68.0/255.0 green:120.0/255.0 blue:201.0/255.0 alpha:1];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.tag = 1;
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"login"])
    {
        if(indexPath.row == 0)
        {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            float sizeOfAvata = 50;
            UIImageView *imgView = [UIImageView new];
            imgView.frame = CGRectMake(10, (heightOfHeader - sizeOfAvata)/2, sizeOfAvata, sizeOfAvata);
            imgView.layer.masksToBounds = true;
            imgView.layer.cornerRadius = imgView.frame.size.width/2;
            imgView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] valueForKey:@"avatar"]]]];
            [cell addSubview:imgView];
            
            titleLabel.frame = CGRectMake(imgView.frame.origin.x+imgView.frame.size.width, 0, [UIScreen mainScreen].bounds.size.width - (imgView.frame.origin.x+imgView.frame.size.width) - ([UIScreen mainScreen].bounds.size.width - leftMenuWidth), heightOfHeader);
            
            [DataHandle checkDayRemain];
            
            titleLabel.font = [UIFont systemFontOfSize:15];
            titleLabel.text = [NSString stringWithFormat:@"%@\n\nNgày học còn lại: %d",[[NSUserDefaults standardUserDefaults] valueForKey:@"display_name"], [[NSUserDefaults standardUserDefaults] integerForKey:@"day_remain"]];
            titleLabel.textAlignment = NSTextAlignmentCenter;
            
            NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:titleLabel.text];
            [attrString beginEditing];
            [attrString addAttribute:NSFontAttributeName
                               value:[UIFont boldSystemFontOfSize:15]
                               range:[attrString.string rangeOfString:[[NSUserDefaults standardUserDefaults] valueForKey:@"display_name"]]];
            
            [attrString endEditing];
            titleLabel.attributedText = attrString;
            
            [cell addSubview:titleLabel];
            
        }
        else if(indexPath.row == 1)
        {
            titleLabel.text = logout;
        }
        else if(indexPath.row==2)
        {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            titleLabel.textColor = [UIColor blackColor];
            titleLabel.font = [UIFont boldSystemFontOfSize:17];
            titleLabel.frame = CGRectMake(10, titleLabel.frame.origin.y, titleLabel.frame.size.width, titleLabel.frame.size.height);
            titleLabel.text = @"Akira Education";
            cell.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];
        }
        else if (indexPath.row == 3)
        {
            titleLabel.text = phanHoi;
            UILabel *rule = [[UILabel alloc] initWithFrame:CGRectMake(-30, heightOfCell-1, [UIScreen mainScreen].bounds.size.width, 1)];
            [rule setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1]];
            [titleLabel addSubview:rule];
            
        }
        else if (indexPath.row == 4)
        {
            titleLabel.text = shareFb;
            UILabel *rule = [[UILabel alloc] initWithFrame:CGRectMake(-30, heightOfCell-1, [UIScreen mainScreen].bounds.size.width, 1)];
            [rule setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1]];
            [titleLabel addSubview:rule];
            
        }
        else if (indexPath.row == 5)
        {
            titleLabel.text = shareEmail;
            UILabel *rule = [[UILabel alloc] initWithFrame:CGRectMake(-30, heightOfCell-1, [UIScreen mainScreen].bounds.size.width, 1)];
            [rule setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1]];
            [titleLabel addSubview:rule];
            
        }
        else if (indexPath.row == 6)
        {
            titleLabel.text = about;
            UILabel *rule = [[UILabel alloc] initWithFrame:CGRectMake(-30, heightOfCell-1, [UIScreen mainScreen].bounds.size.width, 1)];
            [rule setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1]];
            [titleLabel addSubview:rule];
            
        }

    }
    else
    {
        if(indexPath.row == 0)
        {
            titleLabel.text = login;
        }
        else if(indexPath.row==1)
        {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            titleLabel.textColor = [UIColor blackColor];
            titleLabel.font = [UIFont boldSystemFontOfSize:17];
            titleLabel.frame = CGRectMake(10, titleLabel.frame.origin.y, titleLabel.frame.size.width, titleLabel.frame.size.height);
            titleLabel.text = @"Akira Education";
            cell.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];
        }
        else if (indexPath.row == 2)
        {
            titleLabel.text = phanHoi;
            UILabel *rule = [[UILabel alloc] initWithFrame:CGRectMake(-30, heightOfCell-1, [UIScreen mainScreen].bounds.size.width, 1)];
            [rule setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1]];
            [titleLabel addSubview:rule];
            
        }
        else if (indexPath.row == 3)
        {
            titleLabel.text = shareFb;
            UILabel *rule = [[UILabel alloc] initWithFrame:CGRectMake(-30, heightOfCell-1, [UIScreen mainScreen].bounds.size.width, 1)];
            [rule setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1]];
            [titleLabel addSubview:rule];
            
        }
        else if (indexPath.row == 4)
        {
            titleLabel.text = shareEmail;
            UILabel *rule = [[UILabel alloc] initWithFrame:CGRectMake(-30, heightOfCell-1, [UIScreen mainScreen].bounds.size.width, 1)];
            [rule setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1]];
            [titleLabel addSubview:rule];
            
        }
        else if (indexPath.row == 5)
        {
            titleLabel.text = about;
            UILabel *rule = [[UILabel alloc] initWithFrame:CGRectMake(-30, heightOfCell-1, [UIScreen mainScreen].bounds.size.width, 1)];
            [rule setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1]];
            [titleLabel addSubview:rule];
            
        }

    }
    [cell addSubview:titleLabel];
    return cell;
}


#pragma mark button

-(void)leftMenuDidClose
{
    NSLog(@"close");
    
    [self.tableView reloadData];
    
}


-(BOOL)currentNetworkStatus
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

@end
