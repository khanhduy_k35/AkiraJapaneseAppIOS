//
//  KanaGame1ViewController.h
//  Pera
//
//  Created by Le Trong Thao on 3/4/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KanaGame1ViewController : UIViewController


@property NSString *topicID;
@property NSString *gameID;
@property int type;


@end
