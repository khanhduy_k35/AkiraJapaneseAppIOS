//
//  AppDelegate.h
//  Pera
//
//  Created by Le Trong Thao on 12/1/15.
//  Copyright © 2015 Le Trong Thao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, GIDSignInDelegate>

@property (strong, nonatomic) UIWindow *window;
@property BOOL isDownload;
@property BOOL isGame2Success;
@property BOOL isGame3Success;
@property BOOL isGame4Success;
@property BOOL isGame5Success;
@end

