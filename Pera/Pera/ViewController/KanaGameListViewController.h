//
//  KanaGameListViewController.h
//  Pera
//
//  Created by Le Trong Thao on 3/3/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KanaGameListViewController : UIViewController

@property int topicId;
@property int type;

@end
